.class public Lorg/droidparts/bus/EventBus;
.super Ljava/lang/Object;
.source "EventBus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/droidparts/bus/EventBus$ReflectiveReceiver;,
        Lorg/droidparts/bus/EventBus$PostEventRunnable;
    }
.end annotation


# static fields
.field private static final ALL:Ljava/lang/String; = "__all__"

.field private static final eventNameToReceivers:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lorg/droidparts/bus/EventReceiver",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field private static handler:Landroid/os/Handler;

.field private static final stickyEvents:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/droidparts/bus/EventBus;->eventNameToReceivers:Ljava/util/concurrent/ConcurrentHashMap;

    .line 38
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lorg/droidparts/bus/EventBus;->stickyEvents:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 190
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-static {p0}, Lorg/droidparts/bus/EventBus;->receiversForEventName(Ljava/lang/String;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lorg/droidparts/bus/EventReceiver;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p0, "x0"    # Lorg/droidparts/bus/EventReceiver;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/Object;

    .prologue
    .line 33
    invoke-static {p0, p1, p2}, Lorg/droidparts/bus/EventBus;->notifyReceiver(Lorg/droidparts/bus/EventReceiver;Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public static varargs clearStickyEvents([Ljava/lang/String;)V
    .locals 5
    .param p0, "eventNames"    # [Ljava/lang/String;

    .prologue
    .line 58
    array-length v4, p0

    if-nez v4, :cond_1

    const/4 v0, 0x1

    .line 59
    .local v0, "allEvents":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 60
    sget-object v4, Lorg/droidparts/bus/EventBus;->stickyEvents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V

    .line 71
    :cond_0
    :goto_1
    return-void

    .line 58
    .end local v0    # "allEvents":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 62
    .restart local v0    # "allEvents":Z
    :cond_2
    new-instance v3, Ljava/util/HashSet;

    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 64
    .local v3, "nameSet":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    sget-object v4, Lorg/droidparts/bus/EventBus;->stickyEvents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 65
    .local v1, "eventName":Ljava/lang/String;
    invoke-virtual {v3, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 66
    sget-object v4, Lorg/droidparts/bus/EventBus;->stickyEvents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method private static notifyReceiver(Lorg/droidparts/bus/EventReceiver;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 5
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/bus/EventReceiver",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 143
    .local p0, "receiver":Lorg/droidparts/bus/EventReceiver;, "Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;"
    :try_start_0
    invoke-interface {p0, p1, p2}, Lorg/droidparts/bus/EventReceiver;->onEvent(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 153
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v0

    .line 145
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "Failed to deliver event %s to %s: %s."

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/droidparts/util/L;->w(Ljava/lang/Object;)V

    goto :goto_0

    .line 147
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 148
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/droidparts/util/L;->w(Ljava/lang/Object;)V

    .line 149
    const-string v1, "Receiver unregistered."

    invoke-static {v1}, Lorg/droidparts/util/L;->w(Ljava/lang/Object;)V

    .line 150
    invoke-static {p0}, Lorg/droidparts/bus/EventBus;->unregisterReceiver(Lorg/droidparts/bus/EventReceiver;)V

    goto :goto_0
.end method

.method public static postEvent(Ljava/lang/String;)V
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 41
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/droidparts/bus/EventBus;->postEvent(Ljava/lang/String;Ljava/lang/Object;)V

    .line 42
    return-void
.end method

.method public static postEvent(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 45
    new-instance v0, Lorg/droidparts/bus/EventBus$PostEventRunnable;

    invoke-direct {v0, p0, p1}, Lorg/droidparts/bus/EventBus$PostEventRunnable;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-static {v0}, Lorg/droidparts/bus/EventBus;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 46
    return-void
.end method

.method public static postEventSticky(Ljava/lang/String;)V
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/droidparts/bus/EventBus;->postEventSticky(Ljava/lang/String;Ljava/lang/Object;)V

    .line 50
    return-void
.end method

.method public static postEventSticky(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 53
    sget-object v0, Lorg/droidparts/bus/EventBus;->stickyEvents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    invoke-static {p0, p1}, Lorg/droidparts/bus/EventBus;->postEvent(Ljava/lang/String;Ljava/lang/Object;)V

    .line 55
    return-void
.end method

.method private static receiversForEventName(Ljava/lang/String;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 2
    .param p0, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lorg/droidparts/bus/EventReceiver",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    sget-object v1, Lorg/droidparts/bus/EventBus;->eventNameToReceivers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentHashMap;

    .line 133
    .local v0, "map":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;Ljava/lang/Boolean;>;"
    if-nez v0, :cond_0

    .line 134
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    .end local v0    # "map":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;Ljava/lang/Boolean;>;"
    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    .line 135
    .restart local v0    # "map":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;Ljava/lang/Boolean;>;"
    sget-object v1, Lorg/droidparts/bus/EventBus;->eventNameToReceivers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p0, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    :cond_0
    return-object v0
.end method

.method public static registerAnnotatedReceiver(Ljava/lang/Object;)V
    .locals 7
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 109
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-static {v5}, Lorg/droidparts/inner/ClassSpecRegistry;->getReceiveEventsSpecs(Ljava/lang/Class;)[Lorg/droidparts/inner/ann/MethodSpec;

    move-result-object v4

    .line 111
    .local v4, "specs":[Lorg/droidparts/inner/ann/MethodSpec;, "[Lorg/droidparts/inner/ann/MethodSpec<Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;>;"
    move-object v0, v4

    .local v0, "arr$":[Lorg/droidparts/inner/ann/MethodSpec;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 112
    .local v3, "spec":Lorg/droidparts/inner/ann/MethodSpec;, "Lorg/droidparts/inner/ann/MethodSpec<Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;>;"
    new-instance v6, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;

    invoke-direct {v6, p0, v3}, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;-><init>(Ljava/lang/Object;Lorg/droidparts/inner/ann/MethodSpec;)V

    iget-object v5, v3, Lorg/droidparts/inner/ann/MethodSpec;->ann:Lorg/droidparts/inner/ann/Ann;

    check-cast v5, Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;

    iget-object v5, v5, Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;->names:[Ljava/lang/String;

    invoke-static {v6, v5}, Lorg/droidparts/bus/EventBus;->registerReceiver(Lorg/droidparts/bus/EventReceiver;[Ljava/lang/String;)V

    .line 111
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 114
    .end local v3    # "spec":Lorg/droidparts/inner/ann/MethodSpec;, "Lorg/droidparts/inner/ann/MethodSpec<Lorg/droidparts/inner/ann/bus/ReceiveEventsAnn;>;"
    :cond_0
    return-void
.end method

.method public static varargs registerReceiver(Lorg/droidparts/bus/EventReceiver;[Ljava/lang/String;)V
    .locals 10
    .param p1, "eventNames"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/bus/EventReceiver",
            "<*>;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "receiver":Lorg/droidparts/bus/EventReceiver;, "Lorg/droidparts/bus/EventReceiver<*>;"
    move-object v7, p0

    .line 77
    .local v7, "rec":Lorg/droidparts/bus/EventReceiver;, "Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;"
    array-length v8, p1

    if-nez v8, :cond_0

    const/4 v1, 0x1

    .line 78
    .local v1, "allEvents":Z
    :goto_0
    if-eqz v1, :cond_3

    .line 79
    sget-object v8, Lorg/droidparts/bus/EventBus;->stickyEvents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 80
    .local v6, "name":Ljava/lang/String;
    sget-object v8, Lorg/droidparts/bus/EventBus;->stickyEvents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    invoke-static {v7, v6, v8}, Lorg/droidparts/bus/EventBus;->notifyReceiver(Lorg/droidparts/bus/EventReceiver;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 77
    .end local v1    # "allEvents":Z
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "name":Ljava/lang/String;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 82
    .restart local v1    # "allEvents":Z
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_1
    const-string v8, "__all__"

    invoke-static {v8}, Lorg/droidparts/bus/EventBus;->receiversForEventName(Ljava/lang/String;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v8

    sget-object v9, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v8, v7, v9}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    .end local v4    # "i$":Ljava/util/Iterator;
    :cond_2
    return-void

    .line 84
    :cond_3
    move-object v2, p1

    .local v2, "arr$":[Ljava/lang/String;
    array-length v5, v2

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_2
    if-ge v4, v5, :cond_5

    aget-object v6, v2, v4

    .line 85
    .restart local v6    # "name":Ljava/lang/String;
    sget-object v8, Lorg/droidparts/bus/EventBus;->stickyEvents:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v8, v6}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 86
    .local v3, "data":Ljava/lang/Object;
    if-eqz v3, :cond_4

    .line 87
    invoke-static {v7, v6, v3}, Lorg/droidparts/bus/EventBus;->notifyReceiver(Lorg/droidparts/bus/EventReceiver;Ljava/lang/String;Ljava/lang/Object;)V

    .line 84
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 90
    .end local v3    # "data":Ljava/lang/Object;
    .end local v6    # "name":Ljava/lang/String;
    :cond_5
    move-object v2, p1

    array-length v5, v2

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v5, :cond_2

    aget-object v0, v2, v4

    .line 91
    .local v0, "action":Ljava/lang/String;
    invoke-static {v0}, Lorg/droidparts/bus/EventBus;->receiversForEventName(Ljava/lang/String;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v8

    sget-object v9, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v8, v7, v9}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    add-int/lit8 v4, v4, 0x1

    goto :goto_3
.end method

.method private static runOnUiThread(Ljava/lang/Runnable;)V
    .locals 3
    .param p0, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 156
    sget-object v1, Lorg/droidparts/bus/EventBus;->handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 157
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lorg/droidparts/bus/EventBus;->handler:Landroid/os/Handler;

    .line 159
    :cond_0
    sget-object v1, Lorg/droidparts/bus/EventBus;->handler:Landroid/os/Handler;

    invoke-virtual {v1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    .line 161
    .local v0, "success":Z
    :goto_0
    if-nez v0, :cond_1

    .line 162
    const/4 v1, 0x0

    sput-object v1, Lorg/droidparts/bus/EventBus;->handler:Landroid/os/Handler;

    .line 163
    invoke-static {p0}, Lorg/droidparts/bus/EventBus;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 165
    :cond_1
    return-void
.end method

.method public static unregisterAnnotatedReceiver(Ljava/lang/Object;)V
    .locals 5
    .param p0, "obj"    # Ljava/lang/Object;

    .prologue
    .line 117
    sget-object v4, Lorg/droidparts/bus/EventBus;->eventNameToReceivers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ConcurrentHashMap;

    .line 119
    .local v3, "receivers":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;Ljava/lang/Boolean;>;"
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/droidparts/bus/EventReceiver;

    .line 120
    .local v2, "receiver":Lorg/droidparts/bus/EventReceiver;, "Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;"
    instance-of v4, v2, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;

    if-eqz v4, :cond_1

    move-object v4, v2

    .line 121
    check-cast v4, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;

    iget-object v4, v4, Lorg/droidparts/bus/EventBus$ReflectiveReceiver;->objectRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-ne p0, v4, :cond_1

    .line 122
    invoke-virtual {v3, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 127
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "receiver":Lorg/droidparts/bus/EventReceiver;, "Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;"
    .end local v3    # "receivers":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;Ljava/lang/Boolean;>;"
    :cond_2
    return-void
.end method

.method public static unregisterReceiver(Lorg/droidparts/bus/EventReceiver;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/bus/EventReceiver",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 97
    .local p0, "receiver":Lorg/droidparts/bus/EventReceiver;, "Lorg/droidparts/bus/EventReceiver<*>;"
    const-string v3, "__all__"

    invoke-static {v3}, Lorg/droidparts/bus/EventBus;->receiversForEventName(Ljava/lang/String;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v3, Lorg/droidparts/bus/EventBus;->eventNameToReceivers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 99
    .local v0, "eventName":Ljava/lang/String;
    sget-object v3, Lorg/droidparts/bus/EventBus;->eventNameToReceivers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ConcurrentHashMap;

    .line 101
    .local v2, "receivers":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;Ljava/lang/Boolean;>;"
    invoke-virtual {v2, p0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 103
    sget-object v3, Lorg/droidparts/bus/EventBus;->eventNameToReceivers:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 106
    .end local v0    # "eventName":Ljava/lang/String;
    .end local v2    # "receivers":Ljava/util/concurrent/ConcurrentHashMap;, "Ljava/util/concurrent/ConcurrentHashMap<Lorg/droidparts/bus/EventReceiver<Ljava/lang/Object;>;Ljava/lang/Boolean;>;"
    :cond_1
    return-void
.end method
