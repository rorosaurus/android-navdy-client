.class public Lorg/droidparts/persist/sql/stmt/Where;
.super Ljava/lang/Object;
.source "Where.java"

# interfaces
.implements Lorg/droidparts/contract/SQL;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/droidparts/persist/sql/stmt/Where$1;,
        Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;
    }
.end annotation


# instance fields
.field private and:Z

.field private final whereSpecs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Ljava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)V
    .locals 3
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "operator"    # Lorg/droidparts/persist/sql/stmt/Is;
    .param p3, "columnValue"    # [Ljava/lang/Object;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/droidparts/persist/sql/stmt/Where;->whereSpecs:Ljava/util/ArrayList;

    .line 33
    iget-object v0, p0, Lorg/droidparts/persist/sql/stmt/Where;->whereSpecs:Ljava/util/ArrayList;

    new-instance v1, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1, p2, p3}, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;-><init>(ZLjava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    return-void
.end method

.method private static build(Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;)Landroid/util/Pair;
    .locals 7
    .param p0, "spec"    # Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    .local v2, "selectionBuilder":Ljava/lang/StringBuilder;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 97
    .local v1, "selectionArgsBuilder":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;->columnValue:[Ljava/lang/Object;

    invoke-static {v4}, Lorg/droidparts/inner/PersistUtils;->toWhereArgs([Ljava/lang/Object;)[Ljava/lang/String;

    move-result-object v3

    .line 98
    .local v3, "whereArgs":[Ljava/lang/String;
    array-length v0, v3

    .line 100
    .local v0, "argNum":I
    iget-object v4, p0, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;->columnName:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;->operator:Lorg/droidparts/persist/sql/stmt/Is;

    iget-object v5, v5, Lorg/droidparts/persist/sql/stmt/Is;->str:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    sget-object v4, Lorg/droidparts/persist/sql/stmt/Where$1;->$SwitchMap$org$droidparts$persist$sql$stmt$Is:[I

    iget-object v5, p0, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;->operator:Lorg/droidparts/persist/sql/stmt/Is;

    invoke-virtual {v5}, Lorg/droidparts/persist/sql/stmt/Is;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 124
    if-eq v0, v6, :cond_0

    .line 125
    iget-object v4, p0, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;->operator:Lorg/droidparts/persist/sql/stmt/Is;

    invoke-static {v4, v0}, Lorg/droidparts/persist/sql/stmt/Where;->errArgs(Lorg/droidparts/persist/sql/stmt/Is;I)V

    .line 128
    :cond_0
    :goto_0
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 129
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    return-object v4

    .line 104
    :pswitch_0
    if-eqz v0, :cond_0

    .line 105
    iget-object v4, p0, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;->operator:Lorg/droidparts/persist/sql/stmt/Is;

    invoke-static {v4, v0}, Lorg/droidparts/persist/sql/stmt/Where;->errArgs(Lorg/droidparts/persist/sql/stmt/Is;I)V

    goto :goto_0

    .line 110
    :pswitch_1
    const/4 v4, 0x2

    if-eq v0, v4, :cond_0

    .line 111
    iget-object v4, p0, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;->operator:Lorg/droidparts/persist/sql/stmt/Is;

    invoke-static {v4, v0}, Lorg/droidparts/persist/sql/stmt/Where;->errArgs(Lorg/droidparts/persist/sql/stmt/Is;I)V

    goto :goto_0

    .line 116
    :pswitch_2
    if-ge v0, v6, :cond_1

    .line 117
    iget-object v4, p0, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;->operator:Lorg/droidparts/persist/sql/stmt/Is;

    invoke-static {v4, v0}, Lorg/droidparts/persist/sql/stmt/Where;->errArgs(Lorg/droidparts/persist/sql/stmt/Is;I)V

    .line 119
    :cond_1
    const-string v4, "("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    array-length v4, v3

    invoke-static {v4}, Lorg/droidparts/inner/PersistUtils;->buildPlaceholders(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const-string v4, ")"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 101
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private static build(Lorg/droidparts/persist/sql/stmt/Where;)Landroid/util/Pair;
    .locals 11
    .param p0, "where"    # Lorg/droidparts/persist/sql/stmt/Where;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/persist/sql/stmt/Where;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 64
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .local v6, "selectionBuilder":Ljava/lang/StringBuilder;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v5, "selectionArgsBuilder":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v9, p0, Lorg/droidparts/persist/sql/stmt/Where;->whereSpecs:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v2, v9, :cond_4

    .line 67
    iget-object v9, p0, Lorg/droidparts/persist/sql/stmt/Where;->whereSpecs:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 69
    .local v3, "obj":Ljava/lang/Object;
    const/4 v1, 0x0

    .line 71
    .local v1, "braces":Z
    instance-of v9, v3, Lorg/droidparts/persist/sql/stmt/Where;

    if-eqz v9, :cond_1

    .line 72
    const/4 v1, 0x1

    move-object v8, v3

    .line 73
    check-cast v8, Lorg/droidparts/persist/sql/stmt/Where;

    .line 74
    .local v8, "where2":Lorg/droidparts/persist/sql/stmt/Where;
    iget-boolean v0, v8, Lorg/droidparts/persist/sql/stmt/Where;->and:Z

    .line 75
    .local v0, "and":Z
    invoke-static {v8}, Lorg/droidparts/persist/sql/stmt/Where;->build(Lorg/droidparts/persist/sql/stmt/Where;)Landroid/util/Pair;

    move-result-object v4

    .line 81
    .end local v8    # "where2":Lorg/droidparts/persist/sql/stmt/Where;
    .local v4, "sel":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    :goto_1
    if-lez v2, :cond_0

    .line 82
    if-eqz v0, :cond_2

    const-string v9, " AND "

    :goto_2
    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    :cond_0
    if-eqz v1, :cond_3

    .line 85
    const-string v9, "("

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v9, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ")"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    :goto_3
    iget-object v9, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Ljava/util/Collection;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 66
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .end local v0    # "and":Z
    .end local v4    # "sel":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    :cond_1
    move-object v7, v3

    .line 77
    check-cast v7, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;

    .line 78
    .local v7, "spec":Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;
    iget-boolean v0, v7, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;->and:Z

    .line 79
    .restart local v0    # "and":Z
    invoke-static {v7}, Lorg/droidparts/persist/sql/stmt/Where;->build(Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;)Landroid/util/Pair;

    move-result-object v4

    .restart local v4    # "sel":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    goto :goto_1

    .line 82
    .end local v7    # "spec":Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;
    :cond_2
    const-string v9, " OR "

    goto :goto_2

    .line 87
    :cond_3
    iget-object v9, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v9, Ljava/lang/String;

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 91
    .end local v0    # "and":Z
    .end local v1    # "braces":Z
    .end local v3    # "obj":Ljava/lang/Object;
    .end local v4    # "sel":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    :cond_4
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v9

    return-object v9
.end method

.method private static errArgs(Lorg/droidparts/persist/sql/stmt/Is;I)V
    .locals 4
    .param p0, "operator"    # Lorg/droidparts/persist/sql/stmt/Is;
    .param p1, "num"    # I

    .prologue
    .line 133
    const-string v0, "Invalid number of agruments for \'%s\': %d."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lorg/droidparts/util/L;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    return-void
.end method


# virtual methods
.method public varargs and(Ljava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Where;
    .locals 3
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "operator"    # Lorg/droidparts/persist/sql/stmt/Is;
    .param p3, "columnValue"    # [Ljava/lang/Object;

    .prologue
    .line 49
    iget-object v0, p0, Lorg/droidparts/persist/sql/stmt/Where;->whereSpecs:Ljava/util/ArrayList;

    new-instance v1, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;

    const/4 v2, 0x1

    invoke-direct {v1, v2, p1, p2, p3}, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;-><init>(ZLjava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 50
    return-object p0
.end method

.method public and(Lorg/droidparts/persist/sql/stmt/Where;)Lorg/droidparts/persist/sql/stmt/Where;
    .locals 1
    .param p1, "where"    # Lorg/droidparts/persist/sql/stmt/Where;

    .prologue
    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p1, Lorg/droidparts/persist/sql/stmt/Where;->and:Z

    .line 38
    iget-object v0, p0, Lorg/droidparts/persist/sql/stmt/Where;->whereSpecs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    return-object p0
.end method

.method build()Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    invoke-static {p0}, Lorg/droidparts/persist/sql/stmt/Where;->build(Lorg/droidparts/persist/sql/stmt/Where;)Landroid/util/Pair;

    move-result-object v0

    .line 60
    .local v0, "p":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/util/ArrayList<Ljava/lang/String;>;>;"
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v1

    return-object v1
.end method

.method public varargs or(Ljava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Where;
    .locals 3
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "operator"    # Lorg/droidparts/persist/sql/stmt/Is;
    .param p3, "columnValue"    # [Ljava/lang/Object;

    .prologue
    .line 54
    iget-object v0, p0, Lorg/droidparts/persist/sql/stmt/Where;->whereSpecs:Ljava/util/ArrayList;

    new-instance v1, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;

    const/4 v2, 0x0

    invoke-direct {v1, v2, p1, p2, p3}, Lorg/droidparts/persist/sql/stmt/Where$WhereSpec;-><init>(ZLjava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    return-object p0
.end method

.method public or(Lorg/droidparts/persist/sql/stmt/Where;)Lorg/droidparts/persist/sql/stmt/Where;
    .locals 1
    .param p1, "where"    # Lorg/droidparts/persist/sql/stmt/Where;

    .prologue
    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p1, Lorg/droidparts/persist/sql/stmt/Where;->and:Z

    .line 44
    iget-object v0, p0, Lorg/droidparts/persist/sql/stmt/Where;->whereSpecs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    return-object p0
.end method
