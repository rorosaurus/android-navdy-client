.class public Lorg/droidparts/persist/sql/stmt/MergeSelect;
.super Ljava/lang/Object;
.source "MergeSelect.java"

# interfaces
.implements Lorg/droidparts/persist/sql/stmt/AbstractSelect;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<EntityType:",
        "Lorg/droidparts/model/Entity;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/droidparts/persist/sql/stmt/AbstractSelect",
        "<TEntityType;>;"
    }
.end annotation


# instance fields
.field private final selects:[Lorg/droidparts/persist/sql/stmt/Select;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lorg/droidparts/persist/sql/stmt/Select",
            "<TEntityType;>;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>([Lorg/droidparts/persist/sql/stmt/Select;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/droidparts/persist/sql/stmt/Select",
            "<TEntityType;>;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/MergeSelect;, "Lorg/droidparts/persist/sql/stmt/MergeSelect<TEntityType;>;"
    .local p1, "selects":[Lorg/droidparts/persist/sql/stmt/Select;, "[Lorg/droidparts/persist/sql/stmt/Select<TEntityType;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lorg/droidparts/persist/sql/stmt/MergeSelect;->selects:[Lorg/droidparts/persist/sql/stmt/Select;

    .line 30
    return-void
.end method


# virtual methods
.method public count()I
    .locals 6

    .prologue
    .line 43
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/MergeSelect;, "Lorg/droidparts/persist/sql/stmt/MergeSelect<TEntityType;>;"
    const/4 v1, 0x0

    .line 44
    .local v1, "count":I
    iget-object v0, p0, Lorg/droidparts/persist/sql/stmt/MergeSelect;->selects:[Lorg/droidparts/persist/sql/stmt/Select;

    .local v0, "arr$":[Lorg/droidparts/persist/sql/stmt/Select;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 45
    .local v4, "select":Lorg/droidparts/persist/sql/stmt/Select;, "Lorg/droidparts/persist/sql/stmt/Select<TEntityType;>;"
    invoke-virtual {v4}, Lorg/droidparts/persist/sql/stmt/Select;->count()I

    move-result v5

    add-int/2addr v1, v5

    .line 44
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 47
    .end local v4    # "select":Lorg/droidparts/persist/sql/stmt/Select;, "Lorg/droidparts/persist/sql/stmt/Select<TEntityType;>;"
    :cond_0
    return v1
.end method

.method public execute()Landroid/database/Cursor;
    .locals 3

    .prologue
    .line 34
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/MergeSelect;, "Lorg/droidparts/persist/sql/stmt/MergeSelect<TEntityType;>;"
    iget-object v2, p0, Lorg/droidparts/persist/sql/stmt/MergeSelect;->selects:[Lorg/droidparts/persist/sql/stmt/Select;

    array-length v2, v2

    new-array v0, v2, [Landroid/database/Cursor;

    .line 35
    .local v0, "cursors":[Landroid/database/Cursor;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 36
    iget-object v2, p0, Lorg/droidparts/persist/sql/stmt/MergeSelect;->selects:[Lorg/droidparts/persist/sql/stmt/Select;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lorg/droidparts/persist/sql/stmt/Select;->execute()Landroid/database/Cursor;

    move-result-object v2

    aput-object v2, v0, v1

    .line 35
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 38
    :cond_0
    new-instance v2, Landroid/database/MergeCursor;

    invoke-direct {v2, v0}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v2
.end method
