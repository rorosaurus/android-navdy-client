.class public Lorg/droidparts/persist/sql/stmt/Delete;
.super Lorg/droidparts/persist/sql/stmt/Statement;
.source "Delete.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<EntityType:",
        "Lorg/droidparts/model/Entity;",
        ">",
        "Lorg/droidparts/persist/sql/stmt/Statement",
        "<TEntityType;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "tableName"    # Ljava/lang/String;

    .prologue
    .line 28
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Delete;, "Lorg/droidparts/persist/sql/stmt/Delete<TEntityType;>;"
    invoke-direct {p0, p1, p2}, Lorg/droidparts/persist/sql/stmt/Statement;-><init>(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 29
    return-void
.end method


# virtual methods
.method public execute()I
    .locals 7

    .prologue
    .line 54
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Delete;, "Lorg/droidparts/persist/sql/stmt/Delete<TEntityType;>;"
    invoke-virtual {p0}, Lorg/droidparts/persist/sql/stmt/Delete;->getSelection()Landroid/util/Pair;

    move-result-object v2

    .line 55
    .local v2, "selection":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;[Ljava/lang/String;>;"
    invoke-virtual {p0}, Lorg/droidparts/persist/sql/stmt/Delete;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    .line 56
    const/4 v1, 0x0

    .line 58
    .local v1, "rowCount":I
    :try_start_0
    iget-object v5, p0, Lorg/droidparts/persist/sql/stmt/Delete;->db:Landroid/database/sqlite/SQLiteDatabase;

    iget-object v6, p0, Lorg/droidparts/persist/sql/stmt/Delete;->tableName:Ljava/lang/String;

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, [Ljava/lang/String;

    invoke-virtual {v5, v6, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 63
    :goto_0
    return v1

    .line 59
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Landroid/database/SQLException;
    invoke-virtual {v0}, Landroid/database/SQLException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/droidparts/util/L;->e(Ljava/lang/Object;)V

    .line 61
    invoke-static {v0}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 68
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Delete;, "Lorg/droidparts/persist/sql/stmt/Delete<TEntityType;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DELETE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lorg/droidparts/persist/sql/stmt/Statement;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public varargs where(Ljava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Delete;
    .locals 1
    .param p1, "columnName"    # Ljava/lang/String;
    .param p2, "operator"    # Lorg/droidparts/persist/sql/stmt/Is;
    .param p3, "columnValue"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/droidparts/persist/sql/stmt/Is;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Lorg/droidparts/persist/sql/stmt/Delete",
            "<TEntityType;>;"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Delete;, "Lorg/droidparts/persist/sql/stmt/Delete<TEntityType;>;"
    invoke-super {p0, p1, p2, p3}, Lorg/droidparts/persist/sql/stmt/Statement;->where(Ljava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Statement;

    move-result-object v0

    check-cast v0, Lorg/droidparts/persist/sql/stmt/Delete;

    return-object v0
.end method

.method public varargs where(Ljava/lang/String;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Delete;
    .locals 1
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Lorg/droidparts/persist/sql/stmt/Delete",
            "<TEntityType;>;"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Delete;, "Lorg/droidparts/persist/sql/stmt/Delete<TEntityType;>;"
    invoke-super {p0, p1, p2}, Lorg/droidparts/persist/sql/stmt/Statement;->where(Ljava/lang/String;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Statement;

    move-result-object v0

    check-cast v0, Lorg/droidparts/persist/sql/stmt/Delete;

    return-object v0
.end method

.method public where(Lorg/droidparts/persist/sql/stmt/Where;)Lorg/droidparts/persist/sql/stmt/Delete;
    .locals 1
    .param p1, "where"    # Lorg/droidparts/persist/sql/stmt/Where;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/persist/sql/stmt/Where;",
            ")",
            "Lorg/droidparts/persist/sql/stmt/Delete",
            "<TEntityType;>;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Delete;, "Lorg/droidparts/persist/sql/stmt/Delete<TEntityType;>;"
    invoke-super {p0, p1}, Lorg/droidparts/persist/sql/stmt/Statement;->where(Lorg/droidparts/persist/sql/stmt/Where;)Lorg/droidparts/persist/sql/stmt/Statement;

    move-result-object v0

    check-cast v0, Lorg/droidparts/persist/sql/stmt/Delete;

    return-object v0
.end method

.method public bridge synthetic where(Ljava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Statement;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Lorg/droidparts/persist/sql/stmt/Is;
    .param p3, "x2"    # [Ljava/lang/Object;

    .prologue
    .line 25
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Delete;, "Lorg/droidparts/persist/sql/stmt/Delete<TEntityType;>;"
    invoke-virtual {p0, p1, p2, p3}, Lorg/droidparts/persist/sql/stmt/Delete;->where(Ljava/lang/String;Lorg/droidparts/persist/sql/stmt/Is;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Delete;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic where(Ljava/lang/String;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Statement;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 25
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Delete;, "Lorg/droidparts/persist/sql/stmt/Delete<TEntityType;>;"
    invoke-virtual {p0, p1, p2}, Lorg/droidparts/persist/sql/stmt/Delete;->where(Ljava/lang/String;[Ljava/lang/Object;)Lorg/droidparts/persist/sql/stmt/Delete;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic where(Lorg/droidparts/persist/sql/stmt/Where;)Lorg/droidparts/persist/sql/stmt/Statement;
    .locals 1
    .param p1, "x0"    # Lorg/droidparts/persist/sql/stmt/Where;

    .prologue
    .line 25
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Delete;, "Lorg/droidparts/persist/sql/stmt/Delete<TEntityType;>;"
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/sql/stmt/Delete;->where(Lorg/droidparts/persist/sql/stmt/Where;)Lorg/droidparts/persist/sql/stmt/Delete;

    move-result-object v0

    return-object v0
.end method

.method public varargs whereId([J)Lorg/droidparts/persist/sql/stmt/Delete;
    .locals 1
    .param p1, "oneOrMore"    # [J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J)",
            "Lorg/droidparts/persist/sql/stmt/Delete",
            "<TEntityType;>;"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Delete;, "Lorg/droidparts/persist/sql/stmt/Delete<TEntityType;>;"
    invoke-super {p0, p1}, Lorg/droidparts/persist/sql/stmt/Statement;->whereId([J)Lorg/droidparts/persist/sql/stmt/Statement;

    move-result-object v0

    check-cast v0, Lorg/droidparts/persist/sql/stmt/Delete;

    return-object v0
.end method

.method public bridge synthetic whereId([J)Lorg/droidparts/persist/sql/stmt/Statement;
    .locals 1
    .param p1, "x0"    # [J

    .prologue
    .line 25
    .local p0, "this":Lorg/droidparts/persist/sql/stmt/Delete;, "Lorg/droidparts/persist/sql/stmt/Delete<TEntityType;>;"
    invoke-virtual {p0, p1}, Lorg/droidparts/persist/sql/stmt/Delete;->whereId([J)Lorg/droidparts/persist/sql/stmt/Delete;

    move-result-object v0

    return-object v0
.end method
