.class public abstract Lorg/droidparts/adapter/cursor/EntityCursorAdapter;
.super Lorg/droidparts/adapter/cursor/CursorAdapter;
.source "EntityCursorAdapter.java"

# interfaces
.implements Lorg/droidparts/contract/AlterableContent;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<EntityType:",
        "Lorg/droidparts/model/Entity;",
        ">",
        "Lorg/droidparts/adapter/cursor/CursorAdapter;",
        "Lorg/droidparts/contract/AlterableContent",
        "<",
        "Lorg/droidparts/persist/sql/stmt/AbstractSelect",
        "<TEntityType;>;>;"
    }
.end annotation


# instance fields
.field protected final entityManager:Lorg/droidparts/persist/sql/EntityManager;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/droidparts/persist/sql/EntityManager",
            "<TEntityType;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<TEntityType;>;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    .local p2, "entityCls":Ljava/lang/Class;, "Ljava/lang/Class<TEntityType;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;-><init>(Landroid/content/Context;Ljava/lang/Class;Lorg/droidparts/persist/sql/stmt/AbstractSelect;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;Lorg/droidparts/persist/sql/stmt/AbstractSelect;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<TEntityType;>;",
            "Lorg/droidparts/persist/sql/stmt/AbstractSelect",
            "<TEntityType;>;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    .local p2, "entityCls":Ljava/lang/Class;, "Ljava/lang/Class<TEntityType;>;"
    .local p3, "select":Lorg/droidparts/persist/sql/stmt/AbstractSelect;, "Lorg/droidparts/persist/sql/stmt/AbstractSelect<TEntityType;>;"
    new-instance v0, Lorg/droidparts/persist/sql/EntityManager;

    invoke-direct {v0, p2, p1}, Lorg/droidparts/persist/sql/EntityManager;-><init>(Ljava/lang/Class;Landroid/content/Context;)V

    invoke-direct {p0, p1, v0, p3}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;-><init>(Landroid/content/Context;Lorg/droidparts/persist/sql/EntityManager;Lorg/droidparts/persist/sql/stmt/AbstractSelect;)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/droidparts/persist/sql/EntityManager;Lorg/droidparts/persist/sql/stmt/AbstractSelect;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lorg/droidparts/persist/sql/EntityManager",
            "<TEntityType;>;",
            "Lorg/droidparts/persist/sql/stmt/AbstractSelect",
            "<TEntityType;>;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    .local p2, "entityManager":Lorg/droidparts/persist/sql/EntityManager;, "Lorg/droidparts/persist/sql/EntityManager<TEntityType;>;"
    .local p3, "select":Lorg/droidparts/persist/sql/stmt/AbstractSelect;, "Lorg/droidparts/persist/sql/stmt/AbstractSelect<TEntityType;>;"
    if-eqz p3, :cond_0

    invoke-interface {p3}, Lorg/droidparts/persist/sql/stmt/AbstractSelect;->execute()Landroid/database/Cursor;

    move-result-object v0

    :goto_0
    invoke-direct {p0, p1, v0}, Lorg/droidparts/adapter/cursor/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 45
    iput-object p2, p0, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->entityManager:Lorg/droidparts/persist/sql/EntityManager;

    .line 46
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private requeryOnSuccess(Z)Z
    .locals 0
    .param p1, "success"    # Z

    .prologue
    .line 88
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    if-eqz p1, :cond_0

    .line 89
    invoke-virtual {p0}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->requeryData()V

    .line 91
    :cond_0
    return p1
.end method


# virtual methods
.method public abstract bindView(Landroid/content/Context;Landroid/view/View;Lorg/droidparts/model/Entity;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/View;",
            "TEntityType;)V"
        }
    .end annotation
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 55
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    iget-object v0, p0, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->entityManager:Lorg/droidparts/persist/sql/EntityManager;

    invoke-virtual {v0, p3}, Lorg/droidparts/persist/sql/EntityManager;->readRow(Landroid/database/Cursor;)Lorg/droidparts/model/Entity;

    move-result-object v0

    invoke-virtual {p0, p2, p1, v0}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->bindView(Landroid/content/Context;Landroid/view/View;Lorg/droidparts/model/Entity;)V

    .line 56
    return-void
.end method

.method public create(Lorg/droidparts/model/Entity;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TEntityType;)Z"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    .local p1, "item":Lorg/droidparts/model/Entity;, "TEntityType;"
    iget-object v1, p0, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->entityManager:Lorg/droidparts/persist/sql/EntityManager;

    invoke-virtual {v1, p1}, Lorg/droidparts/persist/sql/EntityManager;->create(Lorg/droidparts/model/Entity;)Z

    move-result v0

    .line 62
    .local v0, "success":Z
    invoke-direct {p0, v0}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->requeryOnSuccess(Z)Z

    move-result v1

    return v1
.end method

.method public createOrUpdate(Lorg/droidparts/model/Entity;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TEntityType;)Z"
        }
    .end annotation

    .prologue
    .line 83
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    .local p1, "entity":Lorg/droidparts/model/Entity;, "TEntityType;"
    iget-object v1, p0, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->entityManager:Lorg/droidparts/persist/sql/EntityManager;

    invoke-virtual {v1, p1}, Lorg/droidparts/persist/sql/EntityManager;->createOrUpdate(Lorg/droidparts/model/Entity;)Z

    move-result v0

    .line 84
    .local v0, "success":Z
    invoke-direct {p0, v0}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->requeryOnSuccess(Z)Z

    move-result v1

    return v1
.end method

.method public delete(I)Z
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 77
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    invoke-virtual {p0, p1}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->getItemId(I)J

    move-result-wide v0

    .line 78
    .local v0, "id":J
    iget-object v3, p0, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->entityManager:Lorg/droidparts/persist/sql/EntityManager;

    invoke-virtual {v3, v0, v1}, Lorg/droidparts/persist/sql/EntityManager;->delete(J)Z

    move-result v2

    .line 79
    .local v2, "success":Z
    invoke-direct {p0, v2}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->requeryOnSuccess(Z)Z

    move-result v3

    return v3
.end method

.method public read(I)Lorg/droidparts/model/Entity;
    .locals 4
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TEntityType;"
        }
    .end annotation

    .prologue
    .line 66
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    invoke-virtual {p0, p1}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->getItemId(I)J

    move-result-wide v0

    .line 67
    .local v0, "id":J
    iget-object v3, p0, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->entityManager:Lorg/droidparts/persist/sql/EntityManager;

    invoke-virtual {v3, v0, v1}, Lorg/droidparts/persist/sql/EntityManager;->read(J)Lorg/droidparts/model/Entity;

    move-result-object v2

    .line 68
    .local v2, "item":Lorg/droidparts/model/Entity;, "TEntityType;"
    return-object v2
.end method

.method public bridge synthetic setContent(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 27
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    check-cast p1, Lorg/droidparts/persist/sql/stmt/AbstractSelect;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->setContent(Lorg/droidparts/persist/sql/stmt/AbstractSelect;)V

    return-void
.end method

.method public setContent(Lorg/droidparts/persist/sql/stmt/AbstractSelect;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/droidparts/persist/sql/stmt/AbstractSelect",
            "<TEntityType;>;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    .local p1, "select":Lorg/droidparts/persist/sql/stmt/AbstractSelect;, "Lorg/droidparts/persist/sql/stmt/AbstractSelect<TEntityType;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lorg/droidparts/persist/sql/stmt/AbstractSelect;->execute()Landroid/database/Cursor;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 51
    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public update(Lorg/droidparts/model/Entity;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TEntityType;)Z"
        }
    .end annotation

    .prologue
    .line 72
    .local p0, "this":Lorg/droidparts/adapter/cursor/EntityCursorAdapter;, "Lorg/droidparts/adapter/cursor/EntityCursorAdapter<TEntityType;>;"
    .local p1, "item":Lorg/droidparts/model/Entity;, "TEntityType;"
    iget-object v1, p0, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->entityManager:Lorg/droidparts/persist/sql/EntityManager;

    invoke-virtual {v1, p1}, Lorg/droidparts/persist/sql/EntityManager;->update(Lorg/droidparts/model/Entity;)Z

    move-result v0

    .line 73
    .local v0, "success":Z
    invoke-direct {p0, v0}, Lorg/droidparts/adapter/cursor/EntityCursorAdapter;->requeryOnSuccess(Z)Z

    move-result v1

    return v1
.end method
