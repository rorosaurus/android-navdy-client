.class public Lorg/droidparts/net/image/cache/BitmapLruCache;
.super Landroid/util/LruCache;
.source "BitmapLruCache.java"

# interfaces
.implements Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;",
        "Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "maxSize"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/util/LruCache;-><init>(I)V

    .line 26
    return-void
.end method


# virtual methods
.method public bridge synthetic get(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-super {p0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public bridge synthetic put(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 21
    invoke-super {p0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method
