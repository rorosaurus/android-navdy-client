.class public Lorg/droidparts/net/image/cache/BitmapMemoryCache;
.super Ljava/lang/Object;
.source "BitmapMemoryCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;
    }
.end annotation


# static fields
.field private static final DEFAULT_APP_MEMORY_PERCENT:I = 0x14

.field private static final DEFAULT_MAX_ITEM_SIZE:I = 0x40000

.field private static instance:Lorg/droidparts/net/image/cache/BitmapMemoryCache;


# instance fields
.field private delegate:Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;

.field private final maxItemSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 7
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "appMemoryPercent"    # I
    .param p3, "maxItemSize"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p3, p0, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->maxItemSize:I

    .line 55
    const-string v4, "activity"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager;

    invoke-virtual {v4}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    .line 57
    .local v0, "maxAvailableMemory":I
    int-to-float v4, v0

    int-to-float v5, p2

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v5, v6

    mul-float/2addr v4, v5

    float-to-int v4, v4

    mul-int/lit16 v4, v4, 0x400

    mul-int/lit16 v1, v4, 0x400

    .line 59
    .local v1, "maxBytes":I
    :try_start_0
    new-instance v4, Lorg/droidparts/net/image/cache/BitmapLruCache;

    invoke-direct {v4, v1}, Lorg/droidparts/net/image/cache/BitmapLruCache;-><init>(I)V

    iput-object v4, p0, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->delegate:Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;

    .line 60
    const-string v4, "Using stock LruCache."

    invoke-static {v4}, Lorg/droidparts/util/L;->i(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v2

    .line 63
    .local v2, "t":Ljava/lang/Throwable;
    :try_start_1
    invoke-static {v1}, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->getSupportLruCache(I)Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;

    move-result-object v4

    iput-object v4, p0, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->delegate:Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;

    .line 64
    const-string v4, "Using Support Package LruCache."

    invoke-static {v4}, Lorg/droidparts/util/L;->i(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 65
    :catch_1
    move-exception v3

    .line 66
    .local v3, "tr":Ljava/lang/Throwable;
    const-string v4, "LruCache not available."

    invoke-static {v4}, Lorg/droidparts/util/L;->i(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static getDefaultInstance(Landroid/content/Context;)Lorg/droidparts/net/image/cache/BitmapMemoryCache;
    .locals 3
    .param p0, "ctx"    # Landroid/content/Context;

    .prologue
    .line 43
    sget-object v0, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->instance:Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    if-nez v0, :cond_0

    .line 44
    new-instance v0, Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    const/16 v1, 0x14

    const/high16 v2, 0x40000

    invoke-direct {v0, p0, v1, v2}, Lorg/droidparts/net/image/cache/BitmapMemoryCache;-><init>(Landroid/content/Context;II)V

    sput-object v0, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->instance:Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    .line 47
    :cond_0
    sget-object v0, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->instance:Lorg/droidparts/net/image/cache/BitmapMemoryCache;

    return-object v0
.end method

.method private static getSupportLruCache(I)Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;
    .locals 6
    .param p0, "maxSize"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 97
    const-string v2, "org.droidparts.net.image.cache.SupportBitmapLruCache"

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 99
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-array v2, v5, [Ljava/lang/Class;

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 100
    .local v1, "con":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<*>;"
    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;

    return-object v2
.end method


# virtual methods
.method public get(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 85
    const/4 v0, 0x0

    .line 86
    .local v0, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    iget-object v1, p0, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->delegate:Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;

    invoke-interface {v1, p1}, Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;->get(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 89
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MemoryCache "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v0, :cond_1

    const-string v1, "miss"

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for \'%s\'."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lorg/droidparts/util/L;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    return-object v0

    .line 89
    :cond_1
    const-string v1, "hit"

    goto :goto_0
.end method

.method public isAvailable()Z
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->delegate:Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public put(Ljava/lang/String;Landroid/graphics/Bitmap;)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 76
    const/4 v0, 0x0

    .line 77
    .local v0, "put":Z
    invoke-virtual {p0}, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->isAvailable()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p2}, Lorg/droidparts/util/ui/BitmapUtils;->getSize(Landroid/graphics/Bitmap;)I

    move-result v1

    iget v2, p0, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->maxItemSize:I

    if-gt v1, v2, :cond_0

    .line 78
    iget-object v1, p0, Lorg/droidparts/net/image/cache/BitmapMemoryCache;->delegate:Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;

    invoke-interface {v1, p1, p2}, Lorg/droidparts/net/image/cache/BitmapMemoryCache$Delegate;->put(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 79
    const/4 v0, 0x1

    .line 81
    :cond_0
    return v0
.end method
