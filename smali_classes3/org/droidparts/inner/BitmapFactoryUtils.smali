.class public Lorg/droidparts/inner/BitmapFactoryUtils;
.super Ljava/lang/Object;
.source "BitmapFactoryUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static calcDecodeSizeHint(Landroid/widget/ImageView;)Landroid/graphics/Point;
    .locals 3
    .param p0, "imageView"    # Landroid/widget/ImageView;

    .prologue
    .line 33
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 34
    .local v0, "p":Landroid/graphics/Point;
    invoke-virtual {p0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 35
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    if-eqz v1, :cond_0

    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    :goto_0
    iput v2, v0, Landroid/graphics/Point;->x:I

    .line 36
    if-eqz v1, :cond_1

    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :goto_1
    iput v2, v0, Landroid/graphics/Point;->y:I

    .line 37
    return-object v0

    .line 35
    :cond_0
    invoke-virtual {p0}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    goto :goto_0

    .line 36
    :cond_1
    invoke-virtual {p0}, Landroid/widget/ImageView;->getHeight()I

    move-result v2

    goto :goto_1
.end method

.method private static calcInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I
    .locals 7
    .param p0, "opts"    # Landroid/graphics/BitmapFactory$Options;
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I

    .prologue
    .line 94
    iget v0, p0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 95
    .local v0, "height":I
    iget v3, p0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 96
    .local v3, "width":I
    const/4 v2, 0x1

    .line 97
    .local v2, "inSampleSize":I
    if-gt v0, p2, :cond_0

    if-le v3, p1, :cond_3

    .line 98
    :cond_0
    const/4 v1, 0x1

    .line 99
    .local v1, "heightRatio":I
    if-lez p2, :cond_1

    .line 100
    int-to-float v5, v0

    int-to-float v6, p2

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 102
    :cond_1
    const/4 v4, 0x1

    .line 103
    .local v4, "widthRatio":I
    if-lez p1, :cond_2

    .line 104
    int-to-float v5, v3

    int-to-float v6, p1

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 106
    :cond_2
    if-ge v1, v4, :cond_4

    move v2, v1

    .line 109
    .end local v1    # "heightRatio":I
    .end local v4    # "widthRatio":I
    :cond_3
    :goto_0
    return v2

    .restart local v1    # "heightRatio":I
    .restart local v4    # "widthRatio":I
    :cond_4
    move v2, v4

    .line 106
    goto :goto_0
.end method

.method public static decodeScaled([BIILandroid/graphics/Bitmap$Config;Landroid/graphics/Bitmap;)Landroid/util/Pair;
    .locals 11
    .param p0, "data"    # [B
    .param p1, "reqWidth"    # I
    .param p2, "reqHeight"    # I
    .param p3, "config"    # Landroid/graphics/Bitmap$Config;
    .param p4, "inBitmap"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII",
            "Landroid/graphics/Bitmap$Config;",
            "Landroid/graphics/Bitmap;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Landroid/graphics/BitmapFactory$Options;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 44
    .local v3, "opts":Landroid/graphics/BitmapFactory$Options;
    if-gtz p1, :cond_0

    if-lez p2, :cond_5

    :cond_0
    const/4 v2, 0x1

    .line 45
    .local v2, "gotSizeHint":Z
    :goto_0
    if-eqz p3, :cond_6

    const/4 v1, 0x1

    .line 46
    .local v1, "gotConfig":Z
    :goto_1
    if-nez v2, :cond_1

    if-eqz v1, :cond_4

    .line 47
    :cond_1
    if-eqz v1, :cond_2

    .line 48
    iput-object p3, v3, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 50
    :cond_2
    if-eqz v2, :cond_3

    .line 51
    const/4 v6, 0x1

    iput-boolean v6, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 52
    const/4 v6, 0x0

    array-length v7, p0

    invoke-static {p0, v6, v7, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 53
    invoke-static {v3, p1, p2}, Lorg/droidparts/inner/BitmapFactoryUtils;->calcInSampleSize(Landroid/graphics/BitmapFactory$Options;II)I

    move-result v6

    iput v6, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 54
    const/4 v6, 0x0

    iput-boolean v6, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 56
    :cond_3
    invoke-static {}, Lorg/droidparts/inner/BitmapFactoryUtils;->inBitmapSupported()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 57
    iput-object p4, v3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 60
    :cond_4
    const/4 v0, 0x0

    .line 62
    .local v0, "bm":Landroid/graphics/Bitmap;
    const/4 v6, 0x0

    :try_start_0
    array-length v7, p0

    invoke-static {p0, v6, v7, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 79
    :goto_2
    if-nez v0, :cond_8

    .line 80
    new-instance v6, Ljava/io/IOException;

    const-string v7, "BitmapFactory returned null."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 44
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v1    # "gotConfig":Z
    .end local v2    # "gotSizeHint":Z
    :cond_5
    const/4 v2, 0x0

    goto :goto_0

    .line 45
    .restart local v2    # "gotSizeHint":Z
    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    .line 63
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    .restart local v1    # "gotConfig":Z
    :catch_0
    move-exception v4

    .line 64
    .local v4, "t":Ljava/lang/Throwable;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 65
    invoke-static {}, Lorg/droidparts/inner/BitmapFactoryUtils;->inBitmapSupported()Z

    move-result v6

    if-eqz v6, :cond_7

    if-eqz p4, :cond_7

    .line 66
    const/4 v6, 0x0

    iput-object v6, v3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 68
    const/4 v6, 0x0

    :try_start_1
    array-length v7, p0

    invoke-static {p0, v6, v7, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 70
    invoke-static {v4}, Lorg/droidparts/util/L;->w(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 71
    :catch_1
    move-exception v5

    .line 72
    .local v5, "th":Ljava/lang/Throwable;
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 73
    new-instance v6, Ljava/lang/Exception;

    invoke-direct {v6, v5}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 76
    .end local v5    # "th":Ljava/lang/Throwable;
    :cond_7
    new-instance v6, Ljava/lang/Exception;

    invoke-direct {v6, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 81
    .end local v4    # "t":Ljava/lang/Throwable;
    :cond_8
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    if-lez v6, :cond_9

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-gtz v6, :cond_a

    .line 82
    :cond_9
    new-instance v6, Ljava/io/IOException;

    const-string v7, "Invalid Bitmap: w:%d, h:%d."

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 85
    :cond_a
    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v6

    return-object v6
.end method

.method private static inBitmapSupported()Z
    .locals 2

    .prologue
    .line 89
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
