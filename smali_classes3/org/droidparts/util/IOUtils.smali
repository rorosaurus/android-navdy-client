.class public Lorg/droidparts/util/IOUtils;
.super Ljava/lang/Object;
.source "IOUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copy(Ljava/io/File;Ljava/io/File;)V
    .locals 9
    .param p0, "fileFrom"    # Ljava/io/File;
    .param p1, "fileTo"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 96
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 97
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 99
    :cond_0
    const/4 v1, 0x0

    .line 100
    .local v1, "src":Ljava/nio/channels/FileChannel;
    const/4 v0, 0x0

    .line 102
    .local v0, "dst":Ljava/nio/channels/FileChannel;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v1

    .line 103
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 104
    const-wide/16 v2, 0x0

    invoke-virtual {v1}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Ljava/nio/channels/FileChannel;->transferFrom(Ljava/nio/channels/ReadableByteChannel;JJ)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    new-array v2, v8, [Ljava/io/Closeable;

    aput-object v1, v2, v6

    aput-object v0, v2, v7

    invoke-static {v2}, Lorg/droidparts/util/IOUtils;->silentlyClose([Ljava/io/Closeable;)V

    .line 109
    return-void

    .line 106
    :catchall_0
    move-exception v2

    new-array v3, v8, [Ljava/io/Closeable;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    invoke-static {v3}, Lorg/droidparts/util/IOUtils;->silentlyClose([Ljava/io/Closeable;)V

    throw v2
.end method

.method public static dumpDBToCacheDir(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8
    .param p0, "ctx"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 112
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 113
    .local v1, "dbFilePath":Ljava/lang/String;
    const/16 v4, 0x2f

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->lastIndexOf(II)I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 115
    .local v0, "dbFileName":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 117
    .local v3, "fileTo":Ljava/io/File;
    :try_start_0
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v4, v3}, Lorg/droidparts/util/IOUtils;->copy(Ljava/io/File;Ljava/io/File;)V

    .line 118
    const-string v4, "Copied DB file to \'%s\'."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lorg/droidparts/util/L;->i(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-void

    .line 119
    :catch_0
    move-exception v2

    .line 120
    .local v2, "e":Ljava/io/IOException;
    invoke-static {v2}, Lorg/droidparts/util/L;->w(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static varargs getFileList(Ljava/io/File;[Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 12
    .param p0, "dir"    # Ljava/io/File;
    .param p1, "fileExtensions"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "[",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 72
    .local v6, "files":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/io/File;>;"
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v4

    .line 73
    .local v4, "fileList":[Ljava/io/File;
    if-eqz v4, :cond_4

    .line 74
    move-object v0, v4

    .local v0, "arr$":[Ljava/io/File;
    array-length v9, v0

    .local v9, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    move v8, v7

    .end local v0    # "arr$":[Ljava/io/File;
    .end local v7    # "i$":I
    .end local v9    # "len$":I
    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_4

    aget-object v3, v0, v8

    .line 75
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->isFile()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 76
    array-length v11, p1

    if-nez v11, :cond_1

    .line 77
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    .end local v8    # "i$":I
    :cond_0
    :goto_1
    add-int/lit8 v7, v8, 0x1

    .restart local v7    # "i$":I
    move v8, v7

    .end local v7    # "i$":I
    .restart local v8    # "i$":I
    goto :goto_0

    .line 79
    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    .line 80
    .local v5, "fileName":Ljava/lang/String;
    move-object v1, p1

    .local v1, "arr$":[Ljava/lang/String;
    array-length v10, v1

    .local v10, "len$":I
    const/4 v7, 0x0

    .end local v8    # "i$":I
    .restart local v7    # "i$":I
    :goto_2
    if-ge v7, v10, :cond_0

    aget-object v2, v1, v7

    .line 81
    .local v2, "ext":Ljava/lang/String;
    invoke-virtual {v5, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 82
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 80
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 88
    .end local v1    # "arr$":[Ljava/lang/String;
    .end local v2    # "ext":Ljava/lang/String;
    .end local v5    # "fileName":Ljava/lang/String;
    .end local v7    # "i$":I
    .end local v10    # "len$":I
    .restart local v8    # "i$":I
    :cond_3
    invoke-static {v3, p1}, Lorg/droidparts/util/IOUtils;->getFileList(Ljava/io/File;[Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 92
    .end local v3    # "file":Ljava/io/File;
    .end local v8    # "i$":I
    :cond_4
    return-object v6
.end method

.method public static readToByteArray(Ljava/io/InputStream;)[B
    .locals 2
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 55
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 56
    .local v0, "baos":Ljava/io/ByteArrayOutputStream;
    invoke-static {p0, v0}, Lorg/droidparts/util/IOUtils;->readToStream(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    .line 57
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1
.end method

.method public static readToStream(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 3
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "os"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    const/16 v2, 0x2000

    new-array v0, v2, [B

    .line 64
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "len":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 65
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    .line 67
    :cond_0
    return-void
.end method

.method public static readToString(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 3
    .param p0, "is"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    invoke-static {p0}, Lorg/droidparts/util/IOUtils;->readToByteArray(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 51
    .local v0, "data":[B
    new-instance v1, Ljava/lang/String;

    const-string v2, "utf-8"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v1
.end method

.method public static varargs silentlyClose([Ljava/io/Closeable;)V
    .locals 5
    .param p0, "closeables"    # [Ljava/io/Closeable;

    .prologue
    .line 38
    move-object v0, p0

    .local v0, "arr$":[Ljava/io/Closeable;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 40
    .local v1, "cl":Ljava/io/Closeable;
    if-eqz v1, :cond_0

    .line 41
    :try_start_0
    invoke-interface {v1}, Ljava/io/Closeable;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 43
    :catch_0
    move-exception v2

    .line 44
    .local v2, "e":Ljava/lang/Exception;
    invoke-static {v2}, Lorg/droidparts/util/L;->d(Ljava/lang/Object;)V

    goto :goto_1

    .line 47
    .end local v1    # "cl":Ljava/io/Closeable;
    .end local v2    # "e":Ljava/lang/Exception;
    :cond_1
    return-void
.end method
