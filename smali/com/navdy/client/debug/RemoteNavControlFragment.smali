.class public Lcom/navdy/client/debug/RemoteNavControlFragment;
.super Landroid/app/Fragment;
.source "RemoteNavControlFragment.java"


# static fields
.field private static final ARG_DESTINATION_LABEL:Ljava/lang/String; = "destination"

.field private static final ARG_ROUTE_ID:Ljava/lang/String; = "routeId"

.field private static final ARG_ROUTE_LABEL:Ljava/lang/String; = "routeLabel"

.field public static final EXT_TAG:Ljava/lang/String; = "RemoteNavControlFragment"

.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private mAllButtons:[Landroid/widget/Button;

.field private mDestinationLabel:Ljava/lang/String;

.field protected mDestinationLabelTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10026d
    .end annotation
.end field

.field protected mNavigationButtonPause:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100270
    .end annotation
.end field

.field protected mNavigationButtonResume:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100271
    .end annotation
.end field

.field protected mNavigationButtonSimulate:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10026f
    .end annotation
.end field

.field protected mNavigationButtonStart:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10026e
    .end annotation
.end field

.field protected mNavigationButtonStop:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100272
    .end annotation
.end field

.field private mNavigationSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field private mRouteResult:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

.field protected mTextViewCurrentSimSpeed:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100232
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/RemoteNavControlFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/RemoteNavControlFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 85
    return-void
.end method

.method public static newInstance(Lcom/navdy/service/library/events/navigation/NavigationRouteResult;Ljava/lang/String;)Lcom/navdy/client/debug/RemoteNavControlFragment;
    .locals 4
    .param p0, "routeResult"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    .param p1, "destinationLabel"    # Ljava/lang/String;

    .prologue
    .line 74
    new-instance v1, Lcom/navdy/client/debug/RemoteNavControlFragment;

    invoke-direct {v1}, Lcom/navdy/client/debug/RemoteNavControlFragment;-><init>()V

    .line 75
    .local v1, "fragment":Lcom/navdy/client/debug/RemoteNavControlFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 76
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "routeId"

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v2, "routeLabel"

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->label:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    const-string v2, "destination"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-virtual {v1, v0}, Lcom/navdy/client/debug/RemoteNavControlFragment;->setArguments(Landroid/os/Bundle;)V

    .line 80
    return-object v1
.end method

.method private sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)V
    .locals 1
    .param p1, "newState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .prologue
    .line 197
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/client/debug/RemoteNavControlFragment;->sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;I)V

    .line 198
    return-void
.end method

.method private sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;I)V
    .locals 6
    .param p1, "newState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .param p2, "simulationSpeed"    # I

    .prologue
    .line 201
    sget-object v1, Lcom/navdy/client/debug/RemoteNavControlFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "RemoteNavControlFragment: Attempting to change state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 202
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->isConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 203
    sget-object v1, Lcom/navdy/client/debug/RemoteNavControlFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "RemoteNavControlFragment: : device is no longer connected"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 213
    :goto_0
    return-void

    .line 206
    :cond_0
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;

    iget-object v2, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mDestinationLabel:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mRouteResult:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v3, v1, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;-><init>(Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 208
    .local v0, "request":Lcom/navdy/service/library/events/navigation/NavigationSessionRequest;
    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 209
    sget-object v1, Lcom/navdy/client/debug/RemoteNavControlFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "RemoteNavControlFragment: State change request sent successfully."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 211
    :cond_1
    sget-object v1, Lcom/navdy/client/debug/RemoteNavControlFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "RemoteNavControlFragment: State change request failed to send."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateNavigationSessionState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)V
    .locals 6
    .param p1, "newState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .prologue
    const/4 v2, 0x0

    .line 138
    sget-object v1, Lcom/navdy/client/debug/RemoteNavControlFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "RemoteNavControlFragment: : Received new state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 140
    iget-object v3, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mAllButtons:[Landroid/widget/Button;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v0, v3, v1

    .line 141
    .local v0, "button":Landroid/widget/Button;
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setEnabled(Z)V

    .line 142
    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    .line 140
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 145
    .end local v0    # "button":Landroid/widget/Button;
    :cond_0
    sget-object v1, Lcom/navdy/client/debug/RemoteNavControlFragment$1;->$SwitchMap$com$navdy$service$library$events$navigation$NavigationSessionState:[I

    invoke-virtual {p1}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    .line 149
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonStart:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 150
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonSimulate:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 151
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonStart:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 152
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonSimulate:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 167
    :goto_1
    iput-object p1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 168
    return-void

    .line 155
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonStart:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 156
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonSimulate:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 159
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonResume:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 160
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonStop:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 163
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonPause:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 89
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 90
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteNavControlFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteNavControlFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "routeId"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 92
    .local v1, "routeId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteNavControlFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "routeLabel"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 93
    .local v2, "routeLabel":Ljava/lang/String;
    new-instance v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 94
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object v7, v3

    move-object v8, v3

    move-object v9, v3

    invoke-direct/range {v0 .. v9}, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mRouteResult:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 95
    invoke-virtual {p0}, Lcom/navdy/client/debug/RemoteNavControlFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "destination"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mDestinationLabel:Ljava/lang/String;

    .line 98
    .end local v1    # "routeId":Ljava/lang/String;
    .end local v2    # "routeLabel":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->getNavigationSessionState()Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 99
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 111
    const v1, 0x7f030094

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 113
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 115
    const/4 v1, 0x5

    new-array v1, v1, [Landroid/widget/Button;

    iget-object v2, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonStart:Landroid/widget/Button;

    aput-object v2, v1, v3

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonSimulate:Landroid/widget/Button;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonPause:Landroid/widget/Button;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonResume:Landroid/widget/Button;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationButtonStop:Landroid/widget/Button;

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mAllButtons:[Landroid/widget/Button;

    .line 118
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mDestinationLabel:Ljava/lang/String;

    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mDestinationLabelTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mDestinationLabel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mNavigationSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-direct {p0, v1}, Lcom/navdy/client/debug/RemoteNavControlFragment;->updateNavigationSessionState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)V

    .line 124
    iget-object v1, p0, Lcom/navdy/client/debug/RemoteNavControlFragment;->mTextViewCurrentSimSpeed:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Current simulation speed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 125
    invoke-static {}, Lcom/navdy/client/debug/DebugActionsFragment;->getSimulationSpeed()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " m/s "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/navdy/client/debug/DebugActionsFragment;->getSimulationSpeed()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x400f295f    # 2.2369f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " MPH"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 124
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    return-object v0
.end method

.method public onDeviceDisconnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 217
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/RemoteNavControlFragment;->updateNavigationSessionState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)V

    .line 218
    return-void
.end method

.method public onNavigationSessionResponse(Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;)V
    .locals 3
    .param p1, "response"    # Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 227
    sget-object v0, Lcom/navdy/client/debug/RemoteNavControlFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "RemoteNavControlFragment: Session response for pendingState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->pendingSessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionResponse;->statusDetail:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 230
    return-void
.end method

.method public onNavigationSessionStatusEvent(Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;)V
    .locals 1
    .param p1, "status"    # Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 222
    iget-object v0, p1, Lcom/navdy/service/library/events/navigation/NavigationSessionStatusEvent;->sessionState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/RemoteNavControlFragment;->updateNavigationSessionState(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)V

    .line 223
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 103
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->unregister(Ljava/lang/Object;)V

    .line 104
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 105
    return-void
.end method

.method protected onPauseClicked()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100270
        }
    .end annotation

    .prologue
    .line 183
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_PAUSED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/RemoteNavControlFragment;->sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)V

    .line 184
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 132
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 134
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 135
    return-void
.end method

.method protected onResumeClicked()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100271
        }
    .end annotation

    .prologue
    .line 188
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/RemoteNavControlFragment;->sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)V

    .line 189
    return-void
.end method

.method protected onSimulateClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10026f
        }
    .end annotation

    .prologue
    .line 177
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 178
    invoke-static {}, Lcom/navdy/client/debug/DebugActionsFragment;->getSimulationSpeed()I

    move-result v1

    .line 177
    invoke-direct {p0, v0, v1}, Lcom/navdy/client/debug/RemoteNavControlFragment;->sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;I)V

    .line 179
    return-void
.end method

.method protected onStartClicked()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f10026e
        }
    .end annotation

    .prologue
    .line 172
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STARTED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/navdy/client/debug/RemoteNavControlFragment;->sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;I)V

    .line 173
    return-void
.end method

.method protected onStopClicked()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f100272
        }
    .end annotation

    .prologue
    .line 193
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_STOPPED:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/RemoteNavControlFragment;->sendStateChangeRequest(Lcom/navdy/service/library/events/navigation/NavigationSessionState;)V

    .line 194
    return-void
.end method
