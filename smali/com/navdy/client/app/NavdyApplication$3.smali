.class Lcom/navdy/client/app/NavdyApplication$3;
.super Ljava/lang/Object;
.source "NavdyApplication.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/NavdyApplication;->initApp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/NavdyApplication;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/NavdyApplication;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/NavdyApplication;

    .prologue
    .line 305
    iput-object p1, p0, Lcom/navdy/client/app/NavdyApplication$3;->this$0:Lcom/navdy/client/app/NavdyApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 308
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "init the TTS audio router"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 309
    iget-object v1, p0, Lcom/navdy/client/app/NavdyApplication$3;->this$0:Lcom/navdy/client/app/NavdyApplication;

    iget-object v1, v1, Lcom/navdy/client/app/NavdyApplication;->mAudioRouter:Lcom/navdy/client/app/framework/util/TTSAudioRouter;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/util/TTSAudioRouter;->init()V

    .line 310
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "init localytics"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 311
    invoke-static {}, Lcom/navdy/client/app/framework/LocalyticsManager;->getInstance()Lcom/navdy/client/app/framework/LocalyticsManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/LocalyticsManager;->initLocalytics()V

    .line 312
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "Setting crashreporter user (again) now that the Localytics engine is initialized."

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 314
    invoke-static {}, Lcom/navdy/client/app/framework/util/CrashReporter;->getInstance()Lcom/navdy/client/app/framework/util/CrashReporter;

    move-result-object v1

    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->getUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/util/CrashReporter;->setUser(Ljava/lang/String;)V

    .line 315
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->getInstance()Lcom/navdy/client/app/framework/location/NavdyLocationManager;

    .line 317
    invoke-static {}, Lcom/navdy/client/app/framework/i18n/I18nManager;->getInstance()Lcom/navdy/client/app/framework/i18n/I18nManager;

    .line 318
    iget-object v1, p0, Lcom/navdy/client/app/NavdyApplication$3;->this$0:Lcom/navdy/client/app/NavdyApplication;

    invoke-static {v1}, Lcom/navdy/client/app/NavdyApplication;->access$100(Lcom/navdy/client/app/NavdyApplication;)V

    .line 319
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->checkObdSettingIfBuildVersionChanged()V

    .line 321
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "pending_tickets_exist"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 322
    .local v0, "pendingTicketsExist":Z
    if-eqz v0, :cond_0

    .line 323
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->submitTicketsIfConditionsAreMet()V

    .line 326
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/NavdyApplication$3;->this$0:Lcom/navdy/client/app/NavdyApplication;

    invoke-static {v1}, Lcom/navdy/client/app/NavdyApplication;->access$200(Lcom/navdy/client/app/NavdyApplication;)V

    .line 327
    invoke-static {}, Lcom/navdy/client/app/service/DataCollectionService;->getInstance()Lcom/navdy/client/app/service/DataCollectionService;

    move-result-object v1

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->access$300()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/service/DataCollectionService;->startService(Landroid/content/Context;)V

    .line 328
    return-void
.end method
