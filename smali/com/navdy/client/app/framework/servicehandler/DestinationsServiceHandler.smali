.class public Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;
.super Ljava/lang/Object;
.source "DestinationsServiceHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;
    }
.end annotation


# static fields
.field private static final VERBOSE:Z

.field public static final logger:Lcom/navdy/service/library/log/Logger;

.field private static singleton:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    .line 37
    const/4 v0, 0x0

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 47
    return-void
.end method

.method static synthetic access$000()V
    .locals 0

    .prologue
    .line 34
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->sendFavoritesToDisplay()V

    return-void
.end method

.method static synthetic access$100()V
    .locals 0

    .prologue
    .line 34
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->sendSuggestionsToDisplay()V

    return-void
.end method

.method private static finalizeSuggestionsSendToNavdy(JLjava/util/List;)V
    .locals 8
    .param p0, "version"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "protobufDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    const/4 v6, 0x0

    .line 177
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "finalizeSuggestionsSendToNavdy sending list of size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 178
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 196
    new-instance v0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2, p2}, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->sendRemoteMessage(Lcom/squareup/wire/Message;)V

    .line 204
    :goto_0
    return-void

    .line 201
    :cond_0
    new-instance v0, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    sget-object v1, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    const-string v2, "No suggestions to send"

    const-wide/16 v4, 0x0

    .line 202
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3, v6}, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V

    .line 201
    invoke-static {v0}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->sendRemoteMessage(Lcom/squareup/wire/Message;)V

    goto :goto_0
.end method

.method public static declared-synchronized getInstance()Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;
    .locals 2

    .prologue
    .line 57
    const-class v1, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;

    .line 60
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->singleton:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static sendDestinationsToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V
    .locals 3
    .param p0, "destinationType"    # Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    .prologue
    .line 91
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 104
    return-void
.end method

.method private static declared-synchronized sendFavoritesToDisplay()V
    .locals 13

    .prologue
    .line 110
    const-class v8, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;

    monitor-enter v8

    const/4 v0, 0x0

    .line 113
    .local v0, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v7

    sget-object v9, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    .line 112
    invoke-static {v7, v9}, Lcom/navdy/client/app/framework/util/VersioningUtils;->getCurrentVersion(Landroid/content/Context;Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)J

    move-result-wide v4

    .line 116
    .local v4, "favoritesVersion":J
    invoke-static {}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getFavoritesCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 117
    if-nez v0, :cond_0

    .line 118
    new-instance v7, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    sget-object v9, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v10, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v7, v9, v10, v11, v12}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V

    invoke-static {v7}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->sendRemoteMessage(Lcom/squareup/wire/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 138
    :try_start_1
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "closing cursor"

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 139
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 141
    :goto_0
    monitor-exit v8

    return-void

    .line 122
    :cond_0
    :try_start_2
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    .line 123
    .local v1, "cursorCount":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 124
    .local v6, "protobufDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/destination/Destination;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v1, :cond_2

    .line 126
    invoke-static {v0, v3}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getDestinationItemAt(Landroid/database/Cursor;I)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    .line 127
    .local v2, "destination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->hasOneValidSetOfCoordinates()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 129
    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->toProtobufDestinationObject()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 133
    .end local v2    # "destination":Lcom/navdy/client/app/framework/models/Destination;
    :cond_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_3

    .line 136
    :cond_3
    new-instance v7, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    sget-object v9, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v10, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-direct {v7, v9, v10, v11, v6}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V

    invoke-static {v7}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->sendRemoteMessage(Lcom/squareup/wire/Message;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 138
    :try_start_3
    sget-object v7, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v9, "closing cursor"

    invoke-virtual {v7, v9}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 139
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 110
    .end local v1    # "cursorCount":I
    .end local v3    # "i":I
    .end local v4    # "favoritesVersion":J
    .end local v6    # "protobufDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/destination/Destination;>;"
    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7

    .line 138
    :catchall_1
    move-exception v7

    :try_start_4
    sget-object v9, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "closing cursor"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 139
    invoke-static {v0}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method private static sendRemoteMessage(Lcom/squareup/wire/Message;)V
    .locals 4
    .param p0, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 211
    invoke-static {p0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v0

    .line 212
    .local v0, "success":Z
    if-nez v0, :cond_0

    .line 213
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to send message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 215
    :cond_0
    return-void
.end method

.method private static sendSuggestionsToDisplay()V
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/SuggestionManager;->buildSuggestionList(Z)V

    .line 151
    return-void
.end method

.method public static declared-synchronized sendSuggestionsToDisplay(JLjava/util/List;)V
    .locals 10
    .param p0, "version"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/client/app/framework/models/Suggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 157
    .local p2, "suggestions":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/Suggestion;>;"
    const-class v5, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;

    monitor-enter v5

    if-eqz p2, :cond_2

    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_2

    .line 158
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "sendSuggestionsToDisplay sending list of size: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 159
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    .line 160
    .local v3, "suggestionsSize":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 161
    .local v1, "protobufDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/navdy/client/app/framework/models/Suggestion;

    .line 162
    .local v2, "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    if-eqz v2, :cond_0

    iget-object v6, v2, Lcom/navdy/client/app/framework/models/Suggestion;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v6, :cond_0

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Suggestion;->shouldSendToHud()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 163
    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Suggestion;->toProtobufDestinationObject()Lcom/navdy/service/library/events/destination/Destination;

    move-result-object v0

    .line 164
    .local v0, "destination":Lcom/navdy/service/library/events/destination/Destination;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 157
    .end local v0    # "destination":Lcom/navdy/service/library/events/destination/Destination;
    .end local v1    # "protobufDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/destination/Destination;>;"
    .end local v2    # "suggestion":Lcom/navdy/client/app/framework/models/Suggestion;
    .end local v3    # "suggestionsSize":I
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4

    .line 168
    .restart local v1    # "protobufDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/destination/Destination;>;"
    .restart local v3    # "suggestionsSize":I
    :cond_1
    :try_start_1
    invoke-static {p0, p1, v1}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->finalizeSuggestionsSendToNavdy(JLjava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173
    .end local v1    # "protobufDestinations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/service/library/events/destination/Destination;>;"
    .end local v3    # "suggestionsSize":I
    :goto_1
    monitor-exit v5

    return-void

    .line 170
    :cond_2
    :try_start_2
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "sendSuggestionsToDisplay list is empty"

    invoke-virtual {v4, v6}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 171
    new-instance v4, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;

    sget-object v6, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v7, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/navdy/service/library/events/destination/RecommendedDestinationsUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V

    invoke-static {v4}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->sendRemoteMessage(Lcom/squareup/wire/Message;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public onFavoriteDestinationsRequest(Lcom/navdy/service/library/events/FavoriteDestinationsRequest;)V
    .locals 9
    .param p1, "favoriteDestinationsRequest"    # Lcom/navdy/service/library/events/FavoriteDestinationsRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 65
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Request for Favorite Destinations received."

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 67
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v4

    sget-object v5, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    .line 66
    invoke-static {v4, v5}, Lcom/navdy/client/app/framework/util/VersioningUtils;->getCurrentVersion(Landroid/content/Context;Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)J

    move-result-wide v0

    .line 69
    .local v0, "favoritesVersion":J
    iget-object v4, p1, Lcom/navdy/service/library/events/FavoriteDestinationsRequest;->serial_number:Ljava/lang/Long;

    const-wide/16 v6, -0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/squareup/wire/Wire;->get(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 70
    .local v2, "serialNumber":J
    cmp-long v4, v2, v0

    if-nez v4, :cond_0

    .line 71
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Favorites Version up to date"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 72
    new-instance v4, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_VERSION_IS_CURRENT:Lcom/navdy/service/library/events/RequestStatus;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v4, v5, v8, v6, v8}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Ljava/util/List;)V

    invoke-static {v4}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->sendRemoteMessage(Lcom/squareup/wire/Message;)V

    .line 76
    :goto_0
    return-void

    .line 74
    :cond_0
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->FAVORITES:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v4}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->sendDestinationsToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    goto :goto_0
.end method

.method public onRecommendedDestinationsRequest(Lcom/navdy/service/library/events/destination/RecommendedDestinationsRequest;)V
    .locals 2
    .param p1, "recommendedDestinationsRequest"    # Lcom/navdy/service/library/events/destination/RecommendedDestinationsRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 80
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Request for Recommended Destinations Received."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 81
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;->SUGGESTIONS:Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;

    invoke-static {v0}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->sendDestinationsToDisplay(Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler$DestinationType;)V

    .line 82
    return-void
.end method
