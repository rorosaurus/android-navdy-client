.class public Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;
.super Ljava/lang/Object;
.source "TelephonyServiceHandler.java"


# static fields
.field private static final TURN_SPEAKER_ON_DELAY:I = 0xbb8

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private currentServiceState:I

.field private mContext:Landroid/content/Context;

.field private mTelephony:Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;

.field private phoneStateListener:Landroid/telephony/PhoneStateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput v4, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->currentServiceState:I

    .line 53
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->phoneStateListener:Landroid/telephony/PhoneStateListener;

    .line 70
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->mContext:Landroid/content/Context;

    .line 71
    invoke-static {p1}, Lcom/navdy/client/app/framework/callcontrol/TelephonyInterfaceFactory;->getTelephonyInterface(Landroid/content/Context;)Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->mTelephony:Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;

    .line 72
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Telephony model["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] manufacturer["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] device["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 73
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Telephony controller is :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->mTelephony:Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 74
    const-string v1, "phone"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 75
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->phoneStateListener:Landroid/telephony/PhoneStateListener;

    invoke-virtual {v0, v1, v4}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 76
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "registeted phone state listener"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 78
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;

    .prologue
    .line 41
    iget v0, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->currentServiceState:I

    return v0
.end method

.method static synthetic access$002(Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;I)I
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;
    .param p1, "x1"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->currentServiceState:I

    return p1
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private isHFPSpeakerConnected()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 165
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/bluetooth/BluetoothAdapter;->getProfileConnectionState(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private turnOnSpeakerPhoneIfNeeded()V
    .locals 4

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->isHFPSpeakerConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    .line 151
    .local v0, "timer":Ljava/util/Timer;
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$2;-><init>(Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 161
    .end local v0    # "timer":Ljava/util/Timer;
    :cond_0
    return-void
.end method


# virtual methods
.method public onDeviceDisconnectedEvent(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 83
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->mContext:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 84
    .local v0, "tm":Landroid/telephony/TelephonyManager;
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->phoneStateListener:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 85
    return-void
.end method

.method public onPhoneStatusRequest(Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;)V
    .locals 3
    .param p1, "phoneStatusRequest"    # Lcom/navdy/service/library/events/callcontrol/PhoneStatusRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 143
    invoke-static {}, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->getCurrentStatus()Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    move-result-object v0

    .line 144
    .local v0, "phoneEvent":Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
    new-instance v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {v1, v2, v0}, Lcom/navdy/service/library/events/callcontrol/PhoneStatusResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Lcom/navdy/service/library/events/callcontrol/PhoneEvent;)V

    invoke-static {v1}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 145
    return-void
.end method

.method public onTelephonyRequest(Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;)V
    .locals 8
    .param p1, "telephonyRequest"    # Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 89
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler$3;->$SwitchMap$com$navdy$service$library$events$callcontrol$CallAction:[I

    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/callcontrol/CallAction;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 135
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown TelephonyServiceHandler request action: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 138
    :goto_0
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;

    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;-><init>(Lcom/navdy/service/library/events/callcontrol/CallAction;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 139
    :goto_1
    return-void

    .line 91
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->mTelephony:Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;

    invoke-interface {v0}, Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;->acceptRingingCall()V

    .line 92
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->turnOnSpeakerPhoneIfNeeded()V

    goto :goto_0

    .line 96
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->mTelephony:Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;

    invoke-interface {v0}, Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;->rejectRingingCall()V

    goto :goto_0

    .line 100
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->mTelephony:Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;

    invoke-interface {v0}, Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;->endCall()V

    goto :goto_0

    .line 104
    :pswitch_3
    iget v0, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->currentServiceState:I

    if-eqz v0, :cond_0

    .line 106
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    sget-object v1, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DISCONNECTING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->number:Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 107
    new-instance v0, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;

    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->action:Lcom/navdy/service/library/events/callcontrol/CallAction;

    sget-object v2, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SERVICE_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/callcontrol/TelephonyResponse;-><init>(Lcom/navdy/service/library/events/callcontrol/CallAction;Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    goto :goto_1

    .line 110
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/TelephonyRequest;->number:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 111
    .local v7, "uri":Ljava/lang/String;
    new-instance v6, Landroid/content/Intent;

    const-string v0, "android.intent.action.CALL"

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 112
    .local v6, "intent":Landroid/content/Intent;
    const/high16 v0, 0x18000000

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 113
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 114
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.CALL_PHONE"

    invoke-static {v0, v1}, Landroid/support/v4/app/ActivityCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_1

    .line 115
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 116
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->turnOnSpeakerPhoneIfNeeded()V

    goto/16 :goto_0

    .line 125
    :cond_1
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "Missing CALL_PHONE permission !"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 132
    .end local v6    # "intent":Landroid/content/Intent;
    .end local v7    # "uri":Ljava/lang/String;
    :pswitch_4
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;->mTelephony:Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;

    invoke-interface {v0}, Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;->toggleMute()V

    goto/16 :goto_0

    .line 89
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
