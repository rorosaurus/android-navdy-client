.class public final Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
.super Ljava/lang/Object;
.source "NavdyRouteHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;,
        Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;,
        Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;,
        Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;,
        Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;
    }
.end annotation


# static fields
.field private static final CACHED_ARRIVED_DESTINATION:Ljava/lang/String; = "cached_arrived_destination"

.field public static final INVALID_DISTANCE:I = -0x1

.field public static final INVALID_DURATION:I = -0x1

.field private static final PENDING_ROUTE_REFRESH:J = 0x895440L

.field private static final SAVED_DESTINATION:Ljava/lang/String; = "saved_destination"

.field private static final SHARED_PREF_SAVED_ROUTE:Ljava/lang/String; = "saved_route"

.field private static final VERBOSE:Z = true

.field private static final instance:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final appInstance:Lcom/navdy/client/app/framework/AppInstance;

.field private currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

.field private currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

.field private currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

.field private final handler:Landroid/os/Handler;

.field private lastDestinationArrived:Lcom/navdy/client/app/framework/models/Destination;

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private final navigationHelper:Lcom/navdy/client/app/framework/navigation/NavigationHelper;

.field private pendingRouteHandle:Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;

.field private final refreshPendingRoute:Ljava/lang/Runnable;

.field private requestedHighAccuracy:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    .line 66
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->instance:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$1;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->refreshPendingRoute:Ljava/lang/Runnable;

    .line 136
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->appInstance:Lcom/navdy/client/app/framework/AppInstance;

    .line 138
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->handler:Landroid/os/Handler;

    .line 139
    new-instance v1, Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    invoke-direct {v1}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->navigationHelper:Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    .line 141
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getLastDestinationArrived()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->lastDestinationArrived:Lcom/navdy/client/app/framework/models/Destination;

    .line 143
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->listeners:Ljava/util/List;

    .line 146
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "init, setting state to INACTIVE"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 148
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->INACTIVE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    iput-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 149
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    iput-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    .line 152
    invoke-static {}, Lcom/navdy/client/app/framework/util/BusProvider;->getInstance()Lcom/navdy/client/app/framework/util/BusProvider;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/navdy/client/app/framework/util/BusProvider;->register(Ljava/lang/Object;)V

    .line 154
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getPendingTrip()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 156
    .local v0, "pendingDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->requestNewRoute(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 159
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    return-object v0
.end method

.method static synthetic access$002(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    return-object p1
.end method

.method static synthetic access$100(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/navigation/NavigationHelper;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->navigationHelper:Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->setPendingTrip(Lcom/navdy/client/app/framework/models/Destination;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->lastDestinationArrived:Lcom/navdy/client/app/framework/models/Destination;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;)Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->pendingRouteHandle:Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->refreshPendingRoute:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->calculatePendingRoute(Lcom/navdy/client/app/framework/models/Destination;)V

    return-void
.end method

.method static synthetic access$500(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    return-object v0
.end method

.method static synthetic access$502(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .param p1, "x1"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    return-object p1
.end method

.method static synthetic access$600(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)Lcom/navdy/client/app/framework/AppInstance;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->appInstance:Lcom/navdy/client/app/framework/AppInstance;

    return-object v0
.end method

.method static synthetic access$700(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removeLastDestinationArrived()V

    return-void
.end method

.method static synthetic access$800(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removePendingTrip()V

    return-void
.end method

.method static synthetic access$900()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method private calculatePendingRoute(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 8
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 717
    iget-wide v0, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static {v0, v1, v6, v7}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 718
    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    .line 719
    .local v2, "latitude":D
    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    .line 736
    .local v4, "longitude":D
    :goto_0
    invoke-static {}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->getInstance()Lcom/navdy/client/app/framework/navigation/HereRouteManager;

    move-result-object v1

    new-instance v6, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;

    invoke-direct {v6, p0, p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$13;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/models/Destination;)V

    invoke-virtual/range {v1 .. v6}, Lcom/navdy/client/app/framework/navigation/HereRouteManager;->calculateRoute(DDLcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;)V

    .line 789
    .end local v2    # "latitude":D
    .end local v4    # "longitude":D
    :goto_1
    return-void

    .line 720
    :cond_0
    iget-wide v0, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    invoke-static {v0, v1, v6, v7}, Lcom/navdy/client/app/framework/map/MapUtils;->isValidSetOfCoordinates(DD)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 721
    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLat:D

    .line 722
    .restart local v2    # "latitude":D
    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->displayLong:D

    .restart local v4    # "longitude":D
    goto :goto_0

    .line 724
    .end local v2    # "latitude":D
    .end local v4    # "longitude":D
    :cond_1
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 725
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NO_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    .line 727
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$12;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$12;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    goto :goto_1
.end method

.method private callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V
    .locals 4
    .param p1, "callListener"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;

    .prologue
    .line 877
    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 878
    .local v1, "listenerWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;

    .line 880
    .local v0, "listener":Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;
    if-eqz v0, :cond_0

    .line 881
    invoke-interface {p1, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;->call(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V

    goto :goto_0

    .line 884
    .end local v0    # "listener":Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;
    .end local v1    # "listenerWeakReference":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;>;"
    :cond_1
    return-void
.end method

.method private getCachedDestination(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;
    .locals 7
    .param p1, "cacheKey"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 914
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 915
    .local v0, "context":Landroid/content/Context;
    const-string v4, "saved_route"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 919
    .local v2, "savedDestinationSharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v2, p1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 920
    .local v1, "destinationString":Ljava/lang/String;
    sget-object v4, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cached destination is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 921
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 922
    new-instance v3, Lcom/google/gson/Gson;

    invoke-direct {v3}, Lcom/google/gson/Gson;-><init>()V

    const-class v4, Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v3, v1, v4}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/framework/models/Destination;

    .line 924
    :cond_0
    return-object v3
.end method

.method public static getInstance()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->instance:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    return-object v0
.end method

.method private getLastDestinationArrived()Lcom/navdy/client/app/framework/models/Destination;
    .locals 1

    .prologue
    .line 900
    const-string v0, "cached_arrived_destination"

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getCachedDestination(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method

.method private getPendingTrip()Lcom/navdy/client/app/framework/models/Destination;
    .locals 1

    .prologue
    .line 888
    const-string v0, "saved_destination"

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getCachedDestination(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    return-object v0
.end method

.method private getRouteGeo(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/here/android/mpa/common/GeoCoordinate;",
            ">;"
        }
    .end annotation

    .prologue
    .line 840
    .local p1, "routeLatLongs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Float;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 842
    .local v1, "routeGeo":Ljava/util/List;, "Ljava/util/List<Lcom/here/android/mpa/common/GeoCoordinate;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 843
    new-instance v3, Lcom/here/android/mpa/common/GeoCoordinate;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v4, v2

    add-int/lit8 v2, v0, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v6, v2

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 842
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 846
    :cond_0
    return-object v1
.end method

.method private removeCachedDestination(Ljava/lang/String;)V
    .locals 6
    .param p1, "cacheKey"    # Ljava/lang/String;

    .prologue
    .line 948
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 949
    .local v0, "context":Landroid/content/Context;
    const-string v4, "saved_route"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 951
    .local v3, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 953
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    :try_start_0
    invoke-interface {v2, p1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 954
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 955
    sget-object v4, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Removed the pending trip."

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_1

    .line 959
    :goto_0
    return-void

    .line 956
    :catch_0
    move-exception v1

    .line 957
    .local v1, "e":Ljava/lang/RuntimeException;
    :goto_1
    sget-object v4, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 956
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private removeLastDestinationArrived()V
    .locals 1

    .prologue
    .line 909
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->lastDestinationArrived:Lcom/navdy/client/app/framework/models/Destination;

    .line 910
    const-string v0, "cached_arrived_destination"

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removeCachedDestination(Ljava/lang/String;)V

    .line 911
    return-void
.end method

.method private removePendingTrip()V
    .locals 1

    .prologue
    .line 896
    const-string v0, "saved_destination"

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removeCachedDestination(Ljava/lang/String;)V

    .line 897
    return-void
.end method

.method private setCachedDestination(Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 8
    .param p1, "cacheKey"    # Ljava/lang/String;
    .param p2, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 928
    if-nez p2, :cond_0

    .line 929
    sget-object v5, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unable to save pending trip. Provided destination is null !"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 945
    :goto_0
    return-void

    .line 933
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 934
    .local v0, "context":Landroid/content/Context;
    const-string v5, "saved_route"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 936
    .local v4, "sharedPreferences":Landroid/content/SharedPreferences;
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 938
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    :try_start_0
    new-instance v5, Lcom/google/gson/Gson;

    invoke-direct {v5}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v5, p2}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 939
    .local v3, "json":Ljava/lang/String;
    invoke-interface {v2, p1, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 940
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 941
    sget-object v5, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Saving this as pending trip: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/gson/JsonSyntaxException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 942
    .end local v3    # "json":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 943
    .local v1, "e":Ljava/lang/RuntimeException;
    :goto_1
    sget-object v5, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v5, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 942
    .end local v1    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private setEnRouteState()V
    .locals 4

    .prologue
    .line 851
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "setEnRouteState, setting state to EN_ROUTE"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 853
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->EN_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    iput-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 854
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    iput-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    .line 855
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getCurrentDestination()Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 856
    .local v0, "currentDestination":Lcom/navdy/client/app/framework/models/Destination;
    if-eqz v0, :cond_0

    .line 857
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/navdy/client/app/framework/models/Destination;->lastRoutedDate:J

    .line 858
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->updateLastRoutedDateInDbAsync()V

    .line 860
    :cond_0
    return-void
.end method

.method private setLastDestinationArrived(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 904
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->lastDestinationArrived:Lcom/navdy/client/app/framework/models/Destination;

    .line 905
    const-string v0, "cached_arrived_destination"

    invoke-direct {p0, v0, p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->setCachedDestination(Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 906
    return-void
.end method

.method private setPendingTrip(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 892
    const-string v0, "saved_destination"

    invoke-direct {p0, v0, p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->setCachedDestination(Ljava/lang/String;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 893
    return-void
.end method

.method private setStoppedState()V
    .locals 2

    .prologue
    .line 864
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setStoppedState, setting state to INACTIVE"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 866
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->INACTIVE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 867
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    .line 869
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .line 871
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->forceSuggestionFullRefresh()V

    .line 872
    invoke-static {}, Lcom/navdy/client/app/framework/util/SuggestionManager;->rebuildSuggestionListAndSendToHudAsync()V

    .line 873
    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->forceLastKnownCountryCodeUpdate()V

    .line 874
    return-void
.end method

.method private updateCurrentProgress(Lcom/here/android/mpa/common/GeoCoordinate;II)V
    .locals 6
    .param p1, "coords"    # Lcom/here/android/mpa/common/GeoCoordinate;
    .param p2, "tta"    # I
    .param p3, "distanceToDestination"    # I

    .prologue
    .line 801
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateCurrentProgressPolyline: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 802
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateCurrentProgressTTA: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 803
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateCurrentProgressDistance: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 806
    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v3}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$1900(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/here/android/mpa/common/GeoPolyline;

    move-result-object v3

    if-nez v3, :cond_1

    .line 807
    :cond_0
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "trying to make progress without a route, no-op"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 837
    :goto_0
    return-void

    .line 811
    :cond_1
    new-instance v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$1;)V

    .line 813
    .local v2, "updatedRoute":Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    invoke-static {v2, p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$2002(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;I)I

    .line 814
    invoke-static {v2, p3}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$2102(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;I)I

    .line 816
    const/4 v1, 0x0

    .line 818
    .local v1, "indexOnRoute":I
    invoke-static {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$1900(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/here/android/mpa/common/GeoPolyline;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 819
    invoke-static {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$1900(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/here/android/mpa/common/GeoPolyline;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/here/android/mpa/common/GeoPolyline;->getNearestIndex(Lcom/here/android/mpa/common/GeoCoordinate;)I

    move-result v1

    .line 822
    :cond_2
    const/4 v3, 0x1

    if-le v1, v3, :cond_3

    .line 823
    new-instance v3, Lcom/here/android/mpa/common/GeoPolyline;

    invoke-direct {v3}, Lcom/here/android/mpa/common/GeoPolyline;-><init>()V

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$402(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;Lcom/here/android/mpa/common/GeoPolyline;)Lcom/here/android/mpa/common/GeoPolyline;

    .line 825
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-gt v0, v1, :cond_3

    .line 826
    invoke-static {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$400(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/here/android/mpa/common/GeoPolyline;

    move-result-object v3

    invoke-static {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$1900(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/here/android/mpa/common/GeoPolyline;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/here/android/mpa/common/GeoPolyline;->getPoint(I)Lcom/here/android/mpa/common/GeoCoordinate;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/here/android/mpa/common/GeoPolyline;->add(Lcom/here/android/mpa/common/GeoCoordinate;)V

    .line 825
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 829
    .end local v0    # "i":I
    :cond_3
    iput-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .line 831
    new-instance v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$14;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$14;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    invoke-direct {p0, v3}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    goto :goto_0
.end method


# virtual methods
.method public addListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V
    .locals 4
    .param p1, "navdyRouteListener"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 194
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->listeners:Ljava/util/List;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$15;->$SwitchMap$com$navdy$client$app$framework$navigation$NavdyRouteHandler$State:[I

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 199
    :pswitch_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->lastDestinationArrived:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->lastDestinationArrived:Lcom/navdy/client/app/framework/models/Destination;

    invoke-interface {p1, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;->onRouteArrived(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 202
    :cond_1
    invoke-interface {p1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;->onStopRoute()V

    goto :goto_0

    .line 205
    :pswitch_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;->onPendingRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V

    goto :goto_0

    .line 208
    :pswitch_2
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-interface {p1, v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;->onPendingRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    goto :goto_0

    .line 211
    :pswitch_3
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;->onRouteCalculating(Lcom/navdy/client/app/framework/models/Destination;)V

    goto :goto_0

    .line 214
    :pswitch_4
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-interface {p1, v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;->onRouteCalculated(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    goto :goto_0

    .line 217
    :pswitch_5
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-interface {p1, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;->onRouteStarted(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    .line 219
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$400(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/here/android/mpa/common/GeoPolyline;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$400(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/here/android/mpa/common/GeoPolyline;

    move-result-object v0

    invoke-virtual {v0}, Lcom/here/android/mpa/common/GeoPolyline;->length()D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-interface {p1, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;->onTripProgress(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    goto :goto_0

    .line 197
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getCurrentDestination()Lcom/navdy/client/app/framework/models/Destination;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 167
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    .line 170
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentState()Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    return-object v0
.end method

.method public isInOneOfTheActiveTripStates()Z
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->ROUTE_CALCULATED:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->EN_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isInOneOfThePendingTripStates()Z
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onArrivedTripEvent(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$ArrivedTripEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$ArrivedTripEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 635
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->EN_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_0

    .line 636
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onArrivedTripEvent, state must be EN_ROUTE, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 650
    :goto_0
    return-void

    .line 640
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/homescreen/CalendarUtils;->forceCalendarRefresh()V

    .line 642
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->lastDestinationArrived:Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->setLastDestinationArrived(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 644
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$10;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$10;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    goto :goto_0
.end method

.method public onDisplayInitiatedRequestEvent(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;)V
    .locals 9
    .param p1, "event"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v6, -0x1

    .line 458
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-ne v0, v1, :cond_1

    .line 459
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDisplayInitiatedRequestEvent but we have a pending route on the phone"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 481
    :goto_0
    return-void

    .line 463
    :cond_1
    iget-object v0, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->updateLastRoutedDateInDb()I

    .line 464
    iget-object v0, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;->destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination;->refreshPlaceIdDataAndUpdateListsAsync()V

    .line 465
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removeLastDestinationArrived()V

    .line 466
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removePendingTrip()V

    .line 469
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDisplayInitiatedRequestEvent, setting state to CALCULATING_ROUTES"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 471
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 472
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    .line 473
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v1, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v4, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$DisplayInitiatedRequestEvent;->requestId:Ljava/lang/String;

    move-object v3, v2

    move-object v5, v2

    move v7, v6

    move v8, v6

    invoke-direct/range {v0 .. v8}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Lcom/here/android/mpa/common/GeoPolyline;Ljava/lang/String;Ljava/lang/String;III)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .line 475
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$4;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$4;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    goto :goto_0
.end method

.method public onHUDConnected(Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 436
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "HUD is connected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 438
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->INACTIVE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-ne v0, v1, :cond_1

    .line 439
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->navigationHelper:Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->getNavigationSessionState()V

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-ne v0, v1, :cond_0

    .line 441
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->refreshPendingRoute:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public onHUDDisconnected(Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 447
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "HUD is disconnected"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 449
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->INACTIVE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_0

    .line 450
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->stopRouting()V

    .line 452
    :cond_0
    return-void
.end method

.method public onHudReadyEvent(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$HUDReadyEvent;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$HUDReadyEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 692
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "HUD is ready"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 694
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-ne v0, v1, :cond_1

    .line 695
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "sending pending route to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 696
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->requestNewRoute(Lcom/navdy/client/app/framework/models/Destination;)V

    .line 698
    :cond_1
    return-void
.end method

.method public onLimitCellDataEvent(Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 702
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-ne v0, v1, :cond_0

    .line 703
    iget-boolean v0, p1, Lcom/navdy/client/app/ui/settings/GeneralSettingsActivity$LimitCellDataEvent;->hasLimitedCellData:Z

    if-eqz v0, :cond_1

    .line 704
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->refreshPendingRoute:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 709
    :cond_0
    :goto_0
    return-void

    .line 706
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->calculatePendingRoute(Lcom/navdy/client/app/framework/models/Destination;)V

    goto :goto_0
.end method

.method public onRerouteActiveTripEvent(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RerouteActiveTripEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RerouteActiveTripEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 620
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->EN_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_0

    .line 621
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onRerouteActiveTripEvent, state is not EN_ROUTE, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 623
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->navigationHelper:Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->getNavigationSessionState()V

    .line 625
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$9;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$9;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    .line 631
    return-void
.end method

.method public onRouteSubmitProgress(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RouteCalculationProgress;)V
    .locals 3
    .param p1, "event"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RouteCalculationProgress;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 485
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v1, v2, :cond_1

    .line 486
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onRouteSubmitProgress, state is not CALCULATING_ROUTES, no-op"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 501
    :cond_0
    :goto_0
    return-void

    .line 490
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v1, v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v1, v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RouteCalculationProgress;->requestId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 491
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onRouteSubmitProgress, requestIds do not match, no-op"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 495
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$1300(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 496
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$1;)V

    .line 497
    .local v0, "updatedRoute":Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    iget-object v1, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RouteCalculationProgress;->handle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->setHandle(Ljava/lang/String;)V

    .line 499
    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    goto :goto_0
.end method

.method public onRoutesCalculatedEvent(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculatedEvent;)V
    .locals 12
    .param p1, "event"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculatedEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 505
    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v4, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v2, v4, :cond_0

    .line 506
    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "onRoutesCalculatedEvent, state is not CALCULATING_ROUTES, no-op"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 548
    :goto_0
    return-void

    .line 510
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v2, v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v2, v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculatedEvent;->requestId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 511
    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "onRoutesCalculatedEvent, requestIds are not the same, no-op"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 514
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v1

    .line 515
    .local v1, "currentDestination":Lcom/navdy/client/app/framework/models/Destination;
    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$1300(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Ljava/lang/String;

    move-result-object v9

    .line 517
    .local v9, "cancelHandle":Ljava/lang/String;
    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "onRoutesCalculatedEvent, setting state to ROUTE_CALCULATED"

    invoke-virtual {v2, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 518
    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->ROUTE_CALCULATED:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    iput-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 519
    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NONE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    iput-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    .line 520
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .line 522
    iget-object v2, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculatedEvent;->routes:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_2
    :goto_1
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    .line 523
    .local v10, "navigationRouteResult":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    new-instance v3, Lcom/here/android/mpa/common/GeoPolyline;

    iget-object v2, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    invoke-direct {p0, v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getRouteGeo(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/here/android/mpa/common/GeoPolyline;-><init>(Ljava/util/List;)V

    .line 524
    .local v3, "routeGeo":Lcom/here/android/mpa/common/GeoPolyline;
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v2, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculatedEvent;->requestId:Ljava/lang/String;

    iget-object v5, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    iget-object v6, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    .line 530
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    .line 531
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, v10, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->length:Ljava/lang/Integer;

    .line 532
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-direct/range {v0 .. v8}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Lcom/here/android/mpa/common/GeoPolyline;Ljava/lang/String;Ljava/lang/String;III)V

    .line 535
    .local v0, "route":Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    invoke-virtual {v0, v9}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->setHandle(Ljava/lang/String;)V

    .line 537
    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    if-nez v2, :cond_2

    .line 538
    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    goto :goto_1

    .line 542
    .end local v0    # "route":Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;
    .end local v3    # "routeGeo":Lcom/here/android/mpa/common/GeoPolyline;
    .end local v10    # "navigationRouteResult":Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
    :cond_3
    new-instance v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$5;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$5;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    invoke-direct {p0, v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    goto/16 :goto_0
.end method

.method public onRoutesCalculationErrorEvent(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculationErrorEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$RoutesCalculationErrorEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 552
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-eq v0, v1, :cond_0

    .line 553
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onRoutesCalculationErrorEvent, state is not CALCULATING_ROUTES, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 567
    :goto_0
    return-void

    .line 556
    :cond_0
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onRoutesCalculationErrorEvent, setting state to CALCULATION_FAILED"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 558
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->ROUTE_CALCULATED:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 559
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;->NO_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentError:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;

    .line 561
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$6;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$6;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    goto :goto_0
.end method

.method public onStartActiveTripEvent(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;)V
    .locals 9
    .param p1, "event"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 571
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->isInOneOfThePendingTripStates()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 572
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onStartActiveTripEvent, state is not an active route state, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 616
    :goto_0
    return-void

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->routeId:Ljava/lang/String;

    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v1, v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 577
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->setEnRouteState()V

    .line 579
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$7;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$7;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    goto :goto_0

    .line 586
    :cond_1
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onStartActiveTripEvent, routeIds in current and new are not the same"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 588
    iget-object v0, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->destination:Lcom/navdy/client/app/framework/models/Destination;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    if-eqz v0, :cond_2

    .line 589
    new-instance v3, Lcom/here/android/mpa/common/GeoPolyline;

    iget-object v0, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v0, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeLatLongs:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->getRouteGeo(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/here/android/mpa/common/GeoPolyline;-><init>(Ljava/util/List;)V

    .line 591
    .local v3, "routeGeo":Lcom/here/android/mpa/common/GeoPolyline;
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v1, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v2, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v2, v2, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->via:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v5, v5, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->routeId:Ljava/lang/String;

    iget-object v6, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v6, v6, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration:Ljava/lang/Integer;

    .line 597
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v7, v7, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->duration_traffic:Ljava/lang/Integer;

    .line 598
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    iget-object v8, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StartActiveTripEvent;->route:Lcom/navdy/service/library/events/navigation/NavigationRouteResult;

    iget-object v8, v8, Lcom/navdy/service/library/events/navigation/NavigationRouteResult;->length:Ljava/lang/Integer;

    .line 599
    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-direct/range {v0 .. v8}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;-><init>(Lcom/navdy/client/app/framework/models/Destination;Ljava/lang/String;Lcom/here/android/mpa/common/GeoPolyline;Ljava/lang/String;Ljava/lang/String;III)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    .line 602
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->setEnRouteState()V

    .line 604
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$8;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$8;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    invoke-direct {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    goto :goto_0

    .line 612
    .end local v3    # "routeGeo":Lcom/here/android/mpa/common/GeoPolyline;
    :cond_2
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "trying to recover from insufficient data from the HUD"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 613
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->navigationHelper:Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->getNavigationSessionState()V

    goto :goto_0
.end method

.method public onStopActiveTripEvent(Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;)V
    .locals 8
    .param p1, "event"    # Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 654
    sget-object v5, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$15;->$SwitchMap$com$navdy$client$app$framework$navigation$NavdyRouteHandler$State:[I

    iget-object v6, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 663
    sget-object v5, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onStopActiveTripEvent.requestId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;->requestId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 664
    sget-object v5, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "currentRoute.requestId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v7, v7, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 665
    sget-object v5, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onStopActiveTripEvent.routeId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;->routeId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 666
    sget-object v5, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "currentRoute.routeId: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v7, v7, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 667
    sget-object v5, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onStopActiveTripEvent.handle: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;->handle:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 668
    sget-object v5, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "currentRoute.handle: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v7}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$1300(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 670
    iget-object v5, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;->requestId:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;->requestId:Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v6, v6, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->requestId:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v1, v3

    .line 671
    .local v1, "sameRequestIds":Z
    :goto_0
    iget-object v5, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;->routeId:Ljava/lang/String;

    if-eqz v5, :cond_2

    iget-object v5, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;->routeId:Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    iget-object v6, v6, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->routeId:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 672
    .local v2, "sameRouteIds":Z
    :goto_1
    iget-object v5, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;->handle:Ljava/lang/String;

    if-eqz v5, :cond_3

    iget-object v5, p1, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler$StopActiveTripEvent;->handle:Ljava/lang/String;

    iget-object v6, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v6}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$1300(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v0, v3

    .line 674
    .local v0, "sameHandles":Z
    :goto_2
    if-nez v1, :cond_0

    if-nez v2, :cond_0

    if-eqz v0, :cond_4

    .line 675
    :cond_0
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->setStoppedState()V

    .line 676
    new-instance v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$11;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$11;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    invoke-direct {p0, v3}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    .line 682
    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->navigationHelper:Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->getNavigationSessionState()V

    .line 688
    .end local v0    # "sameHandles":Z
    .end local v1    # "sameRequestIds":Z
    .end local v2    # "sameRouteIds":Z
    :goto_3
    return-void

    .line 656
    :pswitch_0
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "onStopActiveTripEvent, state is already INACTIVE, no-op"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_3

    .line 660
    :pswitch_1
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onStopActiveTripEvent, state can\'t be "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    invoke-virtual {v5}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_3

    :cond_1
    move v1, v4

    .line 670
    goto :goto_0

    .restart local v1    # "sameRequestIds":Z
    :cond_2
    move v2, v4

    .line 671
    goto :goto_1

    .restart local v2    # "sameRouteIds":Z
    :cond_3
    move v0, v4

    .line 672
    goto :goto_2

    .line 684
    .restart local v0    # "sameHandles":Z
    :cond_4
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "canceling different stuff, no-op"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_3

    .line 654
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public onTripUpdate(Lcom/navdy/service/library/events/TripUpdate;)V
    .locals 8
    .param p1, "tripUpdate"    # Lcom/navdy/service/library/events/TripUpdate;
    .annotation runtime Lcom/squareup/otto/Subscribe;
    .end annotation

    .prologue
    .line 105
    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v4, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->EN_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-ne v3, v4, :cond_0

    .line 106
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Received trip update: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 108
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereMapsManager;->getInstance()Lcom/navdy/client/app/framework/map/HereMapsManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/map/HereMapsManager;->isInitialized()Z

    move-result v3

    if-nez v3, :cond_1

    .line 109
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Here Map Engine not initialized yet."

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    iget-object v0, p1, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 115
    .local v0, "currentLatLng":Lcom/navdy/service/library/events/location/LatLong;
    if-eqz v0, :cond_0

    .line 116
    const/4 v2, -0x1

    .line 117
    .local v2, "tta":I
    const/4 v1, -0x1

    .line 119
    .local v1, "distance":I
    iget-object v3, p1, Lcom/navdy/service/library/events/TripUpdate;->estimated_time_remaining:Ljava/lang/Integer;

    if-eqz v3, :cond_2

    .line 120
    iget-object v3, p1, Lcom/navdy/service/library/events/TripUpdate;->estimated_time_remaining:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 123
    :cond_2
    iget-object v3, p1, Lcom/navdy/service/library/events/TripUpdate;->distance_to_destination:Ljava/lang/Integer;

    if-eqz v3, :cond_3

    .line 124
    iget-object v3, p1, Lcom/navdy/service/library/events/TripUpdate;->distance_to_destination:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 127
    :cond_3
    new-instance v3, Lcom/here/android/mpa/common/GeoCoordinate;

    iget-object v4, v0, Lcom/navdy/service/library/events/location/LatLong;->latitude:Ljava/lang/Double;

    .line 128
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v6, v0, Lcom/navdy/service/library/events/location/LatLong;->longitude:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/here/android/mpa/common/GeoCoordinate;-><init>(DD)V

    .line 127
    invoke-direct {p0, v3, v2, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->updateCurrentProgress(Lcom/here/android/mpa/common/GeoCoordinate;II)V

    goto :goto_0
.end method

.method public removeListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;)V
    .locals 3
    .param p1, "navdyRouteListener"    # Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;

    .prologue
    .line 234
    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->listeners:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    .line 237
    .local v0, "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Ljava/lang/ref/WeakReference<Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;>;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 238
    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;

    .line 240
    .local v1, "listener":Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;
    if-eqz v1, :cond_1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 241
    :cond_1
    invoke-interface {v0}, Ljava/util/ListIterator;->remove()V

    goto :goto_0

    .line 244
    .end local v1    # "listener":Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteListener;
    :cond_2
    return-void
.end method

.method public requestNewRoute(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 1
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 258
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->requestNewRoute(Lcom/navdy/client/app/framework/models/Destination;Z)V

    .line 259
    return-void
.end method

.method public requestNewRoute(Lcom/navdy/client/app/framework/models/Destination;Z)V
    .locals 6
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "initiatedOnHud"    # Z

    .prologue
    .line 262
    if-nez p1, :cond_0

    .line 263
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "attempted to call requestNewRoute with no destination, no-op"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 370
    :goto_0
    return-void

    .line 268
    :cond_0
    if-nez p2, :cond_1

    iget-boolean v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->requestedHighAccuracy:Z

    if-nez v3, :cond_1

    .line 270
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "location_mode"

    invoke-static {v3, v4}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I

    move-result v2

    .line 271
    .local v2, "locationMode":I
    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    .line 272
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->requestedHighAccuracy:Z

    .line 274
    new-instance v1, Landroid/content/Intent;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/navdy/client/app/ui/settings/LocationDialogActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 275
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 276
    const-string v3, "pending_destination"

    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 278
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 281
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "locationMode":I
    :catch_0
    move-exception v0

    .line 282
    .local v0, "e":Landroid/provider/Settings$SettingNotFoundException;
    sget-object v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "location setting not found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 287
    .end local v0    # "e":Landroid/provider/Settings$SettingNotFoundException;
    :cond_1
    iget-object v3, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->handler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->refreshPendingRoute:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 290
    new-instance v3, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;

    invoke-direct {v3, p0, p1, p2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$2;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;Lcom/navdy/client/app/framework/models/Destination;Z)V

    invoke-virtual {p1, v3}, Lcom/navdy/client/app/framework/models/Destination;->refreshPlaceIdDataAndUpdateListsAsync(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public retryRoute()V
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    if-nez v0, :cond_0

    .line 377
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "retryRoute but currentRoute is null, no-op"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 382
    :goto_0
    return-void

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$200(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Lcom/navdy/client/app/framework/models/Destination;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->requestNewRoute(Lcom/navdy/client/app/framework/models/Destination;)V

    goto :goto_0
.end method

.method public stopRouting()V
    .locals 3

    .prologue
    .line 388
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->INACTIVE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    if-ne v1, v2, :cond_0

    .line 389
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "called stopRouting when state is INACTIVE, no-op"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 430
    :goto_0
    return-void

    .line 393
    :cond_0
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$15;->$SwitchMap$com$navdy$client$app$framework$navigation$NavdyRouteHandler$State:[I

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentState:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 421
    :goto_1
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->removePendingTrip()V

    .line 422
    invoke-direct {p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->setStoppedState()V

    .line 424
    new-instance v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$3;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$3;-><init>(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;)V

    invoke-direct {p0, v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->callListeners(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$CallListener;)V

    goto :goto_0

    .line 395
    :pswitch_0
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->pendingRouteHandle:Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;

    if-eqz v1, :cond_1

    .line 396
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->pendingRouteHandle:Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;->cancel()V

    .line 398
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->pendingRouteHandle:Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;

    goto :goto_1

    .line 401
    :pswitch_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->refreshPendingRoute:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 404
    :pswitch_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$1300(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Ljava/lang/String;

    move-result-object v0

    .line 406
    .local v0, "cancelHandle":Ljava/lang/String;
    if-nez v0, :cond_2

    .line 407
    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "stopRouting, cancel handle can\'t be null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 411
    :goto_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->navigationHelper:Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->cancelRoute(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    goto :goto_1

    .line 409
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->navigationHelper:Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-static {v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->access$1300(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->cancelRouteCalculation(Ljava/lang/String;)V

    goto :goto_2

    .line 415
    .end local v0    # "cancelHandle":Ljava/lang/String;
    :pswitch_3
    iget-object v1, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->navigationHelper:Lcom/navdy/client/app/framework/navigation/NavigationHelper;

    iget-object v2, p0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->currentRoute:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-virtual {v1, v2}, Lcom/navdy/client/app/framework/navigation/NavigationHelper;->cancelRoute(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V

    goto :goto_1

    .line 393
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method
