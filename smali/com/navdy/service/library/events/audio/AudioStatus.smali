.class public final Lcom/navdy/service/library/events/audio/AudioStatus;
.super Lcom/squareup/wire/Message;
.source "AudioStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;,
        Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;,
        Lcom/navdy/service/library/events/audio/AudioStatus$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CONNECTIONTYPE:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

.field public static final DEFAULT_HASSCOCONNECTION:Ljava/lang/Boolean;

.field public static final DEFAULT_ISCONNECTED:Ljava/lang/Boolean;

.field public static final DEFAULT_ISPLAYING:Ljava/lang/Boolean;

.field public static final DEFAULT_PROFILETYPE:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

.field private static final serialVersionUID:J


# instance fields
.field public final connectionType:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final hasSCOConnection:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final isConnected:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final isPlaying:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 15
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus;->DEFAULT_ISCONNECTED:Ljava/lang/Boolean;

    .line 16
    sget-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->AUDIO_CONNECTION_NONE:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus;->DEFAULT_CONNECTIONTYPE:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    .line 17
    sget-object v0, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->AUDIO_PROFILE_NONE:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus;->DEFAULT_PROFILETYPE:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    .line 18
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus;->DEFAULT_ISPLAYING:Ljava/lang/Boolean;

    .line 19
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/AudioStatus;->DEFAULT_HASSCOCONNECTION:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/AudioStatus$Builder;)V
    .locals 6
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/AudioStatus$Builder;

    .prologue
    .line 60
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->isConnected:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->connectionType:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    iget-object v3, p1, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    iget-object v4, p1, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->isPlaying:Ljava/lang/Boolean;

    iget-object v5, p1, Lcom/navdy/service/library/events/audio/AudioStatus$Builder;->hasSCOConnection:Ljava/lang/Boolean;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/audio/AudioStatus;-><init>(Ljava/lang/Boolean;Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 61
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/AudioStatus;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 62
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/AudioStatus$Builder;Lcom/navdy/service/library/events/audio/AudioStatus$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/AudioStatus$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/AudioStatus$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/AudioStatus;-><init>(Lcom/navdy/service/library/events/audio/AudioStatus$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "isConnected"    # Ljava/lang/Boolean;
    .param p2, "connectionType"    # Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;
    .param p3, "profileType"    # Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;
    .param p4, "isPlaying"    # Ljava/lang/Boolean;
    .param p5, "hasSCOConnection"    # Ljava/lang/Boolean;

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->isConnected:Ljava/lang/Boolean;

    .line 53
    iput-object p2, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->connectionType:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    .line 54
    iput-object p3, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    .line 55
    iput-object p4, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->isPlaying:Ljava/lang/Boolean;

    .line 56
    iput-object p5, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->hasSCOConnection:Ljava/lang/Boolean;

    .line 57
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    if-ne p1, p0, :cond_1

    .line 73
    :cond_0
    :goto_0
    return v1

    .line 67
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/audio/AudioStatus;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 68
    check-cast v0, Lcom/navdy/service/library/events/audio/AudioStatus;

    .line 69
    .local v0, "o":Lcom/navdy/service/library/events/audio/AudioStatus;
    iget-object v3, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->isConnected:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/AudioStatus;->isConnected:Ljava/lang/Boolean;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/AudioStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->connectionType:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/AudioStatus;->connectionType:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    .line 70
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/AudioStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/AudioStatus;->profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    .line 71
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/AudioStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->isPlaying:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/AudioStatus;->isPlaying:Ljava/lang/Boolean;

    .line 72
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/AudioStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->hasSCOConnection:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/audio/AudioStatus;->hasSCOConnection:Ljava/lang/Boolean;

    .line 73
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/audio/AudioStatus;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 78
    iget v0, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->hashCode:I

    .line 79
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 80
    iget-object v2, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->isConnected:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->isConnected:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    .line 81
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->connectionType:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->connectionType:Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/AudioStatus$ConnectionType;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 82
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->profileType:Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/audio/AudioStatus$ProfileType;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 83
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->isPlaying:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->isPlaying:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 84
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->hasSCOConnection:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->hasSCOConnection:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 85
    iput v0, p0, Lcom/navdy/service/library/events/audio/AudioStatus;->hashCode:I

    .line 87
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 80
    goto :goto_0

    :cond_3
    move v2, v1

    .line 81
    goto :goto_1

    :cond_4
    move v2, v1

    .line 82
    goto :goto_2

    :cond_5
    move v2, v1

    .line 83
    goto :goto_3
.end method
