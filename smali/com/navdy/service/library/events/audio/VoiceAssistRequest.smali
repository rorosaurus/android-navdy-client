.class public final Lcom/navdy/service/library/events/audio/VoiceAssistRequest;
.super Lcom/squareup/wire/Message;
.source "VoiceAssistRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_END:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final end:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;->DEFAULT_END:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;

    .prologue
    .line 31
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;->end:Ljava/lang/Boolean;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;-><init>(Ljava/lang/Boolean;)V

    .line 32
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 33
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;Lcom/navdy/service/library/events/audio/VoiceAssistRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/audio/VoiceAssistRequest$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;-><init>(Lcom/navdy/service/library/events/audio/VoiceAssistRequest$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "end"    # Ljava/lang/Boolean;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;->end:Ljava/lang/Boolean;

    .line 28
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 37
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 39
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 38
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;->end:Ljava/lang/Boolean;

    check-cast p1, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;->end:Ljava/lang/Boolean;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 44
    iget v0, p0, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;->hashCode:I

    .line 45
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;->end:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;->end:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/audio/VoiceAssistRequest;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
