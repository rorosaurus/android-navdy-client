.class public final Lcom/navdy/service/library/events/Capabilities;
.super Lcom/squareup/wire/Message;
.source "Capabilities.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/Capabilities$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CANNEDRESPONSETOSMS:Ljava/lang/Boolean;

.field public static final DEFAULT_COMPACTUI:Ljava/lang/Boolean;

.field public static final DEFAULT_CUSTOMDIALLONGPRESS:Ljava/lang/Boolean;

.field public static final DEFAULT_LOCALMUSICBROWSER:Ljava/lang/Boolean;

.field public static final DEFAULT_MUSICARTWORKCACHE:Ljava/lang/Boolean;

.field public static final DEFAULT_NAVCOORDSLOOKUP:Ljava/lang/Boolean;

.field public static final DEFAULT_PLACETYPESEARCH:Ljava/lang/Boolean;

.field public static final DEFAULT_SEARCHRESULTLIST:Ljava/lang/Boolean;

.field public static final DEFAULT_SUPPORTSPHONETICTTS:Ljava/lang/Boolean;

.field public static final DEFAULT_VOICESEARCH:Ljava/lang/Boolean;

.field public static final DEFAULT_VOICESEARCHNEWIOSPAUSEBEHAVIORS:Ljava/lang/Boolean;

.field private static final serialVersionUID:J


# instance fields
.field public final cannedResponseToSms:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final compactUi:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final customDialLongPress:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xa
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final localMusicBrowser:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final musicArtworkCache:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final navCoordsLookup:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final placeTypeSearch:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final searchResultList:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final supportsPhoneticTts:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xb
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final voiceSearch:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 13
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Capabilities;->DEFAULT_COMPACTUI:Ljava/lang/Boolean;

    .line 14
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Capabilities;->DEFAULT_PLACETYPESEARCH:Ljava/lang/Boolean;

    .line 15
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Capabilities;->DEFAULT_VOICESEARCH:Ljava/lang/Boolean;

    .line 16
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Capabilities;->DEFAULT_LOCALMUSICBROWSER:Ljava/lang/Boolean;

    .line 17
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Capabilities;->DEFAULT_NAVCOORDSLOOKUP:Ljava/lang/Boolean;

    .line 18
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Capabilities;->DEFAULT_SEARCHRESULTLIST:Ljava/lang/Boolean;

    .line 19
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Capabilities;->DEFAULT_MUSICARTWORKCACHE:Ljava/lang/Boolean;

    .line 20
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Capabilities;->DEFAULT_VOICESEARCHNEWIOSPAUSEBEHAVIORS:Ljava/lang/Boolean;

    .line 21
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Capabilities;->DEFAULT_CANNEDRESPONSETOSMS:Ljava/lang/Boolean;

    .line 22
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Capabilities;->DEFAULT_CUSTOMDIALLONGPRESS:Ljava/lang/Boolean;

    .line 23
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Capabilities;->DEFAULT_SUPPORTSPHONETICTTS:Ljava/lang/Boolean;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/Capabilities$Builder;)V
    .locals 12
    .param p1, "builder"    # Lcom/navdy/service/library/events/Capabilities$Builder;

    .prologue
    .line 124
    iget-object v1, p1, Lcom/navdy/service/library/events/Capabilities$Builder;->compactUi:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/navdy/service/library/events/Capabilities$Builder;->placeTypeSearch:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/navdy/service/library/events/Capabilities$Builder;->voiceSearch:Ljava/lang/Boolean;

    iget-object v4, p1, Lcom/navdy/service/library/events/Capabilities$Builder;->localMusicBrowser:Ljava/lang/Boolean;

    iget-object v5, p1, Lcom/navdy/service/library/events/Capabilities$Builder;->navCoordsLookup:Ljava/lang/Boolean;

    iget-object v6, p1, Lcom/navdy/service/library/events/Capabilities$Builder;->searchResultList:Ljava/lang/Boolean;

    iget-object v7, p1, Lcom/navdy/service/library/events/Capabilities$Builder;->musicArtworkCache:Ljava/lang/Boolean;

    iget-object v8, p1, Lcom/navdy/service/library/events/Capabilities$Builder;->voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;

    iget-object v9, p1, Lcom/navdy/service/library/events/Capabilities$Builder;->cannedResponseToSms:Ljava/lang/Boolean;

    iget-object v10, p1, Lcom/navdy/service/library/events/Capabilities$Builder;->customDialLongPress:Ljava/lang/Boolean;

    iget-object v11, p1, Lcom/navdy/service/library/events/Capabilities$Builder;->supportsPhoneticTts:Ljava/lang/Boolean;

    move-object v0, p0

    invoke-direct/range {v0 .. v11}, Lcom/navdy/service/library/events/Capabilities;-><init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 125
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/Capabilities;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 126
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/Capabilities$Builder;Lcom/navdy/service/library/events/Capabilities$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/Capabilities$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/Capabilities$1;

    .prologue
    .line 10
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/Capabilities;-><init>(Lcom/navdy/service/library/events/Capabilities$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0
    .param p1, "compactUi"    # Ljava/lang/Boolean;
    .param p2, "placeTypeSearch"    # Ljava/lang/Boolean;
    .param p3, "voiceSearch"    # Ljava/lang/Boolean;
    .param p4, "localMusicBrowser"    # Ljava/lang/Boolean;
    .param p5, "navCoordsLookup"    # Ljava/lang/Boolean;
    .param p6, "searchResultList"    # Ljava/lang/Boolean;
    .param p7, "musicArtworkCache"    # Ljava/lang/Boolean;
    .param p8, "voiceSearchNewIOSPauseBehaviors"    # Ljava/lang/Boolean;
    .param p9, "cannedResponseToSms"    # Ljava/lang/Boolean;
    .param p10, "customDialLongPress"    # Ljava/lang/Boolean;
    .param p11, "supportsPhoneticTts"    # Ljava/lang/Boolean;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/navdy/service/library/events/Capabilities;->compactUi:Ljava/lang/Boolean;

    .line 111
    iput-object p2, p0, Lcom/navdy/service/library/events/Capabilities;->placeTypeSearch:Ljava/lang/Boolean;

    .line 112
    iput-object p3, p0, Lcom/navdy/service/library/events/Capabilities;->voiceSearch:Ljava/lang/Boolean;

    .line 113
    iput-object p4, p0, Lcom/navdy/service/library/events/Capabilities;->localMusicBrowser:Ljava/lang/Boolean;

    .line 114
    iput-object p5, p0, Lcom/navdy/service/library/events/Capabilities;->navCoordsLookup:Ljava/lang/Boolean;

    .line 115
    iput-object p6, p0, Lcom/navdy/service/library/events/Capabilities;->searchResultList:Ljava/lang/Boolean;

    .line 116
    iput-object p7, p0, Lcom/navdy/service/library/events/Capabilities;->musicArtworkCache:Ljava/lang/Boolean;

    .line 117
    iput-object p8, p0, Lcom/navdy/service/library/events/Capabilities;->voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;

    .line 118
    iput-object p9, p0, Lcom/navdy/service/library/events/Capabilities;->cannedResponseToSms:Ljava/lang/Boolean;

    .line 119
    iput-object p10, p0, Lcom/navdy/service/library/events/Capabilities;->customDialLongPress:Ljava/lang/Boolean;

    .line 120
    iput-object p11, p0, Lcom/navdy/service/library/events/Capabilities;->supportsPhoneticTts:Ljava/lang/Boolean;

    .line 121
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 130
    if-ne p1, p0, :cond_1

    .line 143
    :cond_0
    :goto_0
    return v1

    .line 131
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/Capabilities;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 132
    check-cast v0, Lcom/navdy/service/library/events/Capabilities;

    .line 133
    .local v0, "o":Lcom/navdy/service/library/events/Capabilities;
    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->compactUi:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/Capabilities;->compactUi:Ljava/lang/Boolean;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Capabilities;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->placeTypeSearch:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/Capabilities;->placeTypeSearch:Ljava/lang/Boolean;

    .line 134
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Capabilities;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->voiceSearch:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/Capabilities;->voiceSearch:Ljava/lang/Boolean;

    .line 135
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Capabilities;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->localMusicBrowser:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/Capabilities;->localMusicBrowser:Ljava/lang/Boolean;

    .line 136
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Capabilities;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->navCoordsLookup:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/Capabilities;->navCoordsLookup:Ljava/lang/Boolean;

    .line 137
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Capabilities;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->searchResultList:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/Capabilities;->searchResultList:Ljava/lang/Boolean;

    .line 138
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Capabilities;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->musicArtworkCache:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/Capabilities;->musicArtworkCache:Ljava/lang/Boolean;

    .line 139
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Capabilities;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/Capabilities;->voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;

    .line 140
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Capabilities;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->cannedResponseToSms:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/Capabilities;->cannedResponseToSms:Ljava/lang/Boolean;

    .line 141
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Capabilities;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->customDialLongPress:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/Capabilities;->customDialLongPress:Ljava/lang/Boolean;

    .line 142
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Capabilities;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->supportsPhoneticTts:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/Capabilities;->supportsPhoneticTts:Ljava/lang/Boolean;

    .line 143
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Capabilities;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 148
    iget v0, p0, Lcom/navdy/service/library/events/Capabilities;->hashCode:I

    .line 149
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 150
    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->compactUi:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->compactUi:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    .line 151
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->placeTypeSearch:Ljava/lang/Boolean;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->placeTypeSearch:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 152
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->voiceSearch:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->voiceSearch:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 153
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->localMusicBrowser:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->localMusicBrowser:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 154
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->navCoordsLookup:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->navCoordsLookup:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 155
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->searchResultList:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->searchResultList:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 156
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->musicArtworkCache:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->musicArtworkCache:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 157
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->voiceSearchNewIOSPauseBehaviors:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v3, v2

    .line 158
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->cannedResponseToSms:Ljava/lang/Boolean;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->cannedResponseToSms:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_8
    add-int v0, v3, v2

    .line 159
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->customDialLongPress:Ljava/lang/Boolean;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/navdy/service/library/events/Capabilities;->customDialLongPress:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_9
    add-int v0, v3, v2

    .line 160
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/Capabilities;->supportsPhoneticTts:Ljava/lang/Boolean;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/Capabilities;->supportsPhoneticTts:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 161
    iput v0, p0, Lcom/navdy/service/library/events/Capabilities;->hashCode:I

    .line 163
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 150
    goto/16 :goto_0

    :cond_3
    move v2, v1

    .line 151
    goto/16 :goto_1

    :cond_4
    move v2, v1

    .line 152
    goto :goto_2

    :cond_5
    move v2, v1

    .line 153
    goto :goto_3

    :cond_6
    move v2, v1

    .line 154
    goto :goto_4

    :cond_7
    move v2, v1

    .line 155
    goto :goto_5

    :cond_8
    move v2, v1

    .line 156
    goto :goto_6

    :cond_9
    move v2, v1

    .line 157
    goto :goto_7

    :cond_a
    move v2, v1

    .line 158
    goto :goto_8

    :cond_b
    move v2, v1

    .line 159
    goto :goto_9
.end method
