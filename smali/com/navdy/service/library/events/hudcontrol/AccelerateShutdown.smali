.class public final Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;
.super Lcom/squareup/wire/Message;
.source "AccelerateShutdown.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;,
        Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_REASON:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

.field private static final serialVersionUID:J


# instance fields
.field public final reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->ACCELERATE_REASON_UNKNOWN:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    sput-object v0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->DEFAULT_REASON:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;)V
    .locals 0
    .param p1, "reason"    # Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    .line 25
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;

    .prologue
    .line 28
    iget-object v0, p1, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;->reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;-><init>(Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;)V

    .line 29
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;-><init>(Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    .line 34
    if-ne p1, p0, :cond_0

    const/4 v0, 0x1

    .line 36
    .end local p1    # "other":Ljava/lang/Object;
    :goto_0
    return v0

    .line 35
    .restart local p1    # "other":Ljava/lang/Object;
    :cond_0
    instance-of v0, p1, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    check-cast p1, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;

    .end local p1    # "other":Ljava/lang/Object;
    iget-object v1, p1, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    invoke-virtual {p0, v0, v1}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 41
    iget v0, p0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->hashCode:I

    .line 42
    .local v0, "result":I
    if-eqz v0, :cond_0

    .end local v0    # "result":I
    :goto_0
    return v0

    .restart local v0    # "result":I
    :cond_0
    iget-object v1, p0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->reason:Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown$AccelerateReason;->hashCode()I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/navdy/service/library/events/hudcontrol/AccelerateShutdown;->hashCode:I

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
