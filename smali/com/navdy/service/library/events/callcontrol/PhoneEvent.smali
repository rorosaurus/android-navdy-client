.class public final Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
.super Lcom/squareup/wire/Message;
.source "PhoneEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CALLUUID:Ljava/lang/String; = ""

.field public static final DEFAULT_CONTACT_NAME:Ljava/lang/String; = ""

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_NUMBER:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

.field private static final serialVersionUID:J


# instance fields
.field public final callUUID:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final contact_name:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final number:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    sput-object v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->DEFAULT_STATUS:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;)V
    .locals 6
    .param p1, "builder"    # Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    .prologue
    .line 68
    iget-object v1, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    iget-object v2, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->number:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->contact_name:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->label:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->callUUID:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 70
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;Lcom/navdy/service/library/events/callcontrol/PhoneEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/callcontrol/PhoneEvent$1;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;-><init>(Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;)V

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/callcontrol/PhoneStatus;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "contact_name"    # Ljava/lang/String;
    .param p4, "label"    # Ljava/lang/String;
    .param p5, "callUUID"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 61
    iput-object p2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    .line 62
    iput-object p3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->contact_name:Ljava/lang/String;

    .line 63
    iput-object p4, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->label:Ljava/lang/String;

    .line 64
    iput-object p5, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    .line 65
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 74
    if-ne p1, p0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v1

    .line 75
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 76
    check-cast v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    .line 77
    .local v0, "o":Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    .line 78
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->contact_name:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->contact_name:Ljava/lang/String;

    .line 79
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->label:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->label:Ljava/lang/String;

    .line 80
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    .line 81
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 86
    iget v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->hashCode:I

    .line 87
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 88
    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->status:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->hashCode()I

    move-result v0

    .line 89
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->number:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 90
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->contact_name:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->contact_name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 91
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->label:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->label:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 92
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->callUUID:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 93
    iput v0, p0, Lcom/navdy/service/library/events/callcontrol/PhoneEvent;->hashCode:I

    .line 95
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 88
    goto :goto_0

    :cond_3
    move v2, v1

    .line 89
    goto :goto_1

    :cond_4
    move v2, v1

    .line 90
    goto :goto_2

    :cond_5
    move v2, v1

    .line 91
    goto :goto_3
.end method
