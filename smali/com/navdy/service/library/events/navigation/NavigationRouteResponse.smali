.class public final Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
.super Lcom/squareup/wire/Message;
.source "NavigationRouteResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CONSIDEREDTRAFFIC:Ljava/lang/Boolean;

.field public static final DEFAULT_LABEL:Ljava/lang/String; = ""

.field public static final DEFAULT_REQUESTID:Ljava/lang/String; = ""

.field public static final DEFAULT_RESULTS:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final DEFAULT_STATUSDETAIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final consideredTraffic:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final destination:Lcom/navdy/service/library/events/location/Coordinate;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
    .end annotation
.end field

.field public final label:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final requestId:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final results:Ljava/util/List;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REPEATED:Lcom/squareup/wire/Message$Label;
        messageType = Lcom/navdy/service/library/events/navigation/NavigationRouteResult;
        tag = 0x5
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;"
        }
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final statusDetail:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    .line 24
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->DEFAULT_RESULTS:Ljava/util/List;

    .line 25
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->DEFAULT_CONSIDEREDTRAFFIC:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 1
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "statusDetail"    # Ljava/lang/String;
    .param p3, "destination"    # Lcom/navdy/service/library/events/location/Coordinate;
    .param p4, "label"    # Ljava/lang/String;
    .param p6, "consideredTraffic"    # Ljava/lang/Boolean;
    .param p7, "requestId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/events/RequestStatus;",
            "Ljava/lang/String;",
            "Lcom/navdy/service/library/events/location/Coordinate;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/navigation/NavigationRouteResult;",
            ">;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    .local p5, "results":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/navigation/NavigationRouteResult;>;"
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 71
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 72
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->statusDetail:Ljava/lang/String;

    .line 73
    iput-object p3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    .line 74
    iput-object p4, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->label:Ljava/lang/String;

    .line 75
    invoke-static {p5}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->immutableCopyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    .line 76
    iput-object p6, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->consideredTraffic:Ljava/lang/Boolean;

    .line 77
    iput-object p7, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->requestId:Ljava/lang/String;

    .line 78
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;)V
    .locals 8
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;

    .prologue
    .line 81
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->statusDetail:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->label:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->results:Ljava/util/List;

    iget-object v6, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->consideredTraffic:Ljava/lang/Boolean;

    iget-object v7, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;->requestId:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Lcom/navdy/service/library/events/location/Coordinate;Ljava/lang/String;Ljava/util/List;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 82
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 83
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;-><init>(Lcom/navdy/service/library/events/navigation/NavigationRouteResponse$Builder;)V

    return-void
.end method

.method static synthetic access$000(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 18
    invoke-static {p0}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->copyOf(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    if-ne p1, p0, :cond_1

    .line 96
    :cond_0
    :goto_0
    return v1

    .line 88
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 89
    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;

    .line 90
    .local v0, "o":Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->statusDetail:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->statusDetail:Ljava/lang/String;

    .line 91
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    .line 92
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->label:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->label:Ljava/lang/String;

    .line 93
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    .line 94
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->equals(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->consideredTraffic:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->consideredTraffic:Ljava/lang/Boolean;

    .line 95
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->requestId:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->requestId:Ljava/lang/String;

    .line 96
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 101
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->hashCode:I

    .line 102
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 103
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v0

    .line 104
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->statusDetail:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->statusDetail:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 105
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->destination:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/location/Coordinate;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 106
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->label:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->label:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 107
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->results:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 108
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->consideredTraffic:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->consideredTraffic:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 109
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->requestId:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->requestId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 110
    iput v0, p0, Lcom/navdy/service/library/events/navigation/NavigationRouteResponse;->hashCode:I

    .line 112
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 103
    goto :goto_0

    :cond_3
    move v2, v1

    .line 104
    goto :goto_1

    :cond_4
    move v2, v1

    .line 105
    goto :goto_2

    :cond_5
    move v2, v1

    .line 106
    goto :goto_3

    .line 107
    :cond_6
    const/4 v2, 0x1

    goto :goto_4

    :cond_7
    move v2, v1

    .line 108
    goto :goto_5
.end method
