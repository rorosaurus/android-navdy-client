.class public final Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;
.super Lcom/squareup/wire/Message;
.source "NavigationManeuverEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CURRENTROAD:Ljava/lang/String; = ""

.field public static final DEFAULT_DISTANCE_TO_PENDING_TURN:Ljava/lang/String; = ""

.field public static final DEFAULT_ETA:Ljava/lang/String; = ""

.field public static final DEFAULT_ETAUTCTIME:Ljava/lang/Long;

.field public static final DEFAULT_NAVIGATIONSTATE:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

.field public static final DEFAULT_PENDING_STREET:Ljava/lang/String; = ""

.field public static final DEFAULT_PENDING_TURN:Lcom/navdy/service/library/events/navigation/NavigationTurn;

.field public static final DEFAULT_SPEED:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final currentRoad:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final distance_to_pending_turn:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final eta:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final etaUtcTime:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final navigationState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final pending_street:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final pending_turn:Lcom/navdy/service/library/events/navigation/NavigationTurn;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final speed:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationTurn;->NAV_TURN_START:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->DEFAULT_PENDING_TURN:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 22
    sget-object v0, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->NAV_SESSION_ENGINE_NOT_READY:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->DEFAULT_NAVIGATIONSTATE:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 23
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->DEFAULT_ETAUTCTIME:Ljava/lang/Long;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;)V
    .locals 9
    .param p1, "builder"    # Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;

    .prologue
    .line 85
    iget-object v1, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->currentRoad:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->pending_turn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    iget-object v3, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->distance_to_pending_turn:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->pending_street:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->eta:Ljava/lang/String;

    iget-object v6, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->speed:Ljava/lang/String;

    iget-object v7, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->navigationState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iget-object v8, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;->etaUtcTime:Ljava/lang/Long;

    move-object v0, p0

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;-><init>(Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationTurn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/Long;)V

    .line 86
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 87
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;-><init>(Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationTurn;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/navdy/service/library/events/navigation/NavigationSessionState;Ljava/lang/Long;)V
    .locals 0
    .param p1, "currentRoad"    # Ljava/lang/String;
    .param p2, "pending_turn"    # Lcom/navdy/service/library/events/navigation/NavigationTurn;
    .param p3, "distance_to_pending_turn"    # Ljava/lang/String;
    .param p4, "pending_street"    # Ljava/lang/String;
    .param p5, "eta"    # Ljava/lang/String;
    .param p6, "speed"    # Ljava/lang/String;
    .param p7, "navigationState"    # Lcom/navdy/service/library/events/navigation/NavigationSessionState;
    .param p8, "etaUtcTime"    # Ljava/lang/Long;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->currentRoad:Ljava/lang/String;

    .line 75
    iput-object p2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_turn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 76
    iput-object p3, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->distance_to_pending_turn:Ljava/lang/String;

    .line 77
    iput-object p4, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_street:Ljava/lang/String;

    .line 78
    iput-object p5, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->eta:Ljava/lang/String;

    .line 79
    iput-object p6, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->speed:Ljava/lang/String;

    .line 80
    iput-object p7, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->navigationState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 81
    iput-object p8, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->etaUtcTime:Ljava/lang/Long;

    .line 82
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 91
    if-ne p1, p0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v1

    .line 92
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 93
    check-cast v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;

    .line 94
    .local v0, "o":Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;
    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->currentRoad:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->currentRoad:Ljava/lang/String;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_turn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_turn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    .line 95
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->distance_to_pending_turn:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->distance_to_pending_turn:Ljava/lang/String;

    .line 96
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_street:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_street:Ljava/lang/String;

    .line 97
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->eta:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->eta:Ljava/lang/String;

    .line 98
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->speed:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->speed:Ljava/lang/String;

    .line 99
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->navigationState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->navigationState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    .line 100
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->etaUtcTime:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->etaUtcTime:Ljava/lang/Long;

    .line 101
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 106
    iget v0, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->hashCode:I

    .line 107
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 108
    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->currentRoad:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->currentRoad:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 109
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_turn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_turn:Lcom/navdy/service/library/events/navigation/NavigationTurn;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/NavigationTurn;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 110
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->distance_to_pending_turn:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->distance_to_pending_turn:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 111
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_street:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->pending_street:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 112
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->eta:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->eta:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 113
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->speed:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->speed:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 114
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->navigationState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->navigationState:Lcom/navdy/service/library/events/navigation/NavigationSessionState;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/navigation/NavigationSessionState;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 115
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->etaUtcTime:Ljava/lang/Long;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->etaUtcTime:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 116
    iput v0, p0, Lcom/navdy/service/library/events/navigation/NavigationManeuverEvent;->hashCode:I

    .line 118
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 108
    goto :goto_0

    :cond_3
    move v2, v1

    .line 109
    goto :goto_1

    :cond_4
    move v2, v1

    .line 110
    goto :goto_2

    :cond_5
    move v2, v1

    .line 111
    goto :goto_3

    :cond_6
    move v2, v1

    .line 112
    goto :goto_4

    :cond_7
    move v2, v1

    .line 113
    goto :goto_5

    :cond_8
    move v2, v1

    .line 114
    goto :goto_6
.end method
