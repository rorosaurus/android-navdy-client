.class public final Lcom/navdy/service/library/events/Notification;
.super Lcom/squareup/wire/Message;
.source "Notification.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/Notification$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_MESSAGE_DATA:Ljava/lang/String; = ""

.field public static final DEFAULT_ORIGIN:Ljava/lang/String; = ""

.field public static final DEFAULT_SENDER:Ljava/lang/String; = ""

.field public static final DEFAULT_SMS_CLASS:Ljava/lang/Integer;

.field public static final DEFAULT_TARGET:Ljava/lang/String; = ""

.field public static final DEFAULT_TYPE:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final message_data:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x5
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final origin:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final sender:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final sms_class:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final target:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final type:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/Notification;->DEFAULT_SMS_CLASS:Ljava/lang/Integer;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/Notification$Builder;)V
    .locals 7
    .param p1, "builder"    # Lcom/navdy/service/library/events/Notification$Builder;

    .prologue
    .line 50
    iget-object v1, p1, Lcom/navdy/service/library/events/Notification$Builder;->sms_class:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/navdy/service/library/events/Notification$Builder;->origin:Ljava/lang/String;

    iget-object v3, p1, Lcom/navdy/service/library/events/Notification$Builder;->sender:Ljava/lang/String;

    iget-object v4, p1, Lcom/navdy/service/library/events/Notification$Builder;->target:Ljava/lang/String;

    iget-object v5, p1, Lcom/navdy/service/library/events/Notification$Builder;->message_data:Ljava/lang/String;

    iget-object v6, p1, Lcom/navdy/service/library/events/Notification$Builder;->type:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/service/library/events/Notification;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/Notification;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 52
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/Notification$Builder;Lcom/navdy/service/library/events/Notification$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/Notification$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/Notification$1;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/Notification;-><init>(Lcom/navdy/service/library/events/Notification$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "sms_class"    # Ljava/lang/Integer;
    .param p2, "origin"    # Ljava/lang/String;
    .param p3, "sender"    # Ljava/lang/String;
    .param p4, "target"    # Ljava/lang/String;
    .param p5, "message_data"    # Ljava/lang/String;
    .param p6, "type"    # Ljava/lang/String;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/navdy/service/library/events/Notification;->sms_class:Ljava/lang/Integer;

    .line 42
    iput-object p2, p0, Lcom/navdy/service/library/events/Notification;->origin:Ljava/lang/String;

    .line 43
    iput-object p3, p0, Lcom/navdy/service/library/events/Notification;->sender:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Lcom/navdy/service/library/events/Notification;->target:Ljava/lang/String;

    .line 45
    iput-object p5, p0, Lcom/navdy/service/library/events/Notification;->message_data:Ljava/lang/String;

    .line 46
    iput-object p6, p0, Lcom/navdy/service/library/events/Notification;->type:Ljava/lang/String;

    .line 47
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    if-ne p1, p0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/Notification;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 58
    check-cast v0, Lcom/navdy/service/library/events/Notification;

    .line 59
    .local v0, "o":Lcom/navdy/service/library/events/Notification;
    iget-object v3, p0, Lcom/navdy/service/library/events/Notification;->sms_class:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/Notification;->sms_class:Ljava/lang/Integer;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Notification;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Notification;->origin:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/Notification;->origin:Ljava/lang/String;

    .line 60
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Notification;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Notification;->sender:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/Notification;->sender:Ljava/lang/String;

    .line 61
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Notification;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Notification;->target:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/Notification;->target:Ljava/lang/String;

    .line 62
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Notification;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Notification;->message_data:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/Notification;->message_data:Ljava/lang/String;

    .line 63
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Notification;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/Notification;->type:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/Notification;->type:Ljava/lang/String;

    .line 64
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/Notification;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 69
    iget v0, p0, Lcom/navdy/service/library/events/Notification;->hashCode:I

    .line 70
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 71
    iget-object v2, p0, Lcom/navdy/service/library/events/Notification;->sms_class:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/Notification;->sms_class:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    .line 72
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Notification;->origin:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/Notification;->origin:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 73
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Notification;->sender:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/Notification;->sender:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 74
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Notification;->target:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/Notification;->target:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 75
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/Notification;->message_data:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/Notification;->message_data:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 76
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/Notification;->type:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/Notification;->type:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 77
    iput v0, p0, Lcom/navdy/service/library/events/Notification;->hashCode:I

    .line 79
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 71
    goto :goto_0

    :cond_3
    move v2, v1

    .line 72
    goto :goto_1

    :cond_4
    move v2, v1

    .line 73
    goto :goto_2

    :cond_5
    move v2, v1

    .line 74
    goto :goto_3

    :cond_6
    move v2, v1

    .line 75
    goto :goto_4
.end method
