.class public final Lcom/navdy/service/library/events/preferences/LocalPreferences;
.super Lcom/squareup/wire/Message;
.source "LocalPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_CLOCKFORMAT:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

.field public static final DEFAULT_MANUALZOOM:Ljava/lang/Boolean;

.field public static final DEFAULT_MANUALZOOMLEVEL:Ljava/lang/Float;

.field private static final serialVersionUID:J


# instance fields
.field public final clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final manualZoom:Ljava/lang/Boolean;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->BOOL:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final manualZoomLevel:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->DEFAULT_MANUALZOOM:Ljava/lang/Boolean;

    .line 22
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->DEFAULT_MANUALZOOMLEVEL:Ljava/lang/Float;

    .line 23
    sget-object v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->CLOCK_24_HOUR:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    sput-object v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->DEFAULT_CLOCKFORMAT:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;

    .prologue
    .line 50
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->manualZoom:Ljava/lang/Boolean;

    iget-object v1, p1, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->manualZoomLevel:Ljava/lang/Float;

    iget-object v2, p1, Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;->clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/preferences/LocalPreferences;-><init>(Ljava/lang/Boolean;Ljava/lang/Float;Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V

    .line 51
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/preferences/LocalPreferences;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 52
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;Lcom/navdy/service/library/events/preferences/LocalPreferences$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/preferences/LocalPreferences$1;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/preferences/LocalPreferences;-><init>(Lcom/navdy/service/library/events/preferences/LocalPreferences$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Ljava/lang/Float;Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)V
    .locals 0
    .param p1, "manualZoom"    # Ljava/lang/Boolean;
    .param p2, "manualZoomLevel"    # Ljava/lang/Float;
    .param p3, "clockFormat"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    .line 45
    iput-object p2, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoomLevel:Ljava/lang/Float;

    .line 46
    iput-object p3, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .line 47
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    if-ne p1, p0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/preferences/LocalPreferences;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 58
    check-cast v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;

    .line 59
    .local v0, "o":Lcom/navdy/service/library/events/preferences/LocalPreferences;
    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/LocalPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoomLevel:Ljava/lang/Float;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoomLevel:Ljava/lang/Float;

    .line 60
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/LocalPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    iget-object v4, v0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .line 61
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/preferences/LocalPreferences;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 66
    iget v0, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->hashCode:I

    .line 67
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 68
    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoom:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    .line 69
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoomLevel:Ljava/lang/Float;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->manualZoomLevel:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 70
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->clockFormat:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 71
    iput v0, p0, Lcom/navdy/service/library/events/preferences/LocalPreferences;->hashCode:I

    .line 73
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 68
    goto :goto_0

    :cond_3
    move v2, v1

    .line 69
    goto :goto_1
.end method
