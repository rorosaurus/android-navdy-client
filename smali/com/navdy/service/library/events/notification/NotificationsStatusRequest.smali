.class public final Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;
.super Lcom/squareup/wire/Message;
.source "NotificationsStatusRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_NEWSTATE:Lcom/navdy/service/library/events/notification/NotificationsState;

.field public static final DEFAULT_SERVICE:Lcom/navdy/service/library/events/notification/ServiceType;

.field private static final serialVersionUID:J


# instance fields
.field public final newState:Lcom/navdy/service/library/events/notification/NotificationsState;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final service:Lcom/navdy/service/library/events/notification/ServiceType;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/navdy/service/library/events/notification/NotificationsState;->NOTIFICATIONS_ENABLED:Lcom/navdy/service/library/events/notification/NotificationsState;

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->DEFAULT_NEWSTATE:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 20
    sget-object v0, Lcom/navdy/service/library/events/notification/ServiceType;->SERVICE_ANCS:Lcom/navdy/service/library/events/notification/ServiceType;

    sput-object v0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->DEFAULT_SERVICE:Lcom/navdy/service/library/events/notification/ServiceType;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/notification/NotificationsState;Lcom/navdy/service/library/events/notification/ServiceType;)V
    .locals 0
    .param p1, "newState"    # Lcom/navdy/service/library/events/notification/NotificationsState;
    .param p2, "service"    # Lcom/navdy/service/library/events/notification/ServiceType;

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->newState:Lcom/navdy/service/library/events/notification/NotificationsState;

    .line 42
    iput-object p2, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 43
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;)V
    .locals 2
    .param p1, "builder"    # Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;

    .prologue
    .line 46
    iget-object v0, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;->newState:Lcom/navdy/service/library/events/notification/NotificationsState;

    iget-object v1, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    invoke-direct {p0, v0, v1}, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;-><init>(Lcom/navdy/service/library/events/notification/NotificationsState;Lcom/navdy/service/library/events/notification/ServiceType;)V

    .line 47
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 48
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$1;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;-><init>(Lcom/navdy/service/library/events/notification/NotificationsStatusRequest$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 52
    if-ne p1, p0, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v1

    .line 53
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 54
    check-cast v0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;

    .line 55
    .local v0, "o":Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;
    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->newState:Lcom/navdy/service/library/events/notification/NotificationsState;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->newState:Lcom/navdy/service/library/events/notification/NotificationsState;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    iget-object v4, v0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    .line 56
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 61
    iget v0, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->hashCode:I

    .line 62
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 63
    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->newState:Lcom/navdy/service/library/events/notification/NotificationsState;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->newState:Lcom/navdy/service/library/events/notification/NotificationsState;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/notification/NotificationsState;->hashCode()I

    move-result v0

    .line 64
    :goto_0
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->service:Lcom/navdy/service/library/events/notification/ServiceType;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/notification/ServiceType;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 65
    iput v0, p0, Lcom/navdy/service/library/events/notification/NotificationsStatusRequest;->hashCode:I

    .line 67
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 63
    goto :goto_0
.end method
