.class public final Lcom/navdy/service/library/events/file/PreviewFileResponse;
.super Lcom/squareup/wire/Message;
.source "PreviewFileResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_FILENAME:Ljava/lang/String; = ""

.field public static final DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

.field public static final DEFAULT_STATUS_DETAIL:Ljava/lang/String; = ""

.field private static final serialVersionUID:J


# instance fields
.field public final filename:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status:Lcom/navdy/service/library/events/RequestStatus;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->ENUM:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final status_detail:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_SUCCESS:Lcom/navdy/service/library/events/RequestStatus;

    sput-object v0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->DEFAULT_STATUS:Lcom/navdy/service/library/events/RequestStatus;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;
    .param p2, "status_detail"    # Ljava/lang/String;
    .param p3, "filename"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 37
    iput-object p2, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status_detail:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->filename:Ljava/lang/String;

    .line 39
    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;)V
    .locals 3
    .param p1, "builder"    # Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;

    .prologue
    .line 42
    iget-object v0, p1, Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v1, p1, Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;->status_detail:Ljava/lang/String;

    iget-object v2, p1, Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;->filename:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/navdy/service/library/events/file/PreviewFileResponse;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    invoke-virtual {p0, p1}, Lcom/navdy/service/library/events/file/PreviewFileResponse;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;Lcom/navdy/service/library/events/file/PreviewFileResponse$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/file/PreviewFileResponse$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/file/PreviewFileResponse;-><init>(Lcom/navdy/service/library/events/file/PreviewFileResponse$Builder;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 48
    if-ne p1, p0, :cond_1

    .line 53
    :cond_0
    :goto_0
    return v1

    .line 49
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/file/PreviewFileResponse;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 50
    check-cast v0, Lcom/navdy/service/library/events/file/PreviewFileResponse;

    .line 51
    .local v0, "o":Lcom/navdy/service/library/events/file/PreviewFileResponse;
    iget-object v3, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/PreviewFileResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status_detail:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status_detail:Ljava/lang/String;

    .line 52
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/PreviewFileResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->filename:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->filename:Ljava/lang/String;

    .line 53
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/file/PreviewFileResponse;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 58
    iget v0, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->hashCode:I

    .line 59
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 60
    iget-object v2, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status:Lcom/navdy/service/library/events/RequestStatus;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/RequestStatus;->hashCode()I

    move-result v0

    .line 61
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status_detail:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->status_detail:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 62
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->filename:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->filename:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 63
    iput v0, p0, Lcom/navdy/service/library/events/file/PreviewFileResponse;->hashCode:I

    .line 65
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 60
    goto :goto_0

    :cond_3
    move v2, v1

    .line 61
    goto :goto_1
.end method
