.class public final Lcom/navdy/service/library/events/TripUpdate;
.super Lcom/squareup/wire/Message;
.source "TripUpdate.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/events/TripUpdate$Builder;
    }
.end annotation


# static fields
.field public static final DEFAULT_ARRIVED_AT_DESTINATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_BEARING:Ljava/lang/Float;

.field public static final DEFAULT_CHOSEN_DESTINATION_ID:Ljava/lang/String; = ""

.field public static final DEFAULT_DISTANCE_TO_DESTINATION:Ljava/lang/Integer;

.field public static final DEFAULT_DISTANCE_TRAVELED:Ljava/lang/Integer;

.field public static final DEFAULT_ELEVATION:Ljava/lang/Double;

.field public static final DEFAULT_ELEVATION_ACCURACY:Ljava/lang/Float;

.field public static final DEFAULT_ESTIMATED_TIME_REMAINING:Ljava/lang/Integer;

.field public static final DEFAULT_EXCESSIVE_SPEEDING_RATIO:Ljava/lang/Double;

.field public static final DEFAULT_GPS_SPEED:Ljava/lang/Float;

.field public static final DEFAULT_HARD_ACCELERATION_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_HARD_BREAKING_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_HIGH_G_COUNT:Ljava/lang/Integer;

.field public static final DEFAULT_HORIZONTAL_ACCURACY:Ljava/lang/Float;

.field public static final DEFAULT_METERS_TRAVELED_SINCE_BOOT:Ljava/lang/Integer;

.field public static final DEFAULT_OBD_SPEED:Ljava/lang/Integer;

.field public static final DEFAULT_ROAD_ELEMENT:Ljava/lang/String; = ""

.field public static final DEFAULT_SEQUENCE_NUMBER:Ljava/lang/Integer;

.field public static final DEFAULT_SPEEDING_RATIO:Ljava/lang/Double;

.field public static final DEFAULT_TIMESTAMP:Ljava/lang/Long;

.field public static final DEFAULT_TRIP_NUMBER:Ljava/lang/Long;

.field private static final serialVersionUID:J


# instance fields
.field public final arrived_at_destination_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xc
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final bearing:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x7
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final chosen_destination_id:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xb
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final current_position:Lcom/navdy/service/library/events/location/LatLong;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x5
    .end annotation
.end field

.field public final distance_to_destination:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xe
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final distance_traveled:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x4
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final elevation:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x6
        type = .enum Lcom/squareup/wire/Message$Datatype;->DOUBLE:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final elevation_accuracy:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x16
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final estimated_time_remaining:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xd
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final excessive_speeding_ratio:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x13
        type = .enum Lcom/squareup/wire/Message$Datatype;->DOUBLE:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final gps_speed:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x8
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final hard_acceleration_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x11
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final hard_breaking_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x10
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final high_g_count:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xf
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final horizontal_accuracy:Ljava/lang/Float;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x15
        type = .enum Lcom/squareup/wire/Message$Datatype;->FLOAT:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x17
    .end annotation
.end field

.field public final meters_traveled_since_boot:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x14
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final obd_speed:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x9
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final road_element:Ljava/lang/String;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0xa
        type = .enum Lcom/squareup/wire/Message$Datatype;->STRING:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final sequence_number:Ljava/lang/Integer;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x2
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT32:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final speeding_ratio:Ljava/lang/Double;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        tag = 0x12
        type = .enum Lcom/squareup/wire/Message$Datatype;->DOUBLE:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final timestamp:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x3
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field

.field public final trip_number:Ljava/lang/Long;
    .annotation runtime Lcom/squareup/wire/ProtoField;
        label = .enum Lcom/squareup/wire/Message$Label;->REQUIRED:Lcom/squareup/wire/Message$Label;
        tag = 0x1
        type = .enum Lcom/squareup/wire/Message$Datatype;->INT64:Lcom/squareup/wire/Message$Datatype;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 30
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_TRIP_NUMBER:Ljava/lang/Long;

    .line 31
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_SEQUENCE_NUMBER:Ljava/lang/Integer;

    .line 32
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_TIMESTAMP:Ljava/lang/Long;

    .line 33
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_DISTANCE_TRAVELED:Ljava/lang/Integer;

    .line 34
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_ELEVATION:Ljava/lang/Double;

    .line 35
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_BEARING:Ljava/lang/Float;

    .line 36
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_GPS_SPEED:Ljava/lang/Float;

    .line 37
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_OBD_SPEED:Ljava/lang/Integer;

    .line 41
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_ESTIMATED_TIME_REMAINING:Ljava/lang/Integer;

    .line 42
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_DISTANCE_TO_DESTINATION:Ljava/lang/Integer;

    .line 43
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_HIGH_G_COUNT:Ljava/lang/Integer;

    .line 44
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_HARD_BREAKING_COUNT:Ljava/lang/Integer;

    .line 45
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_HARD_ACCELERATION_COUNT:Ljava/lang/Integer;

    .line 46
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_SPEEDING_RATIO:Ljava/lang/Double;

    .line 47
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_EXCESSIVE_SPEEDING_RATIO:Ljava/lang/Double;

    .line 48
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_METERS_TRAVELED_SINCE_BOOT:Ljava/lang/Integer;

    .line 49
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_HORIZONTAL_ACCURACY:Ljava/lang/Float;

    .line 50
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/events/TripUpdate;->DEFAULT_ELEVATION_ACCURACY:Ljava/lang/Float;

    return-void
.end method

.method private constructor <init>(Lcom/navdy/service/library/events/TripUpdate$Builder;)V
    .locals 25
    .param p1, "builder"    # Lcom/navdy/service/library/events/TripUpdate$Builder;

    .prologue
    .line 223
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->trip_number:Ljava/lang/Long;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->sequence_number:Ljava/lang/Integer;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->timestamp:Ljava/lang/Long;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->distance_traveled:Ljava/lang/Integer;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->elevation:Ljava/lang/Double;

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->bearing:Ljava/lang/Float;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->gps_speed:Ljava/lang/Float;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->obd_speed:Ljava/lang/Integer;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->road_element:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->chosen_destination_id:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->arrived_at_destination_id:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v14, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->estimated_time_remaining:Ljava/lang/Integer;

    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->distance_to_destination:Ljava/lang/Integer;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->high_g_count:Ljava/lang/Integer;

    move-object/from16 v16, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->hard_breaking_count:Ljava/lang/Integer;

    move-object/from16 v17, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->hard_acceleration_count:Ljava/lang/Integer;

    move-object/from16 v18, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->speeding_ratio:Ljava/lang/Double;

    move-object/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->excessive_speeding_ratio:Ljava/lang/Double;

    move-object/from16 v20, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->meters_traveled_since_boot:Ljava/lang/Integer;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->horizontal_accuracy:Ljava/lang/Float;

    move-object/from16 v22, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->elevation_accuracy:Ljava/lang/Float;

    move-object/from16 v23, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/navdy/service/library/events/TripUpdate$Builder;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    move-object/from16 v24, v0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v24}, Lcom/navdy/service/library/events/TripUpdate;-><init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Lcom/navdy/service/library/events/location/LatLong;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Integer;Ljava/lang/Float;Ljava/lang/Float;Lcom/navdy/service/library/events/location/Coordinate;)V

    .line 224
    invoke-virtual/range {p0 .. p1}, Lcom/navdy/service/library/events/TripUpdate;->setBuilder(Lcom/squareup/wire/Message$Builder;)V

    .line 225
    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/service/library/events/TripUpdate$Builder;Lcom/navdy/service/library/events/TripUpdate$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/service/library/events/TripUpdate$Builder;
    .param p2, "x1"    # Lcom/navdy/service/library/events/TripUpdate$1;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/navdy/service/library/events/TripUpdate;-><init>(Lcom/navdy/service/library/events/TripUpdate$Builder;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Lcom/navdy/service/library/events/location/LatLong;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Integer;Ljava/lang/Float;Ljava/lang/Float;Lcom/navdy/service/library/events/location/Coordinate;)V
    .locals 1
    .param p1, "trip_number"    # Ljava/lang/Long;
    .param p2, "sequence_number"    # Ljava/lang/Integer;
    .param p3, "timestamp"    # Ljava/lang/Long;
    .param p4, "distance_traveled"    # Ljava/lang/Integer;
    .param p5, "current_position"    # Lcom/navdy/service/library/events/location/LatLong;
    .param p6, "elevation"    # Ljava/lang/Double;
    .param p7, "bearing"    # Ljava/lang/Float;
    .param p8, "gps_speed"    # Ljava/lang/Float;
    .param p9, "obd_speed"    # Ljava/lang/Integer;
    .param p10, "road_element"    # Ljava/lang/String;
    .param p11, "chosen_destination_id"    # Ljava/lang/String;
    .param p12, "arrived_at_destination_id"    # Ljava/lang/String;
    .param p13, "estimated_time_remaining"    # Ljava/lang/Integer;
    .param p14, "distance_to_destination"    # Ljava/lang/Integer;
    .param p15, "high_g_count"    # Ljava/lang/Integer;
    .param p16, "hard_breaking_count"    # Ljava/lang/Integer;
    .param p17, "hard_acceleration_count"    # Ljava/lang/Integer;
    .param p18, "speeding_ratio"    # Ljava/lang/Double;
    .param p19, "excessive_speeding_ratio"    # Ljava/lang/Double;
    .param p20, "meters_traveled_since_boot"    # Ljava/lang/Integer;
    .param p21, "horizontal_accuracy"    # Ljava/lang/Float;
    .param p22, "elevation_accuracy"    # Ljava/lang/Float;
    .param p23, "last_raw_coordinate"    # Lcom/navdy/service/library/events/location/Coordinate;

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/squareup/wire/Message;-><init>()V

    .line 197
    iput-object p1, p0, Lcom/navdy/service/library/events/TripUpdate;->trip_number:Ljava/lang/Long;

    .line 198
    iput-object p2, p0, Lcom/navdy/service/library/events/TripUpdate;->sequence_number:Ljava/lang/Integer;

    .line 199
    iput-object p3, p0, Lcom/navdy/service/library/events/TripUpdate;->timestamp:Ljava/lang/Long;

    .line 200
    iput-object p4, p0, Lcom/navdy/service/library/events/TripUpdate;->distance_traveled:Ljava/lang/Integer;

    .line 201
    iput-object p5, p0, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 202
    iput-object p6, p0, Lcom/navdy/service/library/events/TripUpdate;->elevation:Ljava/lang/Double;

    .line 203
    iput-object p7, p0, Lcom/navdy/service/library/events/TripUpdate;->bearing:Ljava/lang/Float;

    .line 204
    iput-object p8, p0, Lcom/navdy/service/library/events/TripUpdate;->gps_speed:Ljava/lang/Float;

    .line 205
    iput-object p9, p0, Lcom/navdy/service/library/events/TripUpdate;->obd_speed:Ljava/lang/Integer;

    .line 206
    iput-object p10, p0, Lcom/navdy/service/library/events/TripUpdate;->road_element:Ljava/lang/String;

    .line 207
    iput-object p11, p0, Lcom/navdy/service/library/events/TripUpdate;->chosen_destination_id:Ljava/lang/String;

    .line 208
    iput-object p12, p0, Lcom/navdy/service/library/events/TripUpdate;->arrived_at_destination_id:Ljava/lang/String;

    .line 209
    iput-object p13, p0, Lcom/navdy/service/library/events/TripUpdate;->estimated_time_remaining:Ljava/lang/Integer;

    .line 210
    iput-object p14, p0, Lcom/navdy/service/library/events/TripUpdate;->distance_to_destination:Ljava/lang/Integer;

    .line 211
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate;->high_g_count:Ljava/lang/Integer;

    .line 212
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate;->hard_breaking_count:Ljava/lang/Integer;

    .line 213
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate;->hard_acceleration_count:Ljava/lang/Integer;

    .line 214
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate;->speeding_ratio:Ljava/lang/Double;

    .line 215
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate;->excessive_speeding_ratio:Ljava/lang/Double;

    .line 216
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate;->meters_traveled_since_boot:Ljava/lang/Integer;

    .line 217
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate;->horizontal_accuracy:Ljava/lang/Float;

    .line 218
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate;->elevation_accuracy:Ljava/lang/Float;

    .line 219
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    .line 220
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "other"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 229
    if-ne p1, p0, :cond_1

    .line 254
    :cond_0
    :goto_0
    return v1

    .line 230
    :cond_1
    instance-of v3, p1, Lcom/navdy/service/library/events/TripUpdate;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 231
    check-cast v0, Lcom/navdy/service/library/events/TripUpdate;

    .line 232
    .local v0, "o":Lcom/navdy/service/library/events/TripUpdate;
    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->trip_number:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->trip_number:Ljava/lang/Long;

    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->sequence_number:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->sequence_number:Ljava/lang/Integer;

    .line 233
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->timestamp:Ljava/lang/Long;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->timestamp:Ljava/lang/Long;

    .line 234
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->distance_traveled:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->distance_traveled:Ljava/lang/Integer;

    .line 235
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    .line 236
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->elevation:Ljava/lang/Double;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->elevation:Ljava/lang/Double;

    .line 237
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->bearing:Ljava/lang/Float;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->bearing:Ljava/lang/Float;

    .line 238
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->gps_speed:Ljava/lang/Float;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->gps_speed:Ljava/lang/Float;

    .line 239
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->obd_speed:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->obd_speed:Ljava/lang/Integer;

    .line 240
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->road_element:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->road_element:Ljava/lang/String;

    .line 241
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->chosen_destination_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->chosen_destination_id:Ljava/lang/String;

    .line 242
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->arrived_at_destination_id:Ljava/lang/String;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->arrived_at_destination_id:Ljava/lang/String;

    .line 243
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->estimated_time_remaining:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->estimated_time_remaining:Ljava/lang/Integer;

    .line 244
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->distance_to_destination:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->distance_to_destination:Ljava/lang/Integer;

    .line 245
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->high_g_count:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->high_g_count:Ljava/lang/Integer;

    .line 246
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->hard_breaking_count:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->hard_breaking_count:Ljava/lang/Integer;

    .line 247
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->hard_acceleration_count:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->hard_acceleration_count:Ljava/lang/Integer;

    .line 248
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->speeding_ratio:Ljava/lang/Double;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->speeding_ratio:Ljava/lang/Double;

    .line 249
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->excessive_speeding_ratio:Ljava/lang/Double;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->excessive_speeding_ratio:Ljava/lang/Double;

    .line 250
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->meters_traveled_since_boot:Ljava/lang/Integer;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->meters_traveled_since_boot:Ljava/lang/Integer;

    .line 251
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->horizontal_accuracy:Ljava/lang/Float;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->horizontal_accuracy:Ljava/lang/Float;

    .line 252
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->elevation_accuracy:Ljava/lang/Float;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->elevation_accuracy:Ljava/lang/Float;

    .line 253
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    iget-object v4, v0, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    .line 254
    invoke-virtual {p0, v3, v4}, Lcom/navdy/service/library/events/TripUpdate;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 259
    iget v0, p0, Lcom/navdy/service/library/events/TripUpdate;->hashCode:I

    .line 260
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 261
    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->trip_number:Ljava/lang/Long;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->trip_number:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v0

    .line 262
    :goto_0
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->sequence_number:Ljava/lang/Integer;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->sequence_number:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v3, v2

    .line 263
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->timestamp:Ljava/lang/Long;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->timestamp:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->hashCode()I

    move-result v2

    :goto_2
    add-int v0, v3, v2

    .line 264
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->distance_traveled:Ljava/lang/Integer;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->distance_traveled:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v3, v2

    .line 265
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->current_position:Lcom/navdy/service/library/events/location/LatLong;

    invoke-virtual {v2}, Lcom/navdy/service/library/events/location/LatLong;->hashCode()I

    move-result v2

    :goto_4
    add-int v0, v3, v2

    .line 266
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->elevation:Ljava/lang/Double;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->elevation:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v3, v2

    .line 267
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->bearing:Ljava/lang/Float;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->bearing:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v3, v2

    .line 268
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->gps_speed:Ljava/lang/Float;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->gps_speed:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v3, v2

    .line 269
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->obd_speed:Ljava/lang/Integer;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->obd_speed:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_8
    add-int v0, v3, v2

    .line 270
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->road_element:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->road_element:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_9
    add-int v0, v3, v2

    .line 271
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->chosen_destination_id:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->chosen_destination_id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_a
    add-int v0, v3, v2

    .line 272
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->arrived_at_destination_id:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->arrived_at_destination_id:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_b
    add-int v0, v3, v2

    .line 273
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->estimated_time_remaining:Ljava/lang/Integer;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->estimated_time_remaining:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_c
    add-int v0, v3, v2

    .line 274
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->distance_to_destination:Ljava/lang/Integer;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->distance_to_destination:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_d
    add-int v0, v3, v2

    .line 275
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->high_g_count:Ljava/lang/Integer;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->high_g_count:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_e
    add-int v0, v3, v2

    .line 276
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->hard_breaking_count:Ljava/lang/Integer;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->hard_breaking_count:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_f
    add-int v0, v3, v2

    .line 277
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->hard_acceleration_count:Ljava/lang/Integer;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->hard_acceleration_count:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_10
    add-int v0, v3, v2

    .line 278
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->speeding_ratio:Ljava/lang/Double;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->speeding_ratio:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->hashCode()I

    move-result v2

    :goto_11
    add-int v0, v3, v2

    .line 279
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->excessive_speeding_ratio:Ljava/lang/Double;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->excessive_speeding_ratio:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->hashCode()I

    move-result v2

    :goto_12
    add-int v0, v3, v2

    .line 280
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->meters_traveled_since_boot:Ljava/lang/Integer;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->meters_traveled_since_boot:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->hashCode()I

    move-result v2

    :goto_13
    add-int v0, v3, v2

    .line 281
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->horizontal_accuracy:Ljava/lang/Float;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->horizontal_accuracy:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    :goto_14
    add-int v0, v3, v2

    .line 282
    mul-int/lit8 v3, v0, 0x25

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->elevation_accuracy:Ljava/lang/Float;

    if-eqz v2, :cond_17

    iget-object v2, p0, Lcom/navdy/service/library/events/TripUpdate;->elevation_accuracy:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->hashCode()I

    move-result v2

    :goto_15
    add-int v0, v3, v2

    .line 283
    mul-int/lit8 v2, v0, 0x25

    iget-object v3, p0, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    if-eqz v3, :cond_0

    iget-object v1, p0, Lcom/navdy/service/library/events/TripUpdate;->last_raw_coordinate:Lcom/navdy/service/library/events/location/Coordinate;

    invoke-virtual {v1}, Lcom/navdy/service/library/events/location/Coordinate;->hashCode()I

    move-result v1

    :cond_0
    add-int v0, v2, v1

    .line 284
    iput v0, p0, Lcom/navdy/service/library/events/TripUpdate;->hashCode:I

    .line 286
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 261
    goto/16 :goto_0

    :cond_3
    move v2, v1

    .line 262
    goto/16 :goto_1

    :cond_4
    move v2, v1

    .line 263
    goto/16 :goto_2

    :cond_5
    move v2, v1

    .line 264
    goto/16 :goto_3

    :cond_6
    move v2, v1

    .line 265
    goto/16 :goto_4

    :cond_7
    move v2, v1

    .line 266
    goto/16 :goto_5

    :cond_8
    move v2, v1

    .line 267
    goto/16 :goto_6

    :cond_9
    move v2, v1

    .line 268
    goto/16 :goto_7

    :cond_a
    move v2, v1

    .line 269
    goto/16 :goto_8

    :cond_b
    move v2, v1

    .line 270
    goto/16 :goto_9

    :cond_c
    move v2, v1

    .line 271
    goto/16 :goto_a

    :cond_d
    move v2, v1

    .line 272
    goto/16 :goto_b

    :cond_e
    move v2, v1

    .line 273
    goto/16 :goto_c

    :cond_f
    move v2, v1

    .line 274
    goto/16 :goto_d

    :cond_10
    move v2, v1

    .line 275
    goto/16 :goto_e

    :cond_11
    move v2, v1

    .line 276
    goto/16 :goto_f

    :cond_12
    move v2, v1

    .line 277
    goto/16 :goto_10

    :cond_13
    move v2, v1

    .line 278
    goto :goto_11

    :cond_14
    move v2, v1

    .line 279
    goto :goto_12

    :cond_15
    move v2, v1

    .line 280
    goto :goto_13

    :cond_16
    move v2, v1

    .line 281
    goto :goto_14

    :cond_17
    move v2, v1

    .line 282
    goto :goto_15
.end method
