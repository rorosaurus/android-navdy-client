.class Lcom/nimbusds/jose/AlgorithmFamily;
.super Ljava/util/LinkedHashSet;
.source "AlgorithmFamily.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/nimbusds/jose/Algorithm;",
        ">",
        "Ljava/util/LinkedHashSet",
        "<TT;>;"
    }
.end annotation

.annotation runtime Lnet/jcip/annotations/Immutable;
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public varargs constructor <init>([Lcom/nimbusds/jose/Algorithm;)V
    .locals 3
    .param p1, "algs"    # [Lcom/nimbusds/jose/Algorithm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/nimbusds/jose/AlgorithmFamily;, "Lcom/nimbusds/jose/AlgorithmFamily<TT;>;"
    invoke-direct {p0}, Ljava/util/LinkedHashSet;-><init>()V

    .line 29
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v2, :cond_0

    .line 32
    return-void

    .line 29
    :cond_0
    aget-object v0, p1, v1

    .line 30
    .local v0, "alg":Lcom/nimbusds/jose/Algorithm;, "TT;"
    invoke-super {p0, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 29
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/nimbusds/jose/Algorithm;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 37
    .local p0, "this":Lcom/nimbusds/jose/AlgorithmFamily;, "Lcom/nimbusds/jose/AlgorithmFamily<TT;>;"
    .local p1, "alg":Lcom/nimbusds/jose/Algorithm;, "TT;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public bridge synthetic add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1
    check-cast p1, Lcom/nimbusds/jose/Algorithm;

    invoke-virtual {p0, p1}, Lcom/nimbusds/jose/AlgorithmFamily;->add(Lcom/nimbusds/jose/Algorithm;)Z

    move-result v0

    return v0
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 43
    .local p0, "this":Lcom/nimbusds/jose/AlgorithmFamily;, "Lcom/nimbusds/jose/AlgorithmFamily<TT;>;"
    .local p1, "algs":Ljava/util/Collection;, "Ljava/util/Collection<+TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 49
    .local p0, "this":Lcom/nimbusds/jose/AlgorithmFamily;, "Lcom/nimbusds/jose/AlgorithmFamily<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lcom/nimbusds/jose/AlgorithmFamily;, "Lcom/nimbusds/jose/AlgorithmFamily<TT;>;"
    .local p1, "c":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lcom/nimbusds/jose/AlgorithmFamily;, "Lcom/nimbusds/jose/AlgorithmFamily<TT;>;"
    .local p1, "c":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
