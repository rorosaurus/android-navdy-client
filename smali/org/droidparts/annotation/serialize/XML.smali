.class public interface abstract annotation Lorg/droidparts/annotation/serialize/XML;
.super Ljava/lang/Object;
.source "XML.java"

# interfaces
.implements Ljava/lang/annotation/Annotation;


# annotations
.annotation system Ldalvik/annotation/AnnotationDefault;
    value = .subannotation Lorg/droidparts/annotation/serialize/XML;
        attribute = ""
        optional = false
        tag = ""
    .end subannotation
.end annotation

.annotation runtime Ljava/lang/annotation/Retention;
    value = .enum Ljava/lang/annotation/RetentionPolicy;->RUNTIME:Ljava/lang/annotation/RetentionPolicy;
.end annotation

.annotation runtime Ljava/lang/annotation/Target;
    value = {
        .enum Ljava/lang/annotation/ElementType;->FIELD:Ljava/lang/annotation/ElementType;
    }
.end annotation


# static fields
.field public static final SUB:Ljava/lang/String; = "->\u001d"


# virtual methods
.method public abstract attribute()Ljava/lang/String;
.end method

.method public abstract optional()Z
.end method

.method public abstract tag()Ljava/lang/String;
.end method
