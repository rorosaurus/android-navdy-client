.class Lcom/navdy/client/app/ui/base/BaseHereMapFragment$8;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentAttached;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->hide()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 311
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$8;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttached()V
    .locals 2

    .prologue
    .line 314
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "hide"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$8;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 316
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$8;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .line 317
    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 318
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 319
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$8;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 320
    return-void
.end method
