.class public abstract Lcom/navdy/client/app/ui/base/BaseEditActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "BaseEditActivity.java"


# instance fields
.field checkChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field protected saveOnExit:Z

.field protected somethingChanged:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    .line 11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseEditActivity;->saveOnExit:Z

    .line 12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseEditActivity;->somethingChanged:Z

    .line 41
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseEditActivity$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity$1;-><init>(Lcom/navdy/client/app/ui/base/BaseEditActivity;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/base/BaseEditActivity;->checkChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    return-void
.end method


# virtual methods
.method protected discardChanges()V
    .locals 0

    .prologue
    .line 18
    return-void
.end method

.method protected initCompoundButton(Landroid/widget/CompoundButton;Z)V
    .locals 1
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 49
    iget-object v0, p0, Lcom/navdy/client/app/ui/base/BaseEditActivity;->checkChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->initCompoundButton(Landroid/widget/CompoundButton;ZLandroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 50
    return-void
.end method

.method protected initCompoundButton(Landroid/widget/CompoundButton;ZLandroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 0
    .param p1, "button"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z
    .param p3, "listener"    # Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .prologue
    .line 53
    if-eqz p1, :cond_0

    .line 54
    invoke-virtual {p1, p2}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 55
    invoke-virtual {p1, p3}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 58
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseEditActivity;->saveOnExit:Z

    if-eqz v0, :cond_1

    .line 24
    iget-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseEditActivity;->somethingChanged:Z

    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/base/BaseEditActivity;->saveChanges()V

    .line 27
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseEditActivity;->somethingChanged:Z

    .line 29
    :cond_1
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onPause()V

    .line 30
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseEditActivity;->saveOnExit:Z

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/base/BaseEditActivity;->somethingChanged:Z

    .line 37
    return-void
.end method

.method protected abstract saveChanges()V
.end method
