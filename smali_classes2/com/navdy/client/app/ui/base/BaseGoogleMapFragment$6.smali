.class Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;
.super Ljava/lang/Object;
.source "BaseGoogleMapFragment.java"

# interfaces
.implements Lcom/google/android/gms/maps/OnMapReadyCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .prologue
    .line 327
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapReady(Lcom/google/android/gms/maps/GoogleMap;)V
    .locals 6
    .param p1, "googleMap"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 332
    :try_start_0
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/maps/MapsInitializer;->initialize(Landroid/content/Context;)I

    .line 335
    invoke-virtual {p1}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v2

    .line 336
    .local v2, "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$1000(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/UiSettings;->setScrollGesturesEnabled(Z)V

    .line 337
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$1100(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/UiSettings;->setMyLocationButtonEnabled(Z)V

    .line 338
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$1200(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/UiSettings;->setMapToolbarEnabled(Z)V

    .line 340
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$1300(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->isLimitingCellularData()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v1, 0x1

    .line 341
    .local v1, "shouldEnableTraffic":Z
    :goto_0
    invoke-virtual {p1, v1}, Lcom/google/android/gms/maps/GoogleMap;->setTrafficEnabled(Z)V

    .line 343
    iget-object v3, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    invoke-static {v3}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$1400(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 344
    new-instance v3, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6$1;

    invoke-direct {v3, p0, p1}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6$1;-><init>(Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;Lcom/google/android/gms/maps/GoogleMap;)V

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment$6;->this$0:Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;

    .line 350
    invoke-virtual {v5}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 344
    invoke-static {v3, v4, v5}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestLocationPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 355
    .end local v1    # "shouldEnableTraffic":Z
    .end local v2    # "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    :cond_0
    :goto_1
    return-void

    .line 340
    .restart local v2    # "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 352
    .end local v2    # "uiSettings":Lcom/google/android/gms/maps/UiSettings;
    :catch_0
    move-exception v0

    .line 353
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseGoogleMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1
.end method
