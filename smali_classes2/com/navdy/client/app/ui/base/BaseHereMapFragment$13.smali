.class Lcom/navdy/client/app/ui/base/BaseHereMapFragment$13;
.super Ljava/lang/Object;
.source "BaseHereMapFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->setMapGesture(Lcom/here/android/mpa/mapping/MapGesture$OnGestureListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

.field final synthetic val$listener:Lcom/here/android/mpa/mapping/MapGesture$OnGestureListener;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/base/BaseHereMapFragment;Lcom/here/android/mpa/mapping/MapGesture$OnGestureListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    .prologue
    .line 455
    iput-object p1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$13;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$13;->val$listener:Lcom/here/android/mpa/mapping/MapGesture$OnGestureListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 3
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 469
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "could not add map gesture due to error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 470
    return-void
.end method

.method public onInit(Lcom/here/android/mpa/mapping/Map;)V
    .locals 3
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 458
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$13;->this$0:Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->getMapGesture()Lcom/here/android/mpa/mapping/MapGesture;

    move-result-object v0

    .line 460
    .local v0, "mapGesture":Lcom/here/android/mpa/mapping/MapGesture;
    if-eqz v0, :cond_0

    .line 461
    iget-object v1, p0, Lcom/navdy/client/app/ui/base/BaseHereMapFragment$13;->val$listener:Lcom/here/android/mpa/mapping/MapGesture$OnGestureListener;

    invoke-interface {v0, v1}, Lcom/here/android/mpa/mapping/MapGesture;->addOnGestureListener(Lcom/here/android/mpa/mapping/MapGesture$OnGestureListener;)V

    .line 465
    :goto_0
    return-void

    .line 463
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->access$100()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    const-string v2, "could not add map gesture due MapGesture being null"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    goto :goto_0
.end method
