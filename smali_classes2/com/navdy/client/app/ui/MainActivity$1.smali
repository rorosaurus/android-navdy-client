.class Lcom/navdy/client/app/ui/MainActivity$1;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/MainActivity;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/MainActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/MainActivity;

    .prologue
    .line 31
    iput-object p1, p0, Lcom/navdy/client/app/ui/MainActivity$1;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 34
    iget-object v1, p0, Lcom/navdy/client/app/ui/MainActivity$1;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/MainActivity;->isActivityDestroyed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 36
    invoke-static {}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupActivity;->userHasFinishedAppSetup()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 37
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/navdy/client/app/ui/MainActivity$1;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    const-class v2, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 38
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 42
    :goto_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/MainActivity$1;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    invoke-virtual {v1, v0}, Lcom/navdy/client/app/ui/MainActivity;->startActivity(Landroid/content/Intent;)V

    .line 43
    iget-object v1, p0, Lcom/navdy/client/app/ui/MainActivity$1;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    invoke-virtual {v1}, Lcom/navdy/client/app/ui/MainActivity;->finish()V

    .line 45
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void

    .line 40
    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/navdy/client/app/ui/MainActivity$1;->this$0:Lcom/navdy/client/app/ui/MainActivity;

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/MarketingFlowActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v0    # "i":Landroid/content/Intent;
    goto :goto_0
.end method
