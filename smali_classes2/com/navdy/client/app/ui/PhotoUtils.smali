.class public Lcom/navdy/client/app/ui/PhotoUtils;
.super Ljava/lang/Object;
.source "PhotoUtils.java"


# static fields
.field public static final COMPRESS_PHOTO_QUALITY:I = 0x55

.field public static final PICK_PHOTO_FROM_GALLERY:I = 0x2

.field public static final TAKE_PHOTO:I = 0x1

.field public static final TEMP_PHOTO_FILE:Ljava/lang/String; = "temp_photo.jpg"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPhotoFromGallery(Lcom/navdy/client/app/ui/base/BaseActivity;)V
    .locals 2
    .param p0, "activity"    # Lcom/navdy/client/app/ui/base/BaseActivity;

    .prologue
    .line 105
    if-nez p0, :cond_0

    .line 125
    :goto_0
    return-void

    .line 109
    :cond_0
    new-instance v0, Lcom/navdy/client/app/ui/PhotoUtils$3;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/PhotoUtils$3;-><init>(Lcom/navdy/client/app/ui/base/BaseActivity;)V

    new-instance v1, Lcom/navdy/client/app/ui/PhotoUtils$4;

    invoke-direct {v1}, Lcom/navdy/client/app/ui/PhotoUtils$4;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestStoragePermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static takePhoto(Lcom/navdy/client/app/ui/base/BaseActivity;Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)V
    .locals 3
    .param p0, "activity"    # Lcom/navdy/client/app/ui/base/BaseActivity;
    .param p1, "logger"    # Lcom/navdy/service/library/log/Logger;
    .param p2, "filename"    # Ljava/lang/String;

    .prologue
    .line 34
    invoke-static {}, Lcom/navdy/service/library/util/IOUtils;->isExternalStorageReadable()Z

    move-result v1

    if-nez v1, :cond_0

    .line 35
    const v1, 0x7f08016d

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 99
    :goto_0
    return-void

    .line 39
    :cond_0
    invoke-static {p2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 40
    invoke-static {}, Lcom/navdy/service/library/util/IOUtils;->getTempFilename()Ljava/lang/String;

    move-result-object p2

    .line 42
    :cond_1
    move-object v0, p2

    .line 45
    .local v0, "finalName":Ljava/lang/String;
    new-instance v1, Lcom/navdy/client/app/ui/PhotoUtils$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/navdy/client/app/ui/PhotoUtils$1;-><init>(Lcom/navdy/client/app/ui/base/BaseActivity;Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)V

    new-instance v2, Lcom/navdy/client/app/ui/PhotoUtils$2;

    invoke-direct {v2}, Lcom/navdy/client/app/ui/PhotoUtils$2;-><init>()V

    invoke-virtual {p0, v1, v2}, Lcom/navdy/client/app/ui/base/BaseActivity;->requestCameraPermission(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    goto :goto_0
.end method
