.class public Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "MountPickerActivity.java"


# static fields
.field public static final EXTRA_USE_CASE:Ljava/lang/String; = "extra_use_case"

.field public static final MEDIUM_MOUNT_RECOMMENDED:I = 0x0

.field public static final MEDIUM_TALL_IS_TOO_HIGH:I = 0x2

.field public static final NONE_OF_THE_MOUNTS_WILL_WORK_4_U:I = 0x3

.field public static final SHORT_IS_TOO_LOW:I = 0x1


# instance fields
.field box:Ljava/lang/String;

.field mountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

.field private usecase:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    .line 43
    new-instance v0, Lcom/navdy/client/app/framework/models/MountInfo;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/models/MountInfo;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->mountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Landroid/widget/TextView;
    .param p4, "x4"    # Landroid/widget/TextView;
    .param p5, "x5"    # Landroid/widget/TextView;
    .param p6, "x6"    # Landroid/widget/TextView;

    .prologue
    .line 36
    invoke-direct/range {p0 .. p6}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->handleMountInfo(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    return-void
.end method

.method private getTrackerScreen()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 255
    iget v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->usecase:I

    packed-switch v1, :pswitch_data_0

    .line 258
    const-string v0, "Installation_Mount_Picker_Medium_Mount_Recommended"

    .line 270
    .local v0, "screen":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 261
    .end local v0    # "screen":Ljava/lang/String;
    :pswitch_0
    const-string v0, "Installation_Mount_Picker_Short_Is_Too_Low"

    .line 262
    .restart local v0    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 264
    .end local v0    # "screen":Ljava/lang/String;
    :pswitch_1
    const-string v0, "Installation_Mount_Picker_Medium_Tall_Is_Too_High"

    .line 265
    .restart local v0    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 267
    .end local v0    # "screen":Ljava/lang/String;
    :pswitch_2
    const-string v0, "Installation_Mount_Picker_No_Mount_Supported"

    .restart local v0    # "screen":Ljava/lang/String;
    goto :goto_0

    .line 255
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private handleMountInfo(Ljava/lang/String;Ljava/lang/String;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 9
    .param p1, "box"    # Ljava/lang/String;
    .param p2, "modelString"    # Ljava/lang/String;
    .param p3, "shortMountPill"    # Landroid/widget/TextView;
    .param p4, "mediumMountPill"    # Landroid/widget/TextView;
    .param p5, "title"    # Landroid/widget/TextView;
    .param p6, "desc"    # Landroid/widget/TextView;

    .prologue
    const/4 v8, 0x1

    const/16 v7, 0x8

    const v6, 0x7f0202bf

    const v4, 0x7f1000b1

    const/4 v5, 0x0

    .line 111
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->mountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/models/MountInfo;->noneOfTheMountsWillWork()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 113
    const/4 v3, 0x3

    iput v3, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->usecase:I

    .line 116
    :cond_0
    iget v3, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->usecase:I

    packed-switch v3, :pswitch_data_0

    .line 119
    const v3, 0x7f020058

    invoke-virtual {p0, v4, v3}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->loadImage(II)V

    .line 120
    if-eqz p3, :cond_1

    .line 121
    invoke-virtual {p3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    :cond_1
    if-eqz p4, :cond_2

    .line 124
    const v3, 0x7f0803d1

    new-array v4, v8, [Ljava/lang/Object;

    aput-object p2, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    const v3, 0x7f0202bd

    invoke-virtual {p4, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 126
    invoke-virtual {p4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 188
    :cond_2
    :goto_0
    const v3, 0x7f1001f2

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 189
    .local v0, "backBtn":Landroid/widget/ImageView;
    const v3, 0x7f1001f8

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 191
    .local v1, "selectToContinue":Landroid/widget/TextView;
    iget v3, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->usecase:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_c

    .line 192
    if-eqz v1, :cond_3

    .line 193
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 195
    :cond_3
    if-eqz v0, :cond_4

    .line 196
    const v3, 0x7f020061

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 206
    :cond_4
    :goto_1
    return-void

    .line 130
    .end local v0    # "backBtn":Landroid/widget/ImageView;
    .end local v1    # "selectToContinue":Landroid/widget/TextView;
    :pswitch_0
    const v3, 0x7f020058

    invoke-virtual {p0, v4, v3}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->loadImage(II)V

    .line 131
    if-eqz p3, :cond_5

    .line 132
    const v3, 0x7f080284

    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setText(I)V

    .line 133
    invoke-virtual {p3, v6}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 134
    invoke-virtual {p3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 136
    :cond_5
    if-eqz p4, :cond_2

    .line 137
    invoke-virtual {p4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 141
    :pswitch_1
    const-string v3, "Old_Box"

    invoke-static {p1, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 142
    const v3, 0x7f020054

    invoke-virtual {p0, v4, v3}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->loadImage(II)V

    .line 146
    :goto_2
    if-eqz p3, :cond_6

    .line 147
    invoke-virtual {p3, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    :cond_6
    if-eqz p4, :cond_2

    .line 150
    const v3, 0x7f080283

    invoke-virtual {p4, v3}, Landroid/widget/TextView;->setText(I)V

    .line 151
    invoke-virtual {p4, v6}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 152
    invoke-virtual {p4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 144
    :cond_7
    const v3, 0x7f020056

    invoke-virtual {p0, v4, v3}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->loadImage(II)V

    goto :goto_2

    .line 156
    :pswitch_2
    const-string v3, "Old_Box"

    invoke-static {p1, v3}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 157
    const v3, 0x7f020236

    invoke-virtual {p0, v4, v3}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->loadImage(II)V

    .line 161
    :goto_3
    if-eqz p3, :cond_8

    .line 162
    const v3, 0x7f080310

    invoke-virtual {p3, v3}, Landroid/widget/TextView;->setText(I)V

    .line 163
    invoke-virtual {p3, v6}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 164
    invoke-virtual {p3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 166
    :cond_8
    if-eqz p4, :cond_9

    .line 167
    const v3, 0x7f080310

    invoke-virtual {p4, v3}, Landroid/widget/TextView;->setText(I)V

    .line 168
    invoke-virtual {p4, v6}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 169
    invoke-virtual {p4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 171
    :cond_9
    if-eqz p5, :cond_a

    .line 172
    const v3, 0x7f080303

    invoke-virtual {p5, v3}, Landroid/widget/TextView;->setText(I)V

    .line 174
    :cond_a
    if-eqz p6, :cond_2

    .line 175
    const v3, 0x7f080302

    new-array v4, v8, [Ljava/lang/Object;

    aput-object p2, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 176
    .local v2, "text":Ljava/lang/String;
    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {p6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 177
    new-instance v3, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$2;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$2;-><init>(Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;)V

    invoke-virtual {p6, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 159
    .end local v2    # "text":Ljava/lang/String;
    :cond_b
    const v3, 0x7f020239

    invoke-virtual {p0, v4, v3}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->loadImage(II)V

    goto :goto_3

    .line 199
    .restart local v0    # "backBtn":Landroid/widget/ImageView;
    .restart local v1    # "selectToContinue":Landroid/widget/TextView;
    :cond_c
    if-eqz v1, :cond_d

    .line 200
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 202
    :cond_d
    if-eqz v0, :cond_4

    .line 203
    const v3, 0x7f02005e

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private tagMountClickEvent(Ljava/lang/String;)V
    .locals 3
    .param p1, "mountPickerMountTypeShort"    # Ljava/lang/String;

    .prologue
    .line 274
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 275
    .local v0, "attributes":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, "Mount_Type"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 277
    const-string v1, "Use_Case"

    .line 278
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->getTrackerScreen()Ljava/lang/String;

    move-result-object v2

    .line 277
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 279
    const-string v1, "Mount_Picker_Selection"

    invoke-static {v1, v0}, Lcom/navdy/client/app/tracking/Tracker;->tagEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 280
    return-void
.end method


# virtual methods
.method public onBackClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->finish()V

    .line 250
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v12, 0x0

    .line 49
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v0, 0x7f03007f

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->setContentView(I)V

    .line 53
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCarYearMakeModelString()Ljava/lang/String;

    move-result-object v10

    .line 54
    .local v10, "yearMakeModelString":Ljava/lang/String;
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    invoke-virtual {v0, v10}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(Ljava/lang/String;)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 56
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_use_case"

    invoke-virtual {v0, v1, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->usecase:I

    .line 58
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "box"

    const-string v11, "Old_Box"

    invoke-interface {v0, v1, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->box:Ljava/lang/String;

    .line 60
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v7

    .line 61
    .local v7, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v0, "vehicle-model"

    const-string v1, ""

    invoke-interface {v7, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "modelString":Ljava/lang/String;
    const v0, 0x7f1001fd

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 64
    .local v3, "shortMountPill":Landroid/widget/TextView;
    const v0, 0x7f100201

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 65
    .local v4, "mediumMountPill":Landroid/widget/TextView;
    const v0, 0x7f1001f6

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 66
    .local v5, "title":Landroid/widget/TextView;
    const v0, 0x7f1001f7

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 68
    .local v6, "desc":Landroid/widget/TextView;
    if-eqz v5, :cond_0

    .line 69
    const v0, 0x7f0802d1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v12

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    :cond_0
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;Ljava/lang/String;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    new-array v1, v12, [Ljava/lang/Void;

    .line 88
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 90
    const v0, 0x7f1001fc

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 91
    .local v9, "shortMountDescription":Landroid/widget/TextView;
    if-eqz v9, :cond_1

    .line 92
    const v0, 0x7f080290

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(I)V

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->box:Ljava/lang/String;

    const-string v1, "Old_Box"

    invoke-static {v0, v1}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97
    const v8, 0x7f0201db

    .line 101
    .local v8, "imageRes":I
    :goto_0
    const v0, 0x7f1001fa

    invoke-virtual {p0, v0, v8}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->loadImage(II)V

    .line 102
    const v0, 0x7f1001fe

    const v1, 0x7f0201f5

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->loadImage(II)V

    .line 103
    return-void

    .line 99
    .end local v8    # "imageRes":I
    :cond_2
    const v8, 0x7f0201dc

    .restart local v8    # "imageRes":I
    goto :goto_0
.end method

.method public onMediumTallMountClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 231
    const-string v1, "Medium_Tall"

    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->tagMountClickEvent(Ljava/lang/String;)V

    .line 234
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->mountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    iget-boolean v1, v1, Lcom/navdy/client/app/framework/models/MountInfo;->mediumSupported:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->mountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    iget-boolean v1, v1, Lcom/navdy/client/app/framework/models/MountInfo;->tallSupported:Z

    if-nez v1, :cond_0

    .line 235
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 236
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_mount"

    sget-object v2, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->TALL:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 245
    :goto_0
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->startActivity(Landroid/content/Intent;)V

    .line 246
    return-void

    .line 238
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->box:Ljava/lang/String;

    const-string v2, "New_Box"

    invoke-static {v1, v2}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 239
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/YouWillNeedTheMountKitActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0

    .line 241
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 242
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "extra_step"

    const v2, 0x7f030072

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 210
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 211
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->hideSystemUI()V

    .line 213
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->getTrackerScreen()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 214
    return-void
.end method

.method public onShortMountClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 217
    const-string v1, "Short"

    invoke-direct {p0, v1}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->tagMountClickEvent(Ljava/lang/String;)V

    .line 220
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->mountInfo:Lcom/navdy/client/app/framework/models/MountInfo;

    iget-boolean v1, v1, Lcom/navdy/client/app/framework/models/MountInfo;->shortSupported:Z

    if-nez v1, :cond_0

    .line 221
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/MountNotRecommendedActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 222
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "extra_mount"

    sget-object v2, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 227
    :goto_0
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->startActivity(Landroid/content/Intent;)V

    .line 228
    return-void

    .line 224
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/MountPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/navdy/client/app/ui/firstlaunch/InstallActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 225
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "extra_step"

    const v2, 0x7f030076

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_0
.end method
