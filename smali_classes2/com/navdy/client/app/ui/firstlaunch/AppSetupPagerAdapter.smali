.class public Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "AppSetupPagerAdapter.java"


# static fields
.field static final BT_SCREEN_INDEX:I = 0x1

.field private static final CACHE_SIZE:I = 0x1400000

.field private static final FADE_DURATION:I = 0xc8

.field private static final IMAGE_MAX_SIZE:I = 0x400000

.field public static final PHONE_SCREEN_INDEX:I = 0x8

.field private static final PROFILE_SCREEN_INDEX:I

.field private static final screens:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;


# instance fields
.field private cache:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field private pretendBtConnected:Z

.field private screenCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 14

    .prologue
    .line 43
    const/16 v0, 0xc

    new-array v12, v0, [Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const/4 v7, 0x0

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f020209

    const v2, 0x7f0801cb

    const v3, 0x7f0801ca

    const v4, 0x7f0801c9

    sget-object v5, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->PROFILE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v7

    const/4 v13, 0x1

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f0201f7

    const v2, 0x7f0201f8

    const v3, 0x7f0801c6

    const v4, 0x7f0801c3

    const v5, 0x7f08010d

    const v6, 0x7f0801c0

    const v7, 0x7f0801c7

    const v8, 0x7f0801c4

    const v9, 0x7f0801c1

    sget-object v10, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v11, 0x1

    invoke-direct/range {v0 .. v11}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIIIIIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v13

    const/4 v7, 0x2

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f0201fc

    const v2, 0x7f0801c8

    const v3, 0x7f0801c5

    const v4, 0x7f0801c2

    sget-object v5, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH_SUCCESS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v7

    const/4 v13, 0x3

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f0201ff

    const v2, 0x7f020205

    const v3, 0x7f0801a6

    const v4, 0x7f0801a4

    const/4 v5, 0x0

    const v6, 0x7f0801a2

    const v7, 0x7f0801a7

    const v8, 0x7f0801a5

    const v9, 0x7f0801a3

    sget-object v10, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->NOTIFICATIONS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v11, 0x1

    invoke-direct/range {v0 .. v11}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIIIIIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v13

    const/4 v11, 0x4

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f020200

    const v2, 0x7f020206

    const v3, 0x7f0801ac

    const v4, 0x7f0801aa

    const v5, 0x7f0801a8

    const v6, 0x7f0801ad

    const v7, 0x7f0801ab

    const v8, 0x7f0801a9

    sget-object v9, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->ACCESS_FINE_LOCATION:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIIIIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v11

    const/4 v13, 0x5

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f020201

    const v2, 0x7f020206

    const v3, 0x7f0801be

    const v4, 0x7f0801bc

    const/4 v5, 0x0

    const v6, 0x7f0801ba

    const v7, 0x7f0801bf

    const v8, 0x7f0801bd

    const v9, 0x7f0801bb

    sget-object v10, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->USE_MICROPHONE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIIIIIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v13

    const/4 v13, 0x6

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f0201fd

    const v2, 0x7f020206

    const v3, 0x7f08019a

    const v4, 0x7f080198

    const/4 v5, 0x0

    const v6, 0x7f080196

    const v7, 0x7f08019b

    const v8, 0x7f080199

    const v9, 0x7f080197

    sget-object v10, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->READ_CONTACTS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIIIIIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v13

    const/4 v13, 0x7

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f02020d

    const v2, 0x7f020206

    const v3, 0x7f0801b8

    const v4, 0x7f0801b6

    const/4 v5, 0x0

    const v6, 0x7f0801b4

    const v7, 0x7f0801b9

    const v8, 0x7f0801b7

    const v9, 0x7f0801b5

    sget-object v10, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->RECEIVE_SMS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIIIIIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v13

    const/16 v13, 0x8

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f020204

    const v2, 0x7f020206

    const v3, 0x7f0801b2

    const v4, 0x7f0801b0

    const/4 v5, 0x0

    const v6, 0x7f0801ae

    const v7, 0x7f0801b3

    const v8, 0x7f0801b1

    const v9, 0x7f0801af

    sget-object v10, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->CALL_PHONE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIIIIIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v13

    const/16 v13, 0x9

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f0201fa

    const v2, 0x7f020206

    const v3, 0x7f080194

    const v4, 0x7f080192

    const/4 v5, 0x0

    const v6, 0x7f080190

    const v7, 0x7f080195

    const v8, 0x7f080193

    const v9, 0x7f080191

    sget-object v10, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->READ_CALENDAR:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIIIIIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v13

    const/16 v11, 0xa

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f02020c

    const v2, 0x7f020206

    const v3, 0x7f0801d0

    const v4, 0x7f0801ce

    const v5, 0x7f0801cc

    const v6, 0x7f0801d1

    const v7, 0x7f0801cf

    const v8, 0x7f0801cd

    sget-object v9, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->WRITE_EXTERNAL_STORAGE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v10, 0x1

    invoke-direct/range {v0 .. v10}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIIIIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v11

    const/16 v7, 0xb

    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    const v1, 0x7f0201fe

    const v2, 0x7f0801a1

    const v3, 0x7f0801a0

    const v4, 0x7f08019f

    sget-object v5, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->END:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;-><init>(IIIILcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;Z)V

    aput-object v0, v12, v7

    sput-object v12, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->screens:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    return-void
.end method

.method constructor <init>(Landroid/support/v4/app/FragmentManager;Lcom/navdy/client/app/framework/util/ImageCache;)V
    .locals 3
    .param p1, "fm"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "imageCache"    # Lcom/navdy/client/app/framework/util/ImageCache;

    .prologue
    .line 179
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 32
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    .line 41
    new-instance v0, Lcom/navdy/client/app/framework/util/ImageCache;

    const/high16 v1, 0x1400000

    const/high16 v2, 0x400000

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/util/ImageCache;-><init>(II)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 175
    new-instance v0, Landroid/util/SparseArray;

    sget-object v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->screens:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    array-length v1, v1

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->cache:Landroid/util/SparseArray;

    .line 176
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->pretendBtConnected:Z

    .line 180
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->recalculateScreenCount()V

    .line 181
    iput-object p2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 182
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;)Lcom/navdy/client/app/framework/util/ImageCache;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    return-object v0
.end method

.method public static getScreen(I)Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    .locals 1
    .param p0, "position"    # I

    .prologue
    .line 307
    if-ltz p0, :cond_0

    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->screens:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 308
    :cond_0
    const/4 v0, 0x0

    .line 310
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->screens:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    aget-object v0, v0, p0

    goto :goto_0
.end method

.method public static getScreensCount()I
    .locals 1

    .prologue
    .line 303
    sget-object v0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->screens:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    array-length v0, v0

    return v0
.end method

.method public static weHavePermissionForThisScreen(Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;)Z
    .locals 4
    .param p0, "screen"    # Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 319
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->PROFILE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v2, v3, :cond_1

    .line 320
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->isUserRegistered()Z

    move-result v0

    .line 345
    :cond_0
    :goto_0
    return v0

    .line 321
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->NOTIFICATIONS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v2, v3, :cond_2

    .line 322
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveNotificationPermission()Z

    move-result v0

    goto :goto_0

    .line 323
    :cond_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->ACCESS_FINE_LOCATION:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v2, v3, :cond_4

    .line 324
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveLocationPermission()Z

    move-result v2

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v2, :cond_0

    .line 325
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->alreadyAskedForLocationPermission()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 326
    :cond_4
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->USE_MICROPHONE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v2, v3, :cond_6

    .line 327
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveMicrophonePermission()Z

    move-result v2

    if-nez v2, :cond_5

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v2, :cond_0

    .line 328
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->alreadyAskedForMicrophonePermission()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0

    .line 329
    :cond_6
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->READ_CONTACTS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v2, v3, :cond_8

    .line 330
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveContactsPermission()Z

    move-result v2

    if-nez v2, :cond_7

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v2, :cond_0

    .line 331
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->alreadyAskedForContactsPermission()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_7
    move v0, v1

    goto :goto_0

    .line 332
    :cond_8
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->RECEIVE_SMS:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v2, v3, :cond_a

    .line 333
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveSmsPermission()Z

    move-result v2

    if-nez v2, :cond_9

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v2, :cond_0

    .line 334
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->alreadyAskedForSmsPermission()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_9
    move v0, v1

    goto :goto_0

    .line 335
    :cond_a
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->CALL_PHONE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v2, v3, :cond_c

    .line 336
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHavePhonePermission()Z

    move-result v2

    if-nez v2, :cond_b

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v2, :cond_0

    .line 337
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->alreadyAskedForPhonePermission()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_b
    move v0, v1

    goto/16 :goto_0

    .line 338
    :cond_c
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->READ_CALENDAR:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v2, v3, :cond_e

    .line 339
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveCalendarPermission()Z

    move-result v2

    if-nez v2, :cond_d

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v2, :cond_0

    .line 340
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->alreadyAskedForCalendarPermission()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_d
    move v0, v1

    goto/16 :goto_0

    .line 341
    :cond_e
    iget-object v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->WRITE_EXTERNAL_STORAGE:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v2, v3, :cond_10

    .line 342
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveStoragePermission()Z

    move-result v2

    if-nez v2, :cond_f

    iget-boolean v2, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->isMandatory:Z

    if-nez v2, :cond_0

    .line 343
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->alreadyAskedForStoragePermission()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_f
    move v0, v1

    goto/16 :goto_0

    :cond_10
    move v0, v1

    .line 345
    goto/16 :goto_0
.end method


# virtual methods
.method clearCache()V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->cache:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 219
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/ImageCache;->clearCache()V

    .line 220
    return-void
.end method

.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 1
    .param p1, "container"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 213
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentStatePagerAdapter;->destroyItem(Landroid/view/View;ILjava/lang/Object;)V

    .line 214
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->cache:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 215
    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 207
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentStatePagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 208
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->cache:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->remove(I)V

    .line 209
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 224
    iget v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->screenCount:I

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 186
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->cache:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/Fragment;

    .line 187
    .local v2, "screen":Landroid/support/v4/app/Fragment;
    if-eqz v2, :cond_0

    move-object v1, v2

    .line 201
    :goto_0
    return-object v1

    .line 191
    :cond_0
    if-nez p1, :cond_1

    .line 192
    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;

    invoke-direct {v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;-><init>()V

    .line 193
    .local v1, "fragment":Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->cache:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 196
    .end local v1    # "fragment":Lcom/navdy/client/app/ui/firstlaunch/AppSetupProfileFragment;
    :cond_1
    new-instance v1, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;

    invoke-direct {v1}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;-><init>()V

    .line 197
    .local v1, "fragment":Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 198
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "screen"

    sget-object v4, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->screens:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    aget-object v4, v4, p1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 199
    invoke-virtual {v1, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupBottomCardGenericFragment;->setArguments(Landroid/os/Bundle;)V

    .line 200
    iget-object v3, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->cache:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method pretendBtConnected()V
    .locals 1

    .prologue
    .line 349
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->pretendBtConnected:Z

    .line 350
    return-void
.end method

.method public recalculateScreenCount()V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 228
    const/4 v1, 0x1

    .line 231
    .local v1, "count":I
    sget-object v6, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->screens:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    array-length v7, v6

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_3

    aget-object v3, v6, v5

    .line 232
    .local v3, "screen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    iget-object v8, v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v9, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->BLUETOOTH:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-ne v8, v9, :cond_2

    .line 233
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    .line 234
    .local v0, "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    iget-boolean v8, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->pretendBtConnected:Z

    if-nez v8, :cond_0

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->isDeviceConnected()Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    const/4 v2, 0x1

    .line 235
    .local v2, "deviceIsConnected":Z
    :goto_1
    if-eqz v2, :cond_3

    .line 236
    add-int/lit8 v1, v1, 0x1

    .line 231
    .end local v0    # "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    .end local v2    # "deviceIsConnected":Z
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .restart local v0    # "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    :cond_1
    move v2, v4

    .line 234
    goto :goto_1

    .line 241
    .end local v0    # "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    :cond_2
    iget-object v8, v3, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->screenType:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    sget-object v9, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;->END:Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen$ScreenType;

    if-eq v8, v9, :cond_3

    .line 242
    invoke-static {v3}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->weHavePermissionForThisScreen(Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 243
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 249
    .end local v3    # "screen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    :cond_3
    iput v1, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->screenCount:I

    .line 250
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->notifyDataSetChanged()V

    .line 251
    return-void
.end method

.method updateIllustration(Ljava/lang/ref/WeakReference;IZ)V
    .locals 10
    .param p2, "position"    # I
    .param p3, "showingFail"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/ImageView;",
            ">;IZ)V"
        }
    .end annotation

    .prologue
    .local p1, "hudRef":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Landroid/widget/ImageView;>;"
    const-wide/16 v8, 0xc8

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 254
    if-nez p1, :cond_1

    .line 255
    iget-object v5, p0, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Trying to call updateIllustration with null layout elements"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 261
    .local v2, "hud":Landroid/widget/ImageView;
    if-eqz v2, :cond_0

    .line 262
    sget-object v5, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;->screens:[Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;

    aget-object v4, v5, p2

    .line 264
    .local v4, "screen":Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;
    if-eqz v4, :cond_0

    .line 267
    if-eqz p3, :cond_2

    .line 268
    iget v3, v4, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudResFail:I

    .line 273
    .local v3, "hudRes":I
    :goto_1
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 274
    .local v1, "fadeOut":Landroid/view/animation/Animation;
    new-instance v5, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 275
    invoke-virtual {v1, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 277
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v7, v6}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 278
    .local v0, "fadeIn":Landroid/view/animation/Animation;
    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v5}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 279
    invoke-virtual {v0, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 281
    new-instance v5, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;

    invoke-direct {v5, p0, v2, v3, v0}, Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/AppSetupPagerAdapter;Landroid/widget/ImageView;ILandroid/view/animation/Animation;)V

    invoke-virtual {v1, v5}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 295
    invoke-virtual {v2}, Landroid/widget/ImageView;->clearAnimation()V

    .line 296
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 297
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 270
    .end local v0    # "fadeIn":Landroid/view/animation/Animation;
    .end local v1    # "fadeOut":Landroid/view/animation/Animation;
    .end local v3    # "hudRes":I
    :cond_2
    iget v3, v4, Lcom/navdy/client/app/ui/firstlaunch/AppSetupScreen;->hudRes:I

    .restart local v3    # "hudRes":I
    goto :goto_1
.end method
