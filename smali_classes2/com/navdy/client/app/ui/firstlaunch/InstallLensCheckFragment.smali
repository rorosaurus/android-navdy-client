.class public Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;
.super Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;
.source "InstallLensCheckFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;
    }
.end annotation


# static fields
.field public static final ANIMATION_DURATION:I = 0x9c4

.field private static final MEDIUM_TALL_MOUNT_IMAGES:[I

.field private static final SHORT_MOUNT_2016_IMAGES:[I

.field private static final SHORT_MOUNT_2017_IMAGES:[I

.field protected static final VERBOSE:Z

.field protected static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

.field private isAnimating:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private mountType:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

.field mountViewPager:Lcom/navdy/client/app/ui/VerticalViewPager;

.field private mountViewPagerAdapter:Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 33
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->logger:Lcom/navdy/service/library/log/Logger;

    .line 40
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->SHORT_MOUNT_2016_IMAGES:[I

    .line 46
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->SHORT_MOUNT_2017_IMAGES:[I

    .line 52
    new-array v0, v2, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->MEDIUM_TALL_MOUNT_IMAGES:[I

    return-void

    .line 40
    :array_0
    .array-data 4
        0x7f020235
        0x7f020237
        0x7f020236
    .end array-data

    .line 46
    :array_1
    .array-data 4
        0x7f020238
        0x7f02023a
        0x7f020239
    .end array-data

    .line 52
    :array_2
    .array-data 4
        0x7f02023b
        0x7f02023d
        0x7f02023c
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;-><init>()V

    .line 38
    new-instance v0, Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/util/ImageCache;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    .line 59
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->isAnimating:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 62
    const v0, 0x7f030070

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->setLayoutId(I)V

    .line 63
    return-void
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;)Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPagerAdapter:Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private animate()V
    .locals 4

    .prologue
    .line 138
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPager:Lcom/navdy/client/app/ui/VerticalViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPagerAdapter:Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->isAnimating:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->isAnimating:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 143
    new-instance v0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$1;-><init>(Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;)V

    .line 155
    .local v0, "moveToNextPage":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0x9c4

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 70
    const v0, 0x7f030070

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->rootView:Landroid/view/View;

    .line 72
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->rootView:Landroid/view/View;

    const v1, 0x7f1001c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/VerticalViewPager;

    iput-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPager:Lcom/navdy/client/app/ui/VerticalViewPager;

    .line 74
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->updateMountType()V

    .line 75
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->rootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/util/ImageCache;->clearCache()V

    .line 174
    invoke-super {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->onDestroy()V

    .line 175
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 161
    iget-object v0, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->isAnimating:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 162
    invoke-super {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->onPause()V

    .line 163
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 167
    invoke-super {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallCardFragment;->onResume()V

    .line 168
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->animate()V

    .line 169
    return-void
.end method

.method public setMountType(Lcom/navdy/client/app/framework/models/MountInfo$MountType;)V
    .locals 0
    .param p1, "type"    # Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountType:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    .line 80
    return-void
.end method

.method public updateMountType()V
    .locals 8

    .prologue
    .line 83
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->rootView:Landroid/view/View;

    if-nez v4, :cond_0

    .line 112
    :goto_0
    return-void

    .line 87
    :cond_0
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1001cc

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 88
    .local v2, "textTooHigh":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1001d1

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 89
    .local v1, "textOk":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->rootView:Landroid/view/View;

    const v5, 0x7f1001d6

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 91
    .local v3, "textTooLow":Landroid/widget/TextView;
    const v4, 0x7f080286

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    const v4, 0x7f080285

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    const v4, 0x7f080287

    invoke-static {v4}, Lcom/navdy/client/app/framework/util/StringUtils;->fromHtml(I)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPager:Lcom/navdy/client/app/ui/VerticalViewPager;

    if-eqz v4, :cond_1

    .line 96
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountType:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    sget-object v5, Lcom/navdy/client/app/framework/models/MountInfo$MountType;->SHORT:Lcom/navdy/client/app/framework/models/MountInfo$MountType;

    if-ne v4, v5, :cond_3

    .line 97
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "box"

    const-string v6, "Old_Box"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 98
    .local v0, "box":Ljava/lang/String;
    const-string v4, "Old_Box"

    invoke-static {v0, v4}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 99
    new-instance v4, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->SHORT_MOUNT_2016_IMAGES:[I

    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v4, v5, v6, v7}, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;-><init>(Landroid/content/Context;[ILcom/navdy/client/app/framework/util/ImageCache;)V

    iput-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPagerAdapter:Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;

    .line 107
    .end local v0    # "box":Ljava/lang/String;
    :goto_1
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPager:Lcom/navdy/client/app/ui/VerticalViewPager;

    iget-object v5, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPagerAdapter:Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/ui/VerticalViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 108
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPager:Lcom/navdy/client/app/ui/VerticalViewPager;

    new-instance v5, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$LensCheckPageListener;-><init>(Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment$1;)V

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/ui/VerticalViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 109
    iget-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPager:Lcom/navdy/client/app/ui/VerticalViewPager;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/navdy/client/app/ui/VerticalViewPager;->setCurrentItem(I)V

    .line 111
    :cond_1
    invoke-direct {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->animate()V

    goto/16 :goto_0

    .line 101
    .restart local v0    # "box":Ljava/lang/String;
    :cond_2
    new-instance v4, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->SHORT_MOUNT_2017_IMAGES:[I

    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v4, v5, v6, v7}, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;-><init>(Landroid/content/Context;[ILcom/navdy/client/app/framework/util/ImageCache;)V

    iput-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPagerAdapter:Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;

    goto :goto_1

    .line 104
    .end local v0    # "box":Ljava/lang/String;
    :cond_3
    new-instance v4, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    sget-object v6, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->MEDIUM_TALL_MOUNT_IMAGES:[I

    iget-object v7, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->imageCache:Lcom/navdy/client/app/framework/util/ImageCache;

    invoke-direct {v4, v5, v6, v7}, Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;-><init>(Landroid/content/Context;[ILcom/navdy/client/app/framework/util/ImageCache;)V

    iput-object v4, p0, Lcom/navdy/client/app/ui/firstlaunch/InstallLensCheckFragment;->mountViewPagerAdapter:Lcom/navdy/client/app/ui/ImageResourcePagerAdaper;

    goto :goto_1
.end method
