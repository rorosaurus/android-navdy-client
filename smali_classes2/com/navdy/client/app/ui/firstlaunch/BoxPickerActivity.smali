.class public Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;
.super Lcom/navdy/client/app/ui/base/BaseToolbarActivity;
.source "BoxPickerActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->finish()V

    .line 74
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const v0, 0x7f030068

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->setContentView(I)V

    .line 30
    new-instance v0, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;-><init>(Lcom/navdy/client/app/ui/base/BaseToolbarActivity;)V

    const v1, 0x7f0803f7

    .line 31
    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->title(I)Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;

    move-result-object v0

    .line 32
    invoke-virtual {v0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity$ToolbarBuilder;->build()Landroid/support/v7/widget/Toolbar;

    .line 34
    const v0, 0x7f1000b1

    const v1, 0x7f02020a

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->loadImage(II)V

    .line 35
    const v0, 0x7f100173

    const v1, 0x7f02020b

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->loadImage(II)V

    .line 36
    const v0, 0x7f100178

    const v1, 0x7f020208

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->loadImage(II)V

    .line 37
    const v0, 0x7f10017d

    const v1, 0x7f020203

    invoke-virtual {p0, v0, v1}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->loadImage(II)V

    .line 38
    return-void
.end method

.method public onNewBoxClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 49
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 50
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "box"

    const-string v4, "New_Box"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 51
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 52
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "extra_box"

    const-string v3, "New_Box"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 53
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->startActivity(Landroid/content/Intent;)V

    .line 54
    return-void
.end method

.method public onNewBoxPlusMountsClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 57
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 58
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "box"

    const-string v4, "New_Box_Plus_Mounts"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 59
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "extra_box"

    const-string v3, "New_Box_Plus_Mounts"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 61
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->startActivity(Landroid/content/Intent;)V

    .line 62
    return-void
.end method

.method public onOldBoxClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 65
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 66
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "box"

    const-string v4, "Old_Box"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 67
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/BoxViewerActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 68
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "extra_box"

    const-string v3, "Old_Box"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseToolbarActivity;->onResume()V

    .line 43
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/firstlaunch/BoxPickerActivity;->hideSystemUI()V

    .line 45
    const-string v0, "Installation_Box_Picker"

    invoke-static {v0}, Lcom/navdy/client/app/tracking/Tracker;->tagScreen(Ljava/lang/String;)V

    .line 46
    return-void
.end method
