.class Lcom/navdy/client/app/ui/search/SearchActivity$13;
.super Ljava/lang/Object;
.source "SearchActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/search/SearchActivity;->updateRecyclerViewWithSearchResults(Lcom/navdy/client/app/framework/search/SearchResults;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

.field final synthetic val$searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

.field final synthetic val$showNoResultsFoundDialog:Z


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/search/SearchActivity;Lcom/navdy/client/app/framework/search/SearchResults;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/search/SearchActivity;

    .prologue
    .line 793
    iput-object p1, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->val$searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    iput-boolean p3, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->val$showNoResultsFoundDialog:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 796
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$1400(Lcom/navdy/client/app/ui/search/SearchActivity;)V

    .line 800
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->val$searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/search/SearchResults;->getContacts()Ljava/util/List;

    move-result-object v0

    .line 801
    .local v0, "contacts":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/ContactModel;>;"
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateRecyclerViewWithContactsForTextSearch(Ljava/util/List;)V

    .line 802
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->val$searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/search/SearchResults;->getGoogleResults()Ljava/util/List;

    move-result-object v2

    .line 803
    .local v2, "googleResults":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;>;"
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateRecyclerViewWithTextSearchResults(Ljava/util/List;)V

    .line 804
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->val$searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/search/SearchResults;->getHudSearchResults()Ljava/util/List;

    move-result-object v3

    .line 805
    .local v3, "hudDestinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/places/PlacesSearchResult;>;"
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateRecyclerViewWithPlaceSearchResults(Ljava/util/List;)V

    .line 806
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->val$searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    invoke-virtual {v6}, Lcom/navdy/client/app/framework/search/SearchResults;->getDestinationsFromDatabase()Ljava/util/ArrayList;

    move-result-object v1

    .line 807
    .local v1, "destinationsFromDatabase":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/navdy/client/app/framework/models/Destination;>;"
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$900(Lcom/navdy/client/app/ui/search/SearchActivity;)Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->val$searchResults:Lcom/navdy/client/app/framework/search/SearchResults;

    invoke-virtual {v7}, Lcom/navdy/client/app/framework/search/SearchResults;->getQuery()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Lcom/navdy/client/app/ui/search/SearchRecyclerAdapter;->updateRecyclerViewWithDatabaseData(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 809
    iget-boolean v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->val$showNoResultsFoundDialog:Z

    if-eqz v6, :cond_0

    .line 810
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/util/List;

    aput-object v0, v7, v5

    aput-object v2, v7, v4

    const/4 v8, 0x2

    aput-object v3, v7, v8

    const/4 v8, 0x3

    aput-object v1, v7, v8

    invoke-static {v6, v7}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$3300(Lcom/navdy/client/app/ui/search/SearchActivity;[Ljava/util/List;)V

    .line 816
    :cond_0
    iget-object v6, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-virtual {v6}, Lcom/navdy/client/app/ui/search/SearchActivity;->hideProgressDialog()V

    .line 817
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_1
    if-eqz v3, :cond_2

    .line 818
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_2
    if-eqz v1, :cond_4

    .line 819
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    .line 820
    .local v4, "showMapFab":Z
    :cond_3
    :goto_0
    iget-object v5, p0, Lcom/navdy/client/app/ui/search/SearchActivity$13;->this$0:Lcom/navdy/client/app/ui/search/SearchActivity;

    invoke-static {v5, v4}, Lcom/navdy/client/app/ui/search/SearchActivity;->access$3400(Lcom/navdy/client/app/ui/search/SearchActivity;Z)V

    .line 821
    return-void

    .end local v4    # "showMapFab":Z
    :cond_4
    move v4, v5

    .line 819
    goto :goto_0
.end method
