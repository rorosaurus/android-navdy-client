.class Lcom/navdy/client/app/ui/routing/ActiveTripActivity$6;
.super Ljava/lang/Object;
.source "ActiveTripActivity.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->onTripProgress(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

.field final synthetic val$progress:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$6;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    iput-object p2, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$6;->val$progress:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 269
    return-void
.end method

.method public onInit(Lcom/here/android/mpa/mapping/Map;)V
    .locals 3
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 257
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$6;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$400(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 258
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$6;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$400(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/here/android/mpa/mapping/Map;->removeMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 261
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$6;->val$progress:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;->getProgress()Lcom/here/android/mpa/common/GeoPolyline;

    move-result-object v0

    .line 262
    .local v0, "progressPolyline":Lcom/here/android/mpa/common/GeoPolyline;
    if-eqz v0, :cond_1

    .line 263
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$6;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v0}, Lcom/navdy/client/app/framework/map/MapUtils;->generateProgressPolyline(Lcom/here/android/mpa/common/GeoPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$402(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;Lcom/here/android/mpa/mapping/MapPolyline;)Lcom/here/android/mpa/mapping/MapPolyline;

    .line 264
    iget-object v1, p0, Lcom/navdy/client/app/ui/routing/ActiveTripActivity$6;->this$0:Lcom/navdy/client/app/ui/routing/ActiveTripActivity;

    invoke-static {v1}, Lcom/navdy/client/app/ui/routing/ActiveTripActivity;->access$400(Lcom/navdy/client/app/ui/routing/ActiveTripActivity;)Lcom/here/android/mpa/mapping/MapPolyline;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/here/android/mpa/mapping/Map;->addMapObject(Lcom/here/android/mpa/mapping/MapObject;)Z

    .line 266
    :cond_1
    return-void
.end method
