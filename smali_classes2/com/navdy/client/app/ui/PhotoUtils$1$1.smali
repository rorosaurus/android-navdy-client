.class Lcom/navdy/client/app/ui/PhotoUtils$1$1;
.super Ljava/lang/Object;
.source "PhotoUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/PhotoUtils$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/PhotoUtils$1;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/PhotoUtils$1;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/PhotoUtils$1;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/navdy/client/app/ui/PhotoUtils$1$1;->this$0:Lcom/navdy/client/app/ui/PhotoUtils$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getUri(Ljava/io/File;)Landroid/net/Uri;
    .locals 3
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 78
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x17

    if-lt v1, v2, :cond_0

    .line 79
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "com.navdy.client.provider"

    invoke-static {v1, v2, p1}, Landroid/support/v4/content/FileProvider;->getUriForFile(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 84
    .local v0, "photoUri":Landroid/net/Uri;
    :goto_0
    return-object v0

    .line 82
    .end local v0    # "photoUri":Landroid/net/Uri;
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .restart local v0    # "photoUri":Landroid/net/Uri;
    goto :goto_0
.end method

.method public run()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 51
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 52
    .local v2, "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/navdy/client/app/ui/PhotoUtils$1$1;->this$0:Lcom/navdy/client/app/ui/PhotoUtils$1;

    iget-object v5, v5, Lcom/navdy/client/app/ui/PhotoUtils$1;->val$logger:Lcom/navdy/service/library/log/Logger;

    iget-object v6, p0, Lcom/navdy/client/app/ui/PhotoUtils$1$1;->this$0:Lcom/navdy/client/app/ui/PhotoUtils$1;

    iget-object v6, v6, Lcom/navdy/client/app/ui/PhotoUtils$1;->val$finalName:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/navdy/service/library/util/IOUtils;->getTempFile(Lcom/navdy/service/library/log/Logger;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    .line 53
    .local v1, "file":Ljava/io/File;
    if-eqz v1, :cond_0

    .line 57
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/PhotoUtils$1$1;->getUri(Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 63
    .local v3, "photoUri":Landroid/net/Uri;
    const-string v5, "output"

    invoke-virtual {v2, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 64
    const-string v5, "return-data"

    invoke-virtual {v2, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 65
    iget-object v5, p0, Lcom/navdy/client/app/ui/PhotoUtils$1$1;->this$0:Lcom/navdy/client/app/ui/PhotoUtils$1;

    iget-object v5, v5, Lcom/navdy/client/app/ui/PhotoUtils$1;->val$logger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "filename: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/navdy/client/app/ui/PhotoUtils$1$1;->this$0:Lcom/navdy/client/app/ui/PhotoUtils$1;

    iget-object v7, v7, Lcom/navdy/client/app/ui/PhotoUtils$1;->val$finalName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 67
    :try_start_1
    iget-object v5, p0, Lcom/navdy/client/app/ui/PhotoUtils$1$1;->this$0:Lcom/navdy/client/app/ui/PhotoUtils$1;

    iget-object v5, v5, Lcom/navdy/client/app/ui/PhotoUtils$1;->val$activity:Lcom/navdy/client/app/ui/base/BaseActivity;

    const/4 v6, 0x1

    invoke-virtual {v5, v2, v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 74
    .end local v3    # "photoUri":Landroid/net/Uri;
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v4

    .line 59
    .local v4, "t":Ljava/lang/Throwable;
    iget-object v5, p0, Lcom/navdy/client/app/ui/PhotoUtils$1$1;->this$0:Lcom/navdy/client/app/ui/PhotoUtils$1;

    iget-object v5, v5, Lcom/navdy/client/app/ui/PhotoUtils$1;->val$logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unable to get a temp file"

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 60
    const v5, 0x7f08048b

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    goto :goto_0

    .line 68
    .end local v4    # "t":Ljava/lang/Throwable;
    .restart local v3    # "photoUri":Landroid/net/Uri;
    :catch_1
    move-exception v0

    .line 69
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const v5, 0x7f0802fd

    new-array v6, v8, [Ljava/lang/Object;

    invoke-static {v5, v6}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v3    # "photoUri":Landroid/net/Uri;
    :cond_0
    iget-object v5, p0, Lcom/navdy/client/app/ui/PhotoUtils$1$1;->this$0:Lcom/navdy/client/app/ui/PhotoUtils$1;

    iget-object v5, v5, Lcom/navdy/client/app/ui/PhotoUtils$1;->val$logger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "PhotoUtils:: temporary file is null"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0
.end method
