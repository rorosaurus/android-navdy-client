.class public Lcom/navdy/client/app/ui/settings/GestureDialogActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "GestureDialogActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public doTryGestureClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 73
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    new-instance v0, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    invoke-direct {v0}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;-><init>()V

    sget-object v1, Lcom/navdy/service/library/events/ui/Screen;->SCREEN_GESTURE_LEARNING:Lcom/navdy/service/library/events/ui/Screen;

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/ShowScreen$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/navdy/service/library/events/ui/ShowScreen$Builder;->build()Lcom/navdy/service/library/events/ui/ShowScreen;

    move-result-object v0

    invoke-static {v0}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    .line 75
    const v0, 0x7f0803a7

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    const v0, 0x7f0802e3

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;->showLongToast(I[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public doWatchVideoClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 65
    const v3, 0x7f0801d8

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 66
    .local v2, "videoUrl":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 67
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/navdy/client/app/ui/firstlaunch/VideoPlayerActivity;

    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 68
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "video_url"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 70
    return-void
.end method

.method public onCloseClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;->onBackPressed()V

    .line 54
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    const v0, 0x7f03004c

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;->setContentView(I)V

    .line 26
    return-void
.end method

.method public onPrimaryButtonClick(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 57
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;->doTryGestureClick(Landroid/view/View;)V

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;->doWatchVideoClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 30
    invoke-super {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;->onResume()V

    .line 32
    const v2, 0x7f10013f

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 33
    .local v0, "primaryButton":Landroid/widget/Button;
    const v2, 0x7f10013e

    invoke-virtual {p0, v2}, Lcom/navdy/client/app/ui/settings/GestureDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 34
    .local v1, "secondaryButton":Landroid/widget/Button;
    invoke-static {}, Lcom/navdy/client/app/framework/DeviceConnection;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 35
    if-eqz v0, :cond_0

    .line 36
    const v2, 0x7f0804c7

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 38
    :cond_0
    if-eqz v1, :cond_1

    .line 39
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 50
    :cond_1
    :goto_0
    return-void

    .line 42
    :cond_2
    if-eqz v0, :cond_3

    .line 43
    const v2, 0x7f0804f6

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(I)V

    .line 45
    :cond_3
    if-eqz v1, :cond_1

    .line 46
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method
