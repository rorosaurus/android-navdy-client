.class public Lcom/navdy/client/app/ui/settings/CalendarViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "CalendarViewHolder.java"


# instance fields
.field private calendarName:Landroid/widget/TextView;

.field private calendarOwner:Landroid/widget/TextView;

.field private checkMark:Landroid/widget/ImageView;

.field private color:Landroid/view/View;

.field private rootView:Landroid/view/View;

.field private final sharedPrefs:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 27
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->sharedPrefs:Landroid/content/SharedPreferences;

    .line 32
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->rootView:Landroid/view/View;

    .line 33
    const v0, 0x7f100103

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->color:Landroid/view/View;

    .line 34
    const v0, 0x7f100105

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->calendarName:Landroid/widget/TextView;

    .line 35
    const v0, 0x7f100106

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->calendarOwner:Landroid/widget/TextView;

    .line 36
    const v0, 0x7f100104

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->checkMark:Landroid/widget/ImageView;

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/settings/CalendarViewHolder;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->sharedPrefs:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/settings/CalendarViewHolder;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/settings/CalendarViewHolder;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->checkMark:Landroid/widget/ImageView;

    return-object v0
.end method

.method private isEnabled(Lcom/navdy/client/app/framework/models/Calendar;)Z
    .locals 4
    .param p1, "calendar"    # Lcom/navdy/client/app/framework/models/Calendar;

    .prologue
    .line 86
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "calendar_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Calendar;->id:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 87
    .local v0, "sharedPrefKey":Ljava/lang/String;
    iget-object v1, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->sharedPrefs:Landroid/content/SharedPreferences;

    iget-boolean v2, p1, Lcom/navdy/client/app/framework/models/Calendar;->visible:Z

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public setCalendar(Lcom/navdy/client/app/framework/models/Calendar;)V
    .locals 9
    .param p1, "calendar"    # Lcom/navdy/client/app/framework/models/Calendar;

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 40
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->color:Landroid/view/View;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->calendarName:Landroid/widget/TextView;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->calendarOwner:Landroid/widget/TextView;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->checkMark:Landroid/widget/ImageView;

    if-nez v5, :cond_1

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 50
    .local v1, "context":Landroid/content/Context;
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->color:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 51
    .local v0, "bgShape":Landroid/graphics/drawable/GradientDrawable;
    iget v5, p1, Lcom/navdy/client/app/framework/models/Calendar;->calendarColor:I

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 54
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->sharedPrefs:Landroid/content/SharedPreferences;

    const-string v6, "calendars_enabled"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 58
    .local v3, "globalSwitchIsEnabled":Z
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->calendarName:Landroid/widget/TextView;

    iget-object v6, p1, Lcom/navdy/client/app/framework/models/Calendar;->displayName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 60
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->calendarOwner:Landroid/widget/TextView;

    const v6, 0x7f080475

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, p1, Lcom/navdy/client/app/framework/models/Calendar;->accountName:Ljava/lang/String;

    aput-object v8, v7, v4

    invoke-virtual {v1, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    invoke-direct {p0, p1}, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->isEnabled(Lcom/navdy/client/app/framework/models/Calendar;)Z

    move-result v2

    .line 63
    .local v2, "enabled":Z
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->checkMark:Landroid/widget/ImageView;

    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 65
    iget-object v5, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->rootView:Landroid/view/View;

    if-eqz v3, :cond_3

    const/high16 v4, 0x3f800000    # 1.0f

    :goto_2
    invoke-virtual {v5, v4}, Landroid/view/View;->setAlpha(F)V

    .line 66
    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/CalendarViewHolder;->rootView:Landroid/view/View;

    new-instance v5, Lcom/navdy/client/app/ui/settings/CalendarViewHolder$1;

    invoke-direct {v5, p0, p1}, Lcom/navdy/client/app/ui/settings/CalendarViewHolder$1;-><init>(Lcom/navdy/client/app/ui/settings/CalendarViewHolder;Lcom/navdy/client/app/framework/models/Calendar;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 63
    :cond_2
    const/16 v4, 0x8

    goto :goto_1

    .line 65
    :cond_3
    const v4, 0x3e99999a    # 0.3f

    goto :goto_2
.end method
