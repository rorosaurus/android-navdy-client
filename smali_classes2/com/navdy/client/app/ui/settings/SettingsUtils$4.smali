.class final Lcom/navdy/client/app/ui/settings/SettingsUtils$4;
.super Ljava/lang/Object;
.source "SettingsUtils.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/SettingsUtils;->setFeatureMode(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$featureMode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;


# direct methods
.method constructor <init>(Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;)V
    .locals 0

    .prologue
    .line 299
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$4;->val$featureMode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 302
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->incrementDriverProfileSerial()V

    .line 304
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "feature-mode"

    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/SettingsUtils$4;->val$featureMode:Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;

    .line 305
    invoke-virtual {v2}, Lcom/navdy/service/library/events/preferences/DriverProfilePreferences$FeatureMode;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 306
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 308
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendDriverProfileToTheHudBasedOnSharedPrefValue()Z

    .line 309
    return-void
.end method
