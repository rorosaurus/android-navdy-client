.class Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;
.super Ljava/lang/Object;
.source "OtaSettingsActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->onDownloadProgress(Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;JB)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

.field final synthetic val$percent:B

.field final synthetic val$progress:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

.field final synthetic val$totalDownloaded:J


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;BLcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    .prologue
    .line 294
    iput-object p1, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    iput-byte p2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->val$percent:B

    iput-object p3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->val$progress:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    iput-wide p4, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->val$totalDownloaded:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v4, 0x3

    .line 301
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-virtual {v2}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->isInForeground()Z

    move-result v2

    if-nez v2, :cond_0

    .line 337
    :goto_0
    return-void

    .line 304
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const v3, 0x7f080160

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 306
    .local v1, "title":Ljava/lang/String;
    sget-object v2, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$9;->$SwitchMap$com$navdy$client$ota$OTAUpdateListener$DownloadUpdateStatus:[I

    iget-object v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->val$progress:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    invoke-virtual {v3}, Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 334
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-static {v2}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$400(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;)Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported progress: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->val$progress:Lcom/navdy/client/ota/OTAUpdateListener$DownloadUpdateStatus;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 308
    :pswitch_0
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const v3, 0x7f08013b

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 309
    .local v0, "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-virtual {v2, v4, v1, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 313
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_1
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/SystemUtils;->isAirplaneModeOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 314
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const v3, 0x7f0800b4

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 315
    .restart local v0    # "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-virtual {v2, v4, v1, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 317
    .end local v0    # "message":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const v3, 0x7f0802fe

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 318
    .restart local v0    # "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-virtual {v2, v4, v1, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 323
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_2
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const v3, 0x7f08030e

    invoke-virtual {v2, v3}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 324
    .restart local v0    # "message":Ljava/lang/String;
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    invoke-virtual {v2, v4, v1, v0}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->showSimpleDialog(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 328
    .end local v0    # "message":Ljava/lang/String;
    :pswitch_3
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    iget-byte v3, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->val$percent:B

    iget-wide v4, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->val$totalDownloaded:J

    invoke-static {v2, v3, v4, v5}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$300(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;IJ)V

    goto/16 :goto_0

    .line 331
    :pswitch_4
    iget-object v2, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->this$0:Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;

    const/16 v3, 0x64

    iget-wide v4, p0, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity$3;->val$totalDownloaded:J

    invoke-static {v2, v3, v4, v5}, Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;->access$300(Lcom/navdy/client/app/ui/settings/OtaSettingsActivity;IJ)V

    goto/16 :goto_0

    .line 306
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
