.class public Lcom/navdy/client/app/ui/settings/SettingsConstants$ProfilePreferences;
.super Ljava/lang/Object;
.source "SettingsConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/settings/SettingsConstants;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ProfilePreferences"
.end annotation


# static fields
.field public static final CAR_MAKE:Ljava/lang/String; = "vehicle-make"

.field public static final CAR_MANUAL_ENTRY:Ljava/lang/String; = "vehicle-data-entry-manual"

.field public static final CAR_MODEL:Ljava/lang/String; = "vehicle-model"

.field public static final CAR_OBD_LOCATION_ACCESS_NOTE:Ljava/lang/String; = "car_obd_location_access_note"

.field public static final CAR_OBD_LOCATION_NOTE:Ljava/lang/String; = "car_obd_location_note"

.field public static final CAR_OBD_LOCATION_NUM:Ljava/lang/String; = "CAR_OBD_LOCATION_NUM"

.field public static final CAR_YEAR:Ljava/lang/String; = "vehicle-year"

.field public static final CUSTOMER_PREF_FILE:Ljava/lang/String; = "tracker"

.field public static final EMAIL:Ljava/lang/String; = "email"

.field public static final FULL_NAME:Ljava/lang/String; = "fname"

.field public static final OBD_IMAGE_FILE_NAME:Ljava/lang/String; = "obdImage"

.field public static final SERIAL_NUM:Ljava/lang/String; = "profile_serial_number"

.field public static final SERIAL_NUM_DEFAULT:Ljava/lang/Long;

.field public static final SKIPPED_SIGNUP:Ljava/lang/String; = "skipped"

.field public static final UNIT_SYSTEM:Ljava/lang/String; = "unit-system"

.field public static final UNIT_SYSTEM_DEFAULT:Ljava/lang/String; = ""

.field public static final USER_PHOTO_FILE_NAME:Ljava/lang/String; = "userPhoto"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 223
    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sput-object v0, Lcom/navdy/client/app/ui/settings/SettingsConstants$ProfilePreferences;->SERIAL_NUM_DEFAULT:Ljava/lang/Long;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
