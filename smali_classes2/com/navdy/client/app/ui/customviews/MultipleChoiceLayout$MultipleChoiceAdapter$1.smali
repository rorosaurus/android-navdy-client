.class Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;
.super Ljava/lang/Object;
.source "MultipleChoiceLayout.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->onBindViewHolder(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

.field final synthetic val$position:I

.field final synthetic val$text:Ljava/lang/CharSequence;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;ILjava/lang/CharSequence;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    .prologue
    .line 100
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;->this$1:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    iput p2, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;->val$position:I

    iput-object p3, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;->val$text:Ljava/lang/CharSequence;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;->this$1:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    iget v1, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;->val$position:I

    invoke-virtual {v0, v1}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->setSelectedIndex(I)V

    .line 104
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;->this$1:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    iget-object v0, v0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->this$0:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    invoke-static {v0}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->access$200(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;)Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;->this$1:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    iget-object v0, v0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->this$0:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    invoke-static {v0}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->access$200(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;)Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;

    move-result-object v1

    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;->val$text:Ljava/lang/CharSequence;

    check-cast v0, Ljava/lang/String;

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;->val$position:I

    invoke-interface {v1, v0, v2}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;->onChoiceSelected(Ljava/lang/String;I)V

    .line 107
    :cond_0
    return-void
.end method
