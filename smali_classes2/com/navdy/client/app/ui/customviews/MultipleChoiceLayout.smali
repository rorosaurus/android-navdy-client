.class public Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;
.super Landroid/support/v7/widget/RecyclerView;
.source "MultipleChoiceLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;,
        Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;,
        Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;
    }
.end annotation


# instance fields
.field private adapter:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

.field private choiceListener:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;

.field private layoutManager:Landroid/support/v7/widget/RecyclerView$LayoutManager;

.field private regularTextColor:I

.field private selectedColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 36
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    sget-object v3, Lcom/navdy/client/R$styleable;->MultipleChoiceLayout:[I

    invoke-virtual {p1, p2, v3, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 42
    .local v0, "customAttributes":Landroid/content/res/TypedArray;
    new-instance v3, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    invoke-direct {v3, p0}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;-><init>(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;)V

    iput-object v3, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->adapter:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    .line 43
    iget-object v3, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->adapter:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 44
    const/4 v1, 0x0

    .line 45
    .local v1, "spanCount":I
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0, v4}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v2

    .line 47
    .local v2, "texts":[Ljava/lang/CharSequence;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 48
    iget-object v3, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->adapter:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    invoke-virtual {v3, v2}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->setTexts([Ljava/lang/CharSequence;)V

    .line 49
    array-length v1, v2

    .line 51
    .end local v2    # "texts":[Ljava/lang/CharSequence;
    :cond_0
    new-instance v3, Landroid/support/v7/widget/GridLayoutManager;

    invoke-direct {v3, p1, v1}, Landroid/support/v7/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->layoutManager:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    .line 52
    iget-object v3, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->layoutManager:Landroid/support/v7/widget/RecyclerView$LayoutManager;

    invoke-virtual {p0, v3}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f005d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->regularTextColor:I

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0092

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    iput v3, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->selectedColor:I

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    .prologue
    .line 22
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->selectedColor:I

    return v0
.end method

.method static synthetic access$100(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;)I
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    .prologue
    .line 22
    iget v0, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->regularTextColor:I

    return v0
.end method

.method static synthetic access$200(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;)Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->choiceListener:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->getAdapter()Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->adapter:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    return-object v0
.end method

.method public setChoiceListener(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;)V
    .locals 0
    .param p1, "choiceListener"    # Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->choiceListener:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceListener;

    .line 137
    return-void
.end method

.method public setSelectedIndex(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->adapter:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->setSelectedIndex(I)V

    .line 124
    return-void
.end method
