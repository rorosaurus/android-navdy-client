.class public Lcom/navdy/client/app/ui/customviews/DestinationImageView;
.super Landroid/widget/ImageView;
.source "DestinationImageView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;
    }
.end annotation


# static fields
.field private static final EMPTY:Ljava/lang/String; = ""

.field private static final SPACE:Ljava/lang/String; = " "

.field private static greyColor:I

.field private static largeStyleTextSize:I

.field private static smallStyleTextSize:I

.field private static typefaceLarge:Landroid/graphics/Typeface;

.field private static typefaceSmall:Landroid/graphics/Typeface;

.field private static valuesSet:Z

.field private static whiteColor:I


# instance fields
.field private contactImageHelper:Lcom/navdy/client/app/framework/util/ContactImageHelper;

.field private initials:Ljava/lang/String;

.field private paint:Landroid/graphics/Paint;

.field private style:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 40
    sget-object v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;->DEFAULT:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->style:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    .line 58
    invoke-direct {p0}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->init()V

    .line 59
    return-void
.end method

.method public static createCircleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v6, 0x0

    const/4 v12, 0x0

    const/high16 v11, 0x40000000    # 2.0f

    .line 140
    if-nez p0, :cond_0

    move-object v3, v6

    .line 166
    :goto_0
    return-object v3

    .line 144
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    .line 145
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0092

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 146
    .local v5, "width":I
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b008f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 149
    .local v2, "height":I
    const/4 v7, 0x0

    invoke-static {p0, v5, v2, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p0

    .line 152
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v2, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 155
    .local v3, "newBitmap":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 156
    .local v4, "path":Landroid/graphics/Path;
    int-to-float v7, v5

    div-float/2addr v7, v11

    int-to-float v8, v2

    div-float/2addr v8, v11

    int-to-float v9, v5

    int-to-float v10, v2

    div-float/2addr v10, v11

    invoke-static {v9, v10}, Ljava/lang/Math;->min(FF)F

    move-result v9

    sget-object v10, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 159
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 162
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v0, v4}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 165
    invoke-virtual {v0, p0, v12, v12, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public static declared-synchronized getInitials(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 108
    const-class v5, Lcom/navdy/client/app/ui/customviews/DestinationImageView;

    monitor-enter v5

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    .local v0, "builder":Ljava/lang/StringBuilder;
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 110
    const-string v4, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :goto_0
    monitor-exit v5

    return-object v4

    .line 112
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 113
    const-string v4, " "

    invoke-virtual {p0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 114
    .local v2, "index":I
    if-lez v2, :cond_3

    .line 115
    const/4 v4, 0x0

    invoke-virtual {p0, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 116
    .local v1, "first":Ljava/lang/String;
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 117
    .local v3, "last":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 118
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 119
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 121
    :cond_1
    invoke-static {v3}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 122
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 124
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 126
    .end local v1    # "first":Ljava/lang/String;
    .end local v3    # "last":Ljava/lang/String;
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_4

    .line 127
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 129
    :cond_4
    const-string v4, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 108
    .end local v0    # "builder":Ljava/lang/StringBuilder;
    .end local v2    # "index":I
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4
.end method

.method private init()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 62
    invoke-static {}, Lcom/navdy/client/app/framework/util/ContactImageHelper;->getInstance()Lcom/navdy/client/app/framework/util/ContactImageHelper;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->contactImageHelper:Lcom/navdy/client/app/framework/util/ContactImageHelper;

    .line 63
    sget-boolean v1, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->valuesSet:Z

    if-nez v1, :cond_0

    .line 64
    sput-boolean v3, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->valuesSet:Z

    .line 65
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 66
    .local v0, "resources":Landroid/content/res/Resources;
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f008f

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    sput v1, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->greyColor:I

    .line 67
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f00cc

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    sput v1, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->whiteColor:I

    .line 68
    const v1, 0x7f0b0091

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->smallStyleTextSize:I

    .line 69
    const v1, 0x7f0b0090

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->largeStyleTextSize:I

    .line 70
    const-string v1, "sans-serif-medium"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->typefaceSmall:Landroid/graphics/Typeface;

    .line 71
    const-string v1, "sans-serif"

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    sput-object v1, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->typefaceLarge:Landroid/graphics/Typeface;

    .line 73
    .end local v0    # "resources":Landroid/content/res/Resources;
    :cond_0
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->paint:Landroid/graphics/Paint;

    .line 74
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->paint:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 75
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 76
    return-void
.end method


# virtual methods
.method public clearInitials()V
    .locals 1

    .prologue
    .line 135
    const-string v0, ""

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->initials:Ljava/lang/String;

    .line 136
    sget-object v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;->DEFAULT:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->style:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    .line 137
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 171
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 172
    iget-object v4, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->style:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    sget-object v5, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;->DEFAULT:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    if-ne v4, v5, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->getWidth()I

    move-result v1

    .line 179
    .local v1, "width":I
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->getHeight()I

    move-result v0

    .line 182
    .local v0, "height":I
    iget-object v4, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->initials:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 183
    iget-object v4, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->paint:Landroid/graphics/Paint;

    sget v5, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->whiteColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 184
    iget-object v4, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->paint:Landroid/graphics/Paint;

    sget v5, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->largeStyleTextSize:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 185
    iget-object v4, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->paint:Landroid/graphics/Paint;

    sget-object v5, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->typefaceLarge:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 186
    iget-object v4, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->paint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 187
    int-to-float v4, v1

    div-float v2, v4, v7

    .line 188
    .local v2, "x":F
    int-to-float v4, v0

    div-float/2addr v4, v7

    iget-object v5, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->descent()F

    move-result v5

    iget-object v6, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->ascent()F

    move-result v6

    add-float/2addr v5, v6

    div-float/2addr v5, v7

    sub-float v3, v4, v5

    .line 189
    .local v3, "y":F
    iget-object v4, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->initials:Ljava/lang/String;

    iget-object v5, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v2, v3, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setImage(ILjava/lang/String;)V
    .locals 2
    .param p1, "resourceId"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 102
    invoke-static {p2}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->getInitials(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "initials":Ljava/lang/String;
    sget-object v1, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;->INITIALS:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    invoke-virtual {p0, p1, v0, v1}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImage(ILjava/lang/String;Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;)V

    .line 104
    return-void
.end method

.method public setImage(ILjava/lang/String;Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;)V
    .locals 0
    .param p1, "resourceId"    # I
    .param p2, "initials"    # Ljava/lang/String;
    .param p3, "style"    # Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImageResource(I)V

    .line 86
    invoke-virtual {p0, p2, p3}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setInitials(Ljava/lang/String;Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;)V

    .line 87
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->clearInitials()V

    .line 91
    invoke-static {p1}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->createCircleBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 92
    .local v0, "circleBitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 93
    return-void
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 96
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->contactImageHelper:Lcom/navdy/client/app/framework/util/ContactImageHelper;

    invoke-virtual {v2, p1}, Lcom/navdy/client/app/framework/util/ContactImageHelper;->getContactImageIndex(Ljava/lang/String;)I

    move-result v0

    .line 97
    .local v0, "contactImageIndex":I
    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->contactImageHelper:Lcom/navdy/client/app/framework/util/ContactImageHelper;

    invoke-virtual {v2, v0}, Lcom/navdy/client/app/framework/util/ContactImageHelper;->getResourceId(I)I

    move-result v1

    .line 98
    .local v1, "resourceId":I
    invoke-virtual {p0, v1, p1}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->setImage(ILjava/lang/String;)V

    .line 99
    return-void
.end method

.method public setInitials(Ljava/lang/String;Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;)V
    .locals 0
    .param p1, "initials"    # Ljava/lang/String;
    .param p2, "style"    # Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->initials:Ljava/lang/String;

    .line 80
    iput-object p2, p0, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->style:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    .line 81
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/DestinationImageView;->invalidate()V

    .line 82
    return-void
.end method
