.class public final enum Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;
.super Ljava/lang/Enum;
.source "DestinationImageView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/customviews/DestinationImageView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Style"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

.field public static final enum DEFAULT:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

.field public static final enum INITIALS:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 44
    new-instance v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    const-string v1, "INITIALS"

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;->INITIALS:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    .line 45
    new-instance v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;->DEFAULT:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    .line 43
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    sget-object v1, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;->INITIALS:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;->DEFAULT:Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    aput-object v1, v0, v3

    sput-object v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;->$VALUES:[Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 43
    const-class v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;->$VALUES:[Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    invoke-virtual {v0}, [Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/ui/customviews/DestinationImageView$Style;

    return-object v0
.end method
