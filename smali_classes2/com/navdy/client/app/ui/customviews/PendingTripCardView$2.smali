.class Lcom/navdy/client/app/ui/customviews/PendingTripCardView$2;
.super Ljava/lang/Object;
.source "PendingTripCardView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->setUpClickListener(Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$Error;Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$NavdyRouteInfo;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/customviews/PendingTripCardView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$2;->this$0:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 65
    invoke-static {}, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "onClick, retrying route"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 66
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView$2;->this$0:Lcom/navdy/client/app/ui/customviews/PendingTripCardView;

    iget-object v0, v0, Lcom/navdy/client/app/ui/customviews/PendingTripCardView;->navdyRouteHandler:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;->retryRoute()V

    .line 67
    return-void
.end method
