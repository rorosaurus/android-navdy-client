.class public Lcom/navdy/client/app/ui/customviews/ProgressCircle;
.super Landroid/view/View;
.source "ProgressCircle.java"


# instance fields
.field private endAngleNaturalized:F

.field private final logger:Lcom/navdy/service/library/log/Logger;

.field private oval:Landroid/graphics/RectF;

.field private paint:Landroid/graphics/Paint;

.field private startAngle:F

.field private strokeWidth:F

.field private sweepAngle:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/navdy/client/app/ui/customviews/ProgressCircle;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/navdy/client/app/ui/customviews/ProgressCircle;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "ProgressCircle(Context, Attrs)"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 23
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->logger:Lcom/navdy/service/library/log/Logger;

    .line 25
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->paint:Landroid/graphics/Paint;

    .line 26
    const/high16 v0, 0x41f00000    # 30.0f

    iput v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->strokeWidth:F

    .line 27
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->oval:Landroid/graphics/RectF;

    .line 28
    const/high16 v0, 0x42b40000    # 90.0f

    iput v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->startAngle:F

    .line 29
    const/high16 v0, 0x43870000    # 270.0f

    iput v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->sweepAngle:F

    .line 31
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->endAngleNaturalized:F

    .line 44
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "progressCircle initialized"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->paint:Landroid/graphics/Paint;

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0f0012

    invoke-static {v1, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 46
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->strokeWidth:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 47
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 48
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFlags(I)V

    .line 49
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/ui/customviews/ProgressCircle;)Lcom/navdy/service/library/log/Logger;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/ProgressCircle;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$102(Lcom/navdy/client/app/ui/customviews/ProgressCircle;F)F
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/app/ui/customviews/ProgressCircle;
    .param p1, "x1"    # F

    .prologue
    .line 21
    iput p1, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->sweepAngle:F

    return p1
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 67
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onDraw"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->oval:Landroid/graphics/RectF;

    iget v1, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->strokeWidth:F

    div-float/2addr v1, v6

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->strokeWidth:F

    div-float/2addr v2, v6

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->getWidth()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->strokeWidth:F

    div-float/2addr v4, v6

    sub-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->getWidth()I

    move-result v4

    int-to-float v4, v4

    iget v5, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->strokeWidth:F

    div-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 70
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->oval:Landroid/graphics/RectF;

    iget v2, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->startAngle:F

    iget v3, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->sweepAngle:F

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 71
    return-void
.end method

.method public setProgress(F)V
    .locals 3
    .param p1, "progress"    # F

    .prologue
    .line 57
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setProgress to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 58
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 59
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Value must be between 0 and 1: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_1
    iput p1, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->endAngleNaturalized:F

    .line 62
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->invalidate()V

    .line 63
    return-void
.end method

.method public setProgressColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "setProgressColor"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 53
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 54
    return-void
.end method

.method public startAnimation()V
    .locals 4

    .prologue
    .line 74
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "startAnimation"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 75
    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    iget v3, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->sweepAngle:F

    aput v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/navdy/client/app/ui/customviews/ProgressCircle;->endAngleNaturalized:F

    aput v3, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 76
    .local v0, "valueAnimator":Landroid/animation/ValueAnimator;
    new-instance v1, Lcom/navdy/client/app/ui/customviews/ProgressCircle$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/ui/customviews/ProgressCircle$1;-><init>(Lcom/navdy/client/app/ui/customviews/ProgressCircle;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 84
    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 85
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 86
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 87
    return-void
.end method
