.class Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "MultipleChoiceLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MultipleChoiceAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private selectedIndex:I

.field private texts:[Ljava/lang/CharSequence;

.field final synthetic this$0:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    .prologue
    .line 71
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->this$0:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->texts:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->texts:[Ljava/lang/CharSequence;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 71
    check-cast p1, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->onBindViewHolder(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;I)V
    .locals 3
    .param p1, "holder"    # Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 90
    iget-object v1, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->texts:[Ljava/lang/CharSequence;

    aget-object v0, v1, p2

    .line 91
    .local v0, "text":Ljava/lang/CharSequence;
    iget-object v1, p1, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget v1, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->selectedIndex:I

    if-ne p2, v1, :cond_0

    .line 93
    iget-object v1, p1, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;->textView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->this$0:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    invoke-static {v2}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->access$000(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 94
    iget-object v1, p1, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;->highlightView:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 99
    :goto_0
    iget-object v1, p1, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;->itemView:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 100
    iget-object v1, p1, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;->itemView:Landroid/view/View;

    new-instance v2, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;

    invoke-direct {v2, p0, p2, v0}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter$1;-><init>(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;ILjava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 109
    return-void

    .line 96
    :cond_0
    iget-object v1, p1, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;->textView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->this$0:Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;

    invoke-static {v2}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;->access$100(Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 97
    iget-object v1, p1, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;->highlightView:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 83
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030032

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 84
    .local v0, "choiceLayout":Landroid/view/ViewGroup;
    new-instance v1, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;

    invoke-direct {v1, v0}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;-><init>(Landroid/view/View;)V

    .line 85
    .local v1, "choiceViewHolder":Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$ChoiceViewHolder;
    return-object v1
.end method

.method public setSelectedIndex(I)V
    .locals 0
    .param p1, "selectedIndex"    # I

    .prologue
    .line 117
    iput p1, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->selectedIndex:I

    .line 118
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->notifyDataSetChanged()V

    .line 119
    return-void
.end method

.method public setTexts([Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "texts"    # [Ljava/lang/CharSequence;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->texts:[Ljava/lang/CharSequence;

    .line 78
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/customviews/MultipleChoiceLayout$MultipleChoiceAdapter;->notifyDataSetChanged()V

    .line 79
    return-void
.end method
