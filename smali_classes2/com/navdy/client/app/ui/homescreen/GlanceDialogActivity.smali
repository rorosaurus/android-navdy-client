.class public Lcom/navdy/client/app/ui/homescreen/GlanceDialogActivity;
.super Lcom/navdy/client/app/ui/base/BaseActivity;
.source "GlanceDialogActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/navdy/client/app/ui/base/BaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCloseClick(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/GlanceDialogActivity;->onBackPressed()V

    .line 29
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 17
    invoke-super {p0, p1}, Lcom/navdy/client/app/ui/base/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    const v0, 0x7f03004d

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/ui/homescreen/GlanceDialogActivity;->setContentView(I)V

    .line 21
    return-void
.end method

.method public sendATestGlance(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/GlanceDialogActivity;->logger:Lcom/navdy/service/library/log/Logger;

    invoke-static {p0, v0}, Lcom/navdy/client/app/ui/glances/GlanceUtils;->sendTestGlance(Lcom/navdy/client/app/ui/base/BaseActivity;Lcom/navdy/service/library/log/Logger;)V

    .line 25
    return-void
.end method
