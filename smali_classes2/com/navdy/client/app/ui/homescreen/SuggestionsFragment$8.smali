.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$8;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Lcom/navdy/client/app/ui/base/BaseHereMapFragment$OnHereMapFragmentInitialized;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->addTripDestinationMarkerAndCenterMap(Lcom/navdy/client/app/framework/models/Destination;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 692
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$8;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$8;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;)V
    .locals 0
    .param p1, "error"    # Lcom/navdy/client/app/ui/base/BaseHereMapFragment$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 703
    return-void
.end method

.method public onInit(Lcom/here/android/mpa/mapping/Map;)V
    .locals 3
    .param p1, "hereMap"    # Lcom/here/android/mpa/mapping/Map;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 695
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$8;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$8;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v0, p1, v1}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$700(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/here/android/mpa/mapping/Map;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 696
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$8;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-static {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->access$800(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)Lcom/navdy/client/app/ui/base/BaseHereMapFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$8;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    sget-object v2, Lcom/here/android/mpa/mapping/Map$Animation;->LINEAR:Lcom/here/android/mpa/mapping/Map$Animation;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/ui/base/BaseHereMapFragment;->centerOnUserLocationAndDestination(Lcom/navdy/client/app/framework/models/Destination;Lcom/here/android/mpa/mapping/Map$Animation;)V

    .line 700
    return-void
.end method
