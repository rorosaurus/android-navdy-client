.class Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$1;
.super Ljava/lang/Object;
.source "RoutedSuggestionsViewHolder.java"

# interfaces
.implements Lcom/navdy/client/app/framework/navigation/HereRouteManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->calculateEtas(Lcom/navdy/client/app/framework/models/Suggestion;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

.field final synthetic val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;Lcom/navdy/client/app/framework/models/Suggestion;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$1;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    iput-object p2, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$1;->val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreCalculation(Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;)V
    .locals 0
    .param p1, "routeHandle"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$RouteHandle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 88
    return-void
.end method

.method public onRouteCalculated(Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;Lcom/here/android/mpa/routing/Route;)V
    .locals 2
    .param p1, "error"    # Lcom/navdy/client/app/framework/navigation/HereRouteManager$Error;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "route"    # Lcom/here/android/mpa/routing/Route;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$1;->this$0:Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder$1;->val$suggestion:Lcom/navdy/client/app/framework/models/Suggestion;

    invoke-static {v0, p2, v1}, Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;->access$000(Lcom/navdy/client/app/ui/homescreen/RoutedSuggestionsViewHolder;Lcom/here/android/mpa/routing/Route;Lcom/navdy/client/app/framework/models/Suggestion;)V

    .line 94
    return-void
.end method
