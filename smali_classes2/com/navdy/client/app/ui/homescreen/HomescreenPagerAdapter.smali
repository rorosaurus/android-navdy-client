.class Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "HomescreenPagerAdapter.java"


# static fields
.field private static final FAVORITES_FRAGMENT_POSITION:I = 0x1

.field private static final GLANCES_FRAGMENT_POSITION:I = 0x2

.field private static final HOME_FRAGMENT_POSITION:I = 0x0

.field private static final TOTAL_FRAGMENTS:I = 0x4

.field private static final TRIPS_FRAGMENT_POSITION:I = 0x3

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private final fragmentExists:[Ljava/lang/Boolean;

.field private final fragmentList:[Landroid/support/v4/app/Fragment;

.field private final fragmentNames:[Ljava/lang/String;

.field private hiddenFragmentsCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentManager;)V
    .locals 6
    .param p1, "manager"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 38
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 31
    new-array v1, v2, [Landroid/support/v4/app/Fragment;

    iput-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentList:[Landroid/support/v4/app/Fragment;

    .line 32
    new-array v1, v2, [Ljava/lang/String;

    iput-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentNames:[Ljava/lang/String;

    .line 33
    new-array v1, v2, [Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentExists:[Ljava/lang/Boolean;

    .line 35
    iput v4, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->hiddenFragmentsCount:I

    .line 39
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 41
    .local v0, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentList:[Landroid/support/v4/app/Fragment;

    new-instance v2, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-direct {v2}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;-><init>()V

    aput-object v2, v1, v3

    .line 42
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentNames:[Ljava/lang/String;

    const v2, 0x7f080241

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    .line 44
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentList:[Landroid/support/v4/app/Fragment;

    new-instance v2, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;

    invoke-direct {v2}, Lcom/navdy/client/app/ui/favorites/FavoritesFragment;-><init>()V

    aput-object v2, v1, v4

    .line 45
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentNames:[Ljava/lang/String;

    const v2, 0x7f080173

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    .line 47
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentList:[Landroid/support/v4/app/Fragment;

    new-instance v2, Lcom/navdy/client/app/ui/glances/GlancesFragment;

    invoke-direct {v2}, Lcom/navdy/client/app/ui/glances/GlancesFragment;-><init>()V

    aput-object v2, v1, v5

    .line 48
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentNames:[Ljava/lang/String;

    const v2, 0x7f0801dd

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 50
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentExists:[Ljava/lang/Boolean;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 51
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 77
    return-void
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentList:[Landroid/support/v4/app/Fragment;

    array-length v0, v0

    iget v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->hiddenFragmentsCount:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentList:[Landroid/support/v4/app/Fragment;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 92
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentNames:[Ljava/lang/String;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentExists:[Ljava/lang/Boolean;

    aget-object v0, v0, p2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentExists:[Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, p2

    .line 65
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    .line 67
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentList:[Landroid/support/v4/app/Fragment;

    aget-object v0, v0, p2

    goto :goto_0
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 97
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentPagerAdapter;->setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 98
    return-void
.end method

.method public showHiddenTabs()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 101
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 103
    .local v0, "context":Landroid/content/Context;
    const/4 v1, 0x0

    iput v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->hiddenFragmentsCount:I

    .line 104
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentNames:[Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f080176

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 106
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentList:[Landroid/support/v4/app/Fragment;

    new-instance v2, Lcom/navdy/client/app/ui/trips/TripsFragment;

    invoke-direct {v2}, Lcom/navdy/client/app/ui/trips/TripsFragment;-><init>()V

    aput-object v2, v1, v5

    .line 107
    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->fragmentNames:[Ljava/lang/String;

    const v2, 0x7f0804c5

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    .line 109
    invoke-virtual {p0}, Lcom/navdy/client/app/ui/homescreen/HomescreenPagerAdapter;->notifyDataSetChanged()V

    .line 110
    return-void
.end method
