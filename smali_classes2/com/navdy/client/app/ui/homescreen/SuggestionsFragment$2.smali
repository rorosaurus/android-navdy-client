.class Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$2;
.super Ljava/lang/Object;
.source "SuggestionsFragment.java"

# interfaces
.implements Lcom/navdy/client/app/framework/util/CustomSuggestionsLongItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$2;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Landroid/view/View;ILcom/navdy/client/app/framework/models/Suggestion;)Z
    .locals 6
    .param p1, "selectedCardRow"    # Landroid/view/View;
    .param p2, "actionModePosition"    # I
    .param p3, "suggestion"    # Lcom/navdy/client/app/framework/models/Suggestion;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$2;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    invoke-virtual {v0}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;

    .line 175
    .local v3, "homescreenActivity":Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 176
    :cond_0
    const/4 v0, 0x0

    .line 186
    :goto_0
    return v0

    .line 180
    :cond_1
    new-instance v0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;

    iget-object v1, p0, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$2;->this$0:Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;

    move-object v2, p3

    move v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment$CustomSuggestionsActionCallback;-><init>(Lcom/navdy/client/app/ui/homescreen/SuggestionsFragment;Lcom/navdy/client/app/framework/models/Suggestion;Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;ILandroid/view/View;)V

    invoke-virtual {v3, v0}, Lcom/navdy/client/app/ui/homescreen/HomescreenActivity;->startSelectionActionMode(Landroid/support/v7/view/ActionMode$Callback;)V

    .line 186
    const/4 v0, 0x1

    goto :goto_0
.end method
