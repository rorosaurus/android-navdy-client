.class public Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes$FAVORITE_TYPE_VALUES;
.super Ljava/lang/Object;
.source "TrackerConstants.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/tracking/TrackerConstants$Attributes$DestinationAttributes;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "FAVORITE_TYPE_VALUES"
.end annotation


# static fields
.field private static final CONTACT:Ljava/lang/String; = "Contact"

.field private static final HOME:Ljava/lang/String; = "Home"

.field private static final NONE:Ljava/lang/String; = "None"

.field private static final OTHER:Ljava/lang/String; = "Other"

.field private static final WORK:Ljava/lang/String; = "Work"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFavoriteTypeValue(Lcom/navdy/client/app/framework/models/Destination;)Ljava/lang/String;
    .locals 2
    .param p0, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 128
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_0

    .line 129
    const-string v0, "Home"

    .line 137
    :goto_0
    return-object v0

    .line 130
    :cond_0
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_1

    .line 131
    const-string v0, "Work"

    goto :goto_0

    .line 132
    :cond_1
    iget v0, p0, Lcom/navdy/client/app/framework/models/Destination;->favoriteType:I

    const/4 v1, -0x4

    if-ne v0, v1, :cond_2

    .line 133
    const-string v0, "Contact"

    goto :goto_0

    .line 134
    :cond_2
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/models/Destination;->isFavoriteDestination()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 135
    const-string v0, "Other"

    goto :goto_0

    .line 137
    :cond_3
    const-string v0, "None"

    goto :goto_0
.end method
