.class public Lcom/navdy/client/app/framework/DeviceConnection;
.super Ljava/lang/Object;
.source "DeviceConnection.java"

# interfaces
.implements Lcom/navdy/service/library/device/RemoteDevice$Listener;
.implements Lcom/navdy/client/app/framework/AppInstance$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/DeviceConnection$DeviceInfoEvent;,
        Lcom/navdy/client/app/framework/DeviceConnection$DeviceDisconnectedEvent;,
        Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectionFailedEvent;,
        Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectedEvent;,
        Lcom/navdy/client/app/framework/DeviceConnection$DeviceConnectingEvent;,
        Lcom/navdy/client/app/framework/DeviceConnection$DeviceRelatedEvent;
    }
.end annotation


# static fields
.field private static final instance:Lcom/navdy/client/app/framework/DeviceConnection;


# instance fields
.field private handler:Landroid/os/Handler;

.field private logger:Lcom/navdy/service/library/log/Logger;

.field private remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/navdy/client/app/framework/DeviceConnection;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/DeviceConnection;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/DeviceConnection;->instance:Lcom/navdy/client/app/framework/DeviceConnection;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v1, Lcom/navdy/service/library/log/Logger;

    const-class v2, Lcom/navdy/client/app/framework/DeviceConnection;

    invoke-direct {v1, v2}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/DeviceConnection;->logger:Lcom/navdy/service/library/log/Logger;

    .line 36
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/DeviceConnection;->handler:Landroid/os/Handler;

    .line 38
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v0

    .line 39
    .local v0, "appInstance":Lcom/navdy/client/app/framework/AppInstance;
    invoke-virtual {v0, p0}, Lcom/navdy/client/app/framework/AppInstance;->addListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 41
    invoke-virtual {v0}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/navdy/client/app/framework/DeviceConnection;->onDeviceChanged(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 42
    return-void
.end method

.method private declared-synchronized close()V
    .locals 1

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice;->removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :cond_0
    monitor-exit p0

    return-void

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized getInstance()Lcom/navdy/client/app/framework/DeviceConnection;
    .locals 2

    .prologue
    .line 45
    const-class v0, Lcom/navdy/client/app/framework/DeviceConnection;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lcom/navdy/client/app/framework/DeviceConnection;->instance:Lcom/navdy/client/app/framework/DeviceConnection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static isConnected()Z
    .locals 2

    .prologue
    .line 74
    sget-object v1, Lcom/navdy/client/app/framework/DeviceConnection;->instance:Lcom/navdy/client/app/framework/DeviceConnection;

    monitor-enter v1

    .line 75
    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/DeviceConnection;->instance:Lcom/navdy/client/app/framework/DeviceConnection;

    iget-object v0, v0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/navdy/client/app/framework/DeviceConnection;->instance:Lcom/navdy/client/app/framework/DeviceConnection;

    iget-object v0, v0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static postEvent(Lcom/navdy/service/library/events/NavdyEvent;)Z
    .locals 2
    .param p0, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 86
    sget-object v1, Lcom/navdy/client/app/framework/DeviceConnection;->instance:Lcom/navdy/client/app/framework/DeviceConnection;

    monitor-enter v1

    .line 87
    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/DeviceConnection;->instance:Lcom/navdy/client/app/framework/DeviceConnection;

    iget-object v0, v0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/navdy/client/app/framework/DeviceConnection;->instance:Lcom/navdy/client/app/framework/DeviceConnection;

    iget-object v0, v0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/navdy/service/library/events/NavdyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static postEvent(Lcom/squareup/wire/Message;)Z
    .locals 2
    .param p0, "message"    # Lcom/squareup/wire/Message;

    .prologue
    .line 80
    sget-object v1, Lcom/navdy/client/app/framework/DeviceConnection;->instance:Lcom/navdy/client/app/framework/DeviceConnection;

    monitor-enter v1

    .line 81
    :try_start_0
    sget-object v0, Lcom/navdy/client/app/framework/DeviceConnection;->instance:Lcom/navdy/client/app/framework/DeviceConnection;

    iget-object v0, v0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/navdy/client/app/framework/DeviceConnection;->instance:Lcom/navdy/client/app/framework/DeviceConnection;

    iget-object v0, v0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public declared-synchronized getConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .locals 1

    .prologue
    .line 49
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->getActiveConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 52
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getConnectionStatus()Lcom/navdy/service/library/device/connection/Connection$Status;
    .locals 1

    .prologue
    .line 56
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/RemoteDevice;->getConnectionStatus()Lcom/navdy/service/library/device/connection/Connection$Status;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 59
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/navdy/service/library/device/connection/Connection$Status;->DISCONNECTED:Lcom/navdy/service/library/device/connection/Connection$Status;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getConnectionType()Lcom/navdy/service/library/device/connection/ConnectionType;
    .locals 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/DeviceConnection;->getConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;

    move-result-object v0

    .line 65
    .local v0, "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v1

    .line 69
    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    goto :goto_0
.end method

.method public declared-synchronized onDeviceChanged(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 1
    .param p1, "newDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 102
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice;->removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 105
    :cond_0
    iput-object p1, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    .line 106
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->remoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v0, p0}, Lcom/navdy/service/library/device/RemoteDevice;->addListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    :cond_1
    monitor-exit p0

    return-void

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onDeviceConnectFailure(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V
    .locals 3
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connection status changed: CONNECT_FAIL | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/DeviceConnection$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/client/app/framework/DeviceConnection$3;-><init>(Lcom/navdy/client/app/framework/DeviceConnection;Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$ConnectionFailureCause;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 208
    return-void
.end method

.method public onDeviceConnected(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 3
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 189
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connection status changed: CONNECTED | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/DeviceConnection$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/DeviceConnection$2;-><init>(Lcom/navdy/client/app/framework/DeviceConnection;Lcom/navdy/service/library/device/RemoteDevice;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 197
    return-void
.end method

.method public onDeviceConnecting(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 3
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connection status changed: CONNECTING | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/DeviceConnection$1;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/app/framework/DeviceConnection$1;-><init>(Lcom/navdy/client/app/framework/DeviceConnection;Lcom/navdy/service/library/device/RemoteDevice;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 185
    return-void
.end method

.method public onDeviceDisconnected(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V
    .locals 3
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "cause"    # Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->logger:Lcom/navdy/service/library/log/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Connection status changed: DISCONNECTED | "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/navdy/client/app/framework/DeviceConnection;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/app/framework/DeviceConnection$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/navdy/client/app/framework/DeviceConnection$4;-><init>(Lcom/navdy/client/app/framework/DeviceConnection;Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/device/connection/Connection$DisconnectCause;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 220
    return-void
.end method

.method public onNavdyEventReceived(Lcom/navdy/service/library/device/RemoteDevice;Lcom/navdy/service/library/events/NavdyEvent;)V
    .locals 3
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "event"    # Lcom/navdy/service/library/events/NavdyEvent;

    .prologue
    .line 235
    invoke-static {p2}, Lcom/navdy/service/library/events/NavdyEventUtil;->messageFromEvent(Lcom/navdy/service/library/events/NavdyEvent;)Lcom/squareup/wire/Message;

    move-result-object v0

    .line 237
    .local v0, "message":Lcom/squareup/wire/Message;
    if-eqz v0, :cond_0

    .line 238
    iget-object v1, p0, Lcom/navdy/client/app/framework/DeviceConnection;->handler:Landroid/os/Handler;

    new-instance v2, Lcom/navdy/client/app/framework/DeviceConnection$5;

    invoke-direct {v2, p0, v0}, Lcom/navdy/client/app/framework/DeviceConnection$5;-><init>(Lcom/navdy/client/app/framework/DeviceConnection;Lcom/squareup/wire/Message;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 245
    :cond_0
    return-void
.end method

.method public onNavdyEventReceived(Lcom/navdy/service/library/device/RemoteDevice;[B)V
    .locals 0
    .param p1, "device"    # Lcom/navdy/service/library/device/RemoteDevice;
    .param p2, "event"    # [B

    .prologue
    .line 227
    return-void
.end method
