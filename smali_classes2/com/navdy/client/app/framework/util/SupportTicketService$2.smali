.class Lcom/navdy/client/app/framework/util/SupportTicketService$2;
.super Ljava/lang/Object;
.source "SupportTicketService.java"

# interfaces
.implements Lcom/navdy/client/app/framework/util/SubmitTicketWorker$OnFinishSubmittingTickets;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/util/SupportTicketService;->submitPendingTickets()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/util/SupportTicketService;

.field final synthetic val$ticketsDirectory:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/util/SupportTicketService;Ljava/io/File;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/util/SupportTicketService;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$2;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService;

    iput-object p2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$2;->val$ticketsDirectory:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFinish(IZ)V
    .locals 6
    .param p1, "failCount"    # I
    .param p2, "showSuccessNotification"    # Z

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 285
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$2;->val$ticketsDirectory:Ljava/io/File;

    sget-object v3, Lcom/navdy/client/app/framework/util/SupportTicketService;->filterOnReadyFolders:Ljava/io/FilenameFilter;

    invoke-virtual {v2, v3}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v1

    .line 286
    .local v1, "remainingFolders":[Ljava/io/File;
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 287
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz v1, :cond_2

    array-length v2, v1

    if-nez v2, :cond_2

    .line 288
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "no pending tickets remaining"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 289
    const-string v2, "pending_tickets_exist"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 290
    const-string v2, "tickets_awaiting_hud_log_exist"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 298
    :cond_0
    :goto_0
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 300
    if-nez p1, :cond_3

    .line 301
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Folders successfully submitted."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 302
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->resetRetryCount()V

    .line 303
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$2;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/util/SupportTicketService;->endTicketNotifications()V

    .line 304
    if-eqz p2, :cond_1

    .line 305
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$2;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/util/SupportTicketService;->buildNotificationForSuccess()V

    .line 315
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$2;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/util/SupportTicketService;->incrementUpdateDate()V

    .line 316
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$2;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService;

    invoke-virtual {v2, v5}, Lcom/navdy/client/app/framework/util/SupportTicketService;->finishService(Z)V

    .line 317
    return-void

    .line 292
    :cond_2
    sget-object v2, Lcom/navdy/client/app/framework/util/SupportTicketService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "pending tickets awaiting hud logs exist"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 293
    const-string v2, "pending_tickets_exist"

    invoke-interface {v0, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 294
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->isBelowMaxRetryCount()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 295
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->access$000()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/util/SupportTicketService;->scheduleAlarmForSubmittingPendingTickets(J)V

    goto :goto_0

    .line 308
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$2;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/util/SupportTicketService;->buildNotificationForRetry()V

    .line 309
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->isBelowMaxRetryCount()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 310
    invoke-static {}, Lcom/navdy/client/app/framework/util/SupportTicketService;->access$000()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/util/SupportTicketService;->scheduleAlarmForSubmittingPendingTickets(J)V

    .line 312
    :cond_4
    iget-object v2, p0, Lcom/navdy/client/app/framework/util/SupportTicketService$2;->this$0:Lcom/navdy/client/app/framework/util/SupportTicketService;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/util/SupportTicketService;->incrementTryCount()V

    goto :goto_1
.end method
