.class public Lcom/navdy/client/app/framework/util/CalendarObserver;
.super Landroid/database/ContentObserver;
.source "CalendarObserver.java"


# static fields
.field private static final DELAY_THRESHOLD:I = 0x2710

.field private static final VERBOSE:Z

.field private static final logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private handler:Landroid/os/Handler;

.field private lastSendTime:J

.field private sendRunnable:Ljava/lang/Runnable;

.field private shouldSendToHud:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/CalendarObserver;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/CalendarObserver;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 38
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 20
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->lastSendTime:J

    .line 21
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->shouldSendToHud:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 24
    new-instance v0, Lcom/navdy/client/app/framework/util/CalendarObserver$1;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/util/CalendarObserver$1;-><init>(Lcom/navdy/client/app/framework/util/CalendarObserver;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->sendRunnable:Ljava/lang/Runnable;

    .line 39
    iput-object p1, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->handler:Landroid/os/Handler;

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/app/framework/util/CalendarObserver;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/CalendarObserver;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->shouldSendToHud:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic access$100()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/navdy/client/app/framework/util/CalendarObserver;->logger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$202(Lcom/navdy/client/app/framework/util/CalendarObserver;J)J
    .locals 1
    .param p0, "x0"    # Lcom/navdy/client/app/framework/util/CalendarObserver;
    .param p1, "x1"    # J

    .prologue
    .line 16
    iput-wide p1, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->lastSendTime:J

    return-wide p1
.end method


# virtual methods
.method public onChange(Z)V
    .locals 6
    .param p1, "selfChange"    # Z

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 51
    monitor-enter p0

    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->shouldSendToHud:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 56
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->lastSendTime:J

    const-wide/16 v4, 0x2710

    add-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->sendRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 59
    :cond_0
    monitor-exit p0

    .line 60
    return-void

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public postSendCalendarRunnable()V
    .locals 4

    .prologue
    .line 43
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->sendRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 44
    iget-object v0, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/navdy/client/app/framework/util/CalendarObserver;->sendRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 45
    return-void
.end method
