.class public Lcom/navdy/client/app/framework/util/GmsUtils;
.super Ljava/lang/Object;
.source "GmsUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;
    }
.end annotation


# static fields
.field private static final LATEST_VERSION_OF_GMS:I = 0x99dea0

.field private static sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/util/GmsUtils;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/util/GmsUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkIfMissingOrOutOfDate()Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v12, -0x1

    .line 38
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v8

    .line 39
    .local v8, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v9, "gms_version_number"

    invoke-interface {v8, v9, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 41
    .local v4, "gmsVersion":I
    const-string v9, "gms_is_missing"

    invoke-interface {v8, v9, v13}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 44
    .local v3, "gmsIsMissing":Z
    sget-object v9, Lcom/navdy/client/app/framework/util/GmsUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "From Shared Prefs: GMS version code is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " and gmsIsMissing is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 46
    if-nez v3, :cond_0

    if-eq v4, v12, :cond_0

    .line 49
    invoke-static {v4}, Lcom/navdy/client/app/framework/util/GmsUtils;->isUpToDateGmsVersion(I)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 50
    sget-object v9, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->UP_TO_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    .line 84
    :goto_0
    return-object v9

    .line 53
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    .line 54
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 56
    .local v7, "packageManager":Landroid/content/pm/PackageManager;
    if-eqz v7, :cond_3

    .line 57
    const/16 v9, 0x80

    invoke-virtual {v7, v9}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v6

    .line 58
    .local v6, "packageList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const/4 v2, 0x0

    .line 60
    .local v2, "found":Z
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/pm/PackageInfo;

    .line 61
    .local v5, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v10, v5, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    const-string v11, "com.google.android.gms"

    invoke-static {v10, v11}, Lcom/navdy/client/app/framework/util/StringUtils;->equalsOrBothEmptyAfterTrim(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 62
    const/4 v2, 0x1

    .line 63
    sget-object v9, Lcom/navdy/client/app/framework/util/GmsUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Found GMS. version code is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " and versionName is "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v5, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 64
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 65
    .local v1, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v9, "gms_version_number"

    iget v10, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-interface {v1, v9, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 66
    const-string v9, "gms_is_missing"

    invoke-interface {v1, v9, v13}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 67
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 69
    iget v9, v5, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v9}, Lcom/navdy/client/app/framework/util/GmsUtils;->isUpToDateGmsVersion(I)Z

    move-result v9

    if-nez v9, :cond_2

    .line 70
    sget-object v9, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->OUT_OF_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    goto :goto_0

    .line 75
    .end local v1    # "edit":Landroid/content/SharedPreferences$Editor;
    .end local v5    # "packageInfo":Landroid/content/pm/PackageInfo;
    :cond_2
    if-nez v2, :cond_3

    .line 76
    sget-object v9, Lcom/navdy/client/app/framework/util/GmsUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v10, "Could not find GMS !"

    invoke-virtual {v9, v10}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 77
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 78
    .restart local v1    # "edit":Landroid/content/SharedPreferences$Editor;
    const-string v9, "gms_is_missing"

    const/4 v10, 0x1

    invoke-interface {v1, v9, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 79
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 81
    sget-object v9, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->MISSING:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    goto/16 :goto_0

    .line 84
    .end local v1    # "edit":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "found":Z
    .end local v6    # "packageList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    :cond_3
    sget-object v9, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->UP_TO_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    goto/16 :goto_0
.end method

.method public static finishIfGmsIsNotUpToDate(Landroid/app/Activity;)Z
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-static {}, Lcom/navdy/client/app/framework/util/GmsUtils;->checkIfMissingOrOutOfDate()Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    move-result-object v0

    .line 96
    .local v0, "gmsState":Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;
    sget-object v2, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->UP_TO_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    if-eq v0, v2, :cond_1

    .line 97
    sget-object v2, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->OUT_OF_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    if-ne v0, v2, :cond_3

    .line 98
    sget-object v2, Lcom/navdy/client/app/framework/util/GmsUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "OUTDATED GOOGLE PLAY SERVICES!"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 99
    const v2, 0x7f080329

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    .line 105
    :cond_0
    :goto_0
    if-eqz p0, :cond_4

    .line 106
    sget-object v2, Lcom/navdy/client/app/framework/util/GmsUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Killing activity, GMS error"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 115
    :cond_1
    :goto_1
    sget-object v2, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->UP_TO_DATE:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    if-eq v0, v2, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1

    .line 100
    :cond_3
    sget-object v2, Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;->MISSING:Lcom/navdy/client/app/framework/util/GmsUtils$GmsState;

    if-ne v0, v2, :cond_0

    .line 101
    sget-object v2, Lcom/navdy/client/app/framework/util/GmsUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "NO GOOGLE PLAY SERVICES!"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 102
    const v2, 0x7f080300

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/navdy/client/app/ui/base/BaseActivity;->showLongToast(I[Ljava/lang/Object;)V

    goto :goto_0

    .line 110
    :cond_4
    sget-object v2, Lcom/navdy/client/app/framework/util/GmsUtils;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "KILLING APP, GMS ERROR"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 111
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v2

    invoke-static {v2}, Landroid/os/Process;->killProcess(I)V

    goto :goto_1
.end method

.method public static isUpToDateGmsVersion(I)Z
    .locals 1
    .param p0, "versionCode"    # I

    .prologue
    .line 88
    const/4 v0, -0x1

    if-eq p0, v0, :cond_0

    const v0, 0x99dea0

    if-lt p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
