.class public Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PhoneCallReceiver.java"


# static fields
.field private static currentStatus:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private buildPhoneEvent(Landroid/content/Context;Ljava/lang/String;Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "number"    # Ljava/lang/String;
    .param p3, "status"    # Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .prologue
    .line 71
    const-string v0, ""

    .line 72
    .local v0, "name":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 73
    invoke-static {p1, p2}, Lcom/navdy/client/debug/util/Contacts;->lookupNameFromPhoneNumber(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    :cond_0
    new-instance v1, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    invoke-direct {v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;-><init>()V

    .line 76
    invoke-virtual {v1, p2}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->number(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    move-result-object v1

    .line 77
    invoke-virtual {v1, p3}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->status(Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    move-result-object v1

    .line 78
    invoke-virtual {v1, v0}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->contact_name(Ljava/lang/String;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;

    move-result-object v1

    .line 79
    invoke-virtual {v1}, Lcom/navdy/service/library/events/callcontrol/PhoneEvent$Builder;->build()Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    move-result-object v1

    return-object v1
.end method

.method public static getCurrentStatus()Lcom/navdy/service/library/events/callcontrol/PhoneEvent;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->currentStatus:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    return-object v0
.end method

.method private sendEvent(Landroid/content/Context;Lcom/navdy/service/library/events/callcontrol/PhoneEvent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    .prologue
    .line 83
    new-instance v2, Lcom/navdy/service/library/events/NavdyEvent$Builder;

    invoke-direct {v2}, Lcom/navdy/service/library/events/NavdyEvent$Builder;-><init>()V

    sget-object v3, Lcom/navdy/service/library/events/NavdyEvent$MessageType;->PhoneEvent:Lcom/navdy/service/library/events/NavdyEvent$MessageType;

    .line 84
    invoke-virtual {v2, v3}, Lcom/navdy/service/library/events/NavdyEvent$Builder;->type(Lcom/navdy/service/library/events/NavdyEvent$MessageType;)Lcom/navdy/service/library/events/NavdyEvent$Builder;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/Ext_NavdyEvent;->phoneEvent:Lcom/squareup/wire/Extension;

    .line 85
    invoke-virtual {v2, v3, p2}, Lcom/navdy/service/library/events/NavdyEvent$Builder;->setExtension(Lcom/squareup/wire/Extension;Ljava/lang/Object;)Lcom/navdy/service/library/events/NavdyEvent$Builder;

    move-result-object v2

    .line 86
    invoke-virtual {v2}, Lcom/navdy/service/library/events/NavdyEvent$Builder;->build()Lcom/navdy/service/library/events/NavdyEvent;

    move-result-object v1

    .line 88
    .local v1, "event":Lcom/navdy/service/library/events/NavdyEvent;
    invoke-static {}, Lcom/navdy/client/app/framework/AppInstance;->getInstance()Lcom/navdy/client/app/framework/AppInstance;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/AppInstance;->getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;

    move-result-object v0

    .line 89
    .local v0, "device":Lcom/navdy/service/library/device/RemoteDevice;
    if-eqz v0, :cond_0

    .line 90
    invoke-virtual {v0, v1}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/navdy/service/library/events/NavdyEvent;)Z

    .line 92
    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 28
    if-nez p2, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 33
    :cond_1
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "action":Ljava/lang/String;
    const-string v7, "android.intent.action.NEW_OUTGOING_CALL"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 37
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->getResultData()Ljava/lang/String;

    move-result-object v3

    .line 38
    .local v3, "phoneNumber":Ljava/lang/String;
    if-nez v3, :cond_2

    .line 40
    const-string v7, "android.intent.extra.PHONE_NUMBER"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 42
    :cond_2
    sget-object v7, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "New outgoing call "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 43
    sget-object v7, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_DIALING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    invoke-direct {p0, p1, v3, v7}, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->buildPhoneEvent(Landroid/content/Context;Ljava/lang/String;Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    move-result-object v7

    invoke-direct {p0, p1, v7}, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->sendEvent(Landroid/content/Context;Lcom/navdy/service/library/events/callcontrol/PhoneEvent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 65
    .end local v0    # "action":Ljava/lang/String;
    .end local v3    # "phoneNumber":Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 66
    .local v6, "t":Ljava/lang/Throwable;
    sget-object v7, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v7, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 44
    .end local v6    # "t":Ljava/lang/Throwable;
    .restart local v0    # "action":Ljava/lang/String;
    :cond_3
    :try_start_1
    const-string v7, "android.intent.action.PHONE_STATE"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 45
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 46
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_7

    .line 47
    const-string v7, "state"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 48
    .local v4, "stateStr":Ljava/lang/String;
    const-string v7, "incoming_number"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 49
    .local v2, "number":Ljava/lang/String;
    sget-object v5, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 51
    .local v5, "status":Lcom/navdy/service/library/events/callcontrol/PhoneStatus;
    sget-object v7, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Phone state change: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 52
    sget-object v7, Landroid/telephony/TelephonyManager;->EXTRA_STATE_IDLE:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 53
    sget-object v5, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_IDLE:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    .line 59
    :cond_4
    :goto_1
    invoke-direct {p0, p1, v2, v5}, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->buildPhoneEvent(Landroid/content/Context;Ljava/lang/String;Lcom/navdy/service/library/events/callcontrol/PhoneStatus;)Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    move-result-object v7

    sput-object v7, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->currentStatus:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    .line 60
    sget-object v7, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->currentStatus:Lcom/navdy/service/library/events/callcontrol/PhoneEvent;

    invoke-direct {p0, p1, v7}, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->sendEvent(Landroid/content/Context;Lcom/navdy/service/library/events/callcontrol/PhoneEvent;)V

    goto/16 :goto_0

    .line 54
    :cond_5
    sget-object v7, Landroid/telephony/TelephonyManager;->EXTRA_STATE_OFFHOOK:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 55
    sget-object v5, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_OFFHOOK:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    goto :goto_1

    .line 56
    :cond_6
    sget-object v7, Landroid/telephony/TelephonyManager;->EXTRA_STATE_RINGING:Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 57
    sget-object v5, Lcom/navdy/service/library/events/callcontrol/PhoneStatus;->PHONE_RINGING:Lcom/navdy/service/library/events/callcontrol/PhoneStatus;

    goto :goto_1

    .line 62
    .end local v2    # "number":Ljava/lang/String;
    .end local v4    # "stateStr":Ljava/lang/String;
    .end local v5    # "status":Lcom/navdy/service/library/events/callcontrol/PhoneStatus;
    :cond_7
    sget-object v7, Lcom/navdy/client/app/framework/receiver/PhoneCallReceiver;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v8, "no extras for phone event"

    invoke-virtual {v7, v8}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
