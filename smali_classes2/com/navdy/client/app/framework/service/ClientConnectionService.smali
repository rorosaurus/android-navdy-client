.class public Lcom/navdy/client/app/framework/service/ClientConnectionService;
.super Lcom/navdy/service/library/device/connection/ConnectionService;
.source "ClientConnectionService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/service/ClientConnectionService$LocalBinder;,
        Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;
    }
.end annotation


# instance fields
.field private final binder:Landroid/os/IBinder;

.field private deviceChangeBroadcaster:Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;

.field private serviceHandlers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;-><init>()V

    .line 65
    new-instance v0, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;

    invoke-direct {v0}, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->deviceChangeBroadcaster:Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;

    .line 135
    new-instance v0, Lcom/navdy/client/app/framework/service/ClientConnectionService$LocalBinder;

    invoke-direct {v0, p0}, Lcom/navdy/client/app/framework/service/ClientConnectionService$LocalBinder;-><init>(Lcom/navdy/client/app/framework/service/ClientConnectionService;)V

    iput-object v0, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->binder:Landroid/os/IBinder;

    return-void
.end method


# virtual methods
.method public addListener(Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->deviceChangeBroadcaster:Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;->addListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 69
    return-void
.end method

.method protected createProxyService()Lcom/navdy/service/library/device/connection/ProxyService;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 124
    new-instance v0, Lcom/navdy/proxy/ProxyListener;

    new-instance v1, Lcom/navdy/service/library/network/BTSocketAcceptor;

    const-string v2, "Navdy-Proxy-Tunnel"

    sget-object v3, Lcom/navdy/client/app/framework/service/ClientConnectionService;->NAVDY_PROXY_TUNNEL_UUID:Ljava/util/UUID;

    invoke-direct {v1, v2, v3}, Lcom/navdy/service/library/network/BTSocketAcceptor;-><init>(Ljava/lang/String;Ljava/util/UUID;)V

    invoke-direct {v0, v1}, Lcom/navdy/proxy/ProxyListener;-><init>(Lcom/navdy/service/library/network/SocketAcceptor;)V

    return-object v0
.end method

.method protected getConnectionListeners(Landroid/content/Context;)[Lcom/navdy/service/library/device/connection/ConnectionListener;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/navdy/service/library/device/connection/ConnectionListener;

    const/4 v1, 0x0

    new-instance v2, Lcom/navdy/service/library/device/connection/AcceptorListener;

    new-instance v3, Lcom/navdy/service/library/network/BTSocketAcceptor;

    const-string v4, "Navdy"

    sget-object v5, Lcom/navdy/client/app/framework/service/ClientConnectionService;->NAVDY_PROTO_SERVICE_UUID:Ljava/util/UUID;

    invoke-direct {v3, v4, v5}, Lcom/navdy/service/library/network/BTSocketAcceptor;-><init>(Ljava/lang/String;Ljava/util/UUID;)V

    sget-object v4, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    invoke-direct {v2, p1, v3, v4}, Lcom/navdy/service/library/device/connection/AcceptorListener;-><init>(Landroid/content/Context;Lcom/navdy/service/library/network/SocketAcceptor;Lcom/navdy/service/library/device/connection/ConnectionType;)V

    aput-object v2, v0, v1

    return-object v0
.end method

.method public getRemoteDevice()Lcom/navdy/service/library/device/RemoteDevice;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    return-object v0
.end method

.method protected getRemoteDeviceBroadcasters()[Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/navdy/service/library/device/discovery/RemoteDeviceBroadcaster;

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 143
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "returning local binder"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->binder:Landroid/os/IBinder;

    .line 150
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 79
    invoke-super {p0}, Lcom/navdy/service/library/device/connection/ConnectionService;->onCreate()V

    .line 81
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;

    move-result-object v0

    .line 84
    .local v0, "contactServiceHandler":Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    .line 85
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/DisplayNavigationServiceHandler;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/framework/servicehandler/TelephonyServiceHandler;-><init>(Landroid/content/Context;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;

    invoke-direct {v2}, Lcom/navdy/client/app/framework/servicehandler/SpeechServiceHandler;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/VoiceServiceHandler;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/MusicServiceHandler;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;

    invoke-direct {v2, p0}, Lcom/navdy/client/app/framework/servicehandler/PhotoServiceHandler;-><init>(Landroid/content/Context;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/DestinationsServiceHandler;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/MessageServiceHandler;

    invoke-direct {v2}, Lcom/navdy/client/app/framework/servicehandler/MessageServiceHandler;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    invoke-direct {v2}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;->getInstance()Lcom/navdy/client/app/framework/servicehandler/TripUpdateServiceHandler;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandlers:Ljava/util/List;

    new-instance v2, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;

    invoke-direct {v2}, Lcom/navdy/client/app/framework/servicehandler/PlaceTypeSearchServiceHandler;-><init>()V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    invoke-static {}, Lcom/navdy/client/app/ui/base/BaseActivity;->weHaveContactsPermission()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 99
    invoke-virtual {p0}, Lcom/navdy/client/app/framework/service/ClientConnectionService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    new-instance v4, Lcom/navdy/client/app/framework/util/ContactObserver;

    iget-object v5, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->serviceHandler:Lcom/navdy/service/library/device/connection/ConnectionService$ServiceHandler;

    invoke-direct {v4, v0, v5}, Lcom/navdy/client/app/framework/util/ContactObserver;-><init>(Lcom/navdy/client/app/framework/servicehandler/ContactServiceHandler;Landroid/os/Handler;)V

    .line 100
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 105
    :cond_0
    return-void
.end method

.method public removeListener(Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;)V
    .locals 1
    .param p1, "listener"    # Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->deviceChangeBroadcaster:Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;

    invoke-virtual {v0, p1}, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;->removeListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 73
    return-void
.end method

.method protected setRemoteDevice(Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 2
    .param p1, "remoteDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 155
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    if-eq v1, p1, :cond_1

    const/4 v0, 0x1

    .line 156
    .local v0, "deviceChanged":Z
    :goto_0
    invoke-super {p0, p1}, Lcom/navdy/service/library/device/connection/ConnectionService;->setRemoteDevice(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 157
    if-eqz v0, :cond_0

    .line 158
    iget-object v1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService;->deviceChangeBroadcaster:Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;

    invoke-virtual {v1, p1}, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;->dispatchDeviceChangedEvent(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 160
    :cond_0
    return-void

    .line 155
    .end local v0    # "deviceChanged":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
