.class Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$1;
.super Ljava/lang/Object;
.source "ClientConnectionService.java"

# interfaces
.implements Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$EventDispatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;->dispatchDeviceChangedEvent(Lcom/navdy/service/library/device/RemoteDevice;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;

.field final synthetic val$newDevice:Lcom/navdy/service/library/device/RemoteDevice;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$1;->this$0:Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;

    iput-object p2, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$1;->val$newDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchEvent(Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;)V
    .locals 1
    .param p1, "source"    # Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;
    .param p2, "listener"    # Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$1;->val$newDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-interface {p2, v0}, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;->onDeviceChanged(Lcom/navdy/service/library/device/RemoteDevice;)V

    .line 60
    return-void
.end method

.method public bridge synthetic dispatchEvent(Lcom/navdy/service/library/util/Listenable;Lcom/navdy/service/library/util/Listenable$Listener;)V
    .locals 0

    .prologue
    .line 56
    check-cast p1, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;

    check-cast p2, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;

    invoke-virtual {p0, p1, p2}, Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$1;->dispatchEvent(Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster;Lcom/navdy/client/app/framework/service/ClientConnectionService$DeviceChangeBroadcaster$Listener;)V

    return-void
.end method
