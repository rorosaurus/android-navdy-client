.class public Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;
.super Ljava/lang/Object;
.source "TelephonySupport21.java"

# interfaces
.implements Lcom/navdy/client/app/framework/callcontrol/TelephonyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;
    }
.end annotation


# static fields
.field static TELECOM_PACKAGES:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 39
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->TELECOM_PACKAGES:Ljava/util/HashSet;

    .line 40
    sget-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->TELECOM_PACKAGES:Ljava/util/HashSet;

    const-string v1, "com.android.server.telecom"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->TELECOM_PACKAGES:Ljava/util/HashSet;

    const-string v1, "com.android.phone"

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 42
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->context:Landroid/content/Context;

    .line 46
    return-void
.end method


# virtual methods
.method protected accept(Landroid/media/session/MediaController;)V
    .locals 4
    .param p1, "m"    # Landroid/media/session/MediaController;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 113
    new-instance v0, Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Landroid/view/KeyEvent;-><init>(II)V

    .line 114
    .local v0, "event":Landroid/view/KeyEvent;
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/view/KeyEvent;->changeFlags(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    move-result-object v0

    .line 115
    invoke-virtual {p1, v0}, Landroid/media/session/MediaController;->dispatchMediaButtonEvent(Landroid/view/KeyEvent;)Z

    .line 116
    sget-object v1, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HEADSETHOOK keyup sent:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/media/session/MediaController;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 117
    return-void
.end method

.method public acceptRingingCall()V
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->ACCEPT_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->handleCallAction(Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;)V

    .line 51
    return-void
.end method

.method protected end(Landroid/media/session/MediaController;)V
    .locals 0
    .param p1, "m"    # Landroid/media/session/MediaController;

    .prologue
    .line 128
    invoke-virtual {p0, p1}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->reject(Landroid/media/session/MediaController;)V

    .line 129
    return-void
.end method

.method public endCall()V
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->END_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->handleCallAction(Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;)V

    .line 61
    return-void
.end method

.method protected handleCallAction(Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;)V
    .locals 9
    .param p1, "action"    # Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 72
    :try_start_0
    iget-object v5, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->context:Landroid/content/Context;

    const-string v6, "media_session"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/session/MediaSessionManager;

    .line 74
    .local v2, "mediaSessionManager":Landroid/media/session/MediaSessionManager;
    if-nez v2, :cond_1

    .line 75
    sget-object v5, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unable to handleCallAction. mediaSessionManager is null"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 109
    .end local v2    # "mediaSessionManager":Landroid/media/session/MediaSessionManager;
    :cond_0
    :goto_0
    return-void

    .line 79
    .restart local v2    # "mediaSessionManager":Landroid/media/session/MediaSessionManager;
    :cond_1
    new-instance v5, Landroid/content/ComponentName;

    iget-object v6, p0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->context:Landroid/content/Context;

    const-class v7, Lcom/navdy/client/app/framework/service/RemoteControllerNotificationListenerService;

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2, v5}, Landroid/media/session/MediaSessionManager;->getActiveSessions(Landroid/content/ComponentName;)Ljava/util/List;

    move-result-object v1

    .line 82
    .local v1, "mediaControllerList":Ljava/util/List;, "Ljava/util/List<Landroid/media/session/MediaController;>;"
    sget-object v5, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mediaSessions["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "] action["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 84
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/session/MediaController;

    .line 85
    .local v0, "m":Landroid/media/session/MediaController;
    invoke-virtual {v0}, Landroid/media/session/MediaController;->getPackageName()Ljava/lang/String;

    move-result-object v3

    .line 86
    .local v3, "packageName":Ljava/lang/String;
    sget-object v6, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "package is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 87
    sget-object v6, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->TELECOM_PACKAGES:Ljava/util/HashSet;

    invoke-virtual {v6, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 88
    sget-object v6, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$1;->$SwitchMap$com$navdy$client$app$framework$callcontrol$TelephonySupport21$Action:[I

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto :goto_1

    .line 90
    :pswitch_0
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->accept(Landroid/media/session/MediaController;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 106
    .end local v0    # "m":Landroid/media/session/MediaController;
    .end local v1    # "mediaControllerList":Ljava/util/List;, "Ljava/util/List<Landroid/media/session/MediaController;>;"
    .end local v2    # "mediaSessionManager":Landroid/media/session/MediaSessionManager;
    .end local v3    # "packageName":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 107
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v6, "Unable to handleCallAction"

    invoke-virtual {v5, v6, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 94
    .end local v4    # "t":Ljava/lang/Throwable;
    .restart local v0    # "m":Landroid/media/session/MediaController;
    .restart local v1    # "mediaControllerList":Ljava/util/List;, "Ljava/util/List<Landroid/media/session/MediaController;>;"
    .restart local v2    # "mediaSessionManager":Landroid/media/session/MediaSessionManager;
    .restart local v3    # "packageName":Ljava/lang/String;
    :pswitch_1
    :try_start_1
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->end(Landroid/media/session/MediaController;)V

    goto :goto_1

    .line 98
    :pswitch_2
    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->reject(Landroid/media/session/MediaController;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected reject(Landroid/media/session/MediaController;)V
    .locals 4
    .param p1, "m"    # Landroid/media/session/MediaController;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 121
    new-instance v0, Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Landroid/view/KeyEvent;-><init>(II)V

    .line 122
    .local v0, "event":Landroid/view/KeyEvent;
    const/16 v1, 0x80

    invoke-static {v0, v1}, Landroid/view/KeyEvent;->changeFlags(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    move-result-object v0

    .line 123
    invoke-virtual {p1, v0}, Landroid/media/session/MediaController;->dispatchMediaButtonEvent(Landroid/view/KeyEvent;)Z

    .line 124
    sget-object v1, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "HEADSETHOOK keyup sent:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/media/session/MediaController;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    .line 125
    return-void
.end method

.method public rejectRingingCall()V
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;->REJECT_CALL:Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;

    invoke-virtual {p0, v0}, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->handleCallAction(Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21$Action;)V

    .line 56
    return-void
.end method

.method public toggleMute()V
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lcom/navdy/client/app/framework/callcontrol/TelephonySupport21;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "not implemented"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 66
    return-void
.end method
