.class final Lcom/navdy/client/app/framework/location/NavdyLocationManager$5;
.super Ljava/lang/Object;
.source "NavdyLocationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/location/NavdyLocationManager;->updateLastKnownCountryCode(Landroid/location/Location;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;

.field final synthetic val$now:J


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination;J)V
    .locals 0

    .prologue
    .line 647
    iput-object p1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$5;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iput-wide p2, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$5;->val$now:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 650
    iget-object v1, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$5;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v1}, Lcom/navdy/client/app/framework/models/Destination;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 651
    .local v0, "countryCode":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 652
    invoke-static {v0}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$1202(Ljava/lang/String;)Ljava/lang/String;

    .line 653
    iget-wide v2, p0, Lcom/navdy/client/app/framework/location/NavdyLocationManager$5;->val$now:J

    invoke-static {}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$1400()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/location/NavdyLocationManager;->access$1302(J)J

    .line 655
    :cond_0
    return-void
.end method
