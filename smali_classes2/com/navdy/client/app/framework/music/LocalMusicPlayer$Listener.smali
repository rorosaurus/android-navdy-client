.class public interface abstract Lcom/navdy/client/app/framework/music/LocalMusicPlayer$Listener;
.super Ljava/lang/Object;
.source "LocalMusicPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/music/LocalMusicPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onMetadataUpdate(Lcom/navdy/service/library/events/audio/MusicTrackInfo;)V
.end method

.method public abstract onPlaybackStateUpdate(Lcom/navdy/service/library/events/audio/MusicPlaybackState;)V
.end method

.method public abstract onPositionUpdate(II)V
.end method
