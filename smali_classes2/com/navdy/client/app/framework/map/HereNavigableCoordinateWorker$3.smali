.class Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;
.super Ljava/lang/Object;
.source "HereNavigableCoordinateWorker.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->process(Lcom/navdy/client/app/framework/models/CalendarEvent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;

.field final synthetic val$calendarEvent:Lcom/navdy/client/app/framework/models/CalendarEvent;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;Lcom/navdy/client/app/framework/models/CalendarEvent;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;->this$0:Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;

    iput-object p2, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;->val$calendarEvent:Lcom/navdy/client/app/framework/models/CalendarEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 3
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 133
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Now processing: No coordinates found for: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;->val$calendarEvent:Lcom/navdy/client/app/framework/models/CalendarEvent;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/CalendarEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;->this$0:Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;

    invoke-static {v0}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->access$500(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;)V

    .line 135
    return-void
.end method

.method public onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 6
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 118
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Now processing: calendar event: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;->val$calendarEvent:Lcom/navdy/client/app/framework/models/CalendarEvent;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/CalendarEvent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; destination:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 120
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$Precision;->isPrecise()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->hasValidNavCoordinates()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Now processing: Using the navCoords"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;->this$0:Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;->val$calendarEvent:Lcom/navdy/client/app/framework/models/CalendarEvent;

    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->access$400(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;Lcom/navdy/client/app/framework/models/CalendarEvent;DD)V

    .line 129
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v0, p1, Lcom/navdy/client/app/framework/models/Destination;->precisionLevel:Lcom/navdy/client/app/framework/models/Destination$Precision;

    invoke-virtual {v0}, Lcom/navdy/client/app/framework/models/Destination$Precision;->isPrecise()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->hasValidDisplayCoordinates()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 124
    invoke-static {}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->access$300()Lcom/navdy/service/library/log/Logger;

    move-result-object v0

    const-string v1, "Now processing: Using the displayCoords"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;->this$0:Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;

    iget-object v1, p0, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;->val$calendarEvent:Lcom/navdy/client/app/framework/models/CalendarEvent;

    iget-wide v2, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-static/range {v0 .. v5}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;->access$400(Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker;Lcom/navdy/client/app/framework/models/CalendarEvent;DD)V

    goto :goto_0

    .line 127
    :cond_1
    sget-object v0, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;->BAD_COORDS:Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    invoke-virtual {p0, p1, v0}, Lcom/navdy/client/app/framework/map/HereNavigableCoordinateWorker$3;->onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V

    goto :goto_0
.end method
