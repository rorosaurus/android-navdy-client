.class Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;
.super Ljava/lang/Object;
.source "GooglePlacesSearch.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->runSearchWebApiInTaskManager(Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

.field final synthetic val$searchRadius:I

.field final synthetic val$searchText:Ljava/lang/String;

.field final synthetic val$type:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    iput-object p2, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->val$searchText:Ljava/lang/String;

    iput-object p3, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->val$type:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    iput p4, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->val$searchRadius:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 242
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    invoke-static {v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->access$200(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    move-result-object v2

    sget-object v3, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;->DETAILS:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    invoke-static {v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->access$300(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 244
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    invoke-static {v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->access$300(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/navdy/client/app/providers/NavdyContentProvider;->getNonStalePlaceDetailInfoFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 245
    .local v1, "placeDetailJson":Ljava/lang/String;
    invoke-static {v1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 246
    sget-object v2, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->logger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Place detail data is not stale so using it."

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 249
    invoke-static {v1}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->createDestinationObject(Ljava/lang/String;)Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    move-result-object v0

    .line 250
    .local v0, "destinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2, v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->access$002(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;Ljava/util/List;)Ljava/util/List;

    .line 251
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    invoke-static {v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->access$000(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    invoke-static {v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->access$400(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 253
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    invoke-static {v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->access$400(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    invoke-static {v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->access$500(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 262
    .end local v0    # "destinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .end local v1    # "placeDetailJson":Ljava/lang/String;
    :goto_0
    return-void

    .line 255
    .restart local v0    # "destinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .restart local v1    # "placeDetailJson":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    invoke-static {v2}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->access$500(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 261
    .end local v0    # "destinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    .end local v1    # "placeDetailJson":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->this$0:Lcom/navdy/client/app/framework/search/GooglePlacesSearch;

    iget-object v3, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->val$searchText:Ljava/lang/String;

    iget-object v4, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->val$type:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;

    iget v5, p0, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$2;->val$searchRadius:I

    invoke-static {v2, v3, v4, v5}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch;->access$600(Lcom/navdy/client/app/framework/search/GooglePlacesSearch;Ljava/lang/String;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$ServiceTypes;I)V

    goto :goto_0
.end method
