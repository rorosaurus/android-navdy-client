.class public Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;
.super Ljava/lang/Object;
.source "MusicBrainzArtworkSearch.java"


# static fields
.field private static final API_URL_RELEASE_PREFIX:Ljava/lang/String; = "http://musicbrains.org/ws/2/release/?fmt=json&limit=1&query="

.field private static final ARTIST_ALBUM_SEPARATOR:Ljava/lang/String; = " // "

.field private static final BITMAP_CACHE_SIZE:I = 0xa

.field private static final COVER_ART_URL_POSTFIX:Ljava/lang/String; = "/front-250"

.field private static final COVER_ART_URL_PREFIX:Ljava/lang/String; = "http://coverartarchive.org/release/"

.field private static final artworkCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 34
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->artworkCache:Landroid/util/LruCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static buildCacheKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "artist"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "album"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " // "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getArtwork(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 13
    .param p0, "artist"    # Ljava/lang/String;
    .param p1, "album"    # Ljava/lang/String;

    .prologue
    .line 45
    sget-object v10, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Sending artwork query for "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " from "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 47
    invoke-static {p0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    const/4 v6, 0x1

    .line 48
    .local v6, "isArtistSet":Z
    :goto_0
    invoke-static {p1}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_1

    const/4 v5, 0x1

    .line 50
    .local v5, "isAlbumSet":Z
    :goto_1
    if-nez v6, :cond_2

    if-nez v5, :cond_2

    .line 51
    const/4 v9, 0x0

    .line 104
    :goto_2
    return-object v9

    .line 47
    .end local v5    # "isAlbumSet":Z
    .end local v6    # "isArtistSet":Z
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 48
    .restart local v6    # "isArtistSet":Z
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 54
    .restart local v5    # "isAlbumSet":Z
    :cond_2
    invoke-static {p0, p1}, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->buildCacheKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 55
    .local v2, "cacheKey":Ljava/lang/String;
    sget-object v10, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->artworkCache:Landroid/util/LruCache;

    invoke-virtual {v10, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/graphics/Bitmap;

    .line 56
    .local v9, "result":Landroid/graphics/Bitmap;
    if-eqz v9, :cond_3

    .line 57
    sget-object v10, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Found artwork in MusicBrainz client class cache"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    goto :goto_2

    .line 62
    :cond_3
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    .local v8, "queryBuilder":Ljava/lang/StringBuilder;
    if-eqz v6, :cond_4

    .line 64
    const-string v10, "artist:\""

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    :cond_4
    if-eqz v5, :cond_6

    .line 67
    if-eqz v6, :cond_5

    .line 68
    const-string v10, " AND "

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    :cond_5
    const-string v10, "release:\""

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\""

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    :cond_6
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-nez v10, :cond_7

    .line 73
    sget-object v10, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "No artist nor album name is given"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 74
    const/4 v9, 0x0

    goto :goto_2

    .line 77
    :cond_7
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 79
    .local v7, "query":Ljava/lang/String;
    :try_start_0
    const-string v10, "UTF-8"

    invoke-static {v7, v10}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    .line 85
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "http://musicbrains.org/ws/2/release/?fmt=json&limit=1&query="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->getJSONFromURL(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 86
    .local v0, "albumData":Lorg/json/JSONObject;
    if-nez v0, :cond_8

    .line 87
    sget-object v10, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Album not found on MusicBrainz"

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 88
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 80
    .end local v0    # "albumData":Lorg/json/JSONObject;
    :catch_0
    move-exception v3

    .line 81
    .local v3, "e":Ljava/io/UnsupportedEncodingException;
    sget-object v10, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Cannot build query for album ID"

    invoke-virtual {v10, v11, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 82
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 93
    .end local v3    # "e":Ljava/io/UnsupportedEncodingException;
    .restart local v0    # "albumData":Lorg/json/JSONObject;
    :cond_8
    :try_start_1
    const-string v10, "releases"

    invoke-virtual {v0, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/json/JSONObject;

    const-string v11, "id"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    .line 99
    .local v1, "albumId":Ljava/lang/String;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "http://coverartarchive.org/release/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/front-250"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/navdy/service/library/util/IOUtils;->downloadImage(Ljava/lang/String;)[B

    move-result-object v4

    .line 100
    .local v4, "img":[B
    const/4 v10, 0x0

    array-length v11, v4

    invoke-static {v4, v10, v11}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 101
    sget-object v10, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Got album artwork from MusicBrainz with size "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    array-length v12, v4

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 103
    sget-object v10, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->artworkCache:Landroid/util/LruCache;

    invoke-virtual {v10, v2, v9}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 94
    .end local v1    # "albumId":Ljava/lang/String;
    .end local v4    # "img":[B
    :catch_1
    move-exception v3

    .line 95
    .local v3, "e":Ljava/lang/Exception;
    :goto_3
    sget-object v10, Lcom/navdy/client/app/framework/search/MusicBrainzArtworkSearch;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v11, "Cannot read album\'s ID from MusicBrainz data"

    invoke-virtual {v10, v11, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 96
    const/4 v9, 0x0

    goto/16 :goto_2

    .line 94
    .end local v3    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v3

    goto :goto_3

    :catch_3
    move-exception v3

    goto :goto_3
.end method
