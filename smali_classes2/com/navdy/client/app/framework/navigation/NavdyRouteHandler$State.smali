.class public final enum Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;
.super Ljava/lang/Enum;
.source "NavdyRouteHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

.field public static final enum CALCULATING_PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

.field public static final enum CALCULATING_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

.field public static final enum EN_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

.field public static final enum INACTIVE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

.field public static final enum PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

.field public static final enum ROUTE_CALCULATED:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1107
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v3}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->INACTIVE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 1112
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    const-string v1, "CALCULATING_PENDING_ROUTE"

    invoke-direct {v0, v1, v4}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 1117
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    const-string v1, "PENDING_ROUTE"

    invoke-direct {v0, v1, v5}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 1122
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    const-string v1, "CALCULATING_ROUTES"

    invoke-direct {v0, v1, v6}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 1127
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    const-string v1, "ROUTE_CALCULATED"

    invoke-direct {v0, v1, v7}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->ROUTE_CALCULATED:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 1132
    new-instance v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    const-string v1, "EN_ROUTE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->EN_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    .line 1103
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->INACTIVE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->PENDING_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->CALCULATING_ROUTES:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->ROUTE_CALCULATED:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->EN_ROUTE:Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->$VALUES:[Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1103
    const-class v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    return-object v0
.end method

.method public static values()[Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;
    .locals 1

    .prologue
    .line 1103
    sget-object v0, Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->$VALUES:[Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    invoke-virtual {v0}, [Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/client/app/framework/navigation/NavdyRouteHandler$State;

    return-object v0
.end method
