.class public Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;
.super Ljava/lang/Object;
.source "Destination.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/models/Destination;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MultilineAddress"
.end annotation


# instance fields
.field public addressFirstLine:Ljava/lang/String;

.field public addressSecondLine:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 6
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    const/4 v5, 0x1

    .line 1217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1213
    const-string v4, ""

    iput-object v4, p0, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->title:Ljava/lang/String;

    .line 1214
    const-string v4, ""

    iput-object v4, p0, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->addressFirstLine:Ljava/lang/String;

    .line 1215
    const-string v4, ""

    iput-object v4, p0, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->addressSecondLine:Ljava/lang/String;

    .line 1218
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 1221
    .local v0, "destinationTitle":Ljava/lang/String;
    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1222
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getSplitAddress()Landroid/util/Pair;

    move-result-object v2

    .line 1223
    .local v2, "splitAddress":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->isTitleIsInTheAddress(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    .line 1224
    .local v3, "titleIsInFirstLine":Z
    if-nez v3, :cond_0

    .line 1225
    iput-object v0, p0, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->title:Ljava/lang/String;

    .line 1226
    iget-object v4, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->addressFirstLine:Ljava/lang/String;

    .line 1227
    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->sanitizeAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->addressSecondLine:Ljava/lang/String;

    .line 1241
    .end local v2    # "splitAddress":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v3    # "titleIsInFirstLine":Z
    :goto_0
    return-void

    .line 1233
    :cond_0
    invoke-virtual {p1}, Lcom/navdy/client/app/framework/models/Destination;->getAddressLines()Ljava/util/ArrayList;

    move-result-object v1

    .line 1234
    .local v1, "lines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_1

    .line 1235
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, p0, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->title:Ljava/lang/String;

    .line 1237
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-le v4, v5, :cond_2

    .line 1238
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iput-object v4, p0, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->addressFirstLine:Ljava/lang/String;

    .line 1240
    :cond_2
    const/4 v4, 0x2

    invoke-static {v1, v4}, Lcom/navdy/client/app/framework/i18n/AddressUtils;->joinRemainingLines(Ljava/util/ArrayList;I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/navdy/client/app/framework/models/Destination$MultilineAddress;->addressSecondLine:Ljava/lang/String;

    goto :goto_0
.end method
