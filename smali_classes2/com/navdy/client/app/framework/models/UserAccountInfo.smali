.class public Lcom/navdy/client/app/framework/models/UserAccountInfo;
.super Ljava/lang/Object;
.source "UserAccountInfo.java"


# static fields
.field private static logger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field public email:Ljava/lang/String;

.field public fullName:Ljava/lang/String;

.field public photo:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/models/UserAccountInfo;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/models/UserAccountInfo;->logger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "fullName"    # Ljava/lang/String;
    .param p2, "email"    # Ljava/lang/String;
    .param p3, "photo"    # Landroid/graphics/Bitmap;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/UserAccountInfo;->fullName:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/navdy/client/app/framework/models/UserAccountInfo;->email:Ljava/lang/String;

    .line 27
    iput-object p3, p0, Lcom/navdy/client/app/framework/models/UserAccountInfo;->photo:Landroid/graphics/Bitmap;

    .line 28
    return-void
.end method

.method public static getUserAccountInfo()Lcom/navdy/client/app/framework/models/UserAccountInfo;
    .locals 6

    .prologue
    .line 31
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getCustomerPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 32
    .local v0, "customerPrefs":Landroid/content/SharedPreferences;
    const-string v4, "fname"

    const-string v5, ""

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 33
    .local v2, "fullName":Ljava/lang/String;
    const-string v4, "email"

    const-string v5, ""

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 34
    .local v1, "email":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/app/tracking/Tracker;->getUserProfilePhoto()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 35
    .local v3, "photo":Landroid/graphics/Bitmap;
    new-instance v4, Lcom/navdy/client/app/framework/models/UserAccountInfo;

    invoke-direct {v4, v2, v1, v3}, Lcom/navdy/client/app/framework/models/UserAccountInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-object v4
.end method


# virtual methods
.method public hasInfo()Z
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/navdy/client/app/framework/models/UserAccountInfo;->fullName:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/UserAccountInfo;->email:Ljava/lang/String;

    invoke-static {v0}, Lcom/navdy/client/app/framework/util/StringUtils;->isEmptyAfterTrim(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/navdy/client/app/framework/models/UserAccountInfo;->photo:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
