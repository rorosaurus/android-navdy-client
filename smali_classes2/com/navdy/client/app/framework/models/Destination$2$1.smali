.class Lcom/navdy/client/app/framework/models/Destination$2$1;
.super Ljava/lang/Object;
.source "Destination.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/models/Destination$2;->onGoogleSearchResult(Ljava/util/List;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Query;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/app/framework/models/Destination$2;

.field final synthetic val$destinations:Ljava/util/List;

.field final synthetic val$error:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/models/Destination$2;Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;Ljava/util/List;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/app/framework/models/Destination$2;

    .prologue
    .line 1658
    iput-object p1, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->this$1:Lcom/navdy/client/app/framework/models/Destination$2;

    iput-object p2, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->val$error:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    iput-object p3, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->val$destinations:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1661
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->val$error:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    sget-object v2, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->NONE:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    if-eq v1, v2, :cond_1

    .line 1662
    invoke-static {}, Lcom/navdy/client/app/framework/models/Destination;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "received error while refreshing google place details: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->val$error:Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;

    invoke-virtual {v3}, Lcom/navdy/client/app/framework/search/GooglePlacesSearch$Error;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 1663
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->this$1:Lcom/navdy/client/app/framework/models/Destination$2;

    iget-object v1, v1, Lcom/navdy/client/app/framework/models/Destination$2;->val$callback:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    .line 1664
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->this$1:Lcom/navdy/client/app/framework/models/Destination$2;

    iget-object v1, v1, Lcom/navdy/client/app/framework/models/Destination$2;->val$callback:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 1683
    :cond_0
    :goto_0
    return-void

    .line 1669
    :cond_1
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->val$destinations:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->val$destinations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1671
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->val$destinations:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;

    .line 1674
    .local v0, "destinationResult":Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;
    sget-object v1, Lcom/navdy/client/app/framework/models/Destination$SearchType;->DETAILS:Lcom/navdy/client/app/framework/models/Destination$SearchType;

    iget-object v2, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->this$1:Lcom/navdy/client/app/framework/models/Destination$2;

    iget-object v2, v2, Lcom/navdy/client/app/framework/models/Destination$2;->this$0:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v0, v1, v2}, Lcom/navdy/client/app/framework/models/GoogleTextSearchDestinationResult;->toModelDestinationObject(Lcom/navdy/client/app/framework/models/Destination$SearchType;Lcom/navdy/client/app/framework/models/Destination;)V

    .line 1676
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->this$1:Lcom/navdy/client/app/framework/models/Destination$2;

    iget-object v1, v1, Lcom/navdy/client/app/framework/models/Destination$2;->val$callback:Ljava/lang/Runnable;

    if-eqz v1, :cond_2

    .line 1677
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->this$1:Lcom/navdy/client/app/framework/models/Destination$2;

    iget-object v1, v1, Lcom/navdy/client/app/framework/models/Destination$2;->val$callback:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 1681
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/app/framework/models/Destination$2$1;->this$1:Lcom/navdy/client/app/framework/models/Destination$2;

    iget-object v1, v1, Lcom/navdy/client/app/framework/models/Destination$2;->this$0:Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v1}, Lcom/navdy/client/app/framework/models/Destination;->access$100(Lcom/navdy/client/app/framework/models/Destination;)V

    goto :goto_0
.end method
