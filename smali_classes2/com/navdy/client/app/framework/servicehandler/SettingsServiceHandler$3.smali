.class Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$3;
.super Ljava/lang/Object;
.source "SettingsServiceHandler.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->onSpeakerRequest(Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesRequest;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

.field final synthetic val$displaySpeakerPreferencesRequest:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesRequest;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$3;->val$displaySpeakerPreferencesRequest:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const/4 v10, 0x0

    .line 96
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 97
    .local v2, "sharedPrefs":Landroid/content/SharedPreferences;
    const-string v4, "audio_serial_number"

    const-wide/16 v6, 0x0

    invoke-interface {v2, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 99
    .local v0, "serial":J
    iget-object v4, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$3;->val$displaySpeakerPreferencesRequest:Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesRequest;

    iget-object v4, v4, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesRequest;->serial_number:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v0

    if-nez v4, :cond_0

    .line 100
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v5, "Speaker prefs version up to date"

    invoke-virtual {v4, v5}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 101
    iget-object v4, p0, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler$3;->this$0:Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;

    new-instance v5, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;

    sget-object v6, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_VERSION_IS_CURRENT:Lcom/navdy/service/library/events/RequestStatus;

    const/4 v7, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;)V

    invoke-static {v4, v5}, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->access$000(Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;Lcom/squareup/wire/Message;)V

    .line 109
    .end local v0    # "serial":J
    .end local v2    # "sharedPrefs":Landroid/content/SharedPreferences;
    :goto_0
    return-void

    .line 103
    .restart local v0    # "serial":J
    .restart local v2    # "sharedPrefs":Landroid/content/SharedPreferences;
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/ui/settings/SettingsUtils;->sendSpeakerSettingsToTheHudBasedOnSharedPrefValue()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 105
    .end local v0    # "serial":J
    .end local v2    # "sharedPrefs":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v3

    .line 106
    .local v3, "t":Ljava/lang/Throwable;
    sget-object v4, Lcom/navdy/client/app/framework/servicehandler/SettingsServiceHandler;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v4, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    .line 107
    new-instance v4, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;

    sget-object v5, Lcom/navdy/service/library/events/RequestStatus;->REQUEST_UNKNOWN_ERROR:Lcom/navdy/service/library/events/RequestStatus;

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v4, v5, v10, v6, v10}, Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferencesUpdate;-><init>(Lcom/navdy/service/library/events/RequestStatus;Ljava/lang/String;Ljava/lang/Long;Lcom/navdy/service/library/events/preferences/DisplaySpeakerPreferences;)V

    invoke-static {v4}, Lcom/navdy/client/app/framework/DeviceConnection;->postEvent(Lcom/squareup/wire/Message;)Z

    goto :goto_0
.end method
