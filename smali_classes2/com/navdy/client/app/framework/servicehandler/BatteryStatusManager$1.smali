.class Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager$1;
.super Landroid/content/BroadcastReceiver;
.source "BatteryStatusManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;


# direct methods
.method constructor <init>(Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, -0x1

    .line 42
    const-string v6, "status"

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 43
    .local v5, "status":I
    const/4 v6, 0x2

    if-eq v5, v6, :cond_0

    const/4 v6, 0x5

    if-ne v5, v6, :cond_3

    :cond_0
    const/4 v1, 0x1

    .line 45
    .local v1, "isCharging":Z
    :goto_0
    const-string v6, "level"

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 46
    .local v2, "level":I
    const-string v6, "scale"

    invoke-virtual {p2, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 47
    .local v4, "scale":I
    if-eq v2, v7, :cond_1

    if-ne v4, v7, :cond_4

    .line 48
    :cond_1
    invoke-static {}, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->access$000()Lcom/navdy/service/library/log/Logger;

    move-result-object v6

    const-string v7, "[battery] invalid info"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 58
    :cond_2
    :goto_1
    return-void

    .line 43
    .end local v1    # "isCharging":Z
    .end local v2    # "level":I
    .end local v4    # "scale":I
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 51
    .restart local v1    # "isCharging":Z
    .restart local v2    # "level":I
    .restart local v4    # "scale":I
    :cond_4
    mul-int/lit8 v6, v2, 0x64

    div-int v0, v6, v4

    .line 53
    .local v0, "batteryPct":I
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    invoke-static {v6, v0, v1}, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->access$100(Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;IZ)Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;

    move-result-object v3

    .line 54
    .local v3, "phoneBatteryStatus":Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;
    if-eqz v3, :cond_2

    .line 57
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager$1;->this$0:Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;

    invoke-static {v6, v3}, Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;->access$200(Lcom/navdy/client/app/framework/servicehandler/BatteryStatusManager;Lcom/navdy/service/library/events/callcontrol/PhoneBatteryStatus;)V

    goto :goto_1
.end method
