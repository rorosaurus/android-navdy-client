.class public Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;
.super Ljava/lang/Object;
.source "LocationTransmitter.java"


# static fields
.field public static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected isRunning:Z

.field protected mConnected:Z

.field protected final mContext:Landroid/content/Context;

.field private mLocationListener:Landroid/location/LocationListener;

.field protected mLocationManager:Landroid/location/LocationManager;

.field protected final mLocationReceiverLooper:Landroid/os/Looper;

.field protected final mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/navdy/service/library/device/RemoteDevice;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "remoteDevice"    # Lcom/navdy/service/library/device/RemoteDevice;

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter$1;-><init>(Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;)V

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mLocationListener:Landroid/location/LocationListener;

    .line 58
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "location-transmitter"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 59
    .local v0, "handlerThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 60
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mLocationReceiverLooper:Landroid/os/Looper;

    .line 61
    iput-object p1, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mContext:Landroid/content/Context;

    .line 62
    iput-object p2, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    .line 63
    new-instance v1, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter$2;

    invoke-direct {v1, p0}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter$2;-><init>(Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;)V

    invoke-virtual {p2, v1}, Lcom/navdy/service/library/device/RemoteDevice;->addListener(Lcom/navdy/service/library/util/Listenable$Listener;)Z

    .line 86
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/RemoteDevice;->isConnected()Z

    move-result v1

    iput-boolean v1, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mConnected:Z

    .line 87
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mContext:Landroid/content/Context;

    const-string v2, "location"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/location/LocationManager;

    iput-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mLocationManager:Landroid/location/LocationManager;

    .line 88
    return-void
.end method

.method private sendLastLocation()V
    .locals 12

    .prologue
    .line 160
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-virtual {v6, v7}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v2

    .line 161
    .local v2, "permission":I
    const/4 v1, 0x0

    .line 162
    .local v1, "lastLocation":Landroid/location/Location;
    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v6}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v3

    .line 163
    .local v3, "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v2, :cond_3

    .line 164
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 165
    .local v4, "str":Ljava/lang/String;
    iget-object v7, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mLocationManager:Landroid/location/LocationManager;

    invoke-virtual {v7, v4}, Landroid/location/LocationManager;->getLastKnownLocation(Ljava/lang/String;)Landroid/location/Location;

    move-result-object v0

    .line 166
    .local v0, "l":Landroid/location/Location;
    if-eqz v0, :cond_0

    .line 167
    if-nez v1, :cond_1

    .line 168
    move-object v1, v0

    goto :goto_0

    .line 170
    :cond_1
    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v8

    invoke-virtual {v1}, Landroid/location/Location;->getTime()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-lez v7, :cond_0

    .line 171
    move-object v1, v0

    goto :goto_0

    .line 176
    .end local v0    # "l":Landroid/location/Location;
    .end local v4    # "str":Ljava/lang/String;
    :cond_2
    if-nez v1, :cond_4

    .line 177
    sget-object v6, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "no last location to transmit"

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 186
    .end local v1    # "lastLocation":Landroid/location/Location;
    .end local v2    # "permission":I
    .end local v3    # "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    :goto_1
    return-void

    .line 180
    .restart local v1    # "lastLocation":Landroid/location/Location;
    .restart local v2    # "permission":I
    .restart local v3    # "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    sget-object v6, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "transmit last location:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 181
    invoke-virtual {p0, v1}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->transmitLocation(Landroid/location/Location;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 183
    .end local v1    # "lastLocation":Landroid/location/Location;
    .end local v2    # "permission":I
    .end local v3    # "providers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v5

    .line 184
    .local v5, "t":Ljava/lang/Throwable;
    sget-object v6, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v6, v5}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public start()Z
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 117
    :try_start_0
    iget-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->isRunning:Z

    if-eqz v0, :cond_0

    .line 118
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "location transmitter already started."

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->i(Ljava/lang/String;)V

    move v0, v9

    .line 134
    :goto_0
    return v0

    .line 121
    :cond_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v7

    .line 122
    .local v7, "permission":I
    if-nez v7, :cond_1

    .line 123
    iget-object v0, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mLocationManager:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mLocationListener:Landroid/location/LocationListener;

    iget-object v6, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mLocationReceiverLooper:Landroid/os/Looper;

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->isRunning:Z

    .line 125
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "starting location updates"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 126
    invoke-direct {p0}, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sendLastLocation()V

    move v0, v10

    .line 127
    goto :goto_0

    .line 129
    :cond_1
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "permission for ACCESS_FINE_LOCATION failed"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v9

    .line 130
    goto :goto_0

    .line 132
    .end local v7    # "permission":I
    :catch_0
    move-exception v8

    .line 133
    .local v8, "t":Ljava/lang/Throwable;
    sget-object v0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v0, v8}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    move v0, v9

    .line 134
    goto :goto_0
.end method

.method public stop()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 139
    iget-boolean v3, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->isRunning:Z

    if-nez v3, :cond_0

    .line 140
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "location transmitter not running"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 155
    :goto_0
    return v2

    .line 144
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-virtual {v3, v4}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    .line 145
    .local v0, "permission":I
    if-nez v0, :cond_1

    .line 146
    iget-object v3, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mLocationManager:Landroid/location/LocationManager;

    iget-object v4, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mLocationListener:Landroid/location/LocationListener;

    invoke-virtual {v3, v4}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    .end local v0    # "permission":I
    :goto_1
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "stopping location updates"

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 154
    iput-boolean v2, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->isRunning:Z

    .line 155
    const/4 v2, 0x1

    goto :goto_0

    .line 148
    .restart local v0    # "permission":I
    :cond_1
    :try_start_1
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "mLocationManager.removeUpdates failed. Permission for ACCESS_FINE_LOCATION was not found."

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 150
    .end local v0    # "permission":I
    :catch_0
    move-exception v1

    .line 151
    .local v1, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    invoke-virtual {v3, v1}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method protected transmitLocation(Landroid/location/Location;)Z
    .locals 10
    .param p1, "androidLocation"    # Landroid/location/Location;

    .prologue
    const/4 v1, 0x0

    .line 91
    iget-boolean v2, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mConnected:Z

    if-nez v2, :cond_0

    .line 92
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Can\'t transmit: not connected"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 112
    :goto_0
    return v1

    .line 96
    :cond_0
    if-nez p1, :cond_1

    .line 97
    sget-object v2, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "Invalid location - null"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 101
    :cond_1
    new-instance v0, Lcom/navdy/service/library/events/location/Coordinate;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    .line 102
    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    .line 103
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 104
    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 105
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    .line 106
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    .line 107
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 108
    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/navdy/service/library/events/location/Coordinate;-><init>(Ljava/lang/Double;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Long;Ljava/lang/String;)V

    .line 110
    .local v0, "coordinate":Lcom/navdy/service/library/events/location/Coordinate;
    sget-object v1, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sending coordinate: lat="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", lng="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->v(Ljava/lang/String;)V

    .line 112
    iget-object v1, p0, Lcom/navdy/client/app/framework/servicehandler/LocationTransmitter;->mRemoteDevice:Lcom/navdy/service/library/device/RemoteDevice;

    invoke-virtual {v1, v0}, Lcom/navdy/service/library/device/RemoteDevice;->postEvent(Lcom/squareup/wire/Message;)Z

    move-result v1

    goto :goto_0
.end method
