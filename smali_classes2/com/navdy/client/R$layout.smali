.class public final Lcom/navdy/client/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f030000

.field public static final abc_action_bar_up_container:I = 0x7f030001

.field public static final abc_action_bar_view_list_nav_layout:I = 0x7f030002

.field public static final abc_action_menu_item_layout:I = 0x7f030003

.field public static final abc_action_menu_layout:I = 0x7f030004

.field public static final abc_action_mode_bar:I = 0x7f030005

.field public static final abc_action_mode_close_item_material:I = 0x7f030006

.field public static final abc_activity_chooser_view:I = 0x7f030007

.field public static final abc_activity_chooser_view_list_item:I = 0x7f030008

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f030009

.field public static final abc_alert_dialog_material:I = 0x7f03000a

.field public static final abc_alert_dialog_title_material:I = 0x7f03000b

.field public static final abc_dialog_title_material:I = 0x7f03000c

.field public static final abc_expanded_menu_layout:I = 0x7f03000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f03000e

.field public static final abc_list_menu_item_icon:I = 0x7f03000f

.field public static final abc_list_menu_item_layout:I = 0x7f030010

.field public static final abc_list_menu_item_radio:I = 0x7f030011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f030012

.field public static final abc_popup_menu_item_layout:I = 0x7f030013

.field public static final abc_screen_content_include:I = 0x7f030014

.field public static final abc_screen_simple:I = 0x7f030015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f030016

.field public static final abc_screen_toolbar:I = 0x7f030017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f030018

.field public static final abc_search_view:I = 0x7f030019

.field public static final abc_select_dialog_material:I = 0x7f03001a

.field public static final active_trip_activity:I = 0x7f03001b

.field public static final active_trip_card:I = 0x7f03001c

.field public static final active_trip_fragment_layout:I = 0x7f03001d

.field public static final active_trip_slim_card:I = 0x7f03001e

.field public static final activity_contact_zendesk:I = 0x7f03001f

.field public static final activity_main:I = 0x7f030020

.field public static final activity_navigation_demo:I = 0x7f030021

.field public static final activity_requests:I = 0x7f030022

.field public static final activity_support:I = 0x7f030023

.field public static final activity_video_player:I = 0x7f030024

.field public static final activity_view_article:I = 0x7f030025

.field public static final activity_view_request:I = 0x7f030026

.field public static final app_list_row:I = 0x7f030027

.field public static final app_main:I = 0x7f030028

.field public static final app_notifications:I = 0x7f030029

.field public static final app_toolbar:I = 0x7f03002a

.field public static final app_toolbar_transparent:I = 0x7f03002b

.field public static final app_toolbar_transparent_white:I = 0x7f03002c

.field public static final belvedere_dialog:I = 0x7f03002d

.field public static final belvedere_dialog_row:I = 0x7f03002e

.field public static final burger_header:I = 0x7f03002f

.field public static final calendar_list_row:I = 0x7f030030

.field public static final calendar_title_list_row:I = 0x7f030031

.field public static final choice_selected_text:I = 0x7f030032

.field public static final contact_spinner_list_item:I = 0x7f030033

.field public static final crop__activity_crop:I = 0x7f030034

.field public static final crop__layout_done_cancel:I = 0x7f030035

.field public static final debug_nav_coord_test_suite:I = 0x7f030036

.field public static final design_bottom_navigation_item:I = 0x7f030037

.field public static final design_bottom_sheet_dialog:I = 0x7f030038

.field public static final design_layout_snackbar:I = 0x7f030039

.field public static final design_layout_snackbar_include:I = 0x7f03003a

.field public static final design_layout_tab_icon:I = 0x7f03003b

.field public static final design_layout_tab_text:I = 0x7f03003c

.field public static final design_menu_item_action_area:I = 0x7f03003d

.field public static final design_navigation_item:I = 0x7f03003e

.field public static final design_navigation_item_header:I = 0x7f03003f

.field public static final design_navigation_item_separator:I = 0x7f030040

.field public static final design_navigation_item_subheader:I = 0x7f030041

.field public static final design_navigation_menu:I = 0x7f030042

.field public static final design_navigation_menu_item:I = 0x7f030043

.field public static final design_text_input_password_icon:I = 0x7f030044

.field public static final details_activity:I = 0x7f030045

.field public static final details_destination_bottom_sheet:I = 0x7f030046

.field public static final dialog_audio_muted:I = 0x7f030047

.field public static final dialog_audio_settings:I = 0x7f030048

.field public static final dialog_audio_status:I = 0x7f030049

.field public static final dialog_demo_video:I = 0x7f03004a

.field public static final dialog_for_attaching_logs:I = 0x7f03004b

.field public static final dialog_gestures:I = 0x7f03004c

.field public static final dialog_glance_demo:I = 0x7f03004d

.field public static final dialog_google_now:I = 0x7f03004e

.field public static final dialog_hud_local_music_browser:I = 0x7f03004f

.field public static final dialog_location:I = 0x7f030050

.field public static final dialog_mic_permission:I = 0x7f030051

.field public static final dialog_settings_messaging_add_edit:I = 0x7f030052

.field public static final dialog_voice_search:I = 0x7f030053

.field public static final drop_pin_activity:I = 0x7f030054

.field public static final error_loading_google_maps:I = 0x7f030055

.field public static final favorites_edit_activity:I = 0x7f030056

.field public static final favorites_item_layout:I = 0x7f030057

.field public static final feature_video_layout:I = 0x7f030058

.field public static final fl_bluetooth_recyclerview_item:I = 0x7f030059

.field public static final fl_fragment_bluetooth_dialog_connect_failed:I = 0x7f03005a

.field public static final fl_fragment_bluetooth_dialog_disabled:I = 0x7f03005b

.field public static final fl_fragment_bluetooth_dialog_framelayout:I = 0x7f03005c

.field public static final fl_fragment_bluetooth_dialog_not_compatible:I = 0x7f03005d

.field public static final fl_fragment_bluetooth_dialog_pairing:I = 0x7f03005e

.field public static final fl_fragment_bluetooth_dialog_searching:I = 0x7f03005f

.field public static final fl_fragment_bluetooth_dialog_searching_found:I = 0x7f030060

.field public static final fl_fragment_bluetooth_dialog_searching_frame_layout:I = 0x7f030061

.field public static final fl_fragment_bluetooth_dialog_searching_not_found:I = 0x7f030062

.field public static final fl_toolbar_settings_bluetooth_pairing:I = 0x7f030063

.field public static final fle_app_setup:I = 0x7f030064

.field public static final fle_app_setup_bottom_card_generic:I = 0x7f030065

.field public static final fle_app_setup_bottom_card_profile:I = 0x7f030066

.field public static final fle_app_setup_permission:I = 0x7f030067

.field public static final fle_box_picker:I = 0x7f030068

.field public static final fle_box_viewer:I = 0x7f030069

.field public static final fle_cable_picker:I = 0x7f03006a

.field public static final fle_install:I = 0x7f03006b

.field public static final fle_install_cla:I = 0x7f03006c

.field public static final fle_install_complete:I = 0x7f03006d

.field public static final fle_install_dial:I = 0x7f03006e

.field public static final fle_install_intro:I = 0x7f03006f

.field public static final fle_install_lens_check:I = 0x7f030070

.field public static final fle_install_locate_obd:I = 0x7f030071

.field public static final fle_install_medium_or_tall_mount:I = 0x7f030072

.field public static final fle_install_mounts:I = 0x7f030073

.field public static final fle_install_overview:I = 0x7f030074

.field public static final fle_install_secure_mount:I = 0x7f030075

.field public static final fle_install_short_mount:I = 0x7f030076

.field public static final fle_install_tidying_up:I = 0x7f030077

.field public static final fle_install_turn_on:I = 0x7f030078

.field public static final fle_install_video:I = 0x7f030079

.field public static final fle_install_watch_next:I = 0x7f03007a

.field public static final fle_marketing_flow:I = 0x7f03007b

.field public static final fle_marketing_flow_bg_image:I = 0x7f03007c

.field public static final fle_marketing_flow_bottom_text:I = 0x7f03007d

.field public static final fle_mount_not_recommended:I = 0x7f03007e

.field public static final fle_mount_picker:I = 0x7f03007f

.field public static final fle_vehicle_car_info:I = 0x7f030080

.field public static final fle_vehicle_car_md_info:I = 0x7f030081

.field public static final fle_vehicle_obd_locator:I = 0x7f030082

.field public static final fle_you_will_need_the_mount_kit:I = 0x7f030083

.field public static final fragment_about:I = 0x7f030084

.field public static final fragment_connection_status:I = 0x7f030085

.field public static final fragment_contact_zendesk:I = 0x7f030086

.field public static final fragment_custom_notification:I = 0x7f030087

.field public static final fragment_debug_actions:I = 0x7f030088

.field public static final fragment_device_picker:I = 0x7f030089

.field public static final fragment_file_transfer_test:I = 0x7f03008a

.field public static final fragment_gesture_control:I = 0x7f03008b

.field public static final fragment_google_address_picker:I = 0x7f03008c

.field public static final fragment_help:I = 0x7f03008d

.field public static final fragment_hud_settings:I = 0x7f03008e

.field public static final fragment_main:I = 0x7f03008f

.field public static final fragment_music_control:I = 0x7f030090

.field public static final fragment_nav_address_picker:I = 0x7f030091

.field public static final fragment_nav_address_picker_demo:I = 0x7f030092

.field public static final fragment_nav_route_browser:I = 0x7f030093

.field public static final fragment_remote_nav_control:I = 0x7f030094

.field public static final fragment_request_list:I = 0x7f030095

.field public static final fragment_road_snap_debug_tool:I = 0x7f030096

.field public static final fragment_s3_browser:I = 0x7f030097

.field public static final fragment_screen_config:I = 0x7f030098

.field public static final fragment_send_feedback:I = 0x7f030099

.field public static final fragment_settings:I = 0x7f03009a

.field public static final fragment_splash:I = 0x7f03009b

.field public static final fragment_test_screen:I = 0x7f03009c

.field public static final fragment_view_request:I = 0x7f03009d

.field public static final full_screen_image:I = 0x7f03009e

.field public static final google_map_header:I = 0x7f03009f

.field public static final here_map_header:I = 0x7f0300a0

.field public static final hfp_delay_settings_layout:I = 0x7f0300a1

.field public static final hockeyapp_activity_expiry_info:I = 0x7f0300a2

.field public static final hockeyapp_activity_feedback:I = 0x7f0300a3

.field public static final hockeyapp_activity_login:I = 0x7f0300a4

.field public static final hockeyapp_activity_update:I = 0x7f0300a5

.field public static final hockeyapp_fragment_update:I = 0x7f0300a6

.field public static final hockeyapp_view_feedback_message:I = 0x7f0300a7

.field public static final home_search_bar:I = 0x7f0300a8

.field public static final homescreen_container:I = 0x7f0300a9

.field public static final homescreen_toolbar:I = 0x7f0300aa

.field public static final hs_activity:I = 0x7f0300ab

.field public static final hs_fragment_favorites:I = 0x7f0300ac

.field public static final hs_fragment_glances:I = 0x7f0300ad

.field public static final hs_fragment_suggestions:I = 0x7f0300ae

.field public static final hs_fragment_trips:I = 0x7f0300af

.field public static final include_no_network_view:I = 0x7f0300b0

.field public static final include_retry_view:I = 0x7f0300b1

.field public static final list_item:I = 0x7f0300b2

.field public static final list_item_bucket:I = 0x7f0300b3

.field public static final list_item_device:I = 0x7f0300b4

.field public static final list_item_loading:I = 0x7f0300b5

.field public static final list_item_main:I = 0x7f0300b6

.field public static final main_lyt:I = 0x7f0300b7

.field public static final maneuver_panel_children:I = 0x7f0300b8

.field public static final maneuver_panel_root:I = 0x7f0300b9

.field public static final mic_permission_failed:I = 0x7f0300ba

.field public static final more_routes_activity:I = 0x7f0300bb

.field public static final notification_action:I = 0x7f0300bc

.field public static final notification_action_tombstone:I = 0x7f0300bd

.field public static final notification_media_action:I = 0x7f0300be

.field public static final notification_media_cancel_action:I = 0x7f0300bf

.field public static final notification_template_big_media:I = 0x7f0300c0

.field public static final notification_template_big_media_custom:I = 0x7f0300c1

.field public static final notification_template_big_media_narrow:I = 0x7f0300c2

.field public static final notification_template_big_media_narrow_custom:I = 0x7f0300c3

.field public static final notification_template_custom_big:I = 0x7f0300c4

.field public static final notification_template_icon_group:I = 0x7f0300c5

.field public static final notification_template_lines_media:I = 0x7f0300c6

.field public static final notification_template_media:I = 0x7f0300c7

.field public static final notification_template_media_custom:I = 0x7f0300c8

.field public static final notification_template_part_chronometer:I = 0x7f0300c9

.field public static final notification_template_part_time:I = 0x7f0300ca

.field public static final offline_banner:I = 0x7f0300cb

.field public static final padding_footer_layout:I = 0x7f0300cc

.field public static final pending_trip_card:I = 0x7f0300cd

.field public static final photo_hint:I = 0x7f0300ce

.field public static final photo_viewer:I = 0x7f0300cf

.field public static final place_autocomplete_fragment:I = 0x7f0300d0

.field public static final place_autocomplete_item_powered_by_google:I = 0x7f0300d1

.field public static final place_autocomplete_item_prediction:I = 0x7f0300d2

.field public static final place_autocomplete_progress:I = 0x7f0300d3

.field public static final record_route_name_prompt:I = 0x7f0300d4

.field public static final route_description:I = 0x7f0300d5

.field public static final routing_activity:I = 0x7f0300d6

.field public static final row_action:I = 0x7f0300d7

.field public static final row_agent_comment:I = 0x7f0300d8

.field public static final row_article:I = 0x7f0300d9

.field public static final row_article_attachment:I = 0x7f0300da

.field public static final row_category:I = 0x7f0300db

.field public static final row_end_user_comment:I = 0x7f0300dc

.field public static final row_loading_progress:I = 0x7f0300dd

.field public static final row_no_articles_found:I = 0x7f0300de

.field public static final row_padding:I = 0x7f0300df

.field public static final row_request:I = 0x7f0300e0

.field public static final row_search_article:I = 0x7f0300e1

.field public static final row_section:I = 0x7f0300e2

.field public static final screen_light:I = 0x7f0300e3

.field public static final search_activity:I = 0x7f0300e4

.field public static final search_header_powered_by_google:I = 0x7f0300e5

.field public static final search_list_item:I = 0x7f0300e6

.field public static final search_list_item_no_results:I = 0x7f0300e7

.field public static final search_list_item_services:I = 0x7f0300e8

.field public static final search_toolbar:I = 0x7f0300e9

.field public static final select_dialog_item_material:I = 0x7f0300ea

.field public static final select_dialog_multichoice_material:I = 0x7f0300eb

.field public static final select_dialog_singlechoice_material:I = 0x7f0300ec

.field public static final settings_audio:I = 0x7f0300ed

.field public static final settings_audio_hfp_not_connected:I = 0x7f0300ee

.field public static final settings_calendar:I = 0x7f0300ef

.field public static final settings_car:I = 0x7f0300f0

.field public static final settings_contact_us:I = 0x7f0300f1

.field public static final settings_feature_videos_activity:I = 0x7f0300f2

.field public static final settings_general:I = 0x7f0300f3

.field public static final settings_legal:I = 0x7f0300f4

.field public static final settings_messaging:I = 0x7f0300f5

.field public static final settings_messaging_header:I = 0x7f0300f6

.field public static final settings_messaging_list_item:I = 0x7f0300f7

.field public static final settings_navigation:I = 0x7f0300f8

.field public static final settings_ota:I = 0x7f0300f9

.field public static final settings_profile:I = 0x7f0300fa

.field public static final settings_report_a_problem_default_screen:I = 0x7f0300fb

.field public static final settings_support:I = 0x7f0300fc

.field public static final settings_switch_row:I = 0x7f0300fd

.field public static final submit_ticket_layout:I = 0x7f0300fe

.field public static final suggestion_footer:I = 0x7f0300ff

.field public static final suggestion_section_header:I = 0x7f030100

.field public static final support_simple_spinner_dropdown_item:I = 0x7f030101

.field public static final text_bubble:I = 0x7f030102

.field public static final text_choice_layout:I = 0x7f030103

.field public static final trip_overview_bottom_sheet:I = 0x7f030104

.field public static final trip_overview_bottom_sheet_white:I = 0x7f030105

.field public static final versions:I = 0x7f030106

.field public static final versions_app:I = 0x7f030107

.field public static final versions_display:I = 0x7f030108

.field public static final versions_serial_number:I = 0x7f030109

.field public static final view_attachment_container_item:I = 0x7f03010a

.field public static final view_attachment_inline_item:I = 0x7f03010b

.field public static final web_view_activity:I = 0x7f03010c

.field public static final zendesk_toolbar:I = 0x7f03010d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
