.class Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;
.super Ljava/lang/Object;
.source "GoogleAddressPickerFragment.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$OnCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;->onResult(Lcom/google/android/gms/location/places/PlaceBuffer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;

.field final synthetic val$destinationPlace:Lcom/google/android/gms/location/places/Place;

.field final synthetic val$isStreetAddress:Z

.field final synthetic val$places:Lcom/google/android/gms/location/places/PlaceBuffer;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;Lcom/google/android/gms/location/places/Place;ZLcom/google/android/gms/location/places/PlaceBuffer;)V
    .locals 0
    .param p1, "this$1"    # Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;

    .prologue
    .line 465
    iput-object p1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;

    iput-object p2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->val$destinationPlace:Lcom/google/android/gms/location/places/Place;

    iput-boolean p3, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->val$isStreetAddress:Z

    iput-object p4, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->val$places:Lcom/google/android/gms/location/places/PlaceBuffer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;)V
    .locals 4
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;
    .param p2, "error"    # Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$Error;

    .prologue
    .line 474
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;

    iget-object v0, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->val$destinationPlace:Lcom/google/android/gms/location/places/Place;

    iget-boolean v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->val$isStreetAddress:Z

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1300(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Lcom/google/android/gms/location/places/Place;ZLcom/google/android/gms/maps/model/LatLng;)V

    .line 475
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->val$places:Lcom/google/android/gms/location/places/PlaceBuffer;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceBuffer;->release()V

    .line 476
    return-void
.end method

.method public onSuccess(Lcom/navdy/client/app/framework/models/Destination;)V
    .locals 8
    .param p1, "destination"    # Lcom/navdy/client/app/framework/models/Destination;

    .prologue
    .line 468
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->this$1:Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;

    iget-object v0, v0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5;->this$0:Lcom/navdy/client/debug/GoogleAddressPickerFragment;

    iget-object v1, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->val$destinationPlace:Lcom/google/android/gms/location/places/Place;

    iget-boolean v2, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->val$isStreetAddress:Z

    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLat:D

    iget-wide v6, p1, Lcom/navdy/client/app/framework/models/Destination;->navigationLong:D

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-static {v0, v1, v2, v3}, Lcom/navdy/client/debug/GoogleAddressPickerFragment;->access$1300(Lcom/navdy/client/debug/GoogleAddressPickerFragment;Lcom/google/android/gms/location/places/Place;ZLcom/google/android/gms/maps/model/LatLng;)V

    .line 469
    iget-object v0, p0, Lcom/navdy/client/debug/GoogleAddressPickerFragment$5$1;->val$places:Lcom/google/android/gms/location/places/PlaceBuffer;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceBuffer;->release()V

    .line 470
    return-void
.end method
