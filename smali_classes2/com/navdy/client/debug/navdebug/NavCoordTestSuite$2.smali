.class Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;
.super Ljava/lang/Object;
.source "NavCoordTestSuite.java"

# interfaces
.implements Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor$NavigationCoordinatesRetrievalCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->doApiCalls(Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

.field final synthetic val$destination:Lcom/navdy/client/app/framework/models/Destination;

.field final synthetic val$step:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    .prologue
    .line 336
    iput-object p1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    iput-object p2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iput-object p3, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->val$step:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private goToNextStep()V
    .locals 3

    .prologue
    .line 402
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->val$step:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    invoke-static {v0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$1000(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V

    .line 404
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->val$step:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->access$1100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v1, "}\n"

    invoke-static {v0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 408
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$1200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)V

    .line 410
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$600(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)V

    .line 415
    :goto_0
    return-void

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v1, "},\n"

    invoke-static {v0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 413
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    iget-object v1, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    iget-object v2, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->val$step:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    invoke-virtual {v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;->nextStep()Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$500(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Lcom/navdy/client/app/framework/models/Destination;Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;)V

    goto :goto_0
.end method


# virtual methods
.method public onFailure(Ljava/lang/String;)V
    .locals 3
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 391
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v1, " \"is_successful\":false,\n"

    invoke-static {v0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 392
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " \"throwable\":\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\"\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 395
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v1, " ### FAILURE ###"

    invoke-static {v0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 396
    iget-object v0, p0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ### "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 398
    invoke-direct {p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->goToNextStep()V

    .line 399
    return-void
.end method

.method public onSuccess(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;Lcom/here/android/mpa/search/Address;Lcom/navdy/client/app/framework/models/Destination$Precision;)V
    .locals 21
    .param p1, "displayCoords"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "navCoords"    # Lcom/navdy/service/library/events/location/Coordinate;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "address"    # Lcom/here/android/mpa/search/Address;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "precisionLevel"    # Lcom/navdy/client/app/framework/models/Destination$Precision;

    .prologue
    .line 340
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-virtual {v2}, Lcom/navdy/client/app/framework/models/Destination;->getDisplayCoordinate()Lcom/navdy/service/library/events/location/Coordinate;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;)D

    move-result-wide v12

    .line 341
    .local v12, "distanceBetweenInOutCoords":D
    invoke-static/range {p1 .. p2}, Lcom/navdy/client/app/framework/map/MapUtils;->distanceBetween(Lcom/navdy/service/library/events/location/Coordinate;Lcom/navdy/service/library/events/location/Coordinate;)D

    move-result-wide v14

    .line 342
    .local v14, "distanceBetweenOutCoords":D
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->val$destination:Lcom/navdy/client/app/framework/models/Destination;

    invoke-static {v2}, Lcom/navdy/client/app/framework/map/NavCoordsAddressProcessor;->getDistanceThresholdFor(Lcom/navdy/client/app/framework/models/Destination;)D

    move-result-wide v16

    .line 345
    .local v16, "distanceThreshold":D
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v3, "  \"is_successful\":true,\n"

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 346
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"display_coords\":\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"nav_coords\":\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 348
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"distance_between_out_coords\":\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 349
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"distance_between_in_n_out_coords\":\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 350
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"distance_in_n_out_threshold\":\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 352
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    invoke-static {v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$900(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;)Ljava/util/HashMap;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->val$step:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$Step;

    new-instance v4, Landroid/util/Pair;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v4, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 354
    if-eqz p3, :cond_0

    .line 355
    invoke-virtual/range {p3 .. p3}, Lcom/here/android/mpa/search/Address;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "\n"

    const-string v4, ", "

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 356
    .local v8, "addressString":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/here/android/mpa/search/Address;->getHouseNumber()Ljava/lang/String;

    move-result-object v19

    .line 357
    .local v19, "streetNumber":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/here/android/mpa/search/Address;->getStreet()Ljava/lang/String;

    move-result-object v18

    .line 358
    .local v18, "streetName":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/here/android/mpa/search/Address;->getCity()Ljava/lang/String;

    move-result-object v9

    .line 359
    .local v9, "city":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/here/android/mpa/search/Address;->getState()Ljava/lang/String;

    move-result-object v11

    .line 360
    .local v11, "state":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/here/android/mpa/search/Address;->getPostalCode()Ljava/lang/String;

    move-result-object v20

    .line 361
    .local v20, "zipCode":Ljava/lang/String;
    invoke-virtual/range {p3 .. p3}, Lcom/here/android/mpa/search/Address;->getCountryName()Ljava/lang/String;

    move-result-object v10

    .line 363
    .local v10, "country":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v3, "  \"address\": {\n"

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 364
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"full\": \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 365
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"streetNumber\": \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 366
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"streetName\": \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 367
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"city\": \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 368
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"state\": \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 369
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"zipCode\": \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\",\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 370
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  \"country\": \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\"\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 371
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v3, " },\n"

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 375
    .end local v8    # "addressString":Ljava/lang/String;
    .end local v9    # "city":Ljava/lang/String;
    .end local v10    # "country":Ljava/lang/String;
    .end local v11    # "state":Ljava/lang/String;
    .end local v18    # "streetName":Ljava/lang/String;
    .end local v19    # "streetNumber":Ljava/lang/String;
    .end local v20    # "zipCode":Ljava/lang/String;
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " \"precisionLevel\":\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual/range {p4 .. p4}, Lcom/navdy/client/app/framework/models/Destination$Precision;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\"\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 378
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Address:                          "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-eqz p3, :cond_1

    invoke-virtual/range {p3 .. p3}, Lcom/here/android/mpa/search/Address;->getText()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 379
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v3, "  Display coords:                   "

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static/range {v2 .. v7}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$800(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;DD)V

    .line 380
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v3, "  Nav coords:                       "

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/navdy/service/library/events/location/Coordinate;->latitude:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/navdy/service/library/events/location/Coordinate;->longitude:Ljava/lang/Double;

    invoke-virtual {v6}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-static/range {v2 .. v7}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$800(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;DD)V

    .line 381
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Distance between out coords:      "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14, v15}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 382
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Distance between in & out coords: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 383
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "  Distance threshold for in & out:  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, v16

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$200(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    .line 385
    invoke-direct/range {p0 .. p0}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->goToNextStep()V

    .line 386
    return-void

    .line 373
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite$2;->this$0:Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;

    const-string v3, " \"address\": {},\n"

    invoke-static {v2, v3}, Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;->access$100(Lcom/navdy/client/debug/navdebug/NavCoordTestSuite;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 378
    :cond_1
    const-string v2, "null"

    goto/16 :goto_1
.end method
