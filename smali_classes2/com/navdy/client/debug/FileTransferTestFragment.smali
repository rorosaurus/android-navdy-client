.class public Lcom/navdy/client/debug/FileTransferTestFragment;
.super Landroid/app/Fragment;
.source "FileTransferTestFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/FileTransferTestFragment$OnFragmentInteractionListener;
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field private downloadStartTime:J

.field private downloadTotalBytes:J

.field private handler:Landroid/os/Handler;

.field progressContainer:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10023c
    .end annotation
.end field

.field sizeSelection:Landroid/widget/RadioGroup;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f100237
    .end annotation
.end field

.field transferButton:Landroid/widget/Button;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10023b
    .end annotation
.end field

.field transferMessage:Landroid/widget/TextView;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10023e
    .end annotation
.end field

.field transferProgress:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/InjectView;
        value = 0x7f10023d
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/FileTransferTestFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/FileTransferTestFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 41
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->handler:Landroid/os/Handler;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/navdy/client/debug/FileTransferTestFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/client/debug/FileTransferTestFragment;

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->downloadTotalBytes:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/navdy/client/debug/FileTransferTestFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/navdy/client/debug/FileTransferTestFragment;

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->downloadStartTime:J

    return-wide v0
.end method

.method static synthetic access$200()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/navdy/client/debug/FileTransferTestFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/navdy/client/debug/FileTransferTestFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/FileTransferTestFragment;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/navdy/client/debug/FileTransferTestFragment;->recordTransferComplete()V

    return-void
.end method

.method static synthetic access$400(Lcom/navdy/client/debug/FileTransferTestFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/navdy/client/debug/FileTransferTestFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/FileTransferTestFragment;->recordTransferFailed(Ljava/lang/String;)V

    return-void
.end method

.method private getSelectedSize()I
    .locals 7

    .prologue
    .line 83
    iget-object v2, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->sizeSelection:Landroid/widget/RadioGroup;

    invoke-virtual {v2}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    .line 84
    .local v0, "sel":I
    const/4 v1, 0x1

    .line 85
    .local v1, "size":I
    packed-switch v0, :pswitch_data_0

    .line 96
    sget-object v2, Lcom/navdy/client/debug/FileTransferTestFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v3, "unexpected file transfer size selection %x"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 98
    :goto_0
    return v1

    .line 87
    :pswitch_0
    const/4 v1, 0x1

    .line 88
    goto :goto_0

    .line 90
    :pswitch_1
    const/16 v1, 0xa

    .line 91
    goto :goto_0

    .line 93
    :pswitch_2
    const/16 v1, 0x64

    .line 94
    goto :goto_0

    .line 85
    :pswitch_data_0
    .packed-switch 0x7f100238
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private recordTransferComplete()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/debug/FileTransferTestFragment$1;

    invoke-direct {v1, p0}, Lcom/navdy/client/debug/FileTransferTestFragment$1;-><init>(Lcom/navdy/client/debug/FileTransferTestFragment;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 115
    return-void
.end method

.method private recordTransferFailed(Ljava/lang/String;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 118
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/navdy/client/debug/FileTransferTestFragment$2;

    invoke-direct {v1, p0, p1}, Lcom/navdy/client/debug/FileTransferTestFragment$2;-><init>(Lcom/navdy/client/debug/FileTransferTestFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 124
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const-wide/16 v6, 0x400

    const/4 v4, 0x0

    .line 127
    invoke-direct {p0}, Lcom/navdy/client/debug/FileTransferTestFragment;->getSelectedSize()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    int-to-long v2, v0

    mul-long/2addr v2, v6

    iput-wide v2, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->downloadTotalBytes:J

    .line 128
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->transferProgress:Landroid/widget/ProgressBar;

    iget-wide v2, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->downloadTotalBytes:J

    long-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 129
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->transferProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 130
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->transferMessage:Landroid/widget/TextView;

    const v2, 0x7f0804c2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 131
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->progressContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 132
    iget-object v0, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->transferButton:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    .line 134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->downloadStartTime:J

    .line 135
    new-instance v1, Lcom/navdy/client/debug/file/RemoteFileTransferManager;

    .line 136
    invoke-static {}, Lcom/navdy/client/app/NavdyApplication;->getAppContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/navdy/service/library/events/file/FileType;->FILE_TYPE_PERF_TEST:Lcom/navdy/service/library/events/file/FileType;

    .line 138
    invoke-direct {p0}, Lcom/navdy/client/debug/FileTransferTestFragment;->getSelectedSize()I

    move-result v0

    mul-int/lit16 v0, v0, 0x400

    int-to-long v4, v0

    mul-long/2addr v4, v6

    new-instance v6, Lcom/navdy/client/debug/FileTransferTestFragment$3;

    invoke-direct {v6, p0}, Lcom/navdy/client/debug/FileTransferTestFragment$3;-><init>(Lcom/navdy/client/debug/FileTransferTestFragment;)V

    invoke-direct/range {v1 .. v6}, Lcom/navdy/client/debug/file/RemoteFileTransferManager;-><init>(Landroid/content/Context;Lcom/navdy/service/library/events/file/FileType;JLcom/navdy/client/debug/file/FileTransferManager$FileTransferListener;)V

    .line 179
    .local v1, "fileTransferManager":Lcom/navdy/client/debug/file/RemoteFileTransferManager;
    invoke-static {}, Lcom/navdy/service/library/task/TaskManager;->getInstance()Lcom/navdy/service/library/task/TaskManager;

    move-result-object v0

    new-instance v2, Lcom/navdy/client/debug/FileTransferTestFragment$4;

    invoke-direct {v2, p0, v1}, Lcom/navdy/client/debug/FileTransferTestFragment$4;-><init>(Lcom/navdy/client/debug/FileTransferTestFragment;Lcom/navdy/client/debug/file/RemoteFileTransferManager;)V

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Lcom/navdy/service/library/task/TaskManager;->execute(Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    .line 185
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 67
    sget-object v0, Lcom/navdy/client/debug/FileTransferTestFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v1, "onCreate()"

    invoke-virtual {v0, v1}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    sget-object v1, Lcom/navdy/client/debug/FileTransferTestFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v2, "onCreateView()"

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/log/Logger;->w(Ljava/lang/String;)V

    .line 75
    const v1, 0x7f03008a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 76
    .local v0, "rootView":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->inject(Ljava/lang/Object;Landroid/view/View;)V

    .line 77
    iget-object v1, p0, Lcom/navdy/client/debug/FileTransferTestFragment;->transferButton:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    return-object v0
.end method
