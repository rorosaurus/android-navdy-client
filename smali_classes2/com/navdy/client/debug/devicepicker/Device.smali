.class public Lcom/navdy/client/debug/devicepicker/Device;
.super Ljava/lang/Object;
.source "Device.java"


# instance fields
.field protected connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

.field protected deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

.field protected preferredType:I


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/NavdyDeviceId;)V
    .locals 1
    .param p1, "deviceId"    # Lcom/navdy/service/library/device/NavdyDeviceId;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    invoke-static {}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->values()[Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Lcom/navdy/service/library/device/connection/ConnectionInfo;

    iput-object v0, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/debug/devicepicker/Device;->preferredType:I

    .line 32
    iput-object p1, p0, Lcom/navdy/client/debug/devicepicker/Device;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    .line 33
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V
    .locals 1
    .param p1, "info"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .prologue
    .line 27
    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/navdy/client/debug/devicepicker/Device;-><init>(Lcom/navdy/service/library/device/NavdyDeviceId;)V

    .line 28
    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/devicepicker/Device;->add(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V

    .line 29
    return-void
.end method

.method public static prettyName(Lcom/navdy/service/library/device/connection/ConnectionInfo;)Ljava/lang/String;
    .locals 2
    .param p0, "connection"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .prologue
    .line 16
    if-nez p0, :cond_1

    .line 17
    const-string v0, "none"

    .line 23
    :cond_0
    :goto_0
    return-object v0

    .line 19
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    .line 20
    .local v0, "deviceDescription":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getAddress()Lcom/navdy/service/library/device/connection/ServiceAddress;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ServiceAddress;->getAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V
    .locals 1
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .prologue
    .line 36
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/navdy/client/debug/devicepicker/Device;->add(Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V

    .line 37
    return-void
.end method

.method public add(Lcom/navdy/service/library/device/connection/ConnectionInfo;Z)V
    .locals 3
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .param p2, "updateIfFound"    # Z

    .prologue
    .line 40
    invoke-virtual {p1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v1

    invoke-static {v1}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->fromConnectionType(Lcom/navdy/service/library/device/connection/ConnectionType;)Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    move-result-object v0

    .line 41
    .local v0, "type":Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;
    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    invoke-virtual {v0}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->getPriority()I

    move-result v2

    aget-object v1, v1, v2

    if-nez v1, :cond_1

    .line 42
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    invoke-virtual {v0}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->getPriority()I

    move-result v2

    aput-object p1, v1, v2

    .line 44
    :cond_1
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 107
    if-ne p0, p1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v4

    .line 108
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    if-eq v6, v7, :cond_3

    :cond_2
    move v4, v5

    goto :goto_0

    :cond_3
    move-object v3, p1

    .line 110
    check-cast v3, Lcom/navdy/client/debug/devicepicker/Device;

    .line 112
    .local v3, "that":Lcom/navdy/client/debug/devicepicker/Device;
    iget-object v6, p0, Lcom/navdy/client/debug/devicepicker/Device;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    iget-object v7, v3, Lcom/navdy/client/debug/devicepicker/Device;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    move v4, v5

    goto :goto_0

    .line 115
    :cond_4
    iget-object v6, p0, Lcom/navdy/client/debug/devicepicker/Device;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v7, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v6, v7}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 116
    iget-object v6, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    array-length v6, v6

    iget-object v7, v3, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    array-length v7, v7

    if-eq v6, v7, :cond_5

    move v4, v5

    .line 117
    goto :goto_0

    .line 119
    :cond_5
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v6, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    array-length v6, v6

    if-ge v0, v6, :cond_0

    .line 120
    iget-object v6, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    aget-object v1, v6, v0

    .line 121
    .local v1, "lhs":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    iget-object v6, v3, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    aget-object v2, v6, v0

    .line 122
    .local v2, "rhs":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    if-eqz v1, :cond_6

    if-nez v2, :cond_7

    .line 123
    :cond_6
    if-eq v1, v2, :cond_8

    move v4, v5

    .line 124
    goto :goto_0

    .line 126
    :cond_7
    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    move v4, v5

    .line 127
    goto :goto_0

    .line 119
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getBluetoothAddress()Ljava/lang/String;
    .locals 3

    .prologue
    .line 84
    iget-object v1, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    sget-object v2, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->BT:Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    invoke-virtual {v2}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->getPriority()I

    move-result v2

    aget-object v0, v1, v2

    .line 85
    .local v0, "btConnectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getAddress()Lcom/navdy/service/library/device/connection/ServiceAddress;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ServiceAddress;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 87
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getConnectionTypes()[Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    invoke-virtual {v0}, [Lcom/navdy/service/library/device/connection/ConnectionInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/device/connection/ConnectionInfo;

    return-object v0
.end method

.method public getDeviceId()Lcom/navdy/service/library/device/NavdyDeviceId;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/navdy/client/debug/devicepicker/Device;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    return-object v0
.end method

.method public getIpAddress()Ljava/lang/String;
    .locals 3

    .prologue
    .line 77
    iget-object v1, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    sget-object v2, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->TCP:Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    invoke-virtual {v2}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->getPriority()I

    move-result v2

    aget-object v0, v1, v2

    .line 78
    .local v0, "tcpConnectionInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    if-eqz v0, :cond_0

    .line 79
    invoke-virtual {v0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getAddress()Lcom/navdy/service/library/device/connection/ServiceAddress;

    move-result-object v1

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ServiceAddress;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 80
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPreferredConnectionInfo()Lcom/navdy/service/library/device/connection/ConnectionInfo;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 55
    iget v3, p0, Lcom/navdy/client/debug/devicepicker/Device;->preferredType:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    iget v4, p0, Lcom/navdy/client/debug/devicepicker/Device;->preferredType:I

    aget-object v1, v3, v4

    .line 56
    .local v1, "preferredInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :goto_0
    if-eqz v1, :cond_1

    .line 65
    .end local v1    # "preferredInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :goto_1
    return-object v1

    :cond_0
    move-object v1, v2

    .line 55
    goto :goto_0

    .line 60
    .restart local v1    # "preferredInfo":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :cond_1
    iget-object v4, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    array-length v5, v4

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_3

    aget-object v0, v4, v3

    .line 61
    .local v0, "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 62
    goto :goto_1

    .line 60
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v0    # "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :cond_3
    move-object v1, v2

    .line 65
    goto :goto_1
.end method

.method public getPreferredType()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/navdy/client/debug/devicepicker/Device;->preferredType:I

    return v0
.end method

.method public getPrettyName()Ljava/lang/String;
    .locals 3

    .prologue
    .line 95
    iget-object v1, p0, Lcom/navdy/client/debug/devicepicker/Device;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    .local v0, "prettyName":Ljava/lang/String;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/navdy/client/debug/devicepicker/Device;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v2, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v1, v2}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object v1, v0

    .line 102
    :goto_0
    return-object v1

    .line 97
    :cond_0
    invoke-virtual {p0}, Lcom/navdy/client/debug/devicepicker/Device;->getIpAddress()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v1, v0

    .line 98
    goto :goto_0

    .line 99
    :cond_1
    invoke-virtual {p0}, Lcom/navdy/client/debug/devicepicker/Device;->getBluetoothAddress()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move-object v1, v0

    .line 100
    goto :goto_0

    .line 102
    :cond_2
    iget-object v1, p0, Lcom/navdy/client/debug/devicepicker/Device;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v1}, Lcom/navdy/service/library/device/NavdyDeviceId;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 137
    iget-object v3, p0, Lcom/navdy/client/debug/devicepicker/Device;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v3}, Lcom/navdy/service/library/device/NavdyDeviceId;->hashCode()I

    move-result v2

    .line 138
    .local v2, "result":I
    iget-object v3, p0, Lcom/navdy/client/debug/devicepicker/Device;->deviceId:Lcom/navdy/service/library/device/NavdyDeviceId;

    sget-object v4, Lcom/navdy/service/library/device/NavdyDeviceId;->UNKNOWN_ID:Lcom/navdy/service/library/device/NavdyDeviceId;

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/device/NavdyDeviceId;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 139
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 140
    iget-object v3, p0, Lcom/navdy/client/debug/devicepicker/Device;->connectionTypes:[Lcom/navdy/service/library/device/connection/ConnectionInfo;

    aget-object v1, v3, v0

    .line 141
    .local v1, "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    if-eqz v1, :cond_0

    .line 142
    mul-int/lit8 v3, v2, 0x1f

    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->hashCode()I

    move-result v4

    add-int v2, v3, v4

    .line 139
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 145
    .end local v0    # "i":I
    .end local v1    # "info":Lcom/navdy/service/library/device/connection/ConnectionInfo;
    :cond_1
    return v2
.end method

.method public setPreferredType(Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;)V
    .locals 1
    .param p1, "type"    # Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;

    .prologue
    .line 73
    invoke-virtual {p1}, Lcom/navdy/client/debug/devicepicker/PreferredConnectionType;->getPriority()I

    move-result v0

    iput v0, p0, Lcom/navdy/client/debug/devicepicker/Device;->preferredType:I

    .line 74
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/navdy/client/debug/devicepicker/Device;->getPrettyName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
