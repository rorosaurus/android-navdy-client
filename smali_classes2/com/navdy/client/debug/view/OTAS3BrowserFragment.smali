.class public Lcom/navdy/client/debug/view/OTAS3BrowserFragment;
.super Lcom/navdy/client/debug/view/S3BrowserFragment;
.source "OTAS3BrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/client/debug/view/OTAS3BrowserFragment$TempFileTransferListener;
    }
.end annotation


# static fields
.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/navdy/client/debug/view/S3BrowserFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000()Lcom/navdy/service/library/log/Logger;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-object v0
.end method


# virtual methods
.method protected getS3BucketName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string v0, "navdy-prod-release"

    return-object v0
.end method

.method protected onFileSelected(Ljava/lang/String;)V
    .locals 7
    .param p1, "fileS3Key"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 137
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 140
    .local v1, "fileName":Ljava/lang/String;
    :try_start_0
    const-string v3, "fromS3_"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 147
    .local v2, "tempFile":Ljava/io/File;
    :try_start_1
    new-instance v3, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$1;

    invoke-direct {v3, p0, v2, v1}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment$1;-><init>(Lcom/navdy/client/debug/view/OTAS3BrowserFragment;Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v2, v3}, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->storeS3ObjectInTemp(Ljava/lang/String;Ljava/io/File;Lcom/amazonaws/event/ProgressListener;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 163
    .end local v2    # "tempFile":Ljava/io/File;
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v0

    .line 142
    .local v0, "e":Ljava/io/IOException;
    sget-object v3, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Exception while creating temp file"

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 143
    iget-object v3, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    const-string v4, "Exception while creating temp file"

    invoke-virtual {v3, v4, v6}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;Z)V

    goto :goto_0

    .line 158
    .end local v0    # "e":Ljava/io/IOException;
    .restart local v2    # "tempFile":Ljava/io/File;
    :catch_1
    move-exception v0

    .line 159
    .restart local v0    # "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/File;->deleteOnExit()V

    .line 160
    sget-object v3, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v4, "Exception while downloading file from S3"

    invoke-virtual {v3, v4, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 161
    iget-object v3, p0, Lcom/navdy/client/debug/view/OTAS3BrowserFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    const-string v4, "Exception while downloading file from S3"

    invoke-virtual {v3, v4, v6}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;Z)V

    goto :goto_0
.end method
