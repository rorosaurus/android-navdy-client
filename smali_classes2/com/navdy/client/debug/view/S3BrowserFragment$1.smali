.class Lcom/navdy/client/debug/view/S3BrowserFragment$1;
.super Ljava/lang/Object;
.source "S3BrowserFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/view/S3BrowserFragment;->storeS3ObjectInTemp(Ljava/lang/String;Ljava/io/File;Lcom/amazonaws/event/ProgressListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

.field final synthetic val$downloadProgressListener:Lcom/amazonaws/event/ProgressListener;

.field final synthetic val$output:Ljava/io/File;

.field final synthetic val$s3Key:Ljava/lang/String;

.field final synthetic val$transferManager:Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/view/S3BrowserFragment;Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;Ljava/lang/String;Ljava/io/File;Lcom/amazonaws/event/ProgressListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/navdy/client/debug/view/S3BrowserFragment;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iput-object p2, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->val$transferManager:Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;

    iput-object p3, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->val$s3Key:Ljava/lang/String;

    iput-object p4, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->val$output:Ljava/io/File;

    iput-object p5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->val$downloadProgressListener:Lcom/amazonaws/event/ProgressListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 159
    iget-object v4, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->val$transferManager:Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;

    iget-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-virtual {v5}, Lcom/navdy/client/debug/view/S3BrowserFragment;->getS3BucketName()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->val$s3Key:Ljava/lang/String;

    iget-object v7, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->val$output:Ljava/io/File;

    invoke-virtual {v4, v5, v6, v7}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;->download(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

    move-result-object v0

    .line 160
    .local v0, "download":Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 162
    .local v2, "startTime":J
    iget-object v4, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->val$downloadProgressListener:Lcom/amazonaws/event/ProgressListener;

    invoke-interface {v0, v4}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;->addProgressListener(Lcom/amazonaws/event/ProgressListener;)V

    .line 164
    new-instance v4, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;

    invoke-direct {v4, p0, v2, v3, v0}, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment$1;JLcom/amazonaws/mobileconnectors/s3/transfermanager/Download;)V

    invoke-interface {v0, v4}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;->addProgressListener(Lcom/amazonaws/event/ProgressListener;)V

    .line 199
    :try_start_0
    invoke-interface {v0}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;->waitForCompletion()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    new-instance v4, Landroid/os/Handler;

    iget-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iget-object v5, v5, Lcom/navdy/client/debug/view/S3BrowserFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v5, Lcom/navdy/client/debug/view/S3BrowserFragment$1$2;

    invoke-direct {v5, p0}, Lcom/navdy/client/debug/view/S3BrowserFragment$1$2;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 215
    :goto_0
    return-void

    .line 200
    :catch_0
    move-exception v1

    .line 201
    .local v1, "e":Ljava/lang/Throwable;
    :try_start_1
    invoke-static {}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v4

    const-string v5, "Download interrupted "

    invoke-virtual {v4, v5, v1}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 202
    iget-object v4, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iget-object v4, v4, Lcom/navdy/client/debug/view/S3BrowserFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    const-string v5, "S3 download process raised exception"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 204
    new-instance v4, Landroid/os/Handler;

    iget-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iget-object v5, v5, Lcom/navdy/client/debug/view/S3BrowserFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v5, Lcom/navdy/client/debug/view/S3BrowserFragment$1$2;

    invoke-direct {v5, p0}, Lcom/navdy/client/debug/view/S3BrowserFragment$1$2;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .end local v1    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v4

    new-instance v5, Landroid/os/Handler;

    iget-object v6, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iget-object v6, v6, Lcom/navdy/client/debug/view/S3BrowserFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v6, Lcom/navdy/client/debug/view/S3BrowserFragment$1$2;

    invoke-direct {v6, p0}, Lcom/navdy/client/debug/view/S3BrowserFragment$1$2;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    throw v4
.end method
