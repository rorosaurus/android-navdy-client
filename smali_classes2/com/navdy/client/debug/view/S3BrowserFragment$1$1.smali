.class Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;
.super Ljava/lang/Object;
.source "S3BrowserFragment.java"

# interfaces
.implements Lcom/amazonaws/event/ProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/navdy/client/debug/view/S3BrowserFragment$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private downloadedSize:J

.field private progressEventCounter:I

.field final synthetic this$1:Lcom/navdy/client/debug/view/S3BrowserFragment$1;

.field final synthetic val$download:Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

.field final synthetic val$startTime:J


# direct methods
.method constructor <init>(Lcom/navdy/client/debug/view/S3BrowserFragment$1;JLcom/amazonaws/mobileconnectors/s3/transfermanager/Download;)V
    .locals 2
    .param p1, "this$1"    # Lcom/navdy/client/debug/view/S3BrowserFragment$1;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->this$1:Lcom/navdy/client/debug/view/S3BrowserFragment$1;

    iput-wide p2, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->val$startTime:J

    iput-object p4, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->val$download:Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    const/4 v0, 0x0

    iput v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->progressEventCounter:I

    .line 166
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->downloadedSize:J

    return-void
.end method


# virtual methods
.method public progressChanged(Lcom/amazonaws/event/ProgressEvent;)V
    .locals 11
    .param p1, "progressEvent"    # Lcom/amazonaws/event/ProgressEvent;

    .prologue
    const/4 v10, 0x0

    .line 170
    iget-wide v6, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->downloadedSize:J

    invoke-virtual {p1}, Lcom/amazonaws/event/ProgressEvent;->getBytesTransferred()J

    move-result-wide v8

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->downloadedSize:J

    .line 171
    invoke-virtual {p1}, Lcom/amazonaws/event/ProgressEvent;->getEventCode()I

    move-result v1

    .line 172
    .local v1, "eventCode":I
    const/4 v5, 0x4

    if-ne v1, v5, :cond_1

    .line 173
    invoke-static {}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "S3 file downloaded"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    const/16 v5, 0x8

    if-ne v1, v5, :cond_2

    .line 175
    invoke-static {}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "Failed to download file from S3"

    invoke-virtual {v5, v6}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 176
    :cond_2
    iget v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->progressEventCounter:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->progressEventCounter:I

    const/16 v6, 0x3e8

    if-le v5, v6, :cond_0

    .line 177
    iput v10, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->progressEventCounter:I

    .line 178
    const-string v5, "%s downloaded"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-wide v8, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->downloadedSize:J

    .line 179
    invoke-static {v8, v9}, Lcom/navdy/client/debug/util/FormatUtils;->readableFileSize(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    .line 178
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 180
    .local v4, "message":Ljava/lang/String;
    invoke-static {}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 181
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->val$startTime:J

    sub-long v2, v6, v8

    .line 182
    .local v2, "elapsedTime":J
    const-wide/32 v6, 0x493e0

    cmp-long v5, v2, v6

    if-lez v5, :cond_3

    .line 183
    iget-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->this$1:Lcom/navdy/client/debug/view/S3BrowserFragment$1;

    iget-object v5, v5, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iget-object v5, v5, Lcom/navdy/client/debug/view/S3BrowserFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    const-string v6, "S3 download timeout raised"

    invoke-virtual {v5, v6, v10}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;Z)V

    .line 185
    :try_start_0
    iget-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->val$download:Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;

    invoke-interface {v5}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/Download;->abort()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    iget-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->this$1:Lcom/navdy/client/debug/view/S3BrowserFragment$1;

    iget-object v5, v5, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->val$transferManager:Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;

    invoke-virtual {v5, v10}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;->shutdownNow(Z)V

    goto :goto_0

    .line 186
    :catch_0
    move-exception v0

    .line 187
    .local v0, "e":Ljava/io/IOException;
    :try_start_1
    invoke-static {}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v5

    const-string v6, "Exception while aborting S3 download"

    invoke-virtual {v5, v6, v0}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 189
    iget-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->this$1:Lcom/navdy/client/debug/view/S3BrowserFragment$1;

    iget-object v5, v5, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->val$transferManager:Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;

    invoke-virtual {v5, v10}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;->shutdownNow(Z)V

    goto :goto_0

    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    iget-object v6, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->this$1:Lcom/navdy/client/debug/view/S3BrowserFragment$1;

    iget-object v6, v6, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->val$transferManager:Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;

    invoke-virtual {v6, v10}, Lcom/amazonaws/mobileconnectors/s3/transfermanager/TransferManager;->shutdownNow(Z)V

    throw v5

    .line 192
    :cond_3
    iget-object v5, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$1$1;->this$1:Lcom/navdy/client/debug/view/S3BrowserFragment$1;

    iget-object v5, v5, Lcom/navdy/client/debug/view/S3BrowserFragment$1;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iget-object v5, v5, Lcom/navdy/client/debug/view/S3BrowserFragment;->mAppInstance:Lcom/navdy/client/app/framework/AppInstance;

    invoke-virtual {v5, v4, v10, v10}, Lcom/navdy/client/app/framework/AppInstance;->showToast(Ljava/lang/String;ZI)V

    goto :goto_0
.end method
