.class public Lcom/navdy/client/debug/view/VideoS3BrowserFragment;
.super Lcom/navdy/client/debug/view/S3BrowserFragment;
.source "VideoS3BrowserFragment.java"


# static fields
.field protected static final URL_EXPIRATION:Ljava/util/Date;

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 21
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    .line 23
    new-instance v0, Ljava/util/Date;

    .line 24
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x36ee80

    add-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    sput-object v0, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;->URL_EXPIRATION:Ljava/util/Date;

    .line 23
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/navdy/client/debug/view/S3BrowserFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected getS3BucketName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    const-string v0, "gesture-data"

    return-object v0
.end method

.method protected onFileSelected(Ljava/lang/String;)V
    .locals 6
    .param p1, "fileS3Key"    # Ljava/lang/String;

    .prologue
    .line 32
    const-string v3, ".mp4"

    invoke-virtual {p1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-string v4, "Please select a video file"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 34
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 48
    :goto_0
    return-void

    .line 37
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 39
    .local v1, "sessionPath":Ljava/lang/String;
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/navdy/client/debug/videoplayer/VideoPlayerActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 40
    .local v0, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;->mClient:Lcom/amazonaws/services/s3/AmazonS3Client;

    invoke-virtual {p0}, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;->getS3BucketName()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;->URL_EXPIRATION:Ljava/util/Date;

    invoke-virtual {v3, v4, p1, v5}, Lcom/amazonaws/services/s3/AmazonS3Client;->generatePresignedUrl(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)Ljava/net/URL;

    move-result-object v2

    .line 42
    .local v2, "videoUrl":Ljava/net/URL;
    sget-object v3, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;->sLogger:Lcom/navdy/service/library/log/Logger;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Generated signed url "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/navdy/service/library/log/Logger;->d(Ljava/lang/String;)V

    .line 43
    const-string v3, "video_url_extra"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 44
    const-string v3, "session_path_extra"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 45
    const-string v3, "bucket_name_extra"

    invoke-virtual {p0}, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;->getS3BucketName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    invoke-virtual {p0, v0}, Lcom/navdy/client/debug/view/VideoS3BrowserFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
