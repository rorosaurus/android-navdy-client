.class Lcom/navdy/client/debug/view/S3BrowserFragment$ItemClickListener;
.super Ljava/lang/Object;
.source "S3BrowserFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/view/S3BrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;


# direct methods
.method private constructor <init>(Lcom/navdy/client/debug/view/S3BrowserFragment;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ItemClickListener;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/client/debug/view/S3BrowserFragment;Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/client/debug/view/S3BrowserFragment;
    .param p2, "x1"    # Lcom/navdy/client/debug/view/S3BrowserFragment$1;

    .prologue
    .line 264
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/view/S3BrowserFragment$ItemClickListener;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "pos"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 268
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ItemClickListener;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-static {v1}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$400(Lcom/navdy/client/debug/view/S3BrowserFragment;)Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;

    .line 269
    .local v0, "item":Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;
    invoke-virtual {v0}, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->isFolder()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 270
    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ItemClickListener;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-virtual {v0}, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$302(Lcom/navdy/client/debug/view/S3BrowserFragment;Ljava/lang/String;)Ljava/lang/String;

    .line 271
    new-instance v1, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;

    iget-object v2, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ItemClickListener;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment;Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 275
    :goto_0
    return-void

    .line 273
    :cond_0
    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$ItemClickListener;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-virtual {v0}, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/navdy/client/debug/view/S3BrowserFragment;->onFileSelected(Ljava/lang/String;)V

    goto :goto_0
.end method
