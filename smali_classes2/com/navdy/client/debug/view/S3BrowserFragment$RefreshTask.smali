.class Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;
.super Landroid/os/AsyncTask;
.source "S3BrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/client/debug/view/S3BrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RefreshTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/amazonaws/services/s3/model/ObjectListing;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;


# direct methods
.method private constructor <init>(Lcom/navdy/client/debug/view/S3BrowserFragment;)V
    .locals 0

    .prologue
    .line 219
    iput-object p1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/navdy/client/debug/view/S3BrowserFragment;Lcom/navdy/client/debug/view/S3BrowserFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/navdy/client/debug/view/S3BrowserFragment;
    .param p2, "x1"    # Lcom/navdy/client/debug/view/S3BrowserFragment$1;

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;-><init>(Lcom/navdy/client/debug/view/S3BrowserFragment;)V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/amazonaws/services/s3/model/ObjectListing;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    const/4 v7, 0x0

    .line 230
    :try_start_0
    new-instance v0, Lcom/amazonaws/services/s3/model/ListObjectsRequest;

    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    .line 231
    invoke-virtual {v1}, Lcom/navdy/client/debug/view/S3BrowserFragment;->getS3BucketName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-static {v2}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$300(Lcom/navdy/client/debug/view/S3BrowserFragment;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const-string v4, "/"

    const/16 v5, 0x3e8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/amazonaws/services/s3/model/ListObjectsRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 232
    .local v0, "request":Lcom/amazonaws/services/s3/model/ListObjectsRequest;
    iget-object v1, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iget-object v1, v1, Lcom/navdy/client/debug/view/S3BrowserFragment;->mClient:Lcom/amazonaws/services/s3/AmazonS3Client;

    invoke-virtual {v1, v0}, Lcom/amazonaws/services/s3/AmazonS3Client;->listObjects(Lcom/amazonaws/services/s3/model/ListObjectsRequest;)Lcom/amazonaws/services/s3/model/ObjectListing;
    :try_end_0
    .catch Lcom/amazonaws/AmazonClientException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 236
    .end local v0    # "request":Lcom/amazonaws/services/s3/model/ListObjectsRequest;
    :goto_0
    return-object v1

    .line 233
    :catch_0
    move-exception v6

    .line 234
    .local v6, "e":Lcom/amazonaws/AmazonClientException;
    invoke-static {}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Exception while listing the files in the S3 Bucket :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-virtual {v3}, Lcom/navdy/client/debug/view/S3BrowserFragment;->getS3BucketName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", Prefix :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$300(Lcom/navdy/client/debug/view/S3BrowserFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v6}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v7

    .line 236
    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 219
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->doInBackground([Ljava/lang/Void;)Lcom/amazonaws/services/s3/model/ObjectListing;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/amazonaws/services/s3/model/ObjectListing;)V
    .locals 5
    .param p1, "objects"    # Lcom/amazonaws/services/s3/model/ObjectListing;

    .prologue
    .line 241
    if-eqz p1, :cond_0

    .line 242
    invoke-virtual {p1}, Lcom/amazonaws/services/s3/model/ObjectListing;->getCommonPrefixes()Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 243
    invoke-virtual {p1}, Lcom/amazonaws/services/s3/model/ObjectListing;->getObjectSummaries()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_1

    .line 244
    :cond_0
    invoke-static {}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$200()Lcom/navdy/service/library/log/Logger;

    move-result-object v2

    const-string v3, "onPostExecute objects are null !!!"

    invoke-virtual {v2, v3}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;)V

    .line 257
    :goto_0
    return-void

    .line 248
    :cond_1
    iget-object v2, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-static {v2}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$400(Lcom/navdy/client/debug/view/S3BrowserFragment;)Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;->clear()V

    .line 249
    invoke-virtual {p1}, Lcom/amazonaws/services/s3/model/ObjectListing;->getCommonPrefixes()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    .local v0, "commonPrefix":Ljava/lang/String;
    iget-object v3, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$400(Lcom/navdy/client/debug/view/S3BrowserFragment;)Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;

    move-result-object v3

    new-instance v4, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;

    invoke-direct {v4, v0}, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;->add(Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;)V

    goto :goto_1

    .line 252
    .end local v0    # "commonPrefix":Ljava/lang/String;
    :cond_2
    invoke-virtual {p1}, Lcom/amazonaws/services/s3/model/ObjectListing;->getObjectSummaries()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/amazonaws/services/s3/model/S3ObjectSummary;

    .line 253
    .local v1, "summary":Lcom/amazonaws/services/s3/model/S3ObjectSummary;
    iget-object v3, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    invoke-static {v3}, Lcom/navdy/client/debug/view/S3BrowserFragment;->access$400(Lcom/navdy/client/debug/view/S3BrowserFragment;)Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;

    move-result-object v3

    new-instance v4, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;

    invoke-direct {v4, v1}, Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;-><init>(Lcom/amazonaws/services/s3/model/S3ObjectSummary;)V

    invoke-virtual {v3, v4}, Lcom/navdy/client/debug/view/S3BrowserFragment$ObjectAdapter;->add(Lcom/navdy/client/debug/view/S3BrowserFragment$MyS3Object;)V

    goto :goto_2

    .line 255
    .end local v1    # "summary":Lcom/amazonaws/services/s3/model/S3ObjectSummary;
    :cond_3
    iget-object v2, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iget-object v2, v2, Lcom/navdy/client/debug/view/S3BrowserFragment;->mRefreshButton:Landroid/widget/Button;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setEnabled(Z)V

    .line 256
    iget-object v2, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iget-object v2, v2, Lcom/navdy/client/debug/view/S3BrowserFragment;->mRefreshButton:Landroid/widget/Button;

    const v3, 0x7f0803d2

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 219
    check-cast p1, Lcom/amazonaws/services/s3/model/ObjectListing;

    invoke-virtual {p0, p1}, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->onPostExecute(Lcom/amazonaws/services/s3/model/ObjectListing;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iget-object v0, v0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mRefreshButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 223
    iget-object v0, p0, Lcom/navdy/client/debug/view/S3BrowserFragment$RefreshTask;->this$0:Lcom/navdy/client/debug/view/S3BrowserFragment;

    iget-object v0, v0, Lcom/navdy/client/debug/view/S3BrowserFragment;->mRefreshButton:Landroid/widget/Button;

    const v1, 0x7f080567

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 224
    return-void
.end method
