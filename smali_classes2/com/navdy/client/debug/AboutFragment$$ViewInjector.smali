.class public Lcom/navdy/client/debug/AboutFragment$$ViewInjector;
.super Ljava/lang/Object;
.source "AboutFragment$$ViewInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static inject(Lbutterknife/ButterKnife$Finder;Lcom/navdy/client/debug/AboutFragment;Ljava/lang/Object;)V
    .locals 3
    .param p0, "finder"    # Lbutterknife/ButterKnife$Finder;
    .param p1, "target"    # Lcom/navdy/client/debug/AboutFragment;
    .param p2, "source"    # Ljava/lang/Object;

    .prologue
    .line 10
    const v1, 0x7f10021d

    const-string v2, "field \'appVersionLabel\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 11
    .local v0, "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/AboutFragment;->appVersionLabel:Landroid/widget/TextView;

    .line 12
    const v1, 0x7f10021e

    const-string v2, "field \'updateStatusTextView\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 13
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/AboutFragment;->updateStatusTextView:Landroid/widget/TextView;

    .line 14
    const v1, 0x7f100220

    const-string v2, "field \'displayInfo\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 15
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/AboutFragment;->displayInfo:Landroid/widget/TextView;

    .line 16
    const v1, 0x7f100221

    const-string v2, "field \'serialLabel\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 17
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/AboutFragment;->serialLabel:Landroid/widget/TextView;

    .line 18
    const v1, 0x7f100222

    const-string v2, "field \'serialNumber\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 19
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/AboutFragment;->serialNumber:Landroid/widget/TextView;

    .line 20
    const v1, 0x7f100223

    const-string v2, "field \'vinLabel\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 21
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/AboutFragment;->vinLabel:Landroid/widget/TextView;

    .line 22
    const v1, 0x7f100224

    const-string v2, "field \'vin\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 23
    .restart local v0    # "view":Landroid/view/View;
    check-cast v0, Landroid/widget/TextView;

    .end local v0    # "view":Landroid/view/View;
    iput-object v0, p1, Lcom/navdy/client/debug/AboutFragment;->vin:Landroid/widget/TextView;

    .line 24
    const v1, 0x7f10021f

    const-string v2, "method \'onCheckForUpdate\'"

    invoke-virtual {p0, p2, v1, v2}, Lbutterknife/ButterKnife$Finder;->findRequiredView(Ljava/lang/Object;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 25
    .restart local v0    # "view":Landroid/view/View;
    new-instance v1, Lcom/navdy/client/debug/AboutFragment$$ViewInjector$1;

    invoke-direct {v1, p1}, Lcom/navdy/client/debug/AboutFragment$$ViewInjector$1;-><init>(Lcom/navdy/client/debug/AboutFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    return-void
.end method

.method public static reset(Lcom/navdy/client/debug/AboutFragment;)V
    .locals 1
    .param p0, "target"    # Lcom/navdy/client/debug/AboutFragment;

    .prologue
    const/4 v0, 0x0

    .line 36
    iput-object v0, p0, Lcom/navdy/client/debug/AboutFragment;->appVersionLabel:Landroid/widget/TextView;

    .line 37
    iput-object v0, p0, Lcom/navdy/client/debug/AboutFragment;->updateStatusTextView:Landroid/widget/TextView;

    .line 38
    iput-object v0, p0, Lcom/navdy/client/debug/AboutFragment;->displayInfo:Landroid/widget/TextView;

    .line 39
    iput-object v0, p0, Lcom/navdy/client/debug/AboutFragment;->serialLabel:Landroid/widget/TextView;

    .line 40
    iput-object v0, p0, Lcom/navdy/client/debug/AboutFragment;->serialNumber:Landroid/widget/TextView;

    .line 41
    iput-object v0, p0, Lcom/navdy/client/debug/AboutFragment;->vinLabel:Landroid/widget/TextView;

    .line 42
    iput-object v0, p0, Lcom/navdy/client/debug/AboutFragment;->vin:Landroid/widget/TextView;

    .line 43
    return-void
.end method
