.class final Lcom/navdy/service/library/task/TaskManager$PriorityTask;
.super Ljava/util/concurrent/FutureTask;
.source "TaskManager.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/task/TaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PriorityTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/concurrent/FutureTask",
        "<TT;>;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/navdy/service/library/task/TaskManager$PriorityTask",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private final priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "runnable"    # Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;",
            "TT;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lcom/navdy/service/library/task/TaskManager$PriorityTask;, "Lcom/navdy/service/library/task/TaskManager$PriorityTask<TT;>;"
    .local p2, "result":Ljava/lang/Object;, "TT;"
    invoke-direct {p0, p1, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    .line 57
    iput-object p1, p0, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    .line 58
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;Ljava/util/concurrent/Callable;)V
    .locals 0
    .param p1, "runnable"    # Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p0, "this":Lcom/navdy/service/library/task/TaskManager$PriorityTask;, "Lcom/navdy/service/library/task/TaskManager$PriorityTask<TT;>;"
    .local p2, "tCallable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    invoke-direct {p0, p2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 52
    iput-object p1, p0, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    .line 53
    return-void
.end method


# virtual methods
.method public compareTo(Lcom/navdy/service/library/task/TaskManager$PriorityTask;)I
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/task/TaskManager$PriorityTask",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "this":Lcom/navdy/service/library/task/TaskManager$PriorityTask;, "Lcom/navdy/service/library/task/TaskManager$PriorityTask<TT;>;"
    .local p1, "o":Lcom/navdy/service/library/task/TaskManager$PriorityTask;, "Lcom/navdy/service/library/task/TaskManager$PriorityTask<TT;>;"
    iget-object v1, p0, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    iget v1, v1, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->priority:I

    iget-object v2, p1, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    iget v2, v2, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->priority:I

    sub-int v0, v1, v2

    .line 63
    .local v0, "diff":I
    if-nez v0, :cond_0

    .line 64
    iget-object v1, p0, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    iget-wide v2, v1, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->order:J

    iget-object v1, p1, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    iget-wide v4, v1, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->order:J

    sub-long/2addr v2, v4

    long-to-int v0, v2

    .line 66
    .end local v0    # "diff":I
    :cond_0
    return v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 47
    .local p0, "this":Lcom/navdy/service/library/task/TaskManager$PriorityTask;, "Lcom/navdy/service/library/task/TaskManager$PriorityTask<TT;>;"
    check-cast p1, Lcom/navdy/service/library/task/TaskManager$PriorityTask;

    invoke-virtual {p0, p1}, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->compareTo(Lcom/navdy/service/library/task/TaskManager$PriorityTask;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .local p0, "this":Lcom/navdy/service/library/task/TaskManager$PriorityTask;, "Lcom/navdy/service/library/task/TaskManager$PriorityTask<TT;>;"
    const/4 v1, 0x0

    .line 72
    instance-of v2, p1, Lcom/navdy/service/library/task/TaskManager$PriorityTask;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 73
    check-cast v0, Lcom/navdy/service/library/task/TaskManager$PriorityTask;

    .line 74
    .local v0, "other":Lcom/navdy/service/library/task/TaskManager$PriorityTask;
    iget-object v2, p0, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    iget v2, v2, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->priority:I

    iget-object v3, v0, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    iget v3, v3, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->priority:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    iget-wide v2, v2, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->order:J

    iget-object v4, v0, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    iget-wide v4, v4, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->order:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 77
    .end local v0    # "other":Lcom/navdy/service/library/task/TaskManager$PriorityTask;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 83
    .local p0, "this":Lcom/navdy/service/library/task/TaskManager$PriorityTask;, "Lcom/navdy/service/library/task/TaskManager$PriorityTask<TT;>;"
    iget-object v0, p0, Lcom/navdy/service/library/task/TaskManager$PriorityTask;->priorityRunnable:Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;

    iget-wide v0, v0, Lcom/navdy/service/library/task/TaskManager$PriorityRunnable;->order:J

    long-to-int v0, v0

    return v0
.end method
