.class public final enum Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;
.super Ljava/lang/Enum;
.source "ConnectionStateChange.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/connection/ConnectionStateChange;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ConnectionState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

.field public static final enum CONNECTION_CONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

.field public static final enum CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

.field public static final enum CONNECTION_LINK_ESTABLISHED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

.field public static final enum CONNECTION_LINK_LOST:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

.field public static final enum CONNECTION_VERIFIED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 96
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    const-string v1, "CONNECTION_CONNECTED"

    invoke-direct {v0, v1, v7, v3}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_CONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    .line 97
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    const-string v1, "CONNECTION_DISCONNECTED"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    .line 98
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    const-string v1, "CONNECTION_LINK_ESTABLISHED"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_ESTABLISHED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    .line 99
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    const-string v1, "CONNECTION_LINK_LOST"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_LOST:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    .line 100
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    const-string v1, "CONNECTION_VERIFIED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_VERIFIED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    .line 91
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_CONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_DISCONNECTED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_ESTABLISHED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_LINK_LOST:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->CONNECTION_VERIFIED:Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->$VALUES:[Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 105
    iput p3, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->value:I

    .line 106
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 91
    const-class v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;
    .locals 1

    .prologue
    .line 91
    sget-object v0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->$VALUES:[Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/navdy/service/library/events/connection/ConnectionStateChange$ConnectionState;->value:I

    return v0
.end method
