.class public final Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "ConnectionRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/connection/ConnectionRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/connection/ConnectionRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

.field public remoteDeviceId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 70
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/connection/ConnectionRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/connection/ConnectionRequest;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 74
    if-nez p1, :cond_0

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionRequest;->action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    iput-object v0, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;->action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    .line 76
    iget-object v0, p1, Lcom/navdy/service/library/events/connection/ConnectionRequest;->remoteDeviceId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;->remoteDeviceId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public action(Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;)Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;
    .locals 0
    .param p1, "action"    # Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;->action:Lcom/navdy/service/library/events/connection/ConnectionRequest$Action;

    .line 81
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/connection/ConnectionRequest;
    .locals 2

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;->checkRequiredFields()V

    .line 96
    new-instance v0, Lcom/navdy/service/library/events/connection/ConnectionRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/connection/ConnectionRequest;-><init>(Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;Lcom/navdy/service/library/events/connection/ConnectionRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;->build()Lcom/navdy/service/library/events/connection/ConnectionRequest;

    move-result-object v0

    return-object v0
.end method

.method public remoteDeviceId(Ljava/lang/String;)Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;
    .locals 0
    .param p1, "remoteDeviceId"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/navdy/service/library/events/connection/ConnectionRequest$Builder;->remoteDeviceId:Ljava/lang/String;

    .line 90
    return-object p0
.end method
