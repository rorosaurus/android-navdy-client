.class public final Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "StartDrivePlaybackEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public label:Ljava/lang/String;

.field public playSecondaryLocation:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 70
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 74
    if-nez p1, :cond_0

    .line 77
    :goto_0
    return-void

    .line 75
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;->label:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent$Builder;->label:Ljava/lang/String;

    .line 76
    iget-object v0, p1, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;->playSecondaryLocation:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent$Builder;->playSecondaryLocation:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;
    .locals 2

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent$Builder;->checkRequiredFields()V

    .line 98
    new-instance v0, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;-><init>(Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent$Builder;Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent$Builder;->build()Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent;

    move-result-object v0

    return-object v0
.end method

.method public label(Ljava/lang/String;)Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent$Builder;
    .locals 0
    .param p1, "label"    # Ljava/lang/String;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent$Builder;->label:Ljava/lang/String;

    .line 84
    return-object p0
.end method

.method public playSecondaryLocation(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent$Builder;
    .locals 0
    .param p1, "playSecondaryLocation"    # Ljava/lang/Boolean;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/navdy/service/library/events/debug/StartDrivePlaybackEvent$Builder;->playSecondaryLocation:Ljava/lang/Boolean;

    .line 92
    return-object p0
.end method
