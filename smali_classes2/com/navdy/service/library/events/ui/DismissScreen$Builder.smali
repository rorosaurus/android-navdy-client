.class public final Lcom/navdy/service/library/events/ui/DismissScreen$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DismissScreen.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/ui/DismissScreen;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/ui/DismissScreen;",
        ">;"
    }
.end annotation


# instance fields
.field public screen:Lcom/navdy/service/library/events/ui/Screen;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 46
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/ui/DismissScreen;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/ui/DismissScreen;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 50
    if-nez p1, :cond_0

    .line 52
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/ui/DismissScreen;->screen:Lcom/navdy/service/library/events/ui/Screen;

    iput-object v0, p0, Lcom/navdy/service/library/events/ui/DismissScreen$Builder;->screen:Lcom/navdy/service/library/events/ui/Screen;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/ui/DismissScreen;
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/navdy/service/library/events/ui/DismissScreen$Builder;->checkRequiredFields()V

    .line 62
    new-instance v0, Lcom/navdy/service/library/events/ui/DismissScreen;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/ui/DismissScreen;-><init>(Lcom/navdy/service/library/events/ui/DismissScreen$Builder;Lcom/navdy/service/library/events/ui/DismissScreen$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/navdy/service/library/events/ui/DismissScreen$Builder;->build()Lcom/navdy/service/library/events/ui/DismissScreen;

    move-result-object v0

    return-object v0
.end method

.method public screen(Lcom/navdy/service/library/events/ui/Screen;)Lcom/navdy/service/library/events/ui/DismissScreen$Builder;
    .locals 0
    .param p1, "screen"    # Lcom/navdy/service/library/events/ui/Screen;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/navdy/service/library/events/ui/DismissScreen$Builder;->screen:Lcom/navdy/service/library/events/ui/Screen;

    .line 56
    return-object p0
.end method
