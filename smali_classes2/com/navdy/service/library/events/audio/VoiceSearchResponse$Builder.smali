.class public final Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "VoiceSearchResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/VoiceSearchResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public confidenceLevel:Ljava/lang/Integer;

.field public error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

.field public listeningOverBluetooth:Ljava/lang/Boolean;

.field public locale:Ljava/lang/String;

.field public prefix:Ljava/lang/String;

.field public recognizedWords:Ljava/lang/String;

.field public results:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field public state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

.field public volumeLevel:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 154
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/VoiceSearchResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    .prologue
    .line 157
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 158
    if-nez p1, :cond_0

    .line 168
    :goto_0
    return-void

    .line 159
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 160
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->recognizedWords:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->recognizedWords:Ljava/lang/String;

    .line 161
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->volumeLevel:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->volumeLevel:Ljava/lang/Integer;

    .line 162
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->listeningOverBluetooth:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->listeningOverBluetooth:Ljava/lang/Boolean;

    .line 163
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 164
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->confidenceLevel:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->confidenceLevel:Ljava/lang/Integer;

    .line 165
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->results:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->results:Ljava/util/List;

    .line 166
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->prefix:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->prefix:Ljava/lang/String;

    .line 167
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;->locale:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->locale:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
    .locals 2

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->checkRequiredFields()V

    .line 246
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse;-><init>(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;Lcom/navdy/service/library/events/audio/VoiceSearchResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->build()Lcom/navdy/service/library/events/audio/VoiceSearchResponse;

    move-result-object v0

    return-object v0
.end method

.method public confidenceLevel(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    .locals 0
    .param p1, "confidenceLevel"    # Ljava/lang/Integer;

    .prologue
    .line 214
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->confidenceLevel:Ljava/lang/Integer;

    .line 215
    return-object p0
.end method

.method public error(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    .locals 0
    .param p1, "error"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->error:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchError;

    .line 207
    return-object p0
.end method

.method public listeningOverBluetooth(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    .locals 0
    .param p1, "listeningOverBluetooth"    # Ljava/lang/Boolean;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->listeningOverBluetooth:Ljava/lang/Boolean;

    .line 199
    return-object p0
.end method

.method public locale(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    .locals 0
    .param p1, "locale"    # Ljava/lang/String;

    .prologue
    .line 239
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->locale:Ljava/lang/String;

    .line 240
    return-object p0
.end method

.method public prefix(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    .locals 0
    .param p1, "prefix"    # Ljava/lang/String;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->prefix:Ljava/lang/String;

    .line 231
    return-object p0
.end method

.method public recognizedWords(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    .locals 0
    .param p1, "recognizedWords"    # Ljava/lang/String;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->recognizedWords:Ljava/lang/String;

    .line 183
    return-object p0
.end method

.method public results(Ljava/util/List;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;)",
            "Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;"
        }
    .end annotation

    .prologue
    .line 222
    .local p1, "results":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->results:Ljava/util/List;

    .line 223
    return-object p0
.end method

.method public state(Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    .locals 0
    .param p1, "state"    # Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->state:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 175
    return-object p0
.end method

.method public volumeLevel(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;
    .locals 0
    .param p1, "volumeLevel"    # Ljava/lang/Integer;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$Builder;->volumeLevel:Ljava/lang/Integer;

    .line 191
    return-object p0
.end method
