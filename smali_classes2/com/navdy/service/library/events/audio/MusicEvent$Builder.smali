.class public final Lcom/navdy/service/library/events/audio/MusicEvent$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicEvent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

.field public collectionId:Ljava/lang/String;

.field public collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

.field public collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

.field public dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

.field public index:Ljava/lang/Integer;

.field public repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

.field public shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 136
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicEvent;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicEvent;

    .prologue
    .line 139
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 140
    if-nez p1, :cond_0

    .line 149
    :goto_0
    return-void

    .line 141
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicEvent;->action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 142
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicEvent;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 143
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 144
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 145
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicEvent;->collectionId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionId:Ljava/lang/String;

    .line 146
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicEvent;->index:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->index:Ljava/lang/Integer;

    .line 147
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicEvent;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 148
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicEvent;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    goto :goto_0
.end method


# virtual methods
.method public action(Lcom/navdy/service/library/events/audio/MusicEvent$Action;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;
    .locals 0
    .param p1, "action"    # Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->action:Lcom/navdy/service/library/events/audio/MusicEvent$Action;

    .line 156
    return-object p0
.end method

.method public build()Lcom/navdy/service/library/events/audio/MusicEvent;
    .locals 2

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->checkRequiredFields()V

    .line 220
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicEvent;-><init>(Lcom/navdy/service/library/events/audio/MusicEvent$Builder;Lcom/navdy/service/library/events/audio/MusicEvent$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->build()Lcom/navdy/service/library/events/audio/MusicEvent;

    move-result-object v0

    return-object v0
.end method

.method public collectionId(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;
    .locals 0
    .param p1, "collectionId"    # Ljava/lang/String;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionId:Ljava/lang/String;

    .line 191
    return-object p0
.end method

.method public collectionSource(Lcom/navdy/service/library/events/audio/MusicCollectionSource;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;
    .locals 0
    .param p1, "collectionSource"    # Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .prologue
    .line 172
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionSource:Lcom/navdy/service/library/events/audio/MusicCollectionSource;

    .line 173
    return-object p0
.end method

.method public collectionType(Lcom/navdy/service/library/events/audio/MusicCollectionType;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;
    .locals 0
    .param p1, "collectionType"    # Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->collectionType:Lcom/navdy/service/library/events/audio/MusicCollectionType;

    .line 181
    return-object p0
.end method

.method public dataSource(Lcom/navdy/service/library/events/audio/MusicDataSource;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;
    .locals 0
    .param p1, "dataSource"    # Lcom/navdy/service/library/events/audio/MusicDataSource;

    .prologue
    .line 164
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->dataSource:Lcom/navdy/service/library/events/audio/MusicDataSource;

    .line 165
    return-object p0
.end method

.method public index(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;
    .locals 0
    .param p1, "index"    # Ljava/lang/Integer;

    .prologue
    .line 195
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->index:Ljava/lang/Integer;

    .line 196
    return-object p0
.end method

.method public repeatMode(Lcom/navdy/service/library/events/audio/MusicRepeatMode;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;
    .locals 0
    .param p1, "repeatMode"    # Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->repeatMode:Lcom/navdy/service/library/events/audio/MusicRepeatMode;

    .line 214
    return-object p0
.end method

.method public shuffleMode(Lcom/navdy/service/library/events/audio/MusicShuffleMode;)Lcom/navdy/service/library/events/audio/MusicEvent$Builder;
    .locals 0
    .param p1, "shuffleMode"    # Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicEvent$Builder;->shuffleMode:Lcom/navdy/service/library/events/audio/MusicShuffleMode;

    .line 206
    return-object p0
.end method
