.class public final enum Lcom/navdy/service/library/events/audio/SpeechRequest$Category;
.super Ljava/lang/Enum;
.source "SpeechRequest.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/SpeechRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Category"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/SpeechRequest$Category;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

.field public static final enum SPEECH_CAMERA_WARNING:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

.field public static final enum SPEECH_GPS_CONNECTIVITY:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

.field public static final enum SPEECH_MESSAGE_READ_OUT:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

.field public static final enum SPEECH_NOTIFICATION:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

.field public static final enum SPEECH_REROUTE:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

.field public static final enum SPEECH_SPEED_WARNING:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

.field public static final enum SPEECH_TURN_BY_TURN:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

.field public static final enum SPEECH_WELCOME_MESSAGE:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 170
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const-string v1, "SPEECH_TURN_BY_TURN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_TURN_BY_TURN:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 174
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const-string v1, "SPEECH_REROUTE"

    invoke-direct {v0, v1, v4, v5}, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_REROUTE:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 178
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const-string v1, "SPEECH_SPEED_WARNING"

    invoke-direct {v0, v1, v5, v6}, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_SPEED_WARNING:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 182
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const-string v1, "SPEECH_GPS_CONNECTIVITY"

    invoke-direct {v0, v1, v6, v7}, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_GPS_CONNECTIVITY:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 186
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const-string v1, "SPEECH_MESSAGE_READ_OUT"

    invoke-direct {v0, v1, v7, v8}, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_MESSAGE_READ_OUT:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 192
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const-string v1, "SPEECH_WELCOME_MESSAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_WELCOME_MESSAGE:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 196
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const-string v1, "SPEECH_NOTIFICATION"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_NOTIFICATION:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 200
    new-instance v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const-string v1, "SPEECH_CAMERA_WARNING"

    const/4 v2, 0x7

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_CAMERA_WARNING:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    .line 165
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    const/4 v1, 0x0

    sget-object v2, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_TURN_BY_TURN:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    aput-object v2, v0, v1

    sget-object v1, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_REROUTE:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_SPEED_WARNING:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_GPS_CONNECTIVITY:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    aput-object v1, v0, v6

    sget-object v1, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_MESSAGE_READ_OUT:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    aput-object v1, v0, v7

    sget-object v1, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_WELCOME_MESSAGE:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_NOTIFICATION:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->SPEECH_CAMERA_WARNING:Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    aput-object v2, v0, v1

    sput-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->$VALUES:[Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 204
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 205
    iput p3, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->value:I

    .line 206
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/SpeechRequest$Category;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 165
    const-class v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/SpeechRequest$Category;
    .locals 1

    .prologue
    .line 165
    sget-object v0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->$VALUES:[Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/SpeechRequest$Category;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/navdy/service/library/events/audio/SpeechRequest$Category;->value:I

    return v0
.end method
