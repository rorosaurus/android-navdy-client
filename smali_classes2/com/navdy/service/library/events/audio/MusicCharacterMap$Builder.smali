.class public final Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "MusicCharacterMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/MusicCharacterMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/audio/MusicCharacterMap;",
        ">;"
    }
.end annotation


# instance fields
.field public character:Ljava/lang/String;

.field public offset:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 65
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/audio/MusicCharacterMap;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/audio/MusicCharacterMap;

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 69
    if-nez p1, :cond_0

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->character:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;->character:Ljava/lang/String;

    .line 71
    iget-object v0, p1, Lcom/navdy/service/library/events/audio/MusicCharacterMap;->offset:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;->offset:Ljava/lang/Integer;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/audio/MusicCharacterMap;
    .locals 2

    .prologue
    .line 92
    new-instance v0, Lcom/navdy/service/library/events/audio/MusicCharacterMap;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/audio/MusicCharacterMap;-><init>(Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;Lcom/navdy/service/library/events/audio/MusicCharacterMap$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;->build()Lcom/navdy/service/library/events/audio/MusicCharacterMap;

    move-result-object v0

    return-object v0
.end method

.method public character(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;
    .locals 0
    .param p1, "character"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;->character:Ljava/lang/String;

    .line 79
    return-object p0
.end method

.method public offset(Ljava/lang/Integer;)Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;
    .locals 0
    .param p1, "offset"    # Ljava/lang/Integer;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/navdy/service/library/events/audio/MusicCharacterMap$Builder;->offset:Ljava/lang/Integer;

    .line 87
    return-object p0
.end method
