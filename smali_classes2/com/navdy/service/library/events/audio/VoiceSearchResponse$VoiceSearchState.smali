.class public final enum Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;
.super Ljava/lang/Enum;
.source "VoiceSearchResponse.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/audio/VoiceSearchResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "VoiceSearchState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

.field public static final enum VOICE_SEARCH_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

.field public static final enum VOICE_SEARCH_LISTENING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

.field public static final enum VOICE_SEARCH_SEARCHING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

.field public static final enum VOICE_SEARCH_STARTING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

.field public static final enum VOICE_SEARCH_SUCCESS:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 256
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    const-string v1, "VOICE_SEARCH_ERROR"

    invoke-direct {v0, v1, v2, v2}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 257
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    const-string v1, "VOICE_SEARCH_STARTING"

    invoke-direct {v0, v1, v3, v3}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_STARTING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 258
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    const-string v1, "VOICE_SEARCH_LISTENING"

    invoke-direct {v0, v1, v4, v4}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_LISTENING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 259
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    const-string v1, "VOICE_SEARCH_SEARCHING"

    invoke-direct {v0, v1, v5, v5}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SEARCHING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 260
    new-instance v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    const-string v1, "VOICE_SEARCH_SUCCESS"

    invoke-direct {v0, v1, v6, v6}, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SUCCESS:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    .line 250
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_ERROR:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_STARTING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_LISTENING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SEARCHING:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->VOICE_SEARCH_SUCCESS:Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    aput-object v1, v0, v6

    sput-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->$VALUES:[Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 264
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 265
    iput p3, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->value:I

    .line 266
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 250
    const-class v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;
    .locals 1

    .prologue
    .line 250
    sget-object v0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->$VALUES:[Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Lcom/navdy/service/library/events/audio/VoiceSearchResponse$VoiceSearchState;->value:I

    return v0
.end method
