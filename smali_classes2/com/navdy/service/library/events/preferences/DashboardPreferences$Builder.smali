.class public final Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DashboardPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/preferences/DashboardPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/preferences/DashboardPreferences;",
        ">;"
    }
.end annotation


# instance fields
.field public leftGaugeId:Ljava/lang/String;

.field public middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

.field public rightGaugeId:Ljava/lang/String;

.field public scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 75
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/preferences/DashboardPreferences;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/preferences/DashboardPreferences;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 79
    if-nez p1, :cond_0

    .line 84
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    .line 81
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    .line 82
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->rightGaugeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->rightGaugeId:Ljava/lang/String;

    .line 83
    iget-object v0, p1, Lcom/navdy/service/library/events/preferences/DashboardPreferences;->leftGaugeId:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->leftGaugeId:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/preferences/DashboardPreferences;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Lcom/navdy/service/library/events/preferences/DashboardPreferences;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/preferences/DashboardPreferences;-><init>(Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;Lcom/navdy/service/library/events/preferences/DashboardPreferences$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->build()Lcom/navdy/service/library/events/preferences/DashboardPreferences;

    move-result-object v0

    return-object v0
.end method

.method public leftGaugeId(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;
    .locals 0
    .param p1, "leftGaugeId"    # Ljava/lang/String;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->leftGaugeId:Ljava/lang/String;

    .line 103
    return-object p0
.end method

.method public middleGauge(Lcom/navdy/service/library/events/preferences/MiddleGauge;)Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;
    .locals 0
    .param p1, "middleGauge"    # Lcom/navdy/service/library/events/preferences/MiddleGauge;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->middleGauge:Lcom/navdy/service/library/events/preferences/MiddleGauge;

    .line 88
    return-object p0
.end method

.method public rightGaugeId(Ljava/lang/String;)Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;
    .locals 0
    .param p1, "rightGaugeId"    # Ljava/lang/String;

    .prologue
    .line 97
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->rightGaugeId:Ljava/lang/String;

    .line 98
    return-object p0
.end method

.method public scrollableSide(Lcom/navdy/service/library/events/preferences/ScrollableSide;)Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;
    .locals 0
    .param p1, "scrollableSide"    # Lcom/navdy/service/library/events/preferences/ScrollableSide;

    .prologue
    .line 92
    iput-object p1, p0, Lcom/navdy/service/library/events/preferences/DashboardPreferences$Builder;->scrollableSide:Lcom/navdy/service/library/events/preferences/ScrollableSide;

    .line 93
    return-object p0
.end method
