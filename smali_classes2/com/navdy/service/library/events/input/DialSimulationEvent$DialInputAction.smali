.class public final enum Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;
.super Ljava/lang/Enum;
.source "DialSimulationEvent.java"

# interfaces
.implements Lcom/squareup/wire/ProtoEnum;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/input/DialSimulationEvent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DialInputAction"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;",
        ">;",
        "Lcom/squareup/wire/ProtoEnum;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

.field public static final enum DIAL_CLICK:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

.field public static final enum DIAL_LEFT_TURN:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

.field public static final enum DIAL_LONG_CLICK:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

.field public static final enum DIAL_RIGHT_TURN:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;


# instance fields
.field private final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 69
    new-instance v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    const-string v1, "DIAL_CLICK"

    invoke-direct {v0, v1, v5, v2}, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->DIAL_CLICK:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    .line 70
    new-instance v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    const-string v1, "DIAL_LONG_CLICK"

    invoke-direct {v0, v1, v2, v3}, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->DIAL_LONG_CLICK:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    .line 71
    new-instance v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    const-string v1, "DIAL_LEFT_TURN"

    invoke-direct {v0, v1, v3, v4}, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->DIAL_LEFT_TURN:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    .line 72
    new-instance v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    const-string v1, "DIAL_RIGHT_TURN"

    invoke-direct {v0, v1, v4, v6}, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->DIAL_RIGHT_TURN:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    .line 67
    new-array v0, v6, [Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    sget-object v1, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->DIAL_CLICK:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    aput-object v1, v0, v5

    sget-object v1, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->DIAL_LONG_CLICK:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    aput-object v1, v0, v2

    sget-object v1, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->DIAL_LEFT_TURN:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    aput-object v1, v0, v3

    sget-object v1, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->DIAL_RIGHT_TURN:Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    aput-object v1, v0, v4

    sput-object v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->$VALUES:[Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput p3, p0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->value:I

    .line 78
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 67
    const-class v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    return-object v0
.end method

.method public static values()[Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->$VALUES:[Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    invoke-virtual {v0}, [Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/navdy/service/library/events/input/DialSimulationEvent$DialInputAction;->value:I

    return v0
.end method
