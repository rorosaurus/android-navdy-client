.class public final Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PreviewFileRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/file/PreviewFileRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/file/PreviewFileRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public filename:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 49
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/file/PreviewFileRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/file/PreviewFileRequest;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 53
    if-nez p1, :cond_0

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/PreviewFileRequest;->filename:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;->filename:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/file/PreviewFileRequest;
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;->checkRequiredFields()V

    .line 68
    new-instance v0, Lcom/navdy/service/library/events/file/PreviewFileRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/file/PreviewFileRequest;-><init>(Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;Lcom/navdy/service/library/events/file/PreviewFileRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;->build()Lcom/navdy/service/library/events/file/PreviewFileRequest;

    move-result-object v0

    return-object v0
.end method

.method public filename(Ljava/lang/String;)Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;
    .locals 0
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/navdy/service/library/events/file/PreviewFileRequest$Builder;->filename:Ljava/lang/String;

    .line 62
    return-object p0
.end method
