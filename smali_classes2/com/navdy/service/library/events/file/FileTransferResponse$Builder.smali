.class public final Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FileTransferResponse.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/file/FileTransferResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/file/FileTransferResponse;",
        ">;"
    }
.end annotation


# instance fields
.field public checksum:Ljava/lang/String;

.field public destinationFileName:Ljava/lang/String;

.field public error:Lcom/navdy/service/library/events/file/FileTransferError;

.field public fileType:Lcom/navdy/service/library/events/file/FileType;

.field public maxChunkSize:Ljava/lang/Integer;

.field public offset:Ljava/lang/Long;

.field public success:Ljava/lang/Boolean;

.field public supportsAcks:Ljava/lang/Boolean;

.field public transferId:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 150
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/file/FileTransferResponse;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/file/FileTransferResponse;

    .prologue
    .line 153
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 154
    if-nez p1, :cond_0

    .line 164
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->transferId:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->transferId:Ljava/lang/Integer;

    .line 156
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->offset:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->offset:Ljava/lang/Long;

    .line 157
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->checksum:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->checksum:Ljava/lang/String;

    .line 158
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->success:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success:Ljava/lang/Boolean;

    .line 159
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->maxChunkSize:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->maxChunkSize:Ljava/lang/Integer;

    .line 160
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 161
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->fileType:Lcom/navdy/service/library/events/file/FileType;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType:Lcom/navdy/service/library/events/file/FileType;

    .line 162
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->destinationFileName:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName:Ljava/lang/String;

    .line 163
    iget-object v0, p1, Lcom/navdy/service/library/events/file/FileTransferResponse;->supportsAcks:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->supportsAcks:Ljava/lang/Boolean;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/file/FileTransferResponse;
    .locals 2

    .prologue
    .line 244
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->checkRequiredFields()V

    .line 245
    new-instance v0, Lcom/navdy/service/library/events/file/FileTransferResponse;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/file/FileTransferResponse;-><init>(Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;Lcom/navdy/service/library/events/file/FileTransferResponse$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 137
    invoke-virtual {p0}, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->build()Lcom/navdy/service/library/events/file/FileTransferResponse;

    move-result-object v0

    return-object v0
.end method

.method public checksum(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .locals 0
    .param p1, "checksum"    # Ljava/lang/String;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->checksum:Ljava/lang/String;

    .line 192
    return-object p0
.end method

.method public destinationFileName(Ljava/lang/String;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .locals 0
    .param p1, "destinationFileName"    # Ljava/lang/String;

    .prologue
    .line 227
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->destinationFileName:Ljava/lang/String;

    .line 228
    return-object p0
.end method

.method public error(Lcom/navdy/service/library/events/file/FileTransferError;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .locals 0
    .param p1, "error"    # Lcom/navdy/service/library/events/file/FileTransferError;

    .prologue
    .line 214
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->error:Lcom/navdy/service/library/events/file/FileTransferError;

    .line 215
    return-object p0
.end method

.method public fileType(Lcom/navdy/service/library/events/file/FileType;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .locals 0
    .param p1, "fileType"    # Lcom/navdy/service/library/events/file/FileType;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->fileType:Lcom/navdy/service/library/events/file/FileType;

    .line 223
    return-object p0
.end method

.method public maxChunkSize(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .locals 0
    .param p1, "maxChunkSize"    # Ljava/lang/Integer;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->maxChunkSize:Ljava/lang/Integer;

    .line 210
    return-object p0
.end method

.method public offset(Ljava/lang/Long;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .locals 0
    .param p1, "offset"    # Ljava/lang/Long;

    .prologue
    .line 182
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->offset:Ljava/lang/Long;

    .line 183
    return-object p0
.end method

.method public success(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .locals 0
    .param p1, "success"    # Ljava/lang/Boolean;

    .prologue
    .line 200
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->success:Ljava/lang/Boolean;

    .line 201
    return-object p0
.end method

.method public supportsAcks(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .locals 0
    .param p1, "supportsAcks"    # Ljava/lang/Boolean;

    .prologue
    .line 238
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->supportsAcks:Ljava/lang/Boolean;

    .line 239
    return-object p0
.end method

.method public transferId(Ljava/lang/Integer;)Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;
    .locals 0
    .param p1, "transferId"    # Ljava/lang/Integer;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/navdy/service/library/events/file/FileTransferResponse$Builder;->transferId:Ljava/lang/Integer;

    .line 171
    return-object p0
.end method
