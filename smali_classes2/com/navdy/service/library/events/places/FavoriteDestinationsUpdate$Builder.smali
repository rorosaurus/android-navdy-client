.class public final Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "FavoriteDestinationsUpdate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;",
        ">;"
    }
.end annotation


# instance fields
.field public destinations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;"
        }
    .end annotation
.end field

.field public serial_number:Ljava/lang/Long;

.field public status:Lcom/navdy/service/library/events/RequestStatus;

.field public statusDetail:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 100
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    .prologue
    .line 103
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 104
    if-nez p1, :cond_0

    .line 109
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->status:Lcom/navdy/service/library/events/RequestStatus;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 106
    iget-object v0, p1, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->statusDetail:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->statusDetail:Ljava/lang/String;

    .line 107
    iget-object v0, p1, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->serial_number:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 108
    iget-object v0, p1, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->destinations:Ljava/util/List;

    invoke-static {v0}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;->access$000(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->destinations:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->checkRequiredFields()V

    .line 148
    new-instance v0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;-><init>(Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->build()Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate;

    move-result-object v0

    return-object v0
.end method

.method public destinations(Ljava/util/List;)Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/navdy/service/library/events/destination/Destination;",
            ">;)",
            "Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;"
        }
    .end annotation

    .prologue
    .line 141
    .local p1, "destinations":Ljava/util/List;, "Ljava/util/List<Lcom/navdy/service/library/events/destination/Destination;>;"
    invoke-static {p1}, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->checkForNulls(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->destinations:Ljava/util/List;

    .line 142
    return-object p0
.end method

.method public serial_number(Ljava/lang/Long;)Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;
    .locals 0
    .param p1, "serial_number"    # Ljava/lang/Long;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->serial_number:Ljava/lang/Long;

    .line 132
    return-object p0
.end method

.method public status(Lcom/navdy/service/library/events/RequestStatus;)Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;
    .locals 0
    .param p1, "status"    # Lcom/navdy/service/library/events/RequestStatus;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->status:Lcom/navdy/service/library/events/RequestStatus;

    .line 116
    return-object p0
.end method

.method public statusDetail(Ljava/lang/String;)Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;
    .locals 0
    .param p1, "statusDetail"    # Ljava/lang/String;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/navdy/service/library/events/places/FavoriteDestinationsUpdate$Builder;->statusDetail:Ljava/lang/String;

    .line 124
    return-object p0
.end method
