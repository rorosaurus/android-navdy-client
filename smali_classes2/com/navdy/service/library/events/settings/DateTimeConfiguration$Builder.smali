.class public final Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "DateTimeConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/settings/DateTimeConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/settings/DateTimeConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field public format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

.field public timestamp:Ljava/lang/Long;

.field public timezone:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 80
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/settings/DateTimeConfiguration;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 84
    if-nez p1, :cond_0

    .line 88
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timestamp:Ljava/lang/Long;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;->timestamp:Ljava/lang/Long;

    .line 86
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->timezone:Ljava/lang/String;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;->timezone:Ljava/lang/String;

    .line 87
    iget-object v0, p1, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    iput-object v0, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/settings/DateTimeConfiguration;
    .locals 2

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;->checkRequiredFields()V

    .line 118
    new-instance v0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration;-><init>(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;Lcom/navdy/service/library/events/settings/DateTimeConfiguration$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;->build()Lcom/navdy/service/library/events/settings/DateTimeConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public format(Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;)Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;
    .locals 0
    .param p1, "format"    # Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;->format:Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Clock;

    .line 112
    return-object p0
.end method

.method public timestamp(Ljava/lang/Long;)Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;
    .locals 0
    .param p1, "timestamp"    # Ljava/lang/Long;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;->timestamp:Ljava/lang/Long;

    .line 96
    return-object p0
.end method

.method public timezone(Ljava/lang/String;)Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;
    .locals 0
    .param p1, "timezone"    # Ljava/lang/String;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/navdy/service/library/events/settings/DateTimeConfiguration$Builder;->timezone:Ljava/lang/String;

    .line 104
    return-object p0
.end method
