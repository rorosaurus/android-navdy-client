.class public final Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;
.super Lcom/squareup/wire/Message$Builder;
.source "PhotoUpdatesRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/squareup/wire/Message$Builder",
        "<",
        "Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;",
        ">;"
    }
.end annotation


# instance fields
.field public photoType:Lcom/navdy/service/library/events/photo/PhotoType;

.field public start:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/squareup/wire/Message$Builder;-><init>()V

    .line 66
    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;)V
    .locals 1
    .param p1, "message"    # Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/squareup/wire/Message$Builder;-><init>(Lcom/squareup/wire/Message;)V

    .line 70
    if-nez p1, :cond_0

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->start:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;->start:Ljava/lang/Boolean;

    .line 72
    iget-object v0, p1, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    iput-object v0, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;
    .locals 2

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;->checkRequiredFields()V

    .line 94
    new-instance v0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;-><init>(Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$1;)V

    return-object v0
.end method

.method public bridge synthetic build()Lcom/squareup/wire/Message;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;->build()Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest;

    move-result-object v0

    return-object v0
.end method

.method public photoType(Lcom/navdy/service/library/events/photo/PhotoType;)Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;
    .locals 0
    .param p1, "photoType"    # Lcom/navdy/service/library/events/photo/PhotoType;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;->photoType:Lcom/navdy/service/library/events/photo/PhotoType;

    .line 88
    return-object p0
.end method

.method public start(Ljava/lang/Boolean;)Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;
    .locals 0
    .param p1, "start"    # Ljava/lang/Boolean;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/navdy/service/library/events/photo/PhotoUpdatesRequest$Builder;->start:Ljava/lang/Boolean;

    .line 80
    return-object p0
.end method
