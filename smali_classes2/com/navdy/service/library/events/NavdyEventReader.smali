.class public Lcom/navdy/service/library/events/NavdyEventReader;
.super Ljava/lang/Object;
.source "NavdyEventReader.java"


# static fields
.field private static final FRAME_LENGTH:I = 0x5

.field private static final sLogger:Lcom/navdy/service/library/log/Logger;


# instance fields
.field protected mInputStream:Ljava/io/DataInputStream;

.field protected mWire:Lcom/squareup/wire/Wire;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 11
    new-instance v0, Lcom/navdy/service/library/log/Logger;

    const-class v1, Lcom/navdy/service/library/events/NavdyEventReader;

    invoke-direct {v0, v1}, Lcom/navdy/service/library/log/Logger;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lcom/navdy/service/library/events/NavdyEventReader;->sLogger:Lcom/navdy/service/library/log/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 4
    .param p1, "input"    # Ljava/io/InputStream;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/io/DataInputStream;

    invoke-direct {v0, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/navdy/service/library/events/NavdyEventReader;->mInputStream:Ljava/io/DataInputStream;

    .line 20
    new-instance v0, Lcom/squareup/wire/Wire;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/navdy/service/library/events/Ext_NavdyEvent;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/squareup/wire/Wire;-><init>([Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/navdy/service/library/events/NavdyEventReader;->mWire:Lcom/squareup/wire/Wire;

    .line 21
    return-void
.end method

.method private readBytes(I)[B
    .locals 2
    .param p1, "size"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    new-array v0, p1, [B

    .line 51
    .local v0, "buf":[B
    iget-object v1, p0, Lcom/navdy/service/library/events/NavdyEventReader;->mInputStream:Ljava/io/DataInputStream;

    invoke-virtual {v1, v0}, Ljava/io/DataInputStream;->readFully([B)V

    .line 52
    return-object v0
.end method

.method private readSize()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 32
    const/4 v5, 0x5

    :try_start_0
    invoke-direct {p0, v5}, Lcom/navdy/service/library/events/NavdyEventReader;->readBytes(I)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 36
    .local v1, "buf":[B
    const/4 v3, 0x0

    .line 38
    .local v3, "frame":Lcom/navdy/service/library/events/Frame;
    :try_start_1
    iget-object v5, p0, Lcom/navdy/service/library/events/NavdyEventReader;->mWire:Lcom/squareup/wire/Wire;

    const-class v7, Lcom/navdy/service/library/events/Frame;

    invoke-virtual {v5, v1, v7}, Lcom/squareup/wire/Wire;->parseFrom([BLjava/lang/Class;)Lcom/squareup/wire/Message;

    move-result-object v5

    move-object v0, v5

    check-cast v0, Lcom/navdy/service/library/events/Frame;

    move-object v3, v0
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 42
    :goto_0
    if-nez v3, :cond_0

    move v5, v6

    .line 45
    .end local v1    # "buf":[B
    .end local v3    # "frame":Lcom/navdy/service/library/events/Frame;
    :goto_1
    return v5

    .line 33
    :catch_0
    move-exception v2

    .local v2, "e":Ljava/lang/Exception;
    move v5, v6

    .line 34
    goto :goto_1

    .line 39
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v1    # "buf":[B
    .restart local v3    # "frame":Lcom/navdy/service/library/events/Frame;
    :catch_1
    move-exception v4

    .line 40
    .local v4, "t":Ljava/lang/Throwable;
    sget-object v5, Lcom/navdy/service/library/events/NavdyEventReader;->sLogger:Lcom/navdy/service/library/log/Logger;

    const-string v7, "Failed to parse frame"

    invoke-virtual {v5, v7, v4}, Lcom/navdy/service/library/log/Logger;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 45
    .end local v4    # "t":Ljava/lang/Throwable;
    :cond_0
    iget-object v5, v3, Lcom/navdy/service/library/events/Frame;->size:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    goto :goto_1
.end method


# virtual methods
.method public readBytes()[B
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/navdy/service/library/events/NavdyEventReader;->readSize()I

    move-result v0

    .line 25
    .local v0, "size":I
    if-gtz v0, :cond_0

    const/4 v1, 0x0

    .line 26
    :goto_0
    return-object v1

    :cond_0
    invoke-direct {p0, v0}, Lcom/navdy/service/library/events/NavdyEventReader;->readBytes(I)[B

    move-result-object v1

    goto :goto_0
.end method
