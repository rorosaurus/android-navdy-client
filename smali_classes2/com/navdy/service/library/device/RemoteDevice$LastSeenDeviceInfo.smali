.class public Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;
.super Ljava/lang/Object;
.source "RemoteDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/navdy/service/library/device/RemoteDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LastSeenDeviceInfo"
.end annotation


# instance fields
.field private mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$002(Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;Lcom/navdy/service/library/events/DeviceInfo;)Lcom/navdy/service/library/events/DeviceInfo;
    .locals 0
    .param p0, "x0"    # Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;
    .param p1, "x1"    # Lcom/navdy/service/library/events/DeviceInfo;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    return-object p1
.end method


# virtual methods
.method public getDeviceInfo()Lcom/navdy/service/library/events/DeviceInfo;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/navdy/service/library/device/RemoteDevice$LastSeenDeviceInfo;->mDeviceInfo:Lcom/navdy/service/library/events/DeviceInfo;

    return-object v0
.end method
