.class public Lcom/navdy/service/library/device/connection/tunnel/Tunnel;
.super Lcom/navdy/service/library/device/connection/ProxyService;
.source "Tunnel.java"


# static fields
.field static final TAG:Ljava/lang/String;


# instance fields
.field private final acceptor:Lcom/navdy/service/library/network/SocketAcceptor;

.field private final connector:Lcom/navdy/service/library/network/SocketFactory;

.field final transferThreads:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/navdy/service/library/device/connection/tunnel/TransferThread;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/navdy/service/library/network/SocketAcceptor;Lcom/navdy/service/library/network/SocketFactory;)V
    .locals 1
    .param p1, "acceptor"    # Lcom/navdy/service/library/network/SocketAcceptor;
    .param p2, "connector"    # Lcom/navdy/service/library/network/SocketFactory;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/navdy/service/library/device/connection/ProxyService;-><init>()V

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->transferThreads:Ljava/util/ArrayList;

    .line 29
    sget-object v0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->TAG:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->setName(Ljava/lang/String;)V

    .line 30
    iput-object p1, p0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->acceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    .line 31
    iput-object p2, p0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->connector:Lcom/navdy/service/library/network/SocketFactory;

    .line 32
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 5

    .prologue
    .line 86
    :try_start_0
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->acceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    invoke-interface {v2}, Lcom/navdy/service/library/network/SocketAcceptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    iget-object v2, p0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->transferThreads:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;

    .line 91
    .local v1, "thr":Lcom/navdy/service/library/device/connection/tunnel/TransferThread;
    invoke-virtual {v1}, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->cancel()V

    goto :goto_1

    .line 87
    .end local v1    # "thr":Lcom/navdy/service/library/device/connection/tunnel/TransferThread;
    :catch_0
    move-exception v0

    .line 88
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "close failed:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 93
    .end local v0    # "e":Ljava/io/IOException;
    :cond_0
    return-void
.end method

.method public run()V
    .locals 15

    .prologue
    const/4 v11, 0x2

    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 36
    sget-object v9, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->TAG:Ljava/lang/String;

    const-string v10, "start: %s -> %s"

    new-array v11, v11, [Ljava/lang/Object;

    iget-object v12, p0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->acceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    aput-object v12, v11, v13

    iget-object v12, p0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->connector:Lcom/navdy/service/library/network/SocketFactory;

    aput-object v12, v11, v14

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 38
    :goto_0
    const/4 v3, 0x0

    .line 40
    .local v3, "fromSock":Lcom/navdy/service/library/network/SocketAdapter;
    :try_start_0
    iget-object v9, p0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->acceptor:Lcom/navdy/service/library/network/SocketAcceptor;

    invoke-interface {v9}, Lcom/navdy/service/library/network/SocketAcceptor;->accept()Lcom/navdy/service/library/network/SocketAdapter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 45
    sget-object v9, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "accepted connection ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    const/4 v6, 0x0

    .line 47
    .local v6, "toSock":Lcom/navdy/service/library/network/SocketAdapter;
    const/4 v1, 0x0

    .line 48
    .local v1, "fromInput":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 49
    .local v4, "toInput":Ljava/io/InputStream;
    const/4 v2, 0x0

    .line 50
    .local v2, "fromOutput":Ljava/io/OutputStream;
    const/4 v5, 0x0

    .line 52
    .local v5, "toOutput":Ljava/io/OutputStream;
    :try_start_1
    iget-object v9, p0, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->connector:Lcom/navdy/service/library/network/SocketFactory;

    invoke-interface {v9}, Lcom/navdy/service/library/network/SocketFactory;->build()Lcom/navdy/service/library/network/SocketAdapter;

    move-result-object v6

    .line 53
    invoke-interface {v6}, Lcom/navdy/service/library/network/SocketAdapter;->connect()V

    .line 54
    sget-object v9, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "connected ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ")"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    sget-object v9, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->TAG:Ljava/lang/String;

    const-string v10, "starting transfer: %s -> %s"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    aput-object v3, v11, v12

    const/4 v12, 0x1

    aput-object v6, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    invoke-interface {v3}, Lcom/navdy/service/library/network/SocketAdapter;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 57
    invoke-interface {v6}, Lcom/navdy/service/library/network/SocketAdapter;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    .line 58
    new-instance v7, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;

    const/4 v9, 0x1

    invoke-direct {v7, p0, v1, v5, v9}, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;-><init>(Lcom/navdy/service/library/device/connection/tunnel/Tunnel;Ljava/io/InputStream;Ljava/io/OutputStream;Z)V

    .line 59
    .local v7, "tt1":Lcom/navdy/service/library/device/connection/tunnel/TransferThread;
    invoke-interface {v6}, Lcom/navdy/service/library/network/SocketAdapter;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 60
    invoke-interface {v3}, Lcom/navdy/service/library/network/SocketAdapter;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    .line 61
    new-instance v8, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v4, v2, v9}, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;-><init>(Lcom/navdy/service/library/device/connection/tunnel/Tunnel;Ljava/io/InputStream;Ljava/io/OutputStream;Z)V

    .line 62
    .local v8, "tt2":Lcom/navdy/service/library/device/connection/tunnel/TransferThread;
    invoke-virtual {v7}, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->start()V

    .line 63
    invoke-virtual {v8}, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->start()V

    .line 64
    invoke-virtual {v7}, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->join()V

    .line 65
    invoke-virtual {v8}, Lcom/navdy/service/library/device/connection/tunnel/TransferThread;->join()V

    .line 66
    invoke-interface {v3}, Lcom/navdy/service/library/network/SocketAdapter;->close()V

    .line 67
    invoke-interface {v6}, Lcom/navdy/service/library/network/SocketAdapter;->close()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    .line 68
    .end local v7    # "tt1":Lcom/navdy/service/library/device/connection/tunnel/TransferThread;
    .end local v8    # "tt2":Lcom/navdy/service/library/device/connection/tunnel/TransferThread;
    :catch_0
    move-exception v0

    .line 80
    .end local v1    # "fromInput":Ljava/io/InputStream;
    .end local v2    # "fromOutput":Ljava/io/OutputStream;
    .end local v4    # "toInput":Ljava/io/InputStream;
    .end local v5    # "toOutput":Ljava/io/OutputStream;
    .end local v6    # "toSock":Lcom/navdy/service/library/network/SocketAdapter;
    :goto_1
    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->cancel()V

    .line 81
    return-void

    .line 41
    :catch_1
    move-exception v0

    .line 42
    .local v0, "e":Ljava/lang/Exception;
    sget-object v9, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "accept failed:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 70
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "fromInput":Ljava/io/InputStream;
    .restart local v2    # "fromOutput":Ljava/io/OutputStream;
    .restart local v4    # "toInput":Ljava/io/InputStream;
    .restart local v5    # "toOutput":Ljava/io/OutputStream;
    .restart local v6    # "toSock":Lcom/navdy/service/library/network/SocketAdapter;
    :catch_2
    move-exception v0

    .line 71
    .local v0, "e":Ljava/io/IOException;
    sget-object v9, Lcom/navdy/service/library/device/connection/tunnel/Tunnel;->TAG:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "transfer failed:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    invoke-static {v4}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 73
    invoke-static {v5}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 74
    invoke-static {v6}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 75
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 76
    invoke-static {v2}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 77
    invoke-static {v3}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    goto/16 :goto_0
.end method
