.class public Lcom/navdy/service/library/device/connection/tunnel/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field static final hexArray:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, "0123456789ABCDEF"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/device/connection/tunnel/Utils;->hexArray:[C

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bytesToHex([B)Ljava/lang/String;
    .locals 1
    .param p0, "bytes"    # [B

    .prologue
    .line 11
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/navdy/service/library/device/connection/tunnel/Utils;->bytesToHex([BZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static bytesToHex([BZ)Ljava/lang/String;
    .locals 7
    .param p0, "bytes"    # [B
    .param p1, "spaces"    # Z

    .prologue
    .line 15
    if-eqz p1, :cond_1

    const/4 v0, 0x3

    .line 16
    .local v0, "charsPerByte":I
    :goto_0
    array-length v4, p0

    mul-int/2addr v4, v0

    new-array v1, v4, [C

    .line 17
    .local v1, "hexChars":[C
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v4, p0

    if-ge v2, v4, :cond_2

    .line 18
    aget-byte v4, p0, v2

    and-int/lit16 v3, v4, 0xff

    .line 19
    .local v3, "v":I
    mul-int v4, v2, v0

    sget-object v5, Lcom/navdy/service/library/device/connection/tunnel/Utils;->hexArray:[C

    ushr-int/lit8 v6, v3, 0x4

    aget-char v5, v5, v6

    aput-char v5, v1, v4

    .line 20
    mul-int v4, v2, v0

    add-int/lit8 v4, v4, 0x1

    sget-object v5, Lcom/navdy/service/library/device/connection/tunnel/Utils;->hexArray:[C

    and-int/lit8 v6, v3, 0xf

    aget-char v5, v5, v6

    aput-char v5, v1, v4

    .line 21
    if-eqz p1, :cond_0

    .line 22
    mul-int v4, v2, v0

    add-int/lit8 v4, v4, 0x2

    const/16 v5, 0x20

    aput-char v5, v1, v4

    .line 17
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 15
    .end local v0    # "charsPerByte":I
    .end local v1    # "hexChars":[C
    .end local v2    # "j":I
    .end local v3    # "v":I
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 25
    .restart local v0    # "charsPerByte":I
    .restart local v1    # "hexChars":[C
    .restart local v2    # "j":I
    :cond_2
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v1}, Ljava/lang/String;-><init>([C)V

    return-object v4
.end method

.method public static toASCII([B)Ljava/lang/String;
    .locals 6
    .param p0, "bytes"    # [B

    .prologue
    .line 29
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .local v1, "sb":Ljava/lang/StringBuilder;
    array-length v5, p0

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_1

    aget-byte v0, p0, v4

    .line 31
    .local v0, "aByte":B
    and-int/lit16 v2, v0, 0xff

    .line 32
    .local v2, "v":I
    const/16 v3, 0x20

    if-lt v2, v3, :cond_0

    const/16 v3, 0x80

    if-ge v2, v3, :cond_0

    int-to-char v3, v2

    :goto_1
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 30
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 32
    :cond_0
    const/16 v3, 0x2e

    goto :goto_1

    .line 34
    .end local v0    # "aByte":B
    .end local v2    # "v":I
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method
