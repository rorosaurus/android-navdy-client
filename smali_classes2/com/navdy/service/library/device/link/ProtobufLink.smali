.class public Lcom/navdy/service/library/device/link/ProtobufLink;
.super Ljava/lang/Object;
.source "ProtobufLink.java"

# interfaces
.implements Lcom/navdy/service/library/device/link/Link;


# instance fields
.field private bandwidthLevel:I

.field private connectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

.field private readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

.field private writerThread:Lcom/navdy/service/library/device/link/WriterThread;


# direct methods
.method public constructor <init>(Lcom/navdy/service/library/device/connection/ConnectionInfo;)V
    .locals 1
    .param p1, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x1

    iput v0, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->bandwidthLevel:I

    .line 23
    iput-object p1, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->connectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .line 24
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    iget-object v0, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/link/WriterThread;->cancel()V

    .line 66
    iput-object v1, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    .line 69
    :cond_0
    iget-object v0, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    if-eqz v0, :cond_1

    .line 70
    iget-object v0, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    invoke-virtual {v0}, Lcom/navdy/service/library/device/link/ReaderThread;->cancel()V

    .line 71
    iput-object v1, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    .line 73
    :cond_1
    return-void
.end method

.method public getBandWidthLevel()I
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->bandwidthLevel:I

    return v0
.end method

.method public setBandwidthLevel(I)V
    .locals 0
    .param p1, "level"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->bandwidthLevel:I

    .line 55
    return-void
.end method

.method public start(Lcom/navdy/service/library/network/SocketAdapter;Ljava/util/concurrent/LinkedBlockingDeque;Lcom/navdy/service/library/device/link/LinkListener;)Z
    .locals 8
    .param p1, "socket"    # Lcom/navdy/service/library/network/SocketAdapter;
    .param p3, "listener"    # Lcom/navdy/service/library/device/link/LinkListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/network/SocketAdapter;",
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Lcom/navdy/service/library/device/link/EventRequest;",
            ">;",
            "Lcom/navdy/service/library/device/link/LinkListener;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p2, "outboundEventQueue":Ljava/util/concurrent/LinkedBlockingDeque;, "Ljava/util/concurrent/LinkedBlockingDeque<Lcom/navdy/service/library/device/link/EventRequest;>;"
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 28
    iget-object v4, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    invoke-virtual {v4}, Lcom/navdy/service/library/device/link/WriterThread;->isClosing()Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    .line 29
    invoke-virtual {v4}, Lcom/navdy/service/library/device/link/ReaderThread;->isClosing()Z

    move-result v4

    if-nez v4, :cond_2

    .line 30
    :cond_1
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Must stop threads before calling startLink"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 33
    :cond_2
    const/4 v1, 0x0

    .line 36
    .local v1, "inputStream":Ljava/io/InputStream;
    :try_start_0
    invoke-interface {p1}, Lcom/navdy/service/library/network/SocketAdapter;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 37
    invoke-interface {p1}, Lcom/navdy/service/library/network/SocketAdapter;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    .line 38
    .local v2, "outputStream":Ljava/io/OutputStream;
    new-instance v4, Lcom/navdy/service/library/device/link/ReaderThread;

    iget-object v5, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->connectionInfo:Lcom/navdy/service/library/device/connection/ConnectionInfo;

    invoke-virtual {v5}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v4, v5, v1, p3, v6}, Lcom/navdy/service/library/device/link/ReaderThread;-><init>(Lcom/navdy/service/library/device/connection/ConnectionType;Ljava/io/InputStream;Lcom/navdy/service/library/device/link/LinkListener;Z)V

    iput-object v4, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    .line 39
    new-instance v4, Lcom/navdy/service/library/device/link/WriterThread;

    const/4 v5, 0x0

    invoke-direct {v4, p2, v2, v5}, Lcom/navdy/service/library/device/link/WriterThread;-><init>(Ljava/util/concurrent/LinkedBlockingDeque;Ljava/io/OutputStream;Lcom/navdy/service/library/device/link/WriterThread$EventProcessor;)V

    iput-object v4, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    iget-object v4, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    invoke-virtual {v4}, Lcom/navdy/service/library/device/link/ReaderThread;->start()V

    .line 48
    iget-object v4, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    invoke-virtual {v4}, Lcom/navdy/service/library/device/link/WriterThread;->start()V

    .line 49
    .end local v2    # "outputStream":Ljava/io/OutputStream;
    :goto_0
    return v3

    .line 40
    :catch_0
    move-exception v0

    .line 41
    .local v0, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/navdy/service/library/util/IOUtils;->closeStream(Ljava/io/Closeable;)V

    .line 43
    iput-object v7, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->readerThread:Lcom/navdy/service/library/device/link/ReaderThread;

    .line 44
    iput-object v7, p0, Lcom/navdy/service/library/device/link/ProtobufLink;->writerThread:Lcom/navdy/service/library/device/link/WriterThread;

    .line 45
    const/4 v3, 0x0

    goto :goto_0
.end method
