.class public Lcom/navdy/service/library/device/link/LinkManager;
.super Ljava/lang/Object;
.source "LinkManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;
    }
.end annotation


# static fields
.field private static sDefaultFactory:Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;

.field private static sFactoryMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/navdy/service/library/device/connection/ConnectionType;",
            "Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/navdy/service/library/device/link/LinkManager;->sFactoryMap:Ljava/util/HashMap;

    .line 33
    new-instance v0, Lcom/navdy/service/library/device/link/LinkManager$1;

    invoke-direct {v0}, Lcom/navdy/service/library/device/link/LinkManager$1;-><init>()V

    sput-object v0, Lcom/navdy/service/library/device/link/LinkManager;->sDefaultFactory:Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;

    .line 48
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->BT_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    sget-object v1, Lcom/navdy/service/library/device/link/LinkManager;->sDefaultFactory:Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;

    invoke-static {v0, v1}, Lcom/navdy/service/library/device/link/LinkManager;->registerFactory(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;)V

    .line 49
    sget-object v0, Lcom/navdy/service/library/device/connection/ConnectionType;->TCP_PROTOBUF:Lcom/navdy/service/library/device/connection/ConnectionType;

    sget-object v1, Lcom/navdy/service/library/device/link/LinkManager;->sDefaultFactory:Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;

    invoke-static {v0, v1}, Lcom/navdy/service/library/device/link/LinkManager;->registerFactory(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;)V

    .line 50
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static build(Lcom/navdy/service/library/device/connection/ConnectionInfo;)Lcom/navdy/service/library/device/link/Link;
    .locals 3
    .param p0, "connectionInfo"    # Lcom/navdy/service/library/device/connection/ConnectionInfo;

    .prologue
    .line 53
    sget-object v1, Lcom/navdy/service/library/device/link/LinkManager;->sFactoryMap:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/navdy/service/library/device/connection/ConnectionInfo;->getType()Lcom/navdy/service/library/device/connection/ConnectionType;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;

    .line 54
    .local v0, "factory":Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;
    if-eqz v0, :cond_0

    .line 55
    invoke-interface {v0, p0}, Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;->build(Lcom/navdy/service/library/device/connection/ConnectionInfo;)Lcom/navdy/service/library/device/link/Link;

    move-result-object v1

    .line 57
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static registerFactory(Lcom/navdy/service/library/device/connection/ConnectionType;Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;)V
    .locals 1
    .param p0, "type"    # Lcom/navdy/service/library/device/connection/ConnectionType;
    .param p1, "factory"    # Lcom/navdy/service/library/device/link/LinkManager$LinkFactory;

    .prologue
    .line 28
    if-eqz p1, :cond_0

    .line 29
    sget-object v0, Lcom/navdy/service/library/device/link/LinkManager;->sFactoryMap:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    :cond_0
    return-void
.end method
