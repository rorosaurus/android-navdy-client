.class public interface abstract Lcom/navdy/service/library/device/link/Link;
.super Ljava/lang/Object;
.source "Link.java"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field public static final BANDWIDTH_LEVEL_LOW:I = 0x0

.field public static final BANDWIDTH_LEVEL_NORMAL:I = 0x1


# virtual methods
.method public abstract close()V
.end method

.method public abstract getBandWidthLevel()I
.end method

.method public abstract setBandwidthLevel(I)V
.end method

.method public abstract start(Lcom/navdy/service/library/network/SocketAdapter;Ljava/util/concurrent/LinkedBlockingDeque;Lcom/navdy/service/library/device/link/LinkListener;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/navdy/service/library/network/SocketAdapter;",
            "Ljava/util/concurrent/LinkedBlockingDeque",
            "<",
            "Lcom/navdy/service/library/device/link/EventRequest;",
            ">;",
            "Lcom/navdy/service/library/device/link/LinkListener;",
            ")Z"
        }
    .end annotation
.end method
