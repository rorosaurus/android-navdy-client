.class public Lcom/navdy/service/library/network/http/HttpUtils;
.super Ljava/lang/Object;
.source "HttpUtils.java"


# static fields
.field public static final JSON:Lokhttp3/MediaType;

.field public static final MIME_TYPE_TEXT:Ljava/lang/String; = "text/plain"

.field public static final MIME_TYPE_ZIP:Ljava/lang/String; = "application/zip"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const-string v0, "application/json; charset=utf-8"

    .line 11
    invoke-static {v0}, Lokhttp3/MediaType;->parse(Ljava/lang/String;)Lokhttp3/MediaType;

    move-result-object v0

    sput-object v0, Lcom/navdy/service/library/network/http/HttpUtils;->JSON:Lokhttp3/MediaType;

    .line 10
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getJsonRequestBody(Ljava/lang/String;)Lokhttp3/RequestBody;
    .locals 2
    .param p0, "jsonData"    # Ljava/lang/String;

    .prologue
    .line 16
    sget-object v1, Lcom/navdy/service/library/network/http/HttpUtils;->JSON:Lokhttp3/MediaType;

    invoke-static {v1, p0}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v0

    .line 17
    .local v0, "body":Lokhttp3/RequestBody;
    return-object v0
.end method
