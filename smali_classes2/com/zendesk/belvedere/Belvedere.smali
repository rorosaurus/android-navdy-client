.class public Lcom/zendesk/belvedere/Belvedere;
.super Ljava/lang/Object;
.source "Belvedere.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "Belvedere"


# instance fields
.field private final belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

.field private final context:Landroid/content/Context;

.field private final imagePicker:Lcom/zendesk/belvedere/BelvedereImagePicker;

.field private final log:Lcom/zendesk/belvedere/BelvedereLogger;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/zendesk/belvedere/BelvedereConfig;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "belvedereConfig"    # Lcom/zendesk/belvedere/BelvedereConfig;

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/zendesk/belvedere/Belvedere;->context:Landroid/content/Context;

    .line 31
    new-instance v0, Lcom/zendesk/belvedere/BelvedereStorage;

    invoke-direct {v0, p2}, Lcom/zendesk/belvedere/BelvedereStorage;-><init>(Lcom/zendesk/belvedere/BelvedereConfig;)V

    iput-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    .line 32
    new-instance v0, Lcom/zendesk/belvedere/BelvedereImagePicker;

    iget-object v1, p0, Lcom/zendesk/belvedere/Belvedere;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    invoke-direct {v0, p2, v1}, Lcom/zendesk/belvedere/BelvedereImagePicker;-><init>(Lcom/zendesk/belvedere/BelvedereConfig;Lcom/zendesk/belvedere/BelvedereStorage;)V

    iput-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->imagePicker:Lcom/zendesk/belvedere/BelvedereImagePicker;

    .line 33
    invoke-virtual {p2}, Lcom/zendesk/belvedere/BelvedereConfig;->getBelvedereLogger()Lcom/zendesk/belvedere/BelvedereLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    .line 35
    iget-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v1, "Belvedere"

    const-string v2, "Belvedere initialized"

    invoke-interface {v0, v1, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public static from(Landroid/content/Context;)Lcom/zendesk/belvedere/BelvedereConfig$Builder;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 48
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 49
    new-instance v0, Lcom/zendesk/belvedere/BelvedereConfig$Builder;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/zendesk/belvedere/BelvedereConfig$Builder;-><init>(Landroid/content/Context;)V

    return-object v0

    .line 52
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid context provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    .line 163
    iget-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v1, "Belvedere"

    const-string v2, "Clear Belvedere cache"

    invoke-interface {v0, v1, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    iget-object v1, p0, Lcom/zendesk/belvedere/Belvedere;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/zendesk/belvedere/BelvedereStorage;->clearStorage(Landroid/content/Context;)V

    .line 165
    return-void
.end method

.method public getBelvedereIntents()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    iget-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->imagePicker:Lcom/zendesk/belvedere/BelvedereImagePicker;

    iget-object v1, p0, Lcom/zendesk/belvedere/Belvedere;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->getBelvedereIntents(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getFileRepresentation(Ljava/lang/String;)Lcom/zendesk/belvedere/BelvedereResult;
    .locals 8
    .param p1, "fileName"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 103
    iget-object v2, p0, Lcom/zendesk/belvedere/Belvedere;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    iget-object v3, p0, Lcom/zendesk/belvedere/Belvedere;->context:Landroid/content/Context;

    invoke-virtual {v2, v3, p1}, Lcom/zendesk/belvedere/BelvedereStorage;->getTempFileForRequestAttachment(Landroid/content/Context;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 104
    .local v0, "file":Ljava/io/File;
    iget-object v2, p0, Lcom/zendesk/belvedere/Belvedere;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v3, "Belvedere"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Get internal File: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v0, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/zendesk/belvedere/Belvedere;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    iget-object v3, p0, Lcom/zendesk/belvedere/Belvedere;->context:Landroid/content/Context;

    invoke-virtual {v2, v3, v0}, Lcom/zendesk/belvedere/BelvedereStorage;->getFileProviderUri(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 109
    new-instance v2, Lcom/zendesk/belvedere/BelvedereResult;

    invoke-direct {v2, v0, v1}, Lcom/zendesk/belvedere/BelvedereResult;-><init>(Ljava/io/File;Landroid/net/Uri;)V

    .line 112
    .end local v1    # "uri":Landroid/net/Uri;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getFilesFromActivityOnResult(IILandroid/content/Intent;Lcom/zendesk/belvedere/BelvedereCallback;)V
    .locals 6
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;
    .param p4    # Lcom/zendesk/belvedere/BelvedereCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Landroid/content/Intent;",
            "Lcom/zendesk/belvedere/BelvedereCallback",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/belvedere/BelvedereResult;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p4, "callback":Lcom/zendesk/belvedere/BelvedereCallback;, "Lcom/zendesk/belvedere/BelvedereCallback<Ljava/util/List<Lcom/zendesk/belvedere/BelvedereResult;>;>;"
    iget-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->imagePicker:Lcom/zendesk/belvedere/BelvedereImagePicker;

    iget-object v1, p0, Lcom/zendesk/belvedere/Belvedere;->context:Landroid/content/Context;

    move v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/zendesk/belvedere/BelvedereImagePicker;->getFilesFromActivityOnResult(Landroid/content/Context;IILandroid/content/Intent;Lcom/zendesk/belvedere/BelvedereCallback;)V

    .line 89
    return-void
.end method

.method public grantPermissionsForUri(Landroid/content/Intent;Landroid/net/Uri;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v1, "Belvedere"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Grant Permission - Intent: %s - Uri: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x1

    aput-object p2, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    iget-object v1, p0, Lcom/zendesk/belvedere/Belvedere;->context:Landroid/content/Context;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/zendesk/belvedere/BelvedereStorage;->grantPermissionsForUri(Landroid/content/Context;Landroid/content/Intent;Landroid/net/Uri;I)V

    .line 125
    return-void
.end method

.method public isFunctionalityAvailable(Lcom/zendesk/belvedere/BelvedereSource;)Z
    .locals 2
    .param p1, "source"    # Lcom/zendesk/belvedere/BelvedereSource;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 156
    iget-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->imagePicker:Lcom/zendesk/belvedere/BelvedereImagePicker;

    iget-object v1, p0, Lcom/zendesk/belvedere/Belvedere;->context:Landroid/content/Context;

    invoke-virtual {v0, p1, v1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->isFunctionalityAvailable(Lcom/zendesk/belvedere/BelvedereSource;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public oneOrMoreSourceAvailable()Z
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->imagePicker:Lcom/zendesk/belvedere/BelvedereImagePicker;

    iget-object v1, p0, Lcom/zendesk/belvedere/Belvedere;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/zendesk/belvedere/BelvedereImagePicker;->oneOrMoreSourceAvailable(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public revokePermissionsForUri(Landroid/net/Uri;)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->log:Lcom/zendesk/belvedere/BelvedereLogger;

    const-string v1, "Belvedere"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Revoke Permission - Uri: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/zendesk/belvedere/BelvedereLogger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/zendesk/belvedere/Belvedere;->belvedereStorage:Lcom/zendesk/belvedere/BelvedereStorage;

    iget-object v1, p0, Lcom/zendesk/belvedere/Belvedere;->context:Landroid/content/Context;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, p1, v2}, Lcom/zendesk/belvedere/BelvedereStorage;->revokePermissionsFromUri(Landroid/content/Context;Landroid/net/Uri;I)V

    .line 136
    return-void
.end method

.method public showDialog(Landroid/support/v4/app/FragmentManager;)V
    .locals 1
    .param p1, "fragmentManager"    # Landroid/support/v4/app/FragmentManager;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/zendesk/belvedere/Belvedere;->getBelvedereIntents()Ljava/util/List;

    move-result-object v0

    .line 73
    .local v0, "intents":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/belvedere/BelvedereIntent;>;"
    invoke-static {p1, v0}, Lcom/zendesk/belvedere/BelvedereDialog;->showDialog(Landroid/support/v4/app/FragmentManager;Ljava/util/List;)V

    .line 74
    return-void
.end method
