.class public Lcom/zendesk/belvedere/BelvedereFileProvider;
.super Landroid/support/v4/content/FileProvider;
.source "BelvedereFileProvider.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "BelvedereFileProvider"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/content/FileProvider;-><init>()V

    return-void
.end method

.method private columnNamesWithData([Ljava/lang/String;)[Ljava/lang/String;
    .locals 5
    .param p1, "columnNames"    # [Ljava/lang/String;

    .prologue
    .line 65
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, p1, v2

    .line 66
    .local v0, "columnName":Ljava/lang/String;
    const-string v4, "_data"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 73
    .end local v0    # "columnName":Ljava/lang/String;
    .end local p1    # "columnNames":[Ljava/lang/String;
    :goto_1
    return-object p1

    .line 65
    .restart local v0    # "columnName":Ljava/lang/String;
    .restart local p1    # "columnNames":[Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 71
    .end local v0    # "columnName":Ljava/lang/String;
    :cond_1
    array-length v2, p1

    add-int/lit8 v2, v2, 0x1

    invoke-static {p1, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    .line 72
    .local v1, "newColumnNames":[Ljava/lang/String;
    array-length v2, p1

    const-string v3, "_data"

    aput-object v3, v1, v2

    move-object p1, v1

    .line 73
    goto :goto_1
.end method


# virtual methods
.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-super/range {p0 .. p5}, Landroid/support/v4/content/FileProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    .line 41
    .local v5, "source":Landroid/database/Cursor;
    if-nez v5, :cond_0

    .line 42
    const-string v6, "BelvedereFileProvider"

    const-string v7, "Not able to apply workaround, super.query(...) returned null"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    const/4 v1, 0x0

    .line 60
    :goto_0
    return-object v1

    .line 46
    :cond_0
    invoke-interface {v5}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 47
    .local v0, "columnNames":[Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/zendesk/belvedere/BelvedereFileProvider;->columnNamesWithData([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 49
    .local v3, "newColumnNames":[Ljava/lang/String;
    new-instance v1, Landroid/database/MatrixCursor;

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v6

    invoke-direct {v1, v3, v6}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;I)V

    .line 51
    .local v1, "cursor":Landroid/database/MatrixCursor;
    const/4 v6, -0x1

    invoke-interface {v5, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 52
    :cond_1
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 53
    invoke-virtual {v1}, Landroid/database/MatrixCursor;->newRow()Landroid/database/MatrixCursor$RowBuilder;

    move-result-object v4

    .line 54
    .local v4, "row":Landroid/database/MatrixCursor$RowBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v6, v0

    if-ge v2, v6, :cond_1

    .line 55
    invoke-interface {v5, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/database/MatrixCursor$RowBuilder;->add(Ljava/lang/Object;)Landroid/database/MatrixCursor$RowBuilder;

    .line 54
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 58
    .end local v2    # "i":I
    .end local v4    # "row":Landroid/database/MatrixCursor$RowBuilder;
    :cond_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method
