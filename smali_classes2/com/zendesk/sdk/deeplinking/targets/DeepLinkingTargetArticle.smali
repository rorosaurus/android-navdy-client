.class public Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;
.super Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;
.source "DeepLinkingTargetArticle.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget",
        "<",
        "Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;",
        ">;"
    }
.end annotation


# static fields
.field public static EXTRA_ARTICLE:Ljava/lang/String;

.field public static EXTRA_ARTICLE_LOCALE:Ljava/lang/String;

.field public static EXTRA_ARTICLE_SIMPLE:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "extra_article"

    sput-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE:Ljava/lang/String;

    .line 20
    const-string v0, "extra_article_simple"

    sput-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE_SIMPLE:Ljava/lang/String;

    .line 21
    const-string v0, "extra_article_locale"

    sput-object v0, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE_LOCALE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTarget;-><init>()V

    return-void
.end method


# virtual methods
.method extractConfiguration(Landroid/os/Bundle;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "targetConfiguration"    # Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;

    .prologue
    .line 50
    sget-object v3, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE_LOCALE:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 52
    .local v1, "locale":Ljava/lang/String;
    sget-object v3, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    instance-of v3, v3, Lcom/zendesk/sdk/model/helpcenter/Article;

    if-eqz v3, :cond_0

    .line 53
    sget-object v3, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/model/helpcenter/Article;

    .line 54
    .local v0, "article":Lcom/zendesk/sdk/model/helpcenter/Article;
    new-instance v3, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getBackStackActivities()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getFallbackActivity()Landroid/content/Intent;

    move-result-object v5

    invoke-direct {v3, v0, v1, v4, v5}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;-><init>(Lcom/zendesk/sdk/model/helpcenter/Article;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V

    .line 61
    .end local v0    # "article":Lcom/zendesk/sdk/model/helpcenter/Article;
    :goto_0
    return-object v3

    .line 56
    :cond_0
    sget-object v3, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE_SIMPLE:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE_SIMPLE:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    instance-of v3, v3, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    if-eqz v3, :cond_1

    .line 57
    sget-object v3, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE_SIMPLE:Ljava/lang/String;

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    .line 58
    .local v2, "simpleArticle":Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;
    new-instance v3, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getBackStackActivities()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;->getFallbackActivity()Landroid/content/Intent;

    move-result-object v5

    invoke-direct {v3, v2, v1, v4, v5}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;-><init>(Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;Ljava/lang/String;Ljava/util/ArrayList;Landroid/content/Intent;)V

    goto :goto_0

    .line 61
    .end local v2    # "simpleArticle":Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method bridge synthetic extractConfiguration(Landroid/os/Bundle;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->extractConfiguration(Landroid/os/Bundle;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;

    move-result-object v0

    return-object v0
.end method

.method getExtra(Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;)Landroid/os/Bundle;
    .locals 3
    .param p1, "configuration"    # Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 67
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 69
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p1}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 70
    sget-object v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 77
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getLocale()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 78
    sget-object v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE_LOCALE:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getLocale()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_1
    return-object v0

    .line 72
    :cond_2
    invoke-virtual {p1}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getSimpleArticle()Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 73
    sget-object v1, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->EXTRA_ARTICLE_SIMPLE:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getSimpleArticle()Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0
.end method

.method bridge synthetic getExtra(Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)Landroid/os/Bundle;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 17
    check-cast p1, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->getExtra(Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method openTargetActivity(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "configuration"    # Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 26
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 28
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getLocale()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 29
    const-string v2, "locale"

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getLocale()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    :cond_0
    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 33
    const-string v2, "article"

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 40
    :goto_0
    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getBackStackActivities()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->getTaskStackBuilder(Landroid/content/Context;Ljava/util/List;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v1

    .line 42
    .local v1, "taskStackBuilder":Landroid/support/v4/app/TaskStackBuilder;
    const-class v2, Lcom/zendesk/sdk/support/ViewArticleActivity;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/TaskStackBuilder;->addParentStack(Ljava/lang/Class;)Landroid/support/v4/app/TaskStackBuilder;

    .line 43
    invoke-virtual {v1, v0}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    .line 45
    invoke-virtual {v1}, Landroid/support/v4/app/TaskStackBuilder;->startActivities()V

    .line 46
    .end local v1    # "taskStackBuilder":Landroid/support/v4/app/TaskStackBuilder;
    :cond_1
    return-void

    .line 34
    :cond_2
    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getSimpleArticle()Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 35
    const-string v2, "article_simple"

    invoke-virtual {p2}, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;->getSimpleArticle()Lcom/zendesk/sdk/model/helpcenter/SimpleArticle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method bridge synthetic openTargetActivity(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;)V
    .locals 0
    .param p2    # Lcom/zendesk/sdk/deeplinking/targets/TargetConfiguration;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 17
    check-cast p2, Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;

    invoke-virtual {p0, p1, p2}, Lcom/zendesk/sdk/deeplinking/targets/DeepLinkingTargetArticle;->openTargetActivity(Landroid/content/Context;Lcom/zendesk/sdk/deeplinking/targets/ArticleConfiguration;)V

    return-void
.end method
