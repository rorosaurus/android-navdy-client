.class interface abstract Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser$ZendeskDeepLinkParserModule;
.super Ljava/lang/Object;
.source "ZendeskDeepLinkingParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/deeplinking/ZendeskDeepLinkingParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "ZendeskDeepLinkParserModule"
.end annotation


# virtual methods
.method public abstract parse(Ljava/lang/String;Landroid/content/Context;)Landroid/content/Intent;
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method
