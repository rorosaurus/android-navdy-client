.class public Lcom/zendesk/sdk/model/access/AuthenticationResponse;
.super Ljava/lang/Object;
.source "AuthenticationResponse.java"


# instance fields
.field authentication:Lcom/zendesk/sdk/model/access/AccessToken;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAuthentication()Lcom/zendesk/sdk/model/access/AccessToken;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/zendesk/sdk/model/access/AuthenticationResponse;->authentication:Lcom/zendesk/sdk/model/access/AccessToken;

    return-object v0
.end method
