.class public Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
.super Ljava/lang/Object;
.source "SafeMobileSettings.java"


# instance fields
.field private final mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/model/settings/MobileSettings;)V
    .locals 0
    .param p1, "mobileSettings"    # Lcom/zendesk/sdk/model/settings/MobileSettings;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 33
    return-void
.end method

.method private getAttachmentSettings()Lcom/zendesk/sdk/model/settings/AttachmentSettings;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 192
    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 193
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getAccountSettings()Lcom/zendesk/sdk/model/settings/AccountSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 194
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getAccountSettings()Lcom/zendesk/sdk/model/settings/AccountSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/AccountSettings;->getAttachmentSettings()Lcom/zendesk/sdk/model/settings/AttachmentSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 196
    .local v0, "hasAttachmentSettings":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 197
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getAccountSettings()Lcom/zendesk/sdk/model/settings/AccountSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/AccountSettings;->getAttachmentSettings()Lcom/zendesk/sdk/model/settings/AttachmentSettings;

    move-result-object v1

    :goto_1
    return-object v1

    .line 194
    .end local v0    # "hasAttachmentSettings":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 197
    .restart local v0    # "hasAttachmentSettings":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getAuthenticationTypeSetting()Lcom/zendesk/sdk/model/access/AuthenticationType;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 293
    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 294
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 295
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SdkSettings;->getAuthentication()Lcom/zendesk/sdk/model/access/AuthenticationType;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 297
    .local v0, "hasAuthenticationType":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 298
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SdkSettings;->getAuthentication()Lcom/zendesk/sdk/model/access/AuthenticationType;

    move-result-object v1

    :goto_1
    return-object v1

    .line 295
    .end local v0    # "hasAuthenticationType":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 298
    .restart local v0    # "hasAuthenticationType":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getConversationsSettings()Lcom/zendesk/sdk/model/settings/ConversationsSettings;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 214
    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 215
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 216
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SdkSettings;->getConversationsSettings()Lcom/zendesk/sdk/model/settings/ConversationsSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 218
    .local v0, "hasConversationSettings":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 219
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SdkSettings;->getConversationsSettings()Lcom/zendesk/sdk/model/settings/ConversationsSettings;

    move-result-object v1

    :goto_1
    return-object v1

    .line 216
    .end local v0    # "hasConversationSettings":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 219
    .restart local v0    # "hasConversationSettings":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private getRateMyAppSettings()Lcom/zendesk/sdk/model/settings/RateMyAppSettings;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 276
    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 277
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 278
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SdkSettings;->getRateMyAppSettings()Lcom/zendesk/sdk/model/settings/RateMyAppSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 280
    .local v0, "hasRateMyAppSettings":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 281
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SdkSettings;->getRateMyAppSettings()Lcom/zendesk/sdk/model/settings/RateMyAppSettings;

    move-result-object v1

    :goto_1
    return-object v1

    .line 278
    .end local v0    # "hasRateMyAppSettings":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 281
    .restart local v0    # "hasRateMyAppSettings":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getAuthenticationType()Lcom/zendesk/sdk/model/access/AuthenticationType;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getAuthenticationTypeSetting()Lcom/zendesk/sdk/model/access/AuthenticationType;

    move-result-object v0

    return-object v0
.end method

.method public getContactZendeskTags()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 253
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 254
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SdkSettings;->getContactUsSettings()Lcom/zendesk/sdk/model/settings/ContactUsSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 256
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SdkSettings;->getContactUsSettings()Lcom/zendesk/sdk/model/settings/ContactUsSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/ContactUsSettings;->getTags()Ljava/util/List;

    move-result-object v1

    .line 255
    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 258
    .local v0, "hasContactUsSettings":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 259
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SdkSettings;->getContactUsSettings()Lcom/zendesk/sdk/model/settings/ContactUsSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/ContactUsSettings;->getTags()Ljava/util/List;

    move-result-object v1

    :goto_1
    return-object v1

    .line 255
    .end local v0    # "hasContactUsSettings":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 259
    .restart local v0    # "hasContactUsSettings":Z
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_1
.end method

.method public getHelpCenterLocale()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 101
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getHelpCenterSettings()Lcom/zendesk/sdk/model/settings/HelpCenterSettings;

    move-result-object v0

    .line 102
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/HelpCenterSettings;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/HelpCenterSettings;->getLocale()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/HelpCenterSettings;->getLocale()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getHelpCenterSettings()Lcom/zendesk/sdk/model/settings/HelpCenterSettings;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 236
    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 237
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 238
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SdkSettings;->getHelpCenterSettings()Lcom/zendesk/sdk/model/settings/HelpCenterSettings;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 240
    .local v0, "hasHelpCenterSettings":Z
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    .line 241
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;->getSdkSettings()Lcom/zendesk/sdk/model/settings/SdkSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/settings/SdkSettings;->getHelpCenterSettings()Lcom/zendesk/sdk/model/settings/HelpCenterSettings;

    move-result-object v1

    :goto_1
    return-object v1

    .line 238
    .end local v0    # "hasHelpCenterSettings":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 241
    .restart local v0    # "hasHelpCenterSettings":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public getMaxAttachmentSize()J
    .locals 4

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getAttachmentSettings()Lcom/zendesk/sdk/model/settings/AttachmentSettings;

    move-result-object v0

    .line 71
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/AttachmentSettings;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/AttachmentSettings;->getMaxAttachmentSize()J

    move-result-wide v2

    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getMobileSettings()Lcom/zendesk/sdk/model/settings/MobileSettings;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->mobileSettings:Lcom/zendesk/sdk/model/settings/MobileSettings;

    return-object v0
.end method

.method public getRateMyAppDelay()J
    .locals 4

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppSettings()Lcom/zendesk/sdk/model/settings/RateMyAppSettings;

    move-result-object v0

    .line 133
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/RateMyAppSettings;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->getDelay()J

    move-result-wide v2

    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getRateMyAppDuration()J
    .locals 4

    .prologue
    .line 142
    invoke-direct {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppSettings()Lcom/zendesk/sdk/model/settings/RateMyAppSettings;

    move-result-object v0

    .line 143
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/RateMyAppSettings;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->getDuration()J

    move-result-wide v2

    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getRateMyAppStoreUrl()Ljava/lang/String;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 122
    invoke-direct {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppSettings()Lcom/zendesk/sdk/model/settings/RateMyAppSettings;

    move-result-object v0

    .line 123
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/RateMyAppSettings;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->getAndroidStoreUrl()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->getAndroidStoreUrl()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v1, ""

    goto :goto_0
.end method

.method public getRateMyAppTags()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppSettings()Lcom/zendesk/sdk/model/settings/RateMyAppSettings;

    move-result-object v0

    .line 164
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/RateMyAppSettings;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->getTags()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 165
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->getTags()Ljava/util/List;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public getRateMyAppVisits()J
    .locals 4

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppSettings()Lcom/zendesk/sdk/model/settings/RateMyAppSettings;

    move-result-object v0

    .line 153
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/RateMyAppSettings;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->getVisits()I

    move-result v1

    int-to-long v2, v1

    :goto_0
    return-wide v2

    :cond_0
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public hasHelpCenterSettings()Z
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getHelpCenterSettings()Lcom/zendesk/sdk/model/settings/HelpCenterSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAttachmentsEnabled()Z
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getAttachmentSettings()Lcom/zendesk/sdk/model/settings/AttachmentSettings;

    move-result-object v0

    .line 61
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/AttachmentSettings;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/AttachmentSettings;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isConversationsEnabled()Z
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getConversationsSettings()Lcom/zendesk/sdk/model/settings/ConversationsSettings;

    move-result-object v0

    .line 51
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/ConversationsSettings;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/ConversationsSettings;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isHelpCenterEnabled()Z
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getHelpCenterSettings()Lcom/zendesk/sdk/model/settings/HelpCenterSettings;

    move-result-object v0

    .line 91
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/HelpCenterSettings;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/HelpCenterSettings;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isRateMyAppEnabled()Z
    .locals 2

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppSettings()Lcom/zendesk/sdk/model/settings/RateMyAppSettings;

    move-result-object v0

    .line 112
    .local v0, "settings":Lcom/zendesk/sdk/model/settings/RateMyAppSettings;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/RateMyAppSettings;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
