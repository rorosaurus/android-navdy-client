.class Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;
.super Ljava/lang/Object;
.source "RequestCommentsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CommentRowAttachmentHelper"
.end annotation


# static fields
.field private static final IMAGE_MIME_TYPE_START:Ljava/lang/String; = "image/"

.field public static heightMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 288
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;->heightMap:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$1;

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;-><init>()V

    return-void
.end method

.method static synthetic access$600(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;Landroid/view/ViewGroup;Lcom/zendesk/sdk/model/request/CommentResponse;Landroid/content/Context;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;
    .param p1, "x1"    # Landroid/view/ViewGroup;
    .param p2, "x2"    # Lcom/zendesk/sdk/model/request/CommentResponse;
    .param p3, "x3"    # Landroid/content/Context;
    .param p4, "x4"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

    .prologue
    .line 284
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;->attachPhotoToCommentRow(Landroid/view/ViewGroup;Lcom/zendesk/sdk/model/request/CommentResponse;Landroid/content/Context;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;)V

    return-void
.end method

.method private attachPhotoToCommentRow(Landroid/view/ViewGroup;Lcom/zendesk/sdk/model/request/CommentResponse;Landroid/content/Context;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;)V
    .locals 21
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "comment"    # Lcom/zendesk/sdk/model/request/CommentResponse;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "attachmentOnClickListener"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

    .prologue
    .line 292
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/zendesk/sdk/R$dimen;->attachments_horizontal_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 293
    .local v12, "horizontalMargin":I
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/zendesk/sdk/R$dimen;->attachments_vertical_margin:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 299
    .local v6, "verticalMargin":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v18

    .line 301
    .local v18, "widthPixels":I
    if-nez v18, :cond_0

    .line 302
    invoke-virtual/range {p3 .. p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v0, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    move/from16 v18, v0

    .line 305
    :cond_0
    mul-int/lit8 v2, v12, 0x2

    sub-int v18, v18, v2

    .line 307
    invoke-virtual/range {p2 .. p2}, Lcom/zendesk/sdk/model/request/CommentResponse;->getAttachments()Ljava/util/List;

    move-result-object v10

    .line 308
    .local v10, "attachments":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/Attachment;>;"
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 310
    .local v15, "inlineAttachments":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/model/request/Attachment;>;"
    invoke-static {v10}, Lcom/zendesk/util/CollectionUtils;->isNotEmpty(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 311
    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/zendesk/sdk/model/request/Attachment;

    .line 312
    .local v5, "attachment":Lcom/zendesk/sdk/model/request/Attachment;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/zendesk/sdk/model/request/Attachment;->getContentType()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 313
    invoke-virtual {v5}, Lcom/zendesk/sdk/model/request/Attachment;->getContentType()Ljava/lang/String;

    move-result-object v3

    const-string v19, "image/"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 314
    invoke-interface {v15, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 322
    .end local v5    # "attachment":Lcom/zendesk/sdk/model/request/Attachment;
    :cond_2
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 323
    .local v4, "attachmentHeightMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Integer;>;"
    sget-object v2, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;->heightMap:Ljava/util/Map;

    invoke-virtual/range {p2 .. p2}, Lcom/zendesk/sdk/model/request/CommentResponse;->getId()Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    const/4 v13, 0x0

    .line 327
    .local v13, "inlineAttachmentIndex":I
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/zendesk/sdk/model/request/Attachment;

    .line 329
    .restart local v5    # "attachment":Lcom/zendesk/sdk/model/request/Attachment;
    const/4 v11, 0x0

    .line 334
    .local v11, "bottomMargin":I
    add-int/lit8 v14, v13, 0x1

    .end local v13    # "inlineAttachmentIndex":I
    .local v14, "inlineAttachmentIndex":I
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v13, v2, :cond_3

    .line 335
    move v11, v6

    .line 338
    :cond_3
    invoke-static/range {p3 .. p3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    sget v3, Lcom/zendesk/sdk/R$layout;->view_attachment_inline_item:I

    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v2, v3, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/FrameLayout;

    .line 339
    .local v16, "inlineContainer":Landroid/widget/FrameLayout;
    invoke-virtual/range {v16 .. v16}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v17

    check-cast v17, Landroid/widget/LinearLayout$LayoutParams;

    .line 340
    .local v17, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    move-object/from16 v0, v17

    invoke-virtual {v0, v12, v6, v12, v11}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 341
    invoke-virtual/range {v16 .. v17}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 343
    sget v2, Lcom/zendesk/sdk/R$id;->attachment_inline_image:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    .line 345
    .local v8, "imageView":Landroid/widget/ImageView;
    sget v2, Lcom/zendesk/sdk/R$id;->attachment_inline_progressbar:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ProgressBar;

    .line 346
    .local v9, "progressBar":Landroid/widget/ProgressBar;
    const/4 v2, 0x0

    invoke-virtual {v9, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 348
    move v7, v11

    .line 350
    .local v7, "finalBottomMargin":I
    invoke-static/range {p3 .. p3}, Lcom/zendesk/sdk/network/impl/ZendeskPicassoProvider;->getInstance(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v2

    .line 351
    invoke-virtual {v5}, Lcom/zendesk/sdk/model/request/Attachment;->getContentUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    sget-object v3, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->INSTANCE:Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;

    .line 352
    move/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/zendesk/sdk/ui/ZendeskPicassoTransformationFactory;->getResizeTransformationWidth(I)Lcom/squareup/picasso/Transformation;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v2

    .line 353
    invoke-virtual {v2}, Lcom/squareup/picasso/RequestCreator;->noFade()Lcom/squareup/picasso/RequestCreator;

    move-result-object v20

    new-instance v2, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v9}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$1;-><init>(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;Ljava/util/Map;Lcom/zendesk/sdk/model/request/Attachment;IILandroid/widget/ImageView;Landroid/widget/ProgressBar;)V

    .line 354
    move-object/from16 v0, v20

    invoke-virtual {v0, v8, v2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;Lcom/squareup/picasso/Callback;)V

    .line 373
    new-instance v2, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v2, v0, v1, v5}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper$2;-><init>(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;Lcom/zendesk/sdk/model/request/Attachment;)V

    invoke-virtual {v8, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 380
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move v13, v14

    .line 381
    .end local v14    # "inlineAttachmentIndex":I
    .restart local v13    # "inlineAttachmentIndex":I
    goto/16 :goto_1

    .line 382
    .end local v5    # "attachment":Lcom/zendesk/sdk/model/request/Attachment;
    .end local v7    # "finalBottomMargin":I
    .end local v8    # "imageView":Landroid/widget/ImageView;
    .end local v9    # "progressBar":Landroid/widget/ProgressBar;
    .end local v11    # "bottomMargin":I
    .end local v16    # "inlineContainer":Landroid/widget/FrameLayout;
    .end local v17    # "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_4
    return-void
.end method
