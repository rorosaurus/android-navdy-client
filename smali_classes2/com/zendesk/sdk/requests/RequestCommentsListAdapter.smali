.class Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "RequestCommentsListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;,
        Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRowAttachmentHelper;,
        Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;,
        Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$EndUserRowIdHolder;,
        Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AgentRowIdHolder;,
        Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/zendesk/sdk/requests/CommentWithUser;",
        ">;"
    }
.end annotation


# static fields
.field private static final VIEW_COUNT:I = 0x2

.field private static final VIEW_TYPE_AGENT:I = 0x0

.field private static final VIEW_TYPE_ENDUSER:I = 0x1


# instance fields
.field private mAttachmentOnClickListener:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutResource"    # I
    .param p4, "attachmentOnClickListener"    # Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/requests/CommentWithUser;",
            ">;",
            "Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 48
    .local p3, "listOfCommentsForRequest":Ljava/util/List;, "Ljava/util/List<Lcom/zendesk/sdk/requests/CommentWithUser;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 49
    iput-object p1, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;->mContext:Landroid/content/Context;

    .line 50
    iput-object p4, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;->mAttachmentOnClickListener:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

    .line 51
    return-void
.end method


# virtual methods
.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/requests/CommentWithUser;

    .line 61
    .local v0, "commentWithUserForRow":Lcom/zendesk/sdk/requests/CommentWithUser;
    invoke-virtual {v0}, Lcom/zendesk/sdk/requests/CommentWithUser;->getAuthor()Lcom/zendesk/sdk/model/request/User;

    move-result-object v1

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/request/User;->isAgent()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 68
    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/requests/CommentWithUser;

    .line 74
    .local v0, "commentWithUserForRow":Lcom/zendesk/sdk/requests/CommentWithUser;
    invoke-virtual {v0}, Lcom/zendesk/sdk/requests/CommentWithUser;->getAuthor()Lcom/zendesk/sdk/model/request/User;

    move-result-object v2

    invoke-virtual {v2}, Lcom/zendesk/sdk/model/request/User;->isAgent()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 75
    new-instance v1, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;

    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AgentRowIdHolder;

    invoke-direct {v3, v4}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AgentRowIdHolder;-><init>(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$1;)V

    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;->mAttachmentOnClickListener:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

    invoke-direct {v1, v2, v3, v4}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;-><init>(Landroid/content/Context;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;)V

    .line 81
    .local v1, "row":Lcom/zendesk/sdk/ui/ListRowView;
    :goto_0
    invoke-interface {v1, v0}, Lcom/zendesk/sdk/ui/ListRowView;->bind(Ljava/lang/Object;)V

    .line 83
    invoke-interface {v1}, Lcom/zendesk/sdk/ui/ListRowView;->getView()Landroid/view/View;

    move-result-object v2

    return-object v2

    .line 77
    .end local v1    # "row":Lcom/zendesk/sdk/ui/ListRowView;
    :cond_0
    new-instance v1, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;

    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$EndUserRowIdHolder;

    invoke-direct {v3, v4}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$EndUserRowIdHolder;-><init>(Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$1;)V

    iget-object v4, p0, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter;->mAttachmentOnClickListener:Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;

    invoke-direct {v1, v2, v3, v4}, Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$CommentRow;-><init>(Landroid/content/Context;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$IdHolder;Lcom/zendesk/sdk/requests/RequestCommentsListAdapter$AttachmentOnClickListener;)V

    .restart local v1    # "row":Lcom/zendesk/sdk/ui/ListRowView;
    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x2

    return v0
.end method
