.class Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback$1;
.super Ljava/lang/Object;
.source "RequestListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->onSuccess(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;

.field final synthetic val$listView:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;Landroid/widget/ListView;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;

    .prologue
    .line 287
    iput-object p1, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback$1;->this$1:Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;

    iput-object p2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback$1;->val$listView:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 291
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback$1;->this$1:Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;

    iget-object v2, v2, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-virtual {v2}, Lcom/zendesk/sdk/requests/RequestListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/zendesk/sdk/util/NetworkUtils;->isConnected(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 292
    invoke-static {}, Lcom/zendesk/sdk/requests/RequestListFragment;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Ignoring conversation selection because there is no network connection"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 305
    :goto_0
    return-void

    .line 296
    :cond_0
    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback$1;->val$listView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/request/Request;

    .line 297
    .local v1, "request":Lcom/zendesk/sdk/model/request/Request;
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback$1;->this$1:Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;

    iget-object v2, v2, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-virtual {v2}, Lcom/zendesk/sdk/requests/RequestListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/zendesk/sdk/requests/ViewRequestActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 298
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "requestId"

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/request/Request;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 300
    invoke-virtual {v1}, Lcom/zendesk/sdk/model/request/Request;->getSubject()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/zendesk/util/StringUtils;->hasLength(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 301
    const-string v2, "subject"

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/request/Request;->getSubject()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 304
    :cond_1
    iget-object v2, p0, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback$1;->this$1:Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;

    iget-object v2, v2, Lcom/zendesk/sdk/requests/RequestListFragment$RequestsCallback;->this$0:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-virtual {v2, v0}, Lcom/zendesk/sdk/requests/RequestListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
