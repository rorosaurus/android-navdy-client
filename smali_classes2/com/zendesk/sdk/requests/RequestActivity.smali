.class public Lcom/zendesk/sdk/requests/RequestActivity;
.super Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;
.source "RequestActivity.java"


# static fields
.field public static final FRAGMENT_TAG:Ljava/lang/String; = "request_list_fragment_tag"

.field private static final LOG_TAG:Ljava/lang/String; = "RequestActivity"


# instance fields
.field private configuration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

.field private mRequestListFragment:Lcom/zendesk/sdk/requests/RequestListFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;-><init>()V

    return-void
.end method

.method private createFragment()Lcom/zendesk/sdk/requests/RequestListFragment;
    .locals 2

    .prologue
    .line 143
    new-instance v0, Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-direct {v0}, Lcom/zendesk/sdk/requests/RequestListFragment;-><init>()V

    .line 144
    .local v0, "requestListFragment":Lcom/zendesk/sdk/requests/RequestListFragment;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/requests/RequestListFragment;->setRetainInstance(Z)V

    .line 145
    return-object v0
.end method

.method public static startActivity(Landroid/content/Context;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "configuration"    # Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 48
    if-nez p0, :cond_0

    .line 49
    const-string v1, "RequestActivity"

    const-string v2, "Context is null, cannot start the Activity."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 61
    :goto_0
    return-void

    .line 53
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/zendesk/sdk/requests/RequestActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    .local v0, "intent":Landroid/content/Intent;
    if-eqz p1, :cond_1

    instance-of v1, p1, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;

    if-nez v1, :cond_1

    .line 56
    const-string v1, "extra_contact_configuration"

    new-instance v2, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;

    invoke-direct {v2, p1}, Lcom/zendesk/sdk/feedback/WrappedZendeskFeedbackConfiguration;-><init>(Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 60
    :cond_1
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 65
    invoke-super {p0, p1}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    sget-object v6, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v6}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->isAuthenticationAvailable()Z

    move-result v6

    if-nez v6, :cond_0

    .line 68
    const-string v6, "RequestActivity"

    const-string v7, "Cannot show the conversations because no authentication has been set up in the SDK."

    new-array v8, v5, [Ljava/lang/Object;

    invoke-static {v6, v7, v8}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestActivity;->finish()V

    .line 72
    :cond_0
    sget v6, Lcom/zendesk/sdk/R$layout;->activity_requests:I

    invoke-virtual {p0, v6}, Lcom/zendesk/sdk/requests/RequestActivity;->setContentView(I)V

    .line 73
    invoke-static {p0}, Lcom/zendesk/sdk/ui/ToolbarSherlock;->installToolBar(Landroid/support/v7/app/AppCompatActivity;)V

    .line 75
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 77
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz v0, :cond_1

    .line 78
    invoke-virtual {v0, v3}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 81
    :cond_1
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "extra_contact_configuration"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 82
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string v7, "extra_contact_configuration"

    invoke-virtual {v6, v7}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v6

    instance-of v6, v6, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    if-eqz v6, :cond_3

    .line 83
    .local v3, "hasSuppliedContactConfiguration":Z
    :goto_0
    if-eqz v3, :cond_2

    .line 84
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "extra_contact_configuration"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    iput-object v5, p0, Lcom/zendesk/sdk/requests/RequestActivity;->configuration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    .line 87
    :cond_2
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    .line 88
    .local v2, "fragmentManager":Landroid/support/v4/app/FragmentManager;
    const-string v5, "request_list_fragment_tag"

    invoke-virtual {v2, v5}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 90
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    if-eqz v1, :cond_4

    instance-of v5, v1, Lcom/zendesk/sdk/requests/RequestListFragment;

    if-eqz v5, :cond_4

    .line 91
    check-cast v1, Lcom/zendesk/sdk/requests/RequestListFragment;

    .end local v1    # "fragment":Landroid/support/v4/app/Fragment;
    iput-object v1, p0, Lcom/zendesk/sdk/requests/RequestActivity;->mRequestListFragment:Lcom/zendesk/sdk/requests/RequestListFragment;

    .line 100
    :goto_1
    return-void

    .end local v2    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .end local v3    # "hasSuppliedContactConfiguration":Z
    :cond_3
    move v3, v5

    .line 82
    goto :goto_0

    .line 94
    .restart local v1    # "fragment":Landroid/support/v4/app/Fragment;
    .restart local v2    # "fragmentManager":Landroid/support/v4/app/FragmentManager;
    .restart local v3    # "hasSuppliedContactConfiguration":Z
    :cond_4
    invoke-direct {p0}, Lcom/zendesk/sdk/requests/RequestActivity;->createFragment()Lcom/zendesk/sdk/requests/RequestListFragment;

    move-result-object v5

    iput-object v5, p0, Lcom/zendesk/sdk/requests/RequestActivity;->mRequestListFragment:Lcom/zendesk/sdk/requests/RequestListFragment;

    .line 95
    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    .line 96
    .local v4, "transaction":Landroid/support/v4/app/FragmentTransaction;
    sget v5, Lcom/zendesk/sdk/R$id;->activity_request_fragment:I

    iget-object v6, p0, Lcom/zendesk/sdk/requests/RequestActivity;->mRequestListFragment:Lcom/zendesk/sdk/requests/RequestListFragment;

    const-string v7, "request_list_fragment_tag"

    invoke-virtual {v4, v5, v6, v7}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 97
    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/zendesk/sdk/R$menu;->activity_request_list_menu:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 125
    const/4 v0, 0x1

    return v0
.end method

.method public onNetworkAvailable()V
    .locals 3

    .prologue
    .line 104
    invoke-super {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onNetworkAvailable()V

    .line 106
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestActivity;->mRequestListFragment:Lcom/zendesk/sdk/requests/RequestListFragment;

    if-eqz v0, :cond_0

    .line 107
    const-string v0, "RequestActivity"

    const-string v1, "Dispatching onNetworkAvailable() to current fragment."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 108
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestActivity;->mRequestListFragment:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/requests/RequestListFragment;->onNetworkAvailable()V

    .line 110
    :cond_0
    return-void
.end method

.method public onNetworkUnavailable()V
    .locals 3

    .prologue
    .line 114
    invoke-super {p0}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onNetworkUnavailable()V

    .line 116
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestActivity;->mRequestListFragment:Lcom/zendesk/sdk/requests/RequestListFragment;

    if-eqz v0, :cond_0

    .line 117
    const-string v0, "RequestActivity"

    const-string v1, "Dispatching onNetworkUnavailable() to current fragment."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 118
    iget-object v0, p0, Lcom/zendesk/sdk/requests/RequestActivity;->mRequestListFragment:Lcom/zendesk/sdk/requests/RequestListFragment;

    invoke-virtual {v0}, Lcom/zendesk/sdk/requests/RequestListFragment;->onNetworkUnavailable()V

    .line 120
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x1

    .line 131
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sget v2, Lcom/zendesk/sdk/R$id;->activity_request_list_add_icon:I

    if-ne v1, v2, :cond_0

    .line 132
    iget-object v1, p0, Lcom/zendesk/sdk/requests/RequestActivity;->configuration:Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;

    invoke-static {p0, v1}, Lcom/zendesk/sdk/feedback/ui/ContactZendeskActivity;->startActivity(Landroid/content/Context;Lcom/zendesk/sdk/feedback/ZendeskFeedbackConfiguration;)V

    .line 139
    :goto_0
    return v0

    .line 134
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x102002c

    if-ne v1, v2, :cond_1

    .line 135
    invoke-virtual {p0}, Lcom/zendesk/sdk/requests/RequestActivity;->onBackPressed()V

    goto :goto_0

    .line 139
    :cond_1
    invoke-super {p0, p1}, Lcom/zendesk/sdk/ui/NetworkAwareActionbarActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
