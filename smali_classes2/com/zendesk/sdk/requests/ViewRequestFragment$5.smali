.class Lcom/zendesk/sdk/requests/ViewRequestFragment$5;
.super Ljava/lang/Object;
.source "ViewRequestFragment.java"

# interfaces
.implements Lcom/zendesk/sdk/feedback/ui/AttachmentContainerHost$AttachmentContainerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/requests/ViewRequestFragment;->onAttach(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 266
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$5;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public attachmentRemoved(Ljava/io/File;)V
    .locals 1
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 269
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$5;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v0}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$300(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Lcom/zendesk/sdk/attachment/ImageUploadHelper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/zendesk/sdk/attachment/ImageUploadHelper;->removeImage(Ljava/io/File;)V

    .line 270
    return-void
.end method
