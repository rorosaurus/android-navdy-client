.class Lcom/zendesk/sdk/requests/ImageLoader$Result;
.super Ljava/lang/Object;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/requests/ImageLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Result"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private mErrorResponse:Lcom/zendesk/service/ErrorResponse;

.field private mIsError:Z

.field private mResult:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/zendesk/sdk/requests/ImageLoader;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/ImageLoader;Lcom/zendesk/service/ErrorResponse;)V
    .locals 1
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/ImageLoader;
    .param p2, "errorResponse"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 210
    .local p0, "this":Lcom/zendesk/sdk/requests/ImageLoader$Result;, "Lcom/zendesk/sdk/requests/ImageLoader$Result<TE;>;"
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ImageLoader$Result;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    iput-object p2, p0, Lcom/zendesk/sdk/requests/ImageLoader$Result;->mErrorResponse:Lcom/zendesk/service/ErrorResponse;

    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$Result;->mIsError:Z

    .line 213
    return-void
.end method

.method constructor <init>(Lcom/zendesk/sdk/requests/ImageLoader;Ljava/lang/Object;)V
    .locals 1
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/ImageLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)V"
        }
    .end annotation

    .prologue
    .line 205
    .local p0, "this":Lcom/zendesk/sdk/requests/ImageLoader$Result;, "Lcom/zendesk/sdk/requests/ImageLoader$Result<TE;>;"
    .local p2, "result":Ljava/lang/Object;, "TE;"
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ImageLoader$Result;->this$0:Lcom/zendesk/sdk/requests/ImageLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    iput-object p2, p0, Lcom/zendesk/sdk/requests/ImageLoader$Result;->mResult:Ljava/lang/Object;

    .line 207
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$Result;->mIsError:Z

    .line 208
    return-void
.end method


# virtual methods
.method getErrorResponse()Lcom/zendesk/service/ErrorResponse;
    .locals 1

    .prologue
    .line 223
    .local p0, "this":Lcom/zendesk/sdk/requests/ImageLoader$Result;, "Lcom/zendesk/sdk/requests/ImageLoader$Result<TE;>;"
    iget-boolean v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$Result;->mIsError:Z

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$Result;->mErrorResponse:Lcom/zendesk/service/ErrorResponse;

    .line 226
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getResult()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TE;"
        }
    .end annotation

    .prologue
    .line 216
    .local p0, "this":Lcom/zendesk/sdk/requests/ImageLoader$Result;, "Lcom/zendesk/sdk/requests/ImageLoader$Result<TE;>;"
    iget-boolean v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$Result;->mIsError:Z

    if-nez v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$Result;->mResult:Ljava/lang/Object;

    .line 219
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isError()Z
    .locals 1

    .prologue
    .line 230
    .local p0, "this":Lcom/zendesk/sdk/requests/ImageLoader$Result;, "Lcom/zendesk/sdk/requests/ImageLoader$Result<TE;>;"
    iget-boolean v0, p0, Lcom/zendesk/sdk/requests/ImageLoader$Result;->mIsError:Z

    return v0
.end method
