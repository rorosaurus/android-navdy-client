.class Lcom/zendesk/sdk/requests/ViewRequestFragment$7;
.super Lcom/zendesk/service/ZendeskCallback;
.source "ViewRequestFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/requests/ViewRequestFragment;->setupCallbacks()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/service/ZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/CommentsResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/requests/ViewRequestFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/requests/ViewRequestFragment;

    .prologue
    .line 329
    iput-object p1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$7;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-direct {p0}, Lcom/zendesk/service/ZendeskCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/zendesk/service/ErrorResponse;)V
    .locals 2
    .param p1, "error"    # Lcom/zendesk/service/ErrorResponse;

    .prologue
    .line 339
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$7;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$400(Lcom/zendesk/sdk/requests/ViewRequestFragment;Z)V

    .line 340
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$7;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    sget-object v1, Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;->LOAD_COMMENTS:Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;

    invoke-static {v0, p1, v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$700(Lcom/zendesk/sdk/requests/ViewRequestFragment;Lcom/zendesk/service/ErrorResponse;Lcom/zendesk/sdk/requests/ViewRequestFragment$Action;)V

    .line 341
    return-void
.end method

.method public onSuccess(Lcom/zendesk/sdk/model/request/CommentsResponse;)V
    .locals 3
    .param p1, "result"    # Lcom/zendesk/sdk/model/request/CommentsResponse;

    .prologue
    .line 332
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->storage()Lcom/zendesk/sdk/storage/StorageStore;

    move-result-object v0

    invoke-interface {v0}, Lcom/zendesk/sdk/storage/StorageStore;->requestStorage()Lcom/zendesk/sdk/storage/RequestStorage;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$7;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$500(Lcom/zendesk/sdk/requests/ViewRequestFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/CommentsResponse;->getComments()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/zendesk/sdk/storage/RequestStorage;->setCommentCount(Ljava/lang/String;I)V

    .line 333
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$7;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$400(Lcom/zendesk/sdk/requests/ViewRequestFragment;Z)V

    .line 334
    iget-object v0, p0, Lcom/zendesk/sdk/requests/ViewRequestFragment$7;->this$0:Lcom/zendesk/sdk/requests/ViewRequestFragment;

    invoke-static {v0, p1}, Lcom/zendesk/sdk/requests/ViewRequestFragment;->access$600(Lcom/zendesk/sdk/requests/ViewRequestFragment;Lcom/zendesk/sdk/model/request/CommentsResponse;)V

    .line 335
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 329
    check-cast p1, Lcom/zendesk/sdk/model/request/CommentsResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/requests/ViewRequestFragment$7;->onSuccess(Lcom/zendesk/sdk/model/request/CommentsResponse;)V

    return-void
.end method
