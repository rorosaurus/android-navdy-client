.class public Lcom/zendesk/sdk/power/PowerConfig;
.super Ljava/lang/Object;
.source "PowerConfig.java"


# static fields
.field public static final FIFTEEN_PERCENT_BATTERY_CHARGE:F = 0.15f

.field private static final LOG_TAG:Ljava/lang/String;

.field public static final THIRTY_PERCENT_BATTERY_CHARGE:F = 0.3f

.field private static sInstance:Lcom/zendesk/sdk/power/PowerConfig;


# instance fields
.field private mBatteryOk:Z

.field private mHasReadSyncSettingsPermission:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/zendesk/sdk/power/PowerConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/power/PowerConfig;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, -0x1

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    sget-object v9, Lcom/zendesk/sdk/power/PowerConfig;->LOG_TAG:Ljava/lang/String;

    const-string v10, "PowerConfig is initialising..."

    new-array v11, v8, [Ljava/lang/Object;

    invoke-static {v9, v10, v11}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 40
    new-instance v0, Landroid/content/IntentFilter;

    const-string v9, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v9}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 41
    .local v0, "batteryChangedFilter":Landroid/content/IntentFilter;
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v9, v10, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    .line 43
    .local v2, "batteryStatus":Landroid/content/Intent;
    if-nez v2, :cond_0

    .line 44
    sget-object v9, Lcom/zendesk/sdk/power/PowerConfig;->LOG_TAG:Ljava/lang/String;

    const-string v10, "Battery status is unknown, assuming it is ok..."

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v9, v10, v8}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    iput-boolean v7, p0, Lcom/zendesk/sdk/power/PowerConfig;->mBatteryOk:Z

    .line 76
    :goto_0
    return-void

    .line 49
    :cond_0
    const-string v9, "level"

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 50
    .local v4, "level":I
    const-string v9, "scale"

    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 52
    .local v6, "scale":I
    int-to-float v9, v4

    int-to-float v10, v6

    div-float v1, v9, v10

    .line 54
    .local v1, "batteryPercentage":F
    const-string v9, "plugged"

    .line 55
    invoke-virtual {v2, v9, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 56
    .local v3, "chargingMethod":I
    if-ne v3, v7, :cond_3

    move v5, v7

    .line 58
    .local v5, "pluggedIntoAc":Z
    :goto_1
    sget-object v9, Lcom/zendesk/sdk/power/PowerConfig;->LOG_TAG:Ljava/lang/String;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "Ac charging is %s and battery capacity is %s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    .line 60
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    aput-object v13, v12, v8

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v13

    aput-object v13, v12, v7

    .line 58
    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    new-array v11, v8, [Ljava/lang/Object;

    invoke-static {v9, v10, v11}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    if-eqz v5, :cond_1

    const v9, 0x3e19999a    # 0.15f

    cmpl-float v9, v1, v9

    if-gtz v9, :cond_2

    :cond_1
    const v9, 0x3e99999a    # 0.3f

    cmpl-float v9, v1, v9

    if-lez v9, :cond_4

    :cond_2
    move v9, v7

    :goto_2
    iput-boolean v9, p0, Lcom/zendesk/sdk/power/PowerConfig;->mBatteryOk:Z

    .line 65
    sget-object v9, Lcom/zendesk/sdk/power/PowerConfig;->LOG_TAG:Ljava/lang/String;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "Initial battery state is OK?: %s"

    new-array v12, v7, [Ljava/lang/Object;

    iget-boolean v13, p0, Lcom/zendesk/sdk/power/PowerConfig;->mBatteryOk:Z

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    aput-object v13, v12, v8

    invoke-static {v10, v11, v12}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    new-array v11, v8, [Ljava/lang/Object;

    invoke-static {v9, v10, v11}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    const-string v9, "android.permission.READ_SYNC_SETTINGS"

    .line 69
    invoke-virtual {p1, v9}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v9

    if-nez v9, :cond_5

    move v9, v7

    :goto_3
    iput-boolean v9, p0, Lcom/zendesk/sdk/power/PowerConfig;->mHasReadSyncSettingsPermission:Z

    .line 72
    sget-object v9, Lcom/zendesk/sdk/power/PowerConfig;->LOG_TAG:Ljava/lang/String;

    sget-object v10, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v11, "Has permission to read sync settings: %s"

    new-array v7, v7, [Ljava/lang/Object;

    iget-boolean v12, p0, Lcom/zendesk/sdk/power/PowerConfig;->mHasReadSyncSettingsPermission:Z

    .line 75
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v7, v8

    .line 73
    invoke-static {v10, v11, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-array v8, v8, [Ljava/lang/Object;

    .line 72
    invoke-static {v9, v7, v8}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .end local v5    # "pluggedIntoAc":Z
    :cond_3
    move v5, v8

    .line 56
    goto :goto_1

    .restart local v5    # "pluggedIntoAc":Z
    :cond_4
    move v9, v8

    .line 62
    goto :goto_2

    :cond_5
    move v9, v8

    .line 69
    goto :goto_3
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/zendesk/sdk/power/PowerConfig;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    const-class v1, Lcom/zendesk/sdk/power/PowerConfig;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/zendesk/sdk/power/PowerConfig;->sInstance:Lcom/zendesk/sdk/power/PowerConfig;

    if-nez v0, :cond_0

    .line 91
    new-instance v0, Lcom/zendesk/sdk/power/PowerConfig;

    invoke-direct {v0, p0}, Lcom/zendesk/sdk/power/PowerConfig;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/zendesk/sdk/power/PowerConfig;->sInstance:Lcom/zendesk/sdk/power/PowerConfig;

    .line 94
    :cond_0
    sget-object v0, Lcom/zendesk/sdk/power/PowerConfig;->sInstance:Lcom/zendesk/sdk/power/PowerConfig;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public isBatteryLow()Z
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/zendesk/sdk/power/PowerConfig;->mBatteryOk:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBatteryOk()Z
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/zendesk/sdk/power/PowerConfig;->mBatteryOk:Z

    return v0
.end method

.method public isGlobalSyncDisabled()Z
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/zendesk/sdk/power/PowerConfig;->isGlobalSyncEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isGlobalSyncEnabled()Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lcom/zendesk/sdk/power/PowerConfig;->mHasReadSyncSettingsPermission:Z

    if-eqz v0, :cond_0

    .line 170
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    .line 173
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method setBatteryOk(Z)V
    .locals 3
    .param p1, "batteryOk"    # Z

    .prologue
    .line 148
    sget-object v0, Lcom/zendesk/sdk/power/PowerConfig;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting batteryOk to: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    iput-boolean p1, p0, Lcom/zendesk/sdk/power/PowerConfig;->mBatteryOk:Z

    .line 150
    return-void
.end method
