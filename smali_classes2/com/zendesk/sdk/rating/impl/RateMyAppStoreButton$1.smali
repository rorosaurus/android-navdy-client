.class Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton$1;
.super Ljava/lang/Object;
.source "RateMyAppStoreButton.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;->getOnClickListener()Landroid/view/View$OnClickListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;

.field final synthetic val$safeMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton$1;->this$0:Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;

    iput-object p2, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton$1;->val$safeMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 55
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 58
    .local v0, "context":Landroid/content/Context;
    :try_start_0
    iget-object v4, p0, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton$1;->val$safeMobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {v4}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->getRateMyAppStoreUrl()Ljava/lang/String;

    move-result-object v2

    .line 59
    .local v2, "storeDetailsUrl":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 61
    .local v3, "storeIntent":Landroid/content/Intent;
    invoke-static {}, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Using store URL: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/zendesk/logger/Logger;->i(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    invoke-virtual {v0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 65
    new-instance v4, Lcom/zendesk/sdk/storage/RateMyAppStorage;

    invoke-direct {v4, v0}, Lcom/zendesk/sdk/storage/RateMyAppStorage;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Lcom/zendesk/sdk/storage/RateMyAppStorage;->setRatedForCurrentVersion()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    .end local v2    # "storeDetailsUrl":Ljava/lang/String;
    .end local v3    # "storeIntent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v1

    .line 68
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {}, Lcom/zendesk/sdk/rating/impl/RateMyAppStoreButton;->access$000()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    new-array v6, v7, [Ljava/lang/Object;

    invoke-static {v4, v5, v1, v6}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;[Ljava/lang/Object;)V

    goto :goto_0
.end method
