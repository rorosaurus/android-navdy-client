.class Lcom/zendesk/sdk/rating/ui/FeedbackDialog$3;
.super Lcom/zendesk/sdk/ui/TextWatcherAdapter;
.source "FeedbackDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

.field final synthetic val$sendButton:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/rating/ui/FeedbackDialog;Landroid/view/View;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    .prologue
    .line 198
    iput-object p1, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$3;->this$0:Lcom/zendesk/sdk/rating/ui/FeedbackDialog;

    iput-object p2, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$3;->val$sendButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/zendesk/sdk/ui/TextWatcherAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1, "editable"    # Landroid/text/Editable;

    .prologue
    const/4 v2, 0x0

    .line 202
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$3;->val$sendButton:Landroid/view/View;

    if-nez v0, :cond_0

    .line 203
    invoke-static {}, Lcom/zendesk/sdk/rating/ui/FeedbackDialog;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ignoring afterTextChanged() because sendButton is null"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    :goto_0
    return-void

    .line 207
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 208
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$3;->val$sendButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 210
    :cond_2
    iget-object v0, p0, Lcom/zendesk/sdk/rating/ui/FeedbackDialog$3;->val$sendButton:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method
