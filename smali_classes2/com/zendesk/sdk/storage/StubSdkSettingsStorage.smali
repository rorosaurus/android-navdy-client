.class Lcom/zendesk/sdk/storage/StubSdkSettingsStorage;
.super Ljava/lang/Object;
.source "StubSdkSettingsStorage.java"

# interfaces
.implements Lcom/zendesk/sdk/storage/SdkSettingsStorage;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "StubSdkStorage"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public areSettingsUpToDate(JLjava/util/concurrent/TimeUnit;)Z
    .locals 4
    .param p1, "duration"    # J
    .param p3, "timeUnit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    const/4 v3, 0x0

    .line 27
    const-string v0, "StubSdkStorage"

    const-string v1, "Zendesk not initialised"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    return v3
.end method

.method public deleteStoredSettings()V
    .locals 3

    .prologue
    .line 45
    const-string v0, "StubSdkStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 46
    return-void
.end method

.method public getStoredSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 34
    const-string v0, "StubSdkStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    new-instance v0, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    new-instance v1, Lcom/zendesk/sdk/model/settings/MobileSettings;

    invoke-direct {v1}, Lcom/zendesk/sdk/model/settings/MobileSettings;-><init>()V

    invoke-direct {v0, v1}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;-><init>(Lcom/zendesk/sdk/model/settings/MobileSettings;)V

    return-object v0
.end method

.method public hasStoredSdkSettings()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 21
    const-string v0, "StubSdkStorage"

    const-string v1, "Zendesk not initialised"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 22
    return v3
.end method

.method public setStoredSettings(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)V
    .locals 3
    .param p1, "mobileSettings"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    .line 40
    const-string v0, "StubSdkStorage"

    const-string v1, "Zendesk not initialised"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    return-void
.end method
