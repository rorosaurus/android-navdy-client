.class Lcom/zendesk/sdk/support/SupportPresenter;
.super Ljava/lang/Object;
.source "SupportPresenter.java"

# interfaces
.implements Lcom/zendesk/sdk/support/SupportMvp$Presenter;
.implements Lcom/zendesk/sdk/network/NetworkAware;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "SupportPresenter"

.field private static final NETWORK_AWARE_ID:Ljava/lang/Integer;

.field private static final RETRY_ACTION_ID:Ljava/lang/Integer;


# instance fields
.field private config:Lcom/zendesk/sdk/support/SupportUiConfig;

.field private internalRetryActions:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/zendesk/sdk/network/RetryAction;",
            ">;"
        }
    .end annotation
.end field

.field private mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

.field private model:Lcom/zendesk/sdk/support/SupportMvp$Model;

.field private networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

.field private networkPreviouslyUnavailable:Z

.field private view:Lcom/zendesk/sdk/support/SupportMvp$View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/16 v0, 0x1f

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/support/SupportPresenter;->NETWORK_AWARE_ID:Ljava/lang/Integer;

    .line 39
    const/16 v0, 0x21c2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/zendesk/sdk/support/SupportPresenter;->RETRY_ACTION_ID:Ljava/lang/Integer;

    return-void
.end method

.method constructor <init>(Lcom/zendesk/sdk/support/SupportMvp$View;Lcom/zendesk/sdk/support/SupportMvp$Model;Lcom/zendesk/sdk/network/NetworkInfoProvider;)V
    .locals 1
    .param p1, "view"    # Lcom/zendesk/sdk/support/SupportMvp$View;
    .param p2, "model"    # Lcom/zendesk/sdk/support/SupportMvp$Model;
    .param p3, "networkInfoProvider"    # Lcom/zendesk/sdk/network/NetworkInfoProvider;

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->internalRetryActions:Ljava/util/Set;

    .line 64
    iput-object p1, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    .line 65
    iput-object p2, p0, Lcom/zendesk/sdk/support/SupportPresenter;->model:Lcom/zendesk/sdk/support/SupportMvp$Model;

    .line 66
    iput-object p3, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    .line 68
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->INSTANCE:Lcom/zendesk/sdk/network/impl/ZendeskConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/ZendeskConfig;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportMvp$View;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportPresenter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/zendesk/sdk/support/SupportPresenter;)Lcom/zendesk/sdk/support/SupportUiConfig;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportPresenter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->config:Lcom/zendesk/sdk/support/SupportUiConfig;

    return-object v0
.end method

.method static synthetic access$200(Lcom/zendesk/sdk/support/SupportPresenter;)Ljava/util/Set;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportPresenter;

    .prologue
    .line 35
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->internalRetryActions:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic access$402(Lcom/zendesk/sdk/support/SupportPresenter;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Lcom/zendesk/sdk/model/settings/SafeMobileSettings;
    .locals 0
    .param p0, "x0"    # Lcom/zendesk/sdk/support/SupportPresenter;
    .param p1, "x1"    # Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/zendesk/sdk/support/SupportPresenter;->mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    return-object p1
.end method

.method private builderWithHelpCenterIdsFromBundle(Landroid/os/Bundle;)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 95
    new-instance v0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    invoke-direct {v0}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;-><init>()V

    .line 96
    .local v0, "builder":Lcom/zendesk/sdk/support/SupportUiConfig$Builder;
    const/4 v1, 0x0

    .line 97
    .local v1, "idsSpecified":Z
    const-string v2, "extra_category_ids"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    if-eqz v2, :cond_0

    .line 98
    const-string v2, "extra_category_ids"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/zendesk/sdk/support/SupportPresenter;->convertToLongList([J)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->withCategoryIds(Ljava/util/List;)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    move-result-object v0

    .line 99
    const/4 v1, 0x1

    .line 101
    :cond_0
    const-string v2, "extra_section_ids"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    if-eqz v2, :cond_1

    .line 102
    const-string v2, "extra_section_ids"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/zendesk/sdk/support/SupportPresenter;->convertToLongList([J)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->withSectionIds(Ljava/util/List;)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    move-result-object v0

    .line 103
    const/4 v1, 0x1

    .line 105
    :cond_1
    if-nez v1, :cond_2

    .line 106
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportPresenter;->logNoCategoriesOrSectionsSet()V

    .line 108
    :cond_2
    return-object v0
.end method

.method private convertToLongList([J)Ljava/util/List;
    .locals 6
    .param p1, "longArray"    # [J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    if-nez p1, :cond_1

    .line 149
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 156
    :cond_0
    return-object v2

    .line 152
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    array-length v3, p1

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 153
    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    array-length v4, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget-wide v0, p1, v3

    .line 154
    .local v0, "entry":J
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private extractDataFromBundle(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 86
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/support/SupportPresenter;->builderWithHelpCenterIdsFromBundle(Landroid/os/Bundle;)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    move-result-object v0

    const-string v1, "extra_label_names"

    .line 87
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->withLabelNames([Ljava/lang/String;)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    move-result-object v0

    const-string v1, "extra_show_contact_us_button"

    .line 88
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->withShowContactUsButton(Z)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    move-result-object v0

    const-string v1, "extra_show_contact_us_button"

    .line 89
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->withAddListPaddingBottom(Z)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    move-result-object v0

    const-string v1, "extra_categories_collapsed"

    const/4 v2, 0x0

    .line 90
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->withCollapseCategories(Z)Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->build()Lcom/zendesk/sdk/support/SupportUiConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->config:Lcom/zendesk/sdk/support/SupportUiConfig;

    .line 92
    return-void
.end method

.method private invokeRetryActions()V
    .locals 3

    .prologue
    .line 132
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportPresenter;->internalRetryActions:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/RetryAction;

    .line 133
    .local v0, "retryAction":Lcom/zendesk/sdk/network/RetryAction;
    invoke-interface {v0}, Lcom/zendesk/sdk/network/RetryAction;->onRetry()V

    goto :goto_0

    .line 135
    .end local v0    # "retryAction":Lcom/zendesk/sdk/network/RetryAction;
    :cond_0
    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportPresenter;->internalRetryActions:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 136
    return-void
.end method

.method private logNoCategoriesOrSectionsSet()V
    .locals 3

    .prologue
    .line 112
    const-string v0, "SupportPresenter"

    const-string v1, "No category or section IDs have been set."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    return-void
.end method


# virtual methods
.method public determineFirstScreen(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 301
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->showLoadingState()V

    .line 302
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->model:Lcom/zendesk/sdk/support/SupportMvp$Model;

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$5;

    invoke-direct {v1, p0, p1}, Lcom/zendesk/sdk/support/SupportPresenter$5;-><init>(Lcom/zendesk/sdk/support/SupportPresenter;Landroid/os/Bundle;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/support/SupportMvp$Model;->getSettings(Lcom/zendesk/service/ZendeskCallback;)V

    .line 400
    return-void
.end method

.method public initWithBundle(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 73
    if-eqz p1, :cond_1

    .line 74
    invoke-direct {p0, p1}, Lcom/zendesk/sdk/support/SupportPresenter;->extractDataFromBundle(Landroid/os/Bundle;)V

    .line 80
    :goto_0
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->isShowingHelp()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportPresenter;->config:Lcom/zendesk/sdk/support/SupportUiConfig;

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/support/SupportMvp$View;->showHelp(Lcom/zendesk/sdk/support/SupportUiConfig;)V

    .line 83
    :cond_0
    return-void

    .line 76
    :cond_1
    new-instance v0, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;

    invoke-direct {v0}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/zendesk/sdk/support/SupportUiConfig$Builder;->build()Lcom/zendesk/sdk/support/SupportUiConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->config:Lcom/zendesk/sdk/support/SupportUiConfig;

    .line 77
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportPresenter;->logNoCategoriesOrSectionsSet()V

    goto :goto_0
.end method

.method public onErrorWithRetry(Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V
    .locals 2
    .param p1, "errorType"    # Lcom/zendesk/sdk/support/SupportMvp$ErrorType;
    .param p2, "action"    # Lcom/zendesk/sdk/network/RetryAction;

    .prologue
    .line 246
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    if-eqz v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->isShowingHelp()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->hideLoadingState()V

    .line 249
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    invoke-interface {v0, p1, p2}, Lcom/zendesk/sdk/support/SupportMvp$View;->showErrorWithRetry(Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 252
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->internalRetryActions:Ljava/util/Set;

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/zendesk/sdk/support/SupportPresenter$3;-><init>(Lcom/zendesk/sdk/support/SupportPresenter;Lcom/zendesk/sdk/support/SupportMvp$ErrorType;Lcom/zendesk/sdk/network/RetryAction;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onLoad()V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->config:Lcom/zendesk/sdk/support/SupportUiConfig;

    invoke-virtual {v0}, Lcom/zendesk/sdk/support/SupportUiConfig;->isShowContactUsButton()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->showContactUsButton()V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->internalRetryActions:Ljava/util/Set;

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$2;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/SupportPresenter$2;-><init>(Lcom/zendesk/sdk/support/SupportPresenter;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onNetworkAvailable()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 266
    const-string v0, "SupportPresenter"

    const-string v1, "Network is available."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 268
    iget-boolean v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkPreviouslyUnavailable:Z

    if-nez v0, :cond_0

    .line 269
    const-string v0, "SupportPresenter"

    const-string v1, "Network was not previously unavailable, no need to dismiss Snackbar"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 285
    :goto_0
    return-void

    .line 273
    :cond_0
    iput-boolean v3, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkPreviouslyUnavailable:Z

    .line 275
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->dismissError()V

    goto :goto_0

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->internalRetryActions:Ljava/util/Set;

    new-instance v1, Lcom/zendesk/sdk/support/SupportPresenter$4;

    invoke-direct {v1, p0}, Lcom/zendesk/sdk/support/SupportPresenter$4;-><init>(Lcom/zendesk/sdk/support/SupportPresenter;)V

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public onNetworkUnavailable()V
    .locals 3

    .prologue
    .line 289
    const-string v0, "SupportPresenter"

    const-string v1, "Network is unavailable."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 290
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkPreviouslyUnavailable:Z

    .line 292
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    sget v1, Lcom/zendesk/sdk/R$string;->network_activity_no_connectivity:I

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/support/SupportMvp$View;->showError(I)V

    .line 294
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->hideLoadingState()V

    .line 296
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 140
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    .line 142
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    sget-object v1, Lcom/zendesk/sdk/support/SupportPresenter;->NETWORK_AWARE_ID:Ljava/lang/Integer;

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->removeNetworkAwareListener(Ljava/lang/Integer;)V

    .line 143
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    sget-object v1, Lcom/zendesk/sdk/support/SupportPresenter;->RETRY_ACTION_ID:Ljava/lang/Integer;

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->removeRetryAction(Ljava/lang/Integer;)V

    .line 144
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->unregister()V

    .line 145
    return-void
.end method

.method public onResume(Lcom/zendesk/sdk/support/SupportMvp$View;)V
    .locals 2
    .param p1, "view"    # Lcom/zendesk/sdk/support/SupportMvp$View;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    .line 119
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    sget-object v1, Lcom/zendesk/sdk/support/SupportPresenter;->NETWORK_AWARE_ID:Ljava/lang/Integer;

    invoke-interface {v0, v1, p0}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->addNetworkAwareListener(Ljava/lang/Integer;Lcom/zendesk/sdk/network/NetworkAware;)V

    .line 120
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->register()V

    .line 122
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->isNetworkAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    sget v0, Lcom/zendesk/sdk/R$string;->network_activity_no_connectivity:I

    invoke-interface {p1, v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->showError(I)V

    .line 124
    invoke-interface {p1}, Lcom/zendesk/sdk/support/SupportMvp$View;->hideLoadingState()V

    .line 125
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkPreviouslyUnavailable:Z

    .line 128
    :cond_0
    invoke-direct {p0}, Lcom/zendesk/sdk/support/SupportPresenter;->invokeRetryActions()V

    .line 129
    return-void
.end method

.method public onSearchSubmit(Ljava/lang/String;)V
    .locals 7
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    invoke-interface {v0}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->dismissError()V

    .line 163
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->showLoadingState()V

    .line 164
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->view:Lcom/zendesk/sdk/support/SupportMvp$View;

    invoke-interface {v0}, Lcom/zendesk/sdk/support/SupportMvp$View;->clearSearchResults()V

    .line 166
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->model:Lcom/zendesk/sdk/support/SupportMvp$Model;

    iget-object v1, p0, Lcom/zendesk/sdk/support/SupportPresenter;->config:Lcom/zendesk/sdk/support/SupportUiConfig;

    invoke-virtual {v1}, Lcom/zendesk/sdk/support/SupportUiConfig;->getCategoryIds()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/support/SupportPresenter;->config:Lcom/zendesk/sdk/support/SupportUiConfig;

    invoke-virtual {v2}, Lcom/zendesk/sdk/support/SupportUiConfig;->getSectionIds()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/zendesk/sdk/support/SupportPresenter;->config:Lcom/zendesk/sdk/support/SupportUiConfig;

    .line 167
    invoke-virtual {v3}, Lcom/zendesk/sdk/support/SupportUiConfig;->getLabelNames()[Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;

    invoke-direct {v5, p0, p1}, Lcom/zendesk/sdk/support/SupportPresenter$ViewSafeRetryZendeskCallback;-><init>(Lcom/zendesk/sdk/support/SupportPresenter;Ljava/lang/String;)V

    move-object v3, p1

    .line 166
    invoke-interface/range {v0 .. v5}, Lcom/zendesk/sdk/support/SupportMvp$Model;->search(Ljava/util/List;Ljava/util/List;Ljava/lang/String;[Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 177
    :goto_0
    return-void

    .line 169
    :cond_0
    new-instance v6, Lcom/zendesk/sdk/support/SupportPresenter$1;

    invoke-direct {v6, p0, p1}, Lcom/zendesk/sdk/support/SupportPresenter$1;-><init>(Lcom/zendesk/sdk/support/SupportPresenter;Ljava/lang/String;)V

    .line 175
    .local v6, "retryAction":Lcom/zendesk/sdk/network/RetryAction;
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->networkInfoProvider:Lcom/zendesk/sdk/network/NetworkInfoProvider;

    sget-object v1, Lcom/zendesk/sdk/support/SupportPresenter;->RETRY_ACTION_ID:Ljava/lang/Integer;

    invoke-interface {v0, v1, v6}, Lcom/zendesk/sdk/network/NetworkInfoProvider;->addRetryAction(Ljava/lang/Integer;Lcom/zendesk/sdk/network/RetryAction;)V

    goto :goto_0
.end method

.method public shouldShowConversationsMenuItem()Z
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->isConversationsEnabled()Z

    move-result v0

    return v0
.end method

.method public shouldShowSearchMenuItem()Z
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/zendesk/sdk/support/SupportPresenter;->mobileSettings:Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    invoke-virtual {v0}, Lcom/zendesk/sdk/model/settings/SafeMobileSettings;->hasHelpCenterSettings()Z

    move-result v0

    return v0
.end method
