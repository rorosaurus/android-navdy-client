.class Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder$1;
.super Ljava/lang/Object;
.source "HelpSearchRecyclerViewAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->bindTo(Lcom/zendesk/sdk/model/helpcenter/SearchArticle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;

.field final synthetic val$searchArticle:Lcom/zendesk/sdk/model/helpcenter/SearchArticle;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;Lcom/zendesk/sdk/model/helpcenter/SearchArticle;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;

    .prologue
    .line 180
    iput-object p1, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;

    iput-object p2, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder$1;->val$searchArticle:Lcom/zendesk/sdk/model/helpcenter/SearchArticle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 183
    iget-object v0, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder$1;->this$1:Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;

    iget-object v0, v0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/zendesk/sdk/support/help/HelpSearchRecyclerViewAdapter$HelpSearchViewHolder$1;->val$searchArticle:Lcom/zendesk/sdk/model/helpcenter/SearchArticle;

    invoke-virtual {v1}, Lcom/zendesk/sdk/model/helpcenter/SearchArticle;->getArticle()Lcom/zendesk/sdk/model/helpcenter/Article;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/zendesk/sdk/support/ViewArticleActivity;->startActivity(Landroid/content/Context;Lcom/zendesk/sdk/model/helpcenter/Article;)V

    .line 184
    return-void
.end method
