.class Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5$1;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskUserProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/UserResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/request/UserResponse;)V
    .locals 2
    .param p1, "userResponse"    # Lcom/zendesk/sdk/model/request/UserResponse;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/UserResponse;->getUser()Lcom/zendesk/sdk/model/request/User;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 123
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 117
    check-cast p1, Lcom/zendesk/sdk/model/request/UserResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskUserProvider$5$1;->onSuccess(Lcom/zendesk/sdk/model/request/UserResponse;)V

    return-void
.end method
