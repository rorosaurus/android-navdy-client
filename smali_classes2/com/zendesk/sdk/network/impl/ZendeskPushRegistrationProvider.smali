.class Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;
.super Ljava/lang/Object;
.source "ZendeskPushRegistrationProvider.java"

# interfaces
.implements Lcom/zendesk/sdk/network/PushRegistrationProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;
    }
.end annotation


# instance fields
.field private final baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

.field private final identity:Lcom/zendesk/sdk/model/access/Identity;

.field private final pushService:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/BaseProvider;Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;Lcom/zendesk/sdk/model/access/Identity;)V
    .locals 0
    .param p1, "baseProvider"    # Lcom/zendesk/sdk/network/BaseProvider;
    .param p2, "pushService"    # Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;
    .param p3, "identity"    # Lcom/zendesk/sdk/model/access/Identity;

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    .line 38
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->pushService:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;

    .line 39
    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->identity:Lcom/zendesk/sdk/model/access/Identity;

    .line 40
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;)Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->pushService:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;

    return-object v0
.end method

.method private decoratePushRegistrationRequest(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;Lcom/zendesk/sdk/model/push/PushRegistrationRequest;)Lcom/zendesk/sdk/model/push/PushRegistrationRequest;
    .locals 1
    .param p1, "identifier"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "locale"    # Ljava/util/Locale;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "tokenType"    # Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4    # Lcom/zendesk/sdk/model/push/PushRegistrationRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/zendesk/sdk/model/push/PushRegistrationRequest;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            "Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;",
            "TE;)TE;"
        }
    .end annotation

    .prologue
    .line 122
    .local p4, "request":Lcom/zendesk/sdk/model/push/PushRegistrationRequest;, "TE;"
    invoke-virtual {p4, p1}, Lcom/zendesk/sdk/model/push/PushRegistrationRequest;->setIdentifier(Ljava/lang/String;)V

    .line 123
    invoke-static {p2}, Lcom/zendesk/util/LocaleUtil;->toLanguageTag(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4, v0}, Lcom/zendesk/sdk/model/push/PushRegistrationRequest;->setLocale(Ljava/lang/String;)V

    .line 125
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->UrbanAirshipChannelId:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    if-ne p3, v0, :cond_0

    .line 126
    iget-object v0, p3, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->name:Ljava/lang/String;

    invoke-virtual {p4, v0}, Lcom/zendesk/sdk/model/push/PushRegistrationRequest;->setTokenType(Ljava/lang/String;)V

    .line 129
    :cond_0
    return-object p4
.end method


# virtual methods
.method getPushRegistrationRequest(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/sdk/model/access/AuthenticationType;Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;)Lcom/zendesk/sdk/model/push/PushRegistrationRequest;
    .locals 4
    .param p1, "identifier"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "locale"    # Ljava/util/Locale;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "authenticationType"    # Lcom/zendesk/sdk/model/access/AuthenticationType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "tokenType"    # Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 107
    sget-object v2, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$5;->$SwitchMap$com$zendesk$sdk$model$access$AuthenticationType:[I

    invoke-virtual {p3}, Lcom/zendesk/sdk/model/access/AuthenticationType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 118
    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    .line 109
    :pswitch_0
    new-instance v2, Lcom/zendesk/sdk/model/push/JwtPushRegistrationRequest;

    invoke-direct {v2}, Lcom/zendesk/sdk/model/push/JwtPushRegistrationRequest;-><init>()V

    invoke-direct {p0, p1, p2, p4, v2}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->decoratePushRegistrationRequest(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;Lcom/zendesk/sdk/model/push/PushRegistrationRequest;)Lcom/zendesk/sdk/model/push/PushRegistrationRequest;

    move-result-object v1

    goto :goto_0

    .line 111
    :pswitch_1
    new-instance v2, Lcom/zendesk/sdk/model/push/AnonymousPushRegistrationRequest;

    invoke-direct {v2}, Lcom/zendesk/sdk/model/push/AnonymousPushRegistrationRequest;-><init>()V

    invoke-direct {p0, p1, p2, p4, v2}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->decoratePushRegistrationRequest(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;Lcom/zendesk/sdk/model/push/PushRegistrationRequest;)Lcom/zendesk/sdk/model/push/PushRegistrationRequest;

    move-result-object v1

    check-cast v1, Lcom/zendesk/sdk/model/push/AnonymousPushRegistrationRequest;

    .line 112
    .local v1, "anonymousPushRegistrationRequest":Lcom/zendesk/sdk/model/push/AnonymousPushRegistrationRequest;
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->identity:Lcom/zendesk/sdk/model/access/Identity;

    check-cast v0, Lcom/zendesk/sdk/model/access/AnonymousIdentity;

    .line 113
    .local v0, "anonymousIdentity":Lcom/zendesk/sdk/model/access/AnonymousIdentity;
    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->identity:Lcom/zendesk/sdk/model/access/Identity;

    if-eqz v2, :cond_0

    .line 114
    invoke-virtual {v0}, Lcom/zendesk/sdk/model/access/AnonymousIdentity;->getSdkGuid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/zendesk/sdk/model/push/AnonymousPushRegistrationRequest;->setSdkGuid(Ljava/lang/String;)V

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method internalRegister(Ljava/lang/String;Lcom/zendesk/sdk/model/push/PushRegistrationRequest;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 3
    .param p1, "header"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "pushRegistrationRequest"    # Lcom/zendesk/sdk/model/push/PushRegistrationRequest;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/sdk/model/push/PushRegistrationRequest;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/push/PushRegistrationResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/push/PushRegistrationResponse;>;"
    new-instance v0, Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;

    invoke-direct {v0}, Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;-><init>()V

    .line 94
    .local v0, "pushRegistrationRequestWrapper":Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;
    invoke-virtual {v0, p2}, Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;->setPushRegistrationRequest(Lcom/zendesk/sdk/model/push/PushRegistrationRequest;)V

    .line 96
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->pushService:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;

    new-instance v2, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$4;

    invoke-direct {v2, p0, p3, p3}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$4;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual {v1, p1, v0, v2}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationService;->registerDevice(Ljava/lang/String;Lcom/zendesk/sdk/model/push/PushRegistrationRequestWrapper;Lcom/zendesk/service/ZendeskCallback;)V

    .line 104
    return-void
.end method

.method public registerDeviceWithIdentifier(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 7
    .param p1, "identifier"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "locale"    # Ljava/util/Locale;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/push/PushRegistrationResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/push/PushRegistrationResponse;>;"
    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Ljava/util/Locale;)V

    invoke-interface {v6, v0}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 60
    return-void
.end method

.method public registerDeviceWithUAChannelId(Ljava/lang/String;Ljava/util/Locale;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 7
    .param p1, "urbanAirshipChannelId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "locale"    # Ljava/util/Locale;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Locale;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/push/PushRegistrationResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p3, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/push/PushRegistrationResponse;>;"
    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$2;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p3

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$2;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Ljava/util/Locale;)V

    invoke-interface {v6, v0}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 80
    return-void
.end method

.method public unregisterDevice(Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 2
    .param p1, "identifier"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;->baseProvider:Lcom/zendesk/sdk/network/BaseProvider;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$3;

    invoke-direct {v1, p0, p2, p1, p2}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$3;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;Lcom/zendesk/service/ZendeskCallback;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-interface {v0, v1}, Lcom/zendesk/sdk/network/BaseProvider;->configureSdk(Lcom/zendesk/service/ZendeskCallback;)V

    .line 90
    return-void
.end method
