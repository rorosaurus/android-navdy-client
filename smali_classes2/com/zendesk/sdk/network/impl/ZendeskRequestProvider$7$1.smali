.class Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7$1;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskRequestProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/request/RequestResponse;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 0
    .param p1, "this$1"    # Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 292
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/request/RequestResponse;)V
    .locals 2
    .param p1, "result"    # Lcom/zendesk/sdk/model/request/RequestResponse;

    .prologue
    .line 295
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7$1;->this$1:Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7;

    iget-object v0, v0, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/request/RequestResponse;->getRequest()Lcom/zendesk/sdk/model/request/Request;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/zendesk/service/ZendeskCallback;->onSuccess(Ljava/lang/Object;)V

    .line 298
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 292
    check-cast p1, Lcom/zendesk/sdk/model/request/RequestResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskRequestProvider$7$1;->onSuccess(Lcom/zendesk/sdk/model/request/RequestResponse;)V

    return-void
.end method
