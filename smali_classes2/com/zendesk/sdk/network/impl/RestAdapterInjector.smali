.class public Lcom/zendesk/sdk/network/impl/RestAdapterInjector;
.super Ljava/lang/Object;
.source "RestAdapterInjector.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static injectCachedRestAdapter(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/Retrofit;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 16
    invoke-static {p0}, Lcom/zendesk/sdk/util/ModuleInjector;->injectCachedRestAdapterModule(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/zendesk/sdk/network/impl/RestAdapterModule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/zendesk/sdk/network/impl/RestAdapterModule;->getRetrofit()Lretrofit2/Retrofit;

    move-result-object v0

    return-object v0
.end method

.method private static injectGsonConverterFactory(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/converter/gson/GsonConverterFactory;
    .locals 1
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 28
    invoke-static {p0}, Lcom/zendesk/sdk/util/LibraryInjector;->injectCachedGson(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lcom/google/gson/Gson;

    move-result-object v0

    invoke-static {v0}, Lretrofit2/converter/gson/GsonConverterFactory;->create(Lcom/google/gson/Gson;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v0

    return-object v0
.end method

.method public static injectRestAdapter(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/Retrofit;
    .locals 2
    .param p0, "applicationScope"    # Lcom/zendesk/sdk/network/impl/ApplicationScope;

    .prologue
    .line 20
    new-instance v0, Lretrofit2/Retrofit$Builder;

    invoke-direct {v0}, Lretrofit2/Retrofit$Builder;-><init>()V

    .line 21
    invoke-static {p0}, Lcom/zendesk/sdk/util/BaseInjector;->injectUrl(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 22
    invoke-static {p0}, Lcom/zendesk/sdk/network/impl/RestAdapterInjector;->injectGsonConverterFactory(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 23
    invoke-static {p0}, Lcom/zendesk/sdk/util/LibraryInjector;->injectOkHttpClient(Lcom/zendesk/sdk/network/impl/ApplicationScope;)Lokhttp3/OkHttpClient;

    move-result-object v1

    invoke-virtual {v0, v1}, Lretrofit2/Retrofit$Builder;->client(Lokhttp3/OkHttpClient;)Lretrofit2/Retrofit$Builder;

    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v0

    return-object v0
.end method
