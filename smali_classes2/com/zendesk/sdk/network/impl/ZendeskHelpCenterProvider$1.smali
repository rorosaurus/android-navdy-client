.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;
.super Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;
.source "ZendeskHelpCenterProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->getHelp(Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback",
        "<",
        "Lcom/zendesk/sdk/model/SdkConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

.field final synthetic val$callback:Lcom/zendesk/service/ZendeskCallback;

.field final synthetic val$request:Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;
    .param p2, "callback"    # Lcom/zendesk/service/ZendeskCallback;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    iput-object p4, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->val$request:Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;

    invoke-direct {p0, p2}, Lcom/zendesk/sdk/network/impl/PassThroughErrorZendeskCallback;-><init>(Lcom/zendesk/service/ZendeskCallback;)V

    return-void
.end method


# virtual methods
.method public onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V
    .locals 10
    .param p1, "config"    # Lcom/zendesk/sdk/model/SdkConfiguration;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->sanityCheckHelpCenterSettings(Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    invoke-static {v0}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->access$000(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;)Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    move-result-object v0

    .line 87
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getBearerAuthorizationHeader()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;

    .line 88
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/SdkConfiguration;->getMobileSettings()Lcom/zendesk/sdk/model/settings/SafeMobileSettings;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider;->getBestLocale(Lcom/zendesk/sdk/model/settings/SafeMobileSettings;)Ljava/util/Locale;

    move-result-object v2

    iget-object v3, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->val$request:Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;

    .line 89
    invoke-virtual {v3}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->getCategoryIds()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->val$request:Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;

    .line 90
    invoke-virtual {v4}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->getSectionIds()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->val$request:Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;

    .line 91
    invoke-virtual {v5}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->getIncludes()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->val$request:Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;

    .line 92
    invoke-virtual {v6}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->getArticlesPerPageLimit()I

    move-result v6

    iget-object v7, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->val$request:Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;

    .line 93
    invoke-virtual {v7}, Lcom/zendesk/sdk/model/helpcenter/help/HelpRequest;->getLabelNames()[Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/zendesk/util/StringUtils;->toCsvString([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1$1;

    iget-object v9, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->val$callback:Lcom/zendesk/service/ZendeskCallback;

    invoke-direct {v8, p0, v9}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;Lcom/zendesk/service/ZendeskCallback;)V

    .line 86
    invoke-virtual/range {v0 .. v8}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->getHelp(Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V

    .line 103
    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 82
    check-cast p1, Lcom/zendesk/sdk/model/SdkConfiguration;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterProvider$1;->onSuccess(Lcom/zendesk/sdk/model/SdkConfiguration;)V

    return-void
.end method
