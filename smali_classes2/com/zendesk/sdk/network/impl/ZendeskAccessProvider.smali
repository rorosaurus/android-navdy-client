.class Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;
.super Ljava/lang/Object;
.source "ZendeskAccessProvider.java"

# interfaces
.implements Lcom/zendesk/sdk/network/AccessProvider;


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ZendeskAccessProvider"


# instance fields
.field private final accessService:Lcom/zendesk/sdk/network/impl/ZendeskAccessService;

.field private final identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;


# direct methods
.method public constructor <init>(Lcom/zendesk/sdk/storage/IdentityStorage;Lcom/zendesk/sdk/network/impl/ZendeskAccessService;)V
    .locals 0
    .param p1, "identityStorage"    # Lcom/zendesk/sdk/storage/IdentityStorage;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "accessService"    # Lcom/zendesk/sdk/network/impl/ZendeskAccessService;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    .line 27
    iput-object p2, p0, Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;->accessService:Lcom/zendesk/sdk/network/impl/ZendeskAccessService;

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;)Lcom/zendesk/sdk/storage/IdentityStorage;
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;->identityStorage:Lcom/zendesk/sdk/storage/IdentityStorage;

    return-object v0
.end method


# virtual methods
.method public getAndStoreAuthTokenViaAnonymous(Lcom/zendesk/sdk/model/access/AnonymousIdentity;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 3
    .param p1, "identity"    # Lcom/zendesk/sdk/model/access/AnonymousIdentity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/access/AnonymousIdentity;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/access/AccessToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/access/AccessToken;>;"
    const-string v0, "ZendeskAccessProvider"

    const-string v1, "Requesting an access token for anonymous identity."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 69
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;->accessService:Lcom/zendesk/sdk/network/impl/ZendeskAccessService;

    new-instance v1, Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider$2;

    invoke-direct {v1, p0, p2, p2}, Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider$2;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual {v0, p1, v1}, Lcom/zendesk/sdk/network/impl/ZendeskAccessService;->getAuthTokenViaMobileSDK(Lcom/zendesk/sdk/model/access/Identity;Lcom/zendesk/service/ZendeskCallback;)V

    .line 81
    return-void
.end method

.method public getAndStoreAuthTokenViaJwt(Lcom/zendesk/sdk/model/access/JwtIdentity;Lcom/zendesk/service/ZendeskCallback;)V
    .locals 5
    .param p1, "identity"    # Lcom/zendesk/sdk/model/access/JwtIdentity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/zendesk/service/ZendeskCallback;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/access/JwtIdentity;",
            "Lcom/zendesk/service/ZendeskCallback",
            "<",
            "Lcom/zendesk/sdk/model/access/AccessToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "callback":Lcom/zendesk/service/ZendeskCallback;, "Lcom/zendesk/service/ZendeskCallback<Lcom/zendesk/sdk/model/access/AccessToken;>;"
    const/4 v4, 0x0

    .line 32
    const-string v1, "ZendeskAccessProvider"

    const-string v2, "Requesting an access token for jwt identity."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    invoke-virtual {p1}, Lcom/zendesk/sdk/model/access/JwtIdentity;->getJwtUserIdentifier()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/zendesk/util/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 42
    const-string v0, "The jwt user identifier is null or empty. We cannot proceed to get an access token"

    .line 43
    .local v0, "error":Ljava/lang/String;
    const-string v1, "ZendeskAccessProvider"

    const-string v2, "The jwt user identifier is null or empty. We cannot proceed to get an access token"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    if-eqz p2, :cond_0

    .line 46
    new-instance v1, Lcom/zendesk/service/ErrorResponseAdapter;

    const-string v2, "The jwt user identifier is null or empty. We cannot proceed to get an access token"

    invoke-direct {v1, v2}, Lcom/zendesk/service/ErrorResponseAdapter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Lcom/zendesk/service/ZendeskCallback;->onError(Lcom/zendesk/service/ErrorResponse;)V

    .line 62
    .end local v0    # "error":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    iget-object v1, p0, Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;->accessService:Lcom/zendesk/sdk/network/impl/ZendeskAccessService;

    new-instance v2, Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider$1;

    invoke-direct {v2, p0, p2, p2}, Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider$1;-><init>(Lcom/zendesk/sdk/network/impl/ZendeskAccessProvider;Lcom/zendesk/service/ZendeskCallback;Lcom/zendesk/service/ZendeskCallback;)V

    invoke-virtual {v1, p1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskAccessService;->getAuthTokenViaJWT(Lcom/zendesk/sdk/model/access/Identity;Lcom/zendesk/service/ZendeskCallback;)V

    goto :goto_0
.end method
