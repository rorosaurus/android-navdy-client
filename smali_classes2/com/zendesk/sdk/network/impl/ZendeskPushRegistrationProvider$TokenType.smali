.class final enum Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;
.super Ljava/lang/Enum;
.source "ZendeskPushRegistrationProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "TokenType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

.field public static final enum Identifier:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

.field public static final enum UrbanAirshipChannelId:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;


# instance fields
.field final name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 133
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    const-string v1, "Identifier"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->Identifier:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    .line 134
    new-instance v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    const-string v1, "UrbanAirshipChannelId"

    const-string v2, "urban_airship_channel_id"

    invoke-direct {v0, v1, v4, v2}, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->UrbanAirshipChannelId:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    .line 132
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->Identifier:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->UrbanAirshipChannelId:Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->$VALUES:[Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 138
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 139
    iput-object p3, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->name:Ljava/lang/String;

    .line 140
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 132
    const-class v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->$VALUES:[Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    invoke-virtual {v0}, [Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;

    return-object v0
.end method


# virtual methods
.method getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskPushRegistrationProvider$TokenType;->name:Ljava/lang/String;

    return-object v0
.end method
