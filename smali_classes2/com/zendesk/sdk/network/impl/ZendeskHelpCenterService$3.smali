.class Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$3;
.super Ljava/lang/Object;
.source "ZendeskHelpCenterService.java"

# interfaces
.implements Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->getArticlesForSection(Ljava/lang/String;Ljava/lang/Long;Ljava/util/Locale;Ljava/lang/String;Lcom/zendesk/service/ZendeskCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/zendesk/service/RetrofitZendeskCallbackAdapter$RequestExtractor",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;",
        "Ljava/util/List",
        "<",
        "Lcom/zendesk/sdk/model/helpcenter/Article;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;


# direct methods
.method constructor <init>(Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$3;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic extract(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 116
    check-cast p1, Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;

    invoke-virtual {p0, p1}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$3;->extract(Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public extract(Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;)Ljava/util/List;
    .locals 3
    .param p1, "articlesListResponse"    # Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/zendesk/sdk/model/helpcenter/Article;",
            ">;"
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService$3;->this$0:Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;->getUsers()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p1}, Lcom/zendesk/sdk/model/helpcenter/ArticlesListResponse;->getArticles()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/zendesk/sdk/network/impl/ZendeskHelpCenterService;->matchArticlesWithUsers(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
