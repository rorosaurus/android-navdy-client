.class public Lcom/zendesk/sdk/util/UiUtils;
.super Ljava/lang/Object;
.source "UiUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/zendesk/sdk/util/UiUtils$ScreenSize;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "UiUtils"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    return-void
.end method

.method public static dismissKeyboard(Landroid/app/Activity;)V
    .locals 6
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 274
    if-nez p0, :cond_0

    .line 275
    const-string v3, "UiUtils"

    const-string v4, "Cannot dismiss the keyboard when fragment is detached or the activity is null."

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 294
    :goto_0
    return-void

    .line 279
    :cond_0
    const-string v3, "input_method"

    invoke-virtual {p0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 281
    .local v2, "systemService":Ljava/lang/Object;
    instance-of v3, v2, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v3, :cond_2

    move-object v1, v2

    .line 282
    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 284
    .local v1, "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    invoke-virtual {p0}, Landroid/app/Activity;->getCurrentFocus()Landroid/view/View;

    move-result-object v0

    .line 286
    .local v0, "currentFocus":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 287
    invoke-virtual {v0}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v1, v3, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    .line 289
    :cond_1
    const-string v3, "UiUtils"

    const-string v4, "Cannot hide soft input because window token could not be obtained"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 292
    .end local v0    # "currentFocus":Landroid/view/View;
    .end local v1    # "inputMethodManager":Landroid/view/inputmethod/InputMethodManager;
    :cond_2
    const-string v3, "UiUtils"

    const-string v4, "Cannot hide soft input because we could not get the InputMethodManager"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static dpToPixels(ILandroid/util/DisplayMetrics;)I
    .locals 2
    .param p0, "sizeInDp"    # I
    .param p1, "displayMetrics"    # Landroid/util/DisplayMetrics;

    .prologue
    .line 50
    const/4 v0, 0x1

    int-to-float v1, p0

    .line 51
    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    .line 50
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static getScreenSize(Landroid/content/Context;)Lcom/zendesk/sdk/util/UiUtils$ScreenSize;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 184
    .line 185
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v2, 0xf

    .line 187
    .local v0, "screeLayout":I
    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->UNKNOWN:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    .line 189
    .local v1, "screenSize":Lcom/zendesk/sdk/util/UiUtils$ScreenSize;
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 190
    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->SMALL:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    .line 201
    :cond_0
    :goto_0
    return-object v1

    .line 191
    :cond_1
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 192
    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->NORMAL:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    goto :goto_0

    .line 193
    :cond_2
    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    .line 194
    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->LARGE:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    goto :goto_0

    .line 195
    :cond_3
    const/4 v2, 0x4

    if-ne v0, v2, :cond_4

    .line 196
    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->X_LARGE:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    goto :goto_0

    .line 197
    :cond_4
    if-nez v0, :cond_0

    .line 198
    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->UNDEFINED:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    goto :goto_0
.end method

.method public static isTablet(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 217
    invoke-static {p0}, Lcom/zendesk/sdk/util/UiUtils;->getScreenSize(Landroid/content/Context;)Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    move-result-object v0

    .line 218
    .local v0, "screenSize":Lcom/zendesk/sdk/util/UiUtils$ScreenSize;
    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->LARGE:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/zendesk/sdk/util/UiUtils$ScreenSize;->X_LARGE:Lcom/zendesk/sdk/util/UiUtils$ScreenSize;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static resolveColor(ILandroid/content/Context;)I
    .locals 1
    .param p0, "colorId"    # I
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 167
    invoke-static {p1, p0}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public static setVisibility(Landroid/view/View;I)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "visibilityState"    # I

    .prologue
    .line 257
    if-nez p0, :cond_0

    .line 259
    const-string v0, "UiUtils"

    const-string v1, "View is is null and can\'t change visibility"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/zendesk/logger/Logger;->w(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 264
    :goto_0
    return-void

    .line 262
    :cond_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static sizeDialogWidthForTablets(Landroid/app/Dialog;F)V
    .locals 6
    .param p0, "dialog"    # Landroid/app/Dialog;
    .param p1, "percentageWidth"    # F

    .prologue
    .line 232
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/zendesk/sdk/util/UiUtils;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 233
    :cond_0
    const-string v3, "UiUtils"

    const-string v4, "Dialog is null or the device is not a tablet"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3, v4, v5}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 243
    :goto_0
    return-void

    .line 237
    :cond_1
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 239
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 240
    .local v1, "screenWidth":I
    int-to-float v3, v1

    mul-float v2, v3, p1

    .line 242
    .local v2, "widthPixels":F
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v4

    const/4 v5, -0x2

    invoke-virtual {v3, v4, v5}, Landroid/view/Window;->setLayout(II)V

    goto :goto_0
.end method

.method public static spToPixels(ILandroid/util/DisplayMetrics;)I
    .locals 2
    .param p0, "sizeInSp"    # I
    .param p1, "displayMetrics"    # Landroid/util/DisplayMetrics;

    .prologue
    .line 71
    const/4 v0, 0x2

    int-to-float v1, p0

    .line 72
    invoke-static {v0, v1, p1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    .line 71
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static themeAttributeToColor(ILandroid/content/Context;I)I
    .locals 9
    .param p0, "themeAttributeId"    # I
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fallbackColorId"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 141
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 142
    .local v1, "outValue":Landroid/util/TypedValue;
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 144
    .local v2, "theme":Landroid/content/res/Resources$Theme;
    invoke-virtual {v2, p0, v1, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    .line 146
    .local v3, "wasFound":Z
    if-nez v3, :cond_0

    .line 147
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Resource %d not found. Resource is either missing or you are using a non-ui context."

    new-array v6, v6, [Ljava/lang/Object;

    .line 151
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    .line 148
    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "errorMessage":Ljava/lang/String;
    const-string v4, "UiUtils"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v4, v0, v5}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 154
    invoke-static {p2, p1}, Lcom/zendesk/sdk/util/UiUtils;->resolveColor(ILandroid/content/Context;)I

    move-result v4

    .line 156
    .end local v0    # "errorMessage":Ljava/lang/String;
    :goto_0
    return v4

    :cond_0
    iget v4, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-static {v4, p1}, Lcom/zendesk/sdk/util/UiUtils;->resolveColor(ILandroid/content/Context;)I

    move-result v4

    goto :goto_0
.end method

.method public static themeAttributeToPixels(ILandroid/content/Context;IF)I
    .locals 9
    .param p0, "themeAttributeId"    # I
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fallbackUnitType"    # I
    .param p3, "fallbackUnitValue"    # F

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 101
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 103
    .local v1, "outValue":Landroid/util/TypedValue;
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    .line 105
    .local v2, "theme":Landroid/content/res/Resources$Theme;
    invoke-virtual {v2, p0, v1, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    .line 107
    .local v3, "wasFound":Z
    if-nez v3, :cond_0

    .line 108
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "Resource %d not found. Resource is either missing or you are using a non-ui context."

    new-array v6, v6, [Ljava/lang/Object;

    .line 112
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    .line 109
    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 114
    .local v0, "errorMessage":Ljava/lang/String;
    const-string v4, "UiUtils"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v4, v0, v5}, Lcom/zendesk/logger/Logger;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {p2, p3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 118
    .end local v0    # "errorMessage":Ljava/lang/String;
    :goto_0
    return v4

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/util/TypedValue;->getDimension(Landroid/util/DisplayMetrics;)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    goto :goto_0
.end method
