.class final enum Lcom/zendesk/logger/Logger$Priority;
.super Ljava/lang/Enum;
.source "Logger.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/zendesk/logger/Logger;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Priority"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/zendesk/logger/Logger$Priority;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/zendesk/logger/Logger$Priority;

.field public static final enum DEBUG:Lcom/zendesk/logger/Logger$Priority;

.field public static final enum ERROR:Lcom/zendesk/logger/Logger$Priority;

.field public static final enum INFO:Lcom/zendesk/logger/Logger$Priority;

.field public static final enum VERBOSE:Lcom/zendesk/logger/Logger$Priority;

.field public static final enum WARN:Lcom/zendesk/logger/Logger$Priority;


# instance fields
.field private final priority:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    .line 55
    new-instance v0, Lcom/zendesk/logger/Logger$Priority;

    const-string v1, "VERBOSE"

    invoke-direct {v0, v1, v6, v3}, Lcom/zendesk/logger/Logger$Priority;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/zendesk/logger/Logger$Priority;->VERBOSE:Lcom/zendesk/logger/Logger$Priority;

    .line 56
    new-instance v0, Lcom/zendesk/logger/Logger$Priority;

    const-string v1, "DEBUG"

    invoke-direct {v0, v1, v7, v4}, Lcom/zendesk/logger/Logger$Priority;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/zendesk/logger/Logger$Priority;->DEBUG:Lcom/zendesk/logger/Logger$Priority;

    .line 57
    new-instance v0, Lcom/zendesk/logger/Logger$Priority;

    const-string v1, "INFO"

    invoke-direct {v0, v1, v3, v5}, Lcom/zendesk/logger/Logger$Priority;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/zendesk/logger/Logger$Priority;->INFO:Lcom/zendesk/logger/Logger$Priority;

    .line 58
    new-instance v0, Lcom/zendesk/logger/Logger$Priority;

    const-string v1, "WARN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v4, v2}, Lcom/zendesk/logger/Logger$Priority;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/zendesk/logger/Logger$Priority;->WARN:Lcom/zendesk/logger/Logger$Priority;

    .line 59
    new-instance v0, Lcom/zendesk/logger/Logger$Priority;

    const-string v1, "ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v5, v2}, Lcom/zendesk/logger/Logger$Priority;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/zendesk/logger/Logger$Priority;->ERROR:Lcom/zendesk/logger/Logger$Priority;

    .line 54
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/zendesk/logger/Logger$Priority;

    sget-object v1, Lcom/zendesk/logger/Logger$Priority;->VERBOSE:Lcom/zendesk/logger/Logger$Priority;

    aput-object v1, v0, v6

    sget-object v1, Lcom/zendesk/logger/Logger$Priority;->DEBUG:Lcom/zendesk/logger/Logger$Priority;

    aput-object v1, v0, v7

    sget-object v1, Lcom/zendesk/logger/Logger$Priority;->INFO:Lcom/zendesk/logger/Logger$Priority;

    aput-object v1, v0, v3

    sget-object v1, Lcom/zendesk/logger/Logger$Priority;->WARN:Lcom/zendesk/logger/Logger$Priority;

    aput-object v1, v0, v4

    sget-object v1, Lcom/zendesk/logger/Logger$Priority;->ERROR:Lcom/zendesk/logger/Logger$Priority;

    aput-object v1, v0, v5

    sput-object v0, Lcom/zendesk/logger/Logger$Priority;->$VALUES:[Lcom/zendesk/logger/Logger$Priority;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 64
    iput p3, p0, Lcom/zendesk/logger/Logger$Priority;->priority:I

    .line 65
    return-void
.end method

.method static synthetic access$100(Lcom/zendesk/logger/Logger$Priority;)I
    .locals 1
    .param p0, "x0"    # Lcom/zendesk/logger/Logger$Priority;

    .prologue
    .line 54
    iget v0, p0, Lcom/zendesk/logger/Logger$Priority;->priority:I

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/zendesk/logger/Logger$Priority;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 54
    const-class v0, Lcom/zendesk/logger/Logger$Priority;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/zendesk/logger/Logger$Priority;

    return-object v0
.end method

.method public static values()[Lcom/zendesk/logger/Logger$Priority;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/zendesk/logger/Logger$Priority;->$VALUES:[Lcom/zendesk/logger/Logger$Priority;

    invoke-virtual {v0}, [Lcom/zendesk/logger/Logger$Priority;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/zendesk/logger/Logger$Priority;

    return-object v0
.end method
