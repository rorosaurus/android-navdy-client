.class public final Lcom/nimbusds/jose/jca/JCASupport;
.super Ljava/lang/Object;
.source "JCASupport.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 304
    return-void
.end method

.method public static isSupported(Lcom/nimbusds/jose/EncryptionMethod;)Z
    .locals 6
    .param p0, "enc"    # Lcom/nimbusds/jose/EncryptionMethod;

    .prologue
    const/4 v1, 0x0

    .line 239
    invoke-static {}, Ljava/security/Security;->getProviders()[Ljava/security/Provider;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    .line 246
    :goto_1
    return v1

    .line 239
    :cond_0
    aget-object v0, v3, v2

    .line 241
    .local v0, "p":Ljava/security/Provider;
    invoke-static {p0, v0}, Lcom/nimbusds/jose/jca/JCASupport;->isSupported(Lcom/nimbusds/jose/EncryptionMethod;Ljava/security/Provider;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 242
    const/4 v1, 0x1

    goto :goto_1

    .line 239
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static isSupported(Lcom/nimbusds/jose/EncryptionMethod;Ljava/security/Provider;)Z
    .locals 4
    .param p0, "enc"    # Lcom/nimbusds/jose/EncryptionMethod;
    .param p1, "provider"    # Ljava/security/Provider;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 262
    sget-object v3, Lcom/nimbusds/jose/EncryptionMethod$Family;->AES_CBC_HMAC_SHA:Lcom/nimbusds/jose/EncryptionMethod$Family;

    invoke-virtual {v3, p0}, Lcom/nimbusds/jose/EncryptionMethod$Family;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 265
    :try_start_0
    const-string v3, "AES/CBC/PKCS5Padding"

    invoke-static {v3, p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 273
    sget-object v3, Lcom/nimbusds/jose/EncryptionMethod;->A128CBC_HS256:Lcom/nimbusds/jose/EncryptionMethod;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/EncryptionMethod;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 274
    const-string v0, "HmacSHA256"

    .line 280
    .local v0, "hmac":Ljava/lang/String;
    :goto_0
    const-string v3, "KeyGenerator"

    invoke-virtual {p1, v3, v0}, Ljava/security/Provider;->getService(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Provider$Service;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 295
    .end local v0    # "hmac":Ljava/lang/String;
    :goto_1
    return v1

    .line 267
    :catch_0
    move-exception v1

    move v1, v2

    goto :goto_1

    .line 269
    :catch_1
    move-exception v1

    move v1, v2

    goto :goto_1

    .line 275
    :cond_0
    sget-object v3, Lcom/nimbusds/jose/EncryptionMethod;->A192CBC_HS384:Lcom/nimbusds/jose/EncryptionMethod;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/EncryptionMethod;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 276
    const-string v0, "HmacSHA384"

    .line 277
    .restart local v0    # "hmac":Ljava/lang/String;
    goto :goto_0

    .line 278
    .end local v0    # "hmac":Ljava/lang/String;
    :cond_1
    const-string v0, "HmacSHA512"

    .restart local v0    # "hmac":Ljava/lang/String;
    goto :goto_0

    :cond_2
    move v1, v2

    .line 280
    goto :goto_1

    .line 283
    .end local v0    # "hmac":Ljava/lang/String;
    :cond_3
    sget-object v3, Lcom/nimbusds/jose/EncryptionMethod$Family;->AES_GCM:Lcom/nimbusds/jose/EncryptionMethod$Family;

    invoke-virtual {v3, p0}, Lcom/nimbusds/jose/EncryptionMethod$Family;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 286
    :try_start_1
    const-string v3, "AES/GCM/NoPadding"

    invoke-static {v3, p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_1

    .line 288
    :catch_2
    move-exception v1

    move v1, v2

    goto :goto_1

    .line 290
    :catch_3
    move-exception v1

    move v1, v2

    goto :goto_1

    :cond_4
    move v1, v2

    .line 295
    goto :goto_1
.end method

.method public static isSupported(Lcom/nimbusds/jose/JWEAlgorithm;)Z
    .locals 6
    .param p0, "alg"    # Lcom/nimbusds/jose/JWEAlgorithm;

    .prologue
    const/4 v1, 0x0

    .line 141
    invoke-static {}, Ljava/security/Security;->getProviders()[Ljava/security/Provider;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    .line 148
    :goto_1
    return v1

    .line 141
    :cond_0
    aget-object v0, v3, v2

    .line 143
    .local v0, "p":Ljava/security/Provider;
    invoke-static {p0, v0}, Lcom/nimbusds/jose/jca/JCASupport;->isSupported(Lcom/nimbusds/jose/JWEAlgorithm;Ljava/security/Provider;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 144
    const/4 v1, 0x1

    goto :goto_1

    .line 141
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static isSupported(Lcom/nimbusds/jose/JWEAlgorithm;Ljava/security/Provider;)Z
    .locals 6
    .param p0, "alg"    # Lcom/nimbusds/jose/JWEAlgorithm;
    .param p1, "provider"    # Ljava/security/Provider;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 166
    sget-object v4, Lcom/nimbusds/jose/JWEAlgorithm$Family;->RSA:Lcom/nimbusds/jose/JWEAlgorithm$Family;

    invoke-virtual {v4, p0}, Lcom/nimbusds/jose/JWEAlgorithm$Family;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 167
    sget-object v4, Lcom/nimbusds/jose/JWEAlgorithm;->RSA1_5:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {p0, v4}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 168
    const-string v1, "RSA/ECB/PKCS1Padding"

    .line 179
    .local v1, "jcaName":Ljava/lang/String;
    :goto_0
    :try_start_0
    invoke-static {v1, p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    move v2, v3

    .line 224
    .end local v1    # "jcaName":Ljava/lang/String;
    :cond_0
    :goto_1
    return v2

    .line 169
    :cond_1
    sget-object v4, Lcom/nimbusds/jose/JWEAlgorithm;->RSA_OAEP:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {p0, v4}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 170
    const-string v1, "RSA/ECB/OAEPWithSHA-1AndMGF1Padding"

    .line 171
    .restart local v1    # "jcaName":Ljava/lang/String;
    goto :goto_0

    .end local v1    # "jcaName":Ljava/lang/String;
    :cond_2
    sget-object v4, Lcom/nimbusds/jose/JWEAlgorithm;->RSA_OAEP_256:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {p0, v4}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 172
    const-string v1, "RSA/ECB/OAEPWithSHA-256AndMGF1Padding"

    .line 173
    .restart local v1    # "jcaName":Ljava/lang/String;
    goto :goto_0

    .line 188
    .end local v1    # "jcaName":Ljava/lang/String;
    :cond_3
    sget-object v4, Lcom/nimbusds/jose/JWEAlgorithm$Family;->AES_KW:Lcom/nimbusds/jose/JWEAlgorithm$Family;

    invoke-virtual {v4, p0}, Lcom/nimbusds/jose/JWEAlgorithm$Family;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 189
    const-string v4, "Cipher"

    const-string v5, "AESWrap"

    invoke-virtual {p1, v4, v5}, Ljava/security/Provider;->getService(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Provider$Service;

    move-result-object v4

    if-eqz v4, :cond_0

    move v2, v3

    goto :goto_1

    .line 192
    :cond_4
    sget-object v4, Lcom/nimbusds/jose/JWEAlgorithm$Family;->ECDH_ES:Lcom/nimbusds/jose/JWEAlgorithm$Family;

    invoke-virtual {v4, p0}, Lcom/nimbusds/jose/JWEAlgorithm$Family;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 193
    const-string v4, "KeyAgreement"

    const-string v5, "ECDH"

    invoke-virtual {p1, v4, v5}, Ljava/security/Provider;->getService(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Provider$Service;

    move-result-object v4

    if-eqz v4, :cond_0

    move v2, v3

    goto :goto_1

    .line 196
    :cond_5
    sget-object v4, Lcom/nimbusds/jose/JWEAlgorithm$Family;->AES_GCM_KW:Lcom/nimbusds/jose/JWEAlgorithm$Family;

    invoke-virtual {v4, p0}, Lcom/nimbusds/jose/JWEAlgorithm$Family;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 199
    :try_start_1
    const-string v4, "AES/GCM/NoPadding"

    invoke-static {v4, p1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/crypto/Cipher;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_2

    move v2, v3

    .line 205
    goto :goto_1

    .line 208
    :cond_6
    sget-object v4, Lcom/nimbusds/jose/JWEAlgorithm$Family;->PBES2:Lcom/nimbusds/jose/JWEAlgorithm$Family;

    invoke-virtual {v4, p0}, Lcom/nimbusds/jose/JWEAlgorithm$Family;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 210
    sget-object v4, Lcom/nimbusds/jose/JWEAlgorithm;->PBES2_HS256_A128KW:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {p0, v4}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 211
    const-string v0, "HmacSHA256"

    .line 217
    .local v0, "hmac":Ljava/lang/String;
    :goto_2
    const-string v4, "KeyGenerator"

    invoke-virtual {p1, v4, v0}, Ljava/security/Provider;->getService(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Provider$Service;

    move-result-object v4

    if-eqz v4, :cond_0

    move v2, v3

    goto :goto_1

    .line 212
    .end local v0    # "hmac":Ljava/lang/String;
    :cond_7
    sget-object v4, Lcom/nimbusds/jose/JWEAlgorithm;->PBES2_HS384_A192KW:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {p0, v4}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 213
    const-string v0, "HmacSHA384"

    .line 214
    .restart local v0    # "hmac":Ljava/lang/String;
    goto :goto_2

    .line 215
    .end local v0    # "hmac":Ljava/lang/String;
    :cond_8
    const-string v0, "HmacSHA512"

    .restart local v0    # "hmac":Ljava/lang/String;
    goto :goto_2

    .line 220
    .end local v0    # "hmac":Ljava/lang/String;
    :cond_9
    sget-object v4, Lcom/nimbusds/jose/JWEAlgorithm;->DIR:Lcom/nimbusds/jose/JWEAlgorithm;

    invoke-virtual {v4, p0}, Lcom/nimbusds/jose/JWEAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    .line 221
    goto/16 :goto_1

    .line 181
    .restart local v1    # "jcaName":Ljava/lang/String;
    :catch_0
    move-exception v3

    goto/16 :goto_1

    .line 183
    :catch_1
    move-exception v3

    goto/16 :goto_1

    .line 203
    .end local v1    # "jcaName":Ljava/lang/String;
    :catch_2
    move-exception v3

    goto/16 :goto_1

    .line 201
    :catch_3
    move-exception v3

    goto/16 :goto_1
.end method

.method public static isSupported(Lcom/nimbusds/jose/JWSAlgorithm;)Z
    .locals 6
    .param p0, "alg"    # Lcom/nimbusds/jose/JWSAlgorithm;

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-static {}, Ljava/security/Security;->getProviders()[Ljava/security/Provider;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    .line 62
    :goto_1
    return v1

    .line 55
    :cond_0
    aget-object v0, v3, v2

    .line 57
    .local v0, "p":Ljava/security/Provider;
    invoke-static {p0, v0}, Lcom/nimbusds/jose/jca/JCASupport;->isSupported(Lcom/nimbusds/jose/JWSAlgorithm;Ljava/security/Provider;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 58
    const/4 v1, 0x1

    goto :goto_1

    .line 55
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static isSupported(Lcom/nimbusds/jose/JWSAlgorithm;Ljava/security/Provider;)Z
    .locals 4
    .param p0, "alg"    # Lcom/nimbusds/jose/JWSAlgorithm;
    .param p1, "provider"    # Ljava/security/Provider;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 78
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm$Family;->HMAC_SHA:Lcom/nimbusds/jose/JWSAlgorithm$Family;

    invoke-virtual {v3, p0}, Lcom/nimbusds/jose/JWSAlgorithm$Family;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 80
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->HS256:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 81
    const-string v0, "HMACSHA256"

    .line 89
    .local v0, "jcaName":Ljava/lang/String;
    :goto_0
    const-string v3, "KeyGenerator"

    invoke-virtual {p1, v3, v0}, Ljava/security/Provider;->getService(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Provider$Service;

    move-result-object v3

    if-eqz v3, :cond_0

    move v1, v2

    .line 126
    .end local v0    # "jcaName":Ljava/lang/String;
    :cond_0
    :goto_1
    return v1

    .line 82
    :cond_1
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->HS384:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 83
    const-string v0, "HMACSHA384"

    .line 84
    .restart local v0    # "jcaName":Ljava/lang/String;
    goto :goto_0

    .end local v0    # "jcaName":Ljava/lang/String;
    :cond_2
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->HS512:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 85
    const-string v0, "HMACSHA512"

    .line 86
    .restart local v0    # "jcaName":Ljava/lang/String;
    goto :goto_0

    .line 92
    .end local v0    # "jcaName":Ljava/lang/String;
    :cond_3
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm$Family;->RSA:Lcom/nimbusds/jose/JWSAlgorithm$Family;

    invoke-virtual {v3, p0}, Lcom/nimbusds/jose/JWSAlgorithm$Family;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 94
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->RS256:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 95
    const-string v0, "SHA256withRSA"

    .line 109
    .restart local v0    # "jcaName":Ljava/lang/String;
    :goto_2
    const-string v3, "Signature"

    invoke-virtual {p1, v3, v0}, Ljava/security/Provider;->getService(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Provider$Service;

    move-result-object v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_1

    .line 96
    .end local v0    # "jcaName":Ljava/lang/String;
    :cond_4
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->RS384:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 97
    const-string v0, "SHA384withRSA"

    .line 98
    .restart local v0    # "jcaName":Ljava/lang/String;
    goto :goto_2

    .end local v0    # "jcaName":Ljava/lang/String;
    :cond_5
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->RS512:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 99
    const-string v0, "SHA512withRSA"

    .line 100
    .restart local v0    # "jcaName":Ljava/lang/String;
    goto :goto_2

    .end local v0    # "jcaName":Ljava/lang/String;
    :cond_6
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->PS256:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 101
    const-string v0, "SHA256withRSAandMGF1"

    .line 102
    .restart local v0    # "jcaName":Ljava/lang/String;
    goto :goto_2

    .end local v0    # "jcaName":Ljava/lang/String;
    :cond_7
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->PS384:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 103
    const-string v0, "SHA384withRSAandMGF1"

    .line 104
    .restart local v0    # "jcaName":Ljava/lang/String;
    goto :goto_2

    .end local v0    # "jcaName":Ljava/lang/String;
    :cond_8
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->PS512:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 105
    const-string v0, "SHA512withRSAandMGF1"

    .line 106
    .restart local v0    # "jcaName":Ljava/lang/String;
    goto :goto_2

    .line 112
    .end local v0    # "jcaName":Ljava/lang/String;
    :cond_9
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm$Family;->EC:Lcom/nimbusds/jose/JWSAlgorithm$Family;

    invoke-virtual {v3, p0}, Lcom/nimbusds/jose/JWSAlgorithm$Family;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->ES256:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 115
    const-string v0, "SHA256withECDSA"

    .line 123
    .restart local v0    # "jcaName":Ljava/lang/String;
    :goto_3
    const-string v3, "Signature"

    invoke-virtual {p1, v3, v0}, Ljava/security/Provider;->getService(Ljava/lang/String;Ljava/lang/String;)Ljava/security/Provider$Service;

    move-result-object v3

    if-eqz v3, :cond_0

    move v1, v2

    goto/16 :goto_1

    .line 116
    .end local v0    # "jcaName":Ljava/lang/String;
    :cond_a
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->ES384:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 117
    const-string v0, "SHA384withECDSA"

    .line 118
    .restart local v0    # "jcaName":Ljava/lang/String;
    goto :goto_3

    .end local v0    # "jcaName":Ljava/lang/String;
    :cond_b
    sget-object v3, Lcom/nimbusds/jose/JWSAlgorithm;->ES512:Lcom/nimbusds/jose/JWSAlgorithm;

    invoke-virtual {p0, v3}, Lcom/nimbusds/jose/JWSAlgorithm;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 119
    const-string v0, "SHA512withECDSA"

    .line 120
    .restart local v0    # "jcaName":Ljava/lang/String;
    goto :goto_3
.end method

.method public static isUnlimitedStrength()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 37
    :try_start_0
    const-string v1, "AES"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getMaxAllowedKeyLength(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const/16 v2, 0x100

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    .line 39
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method
