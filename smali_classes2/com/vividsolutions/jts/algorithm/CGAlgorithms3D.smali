.class public Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;
.super Ljava/lang/Object;
.source "CGAlgorithms3D.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static distance(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 10
    .param p0, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 51
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    move-result v6

    if-nez v6, :cond_0

    iget-wide v6, p1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 52
    :cond_0
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/Coordinate;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v6

    .line 57
    :goto_0
    return-wide v6

    .line 54
    :cond_1
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double v0, v6, v8

    .line 55
    .local v0, "dx":D
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double v2, v6, v8

    .line 56
    .local v2, "dy":D
    iget-wide v6, p0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    iget-wide v8, p1, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    sub-double v4, v6, v8

    .line 57
    .local v4, "dz":D
    mul-double v6, v0, v0

    mul-double v8, v2, v2

    add-double/2addr v6, v8

    mul-double v8, v4, v4

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    goto :goto_0
.end method

.method public static distancePointSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 26
    .param p0, "p"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "A"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "B"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 63
    invoke-virtual/range {p1 .. p2}, Lcom/vividsolutions/jts/geom/Coordinate;->equals3D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 64
    invoke-static/range {p0 .. p1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distance(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v18

    .line 99
    :goto_0
    return-wide v18

    .line 80
    :cond_0
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    mul-double v18, v18, v20

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    sub-double v22, v22, v24

    mul-double v20, v20, v22

    add-double v18, v18, v20

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v24, v0

    sub-double v22, v22, v24

    mul-double v20, v20, v22

    add-double v8, v18, v20

    .line 81
    .local v8, "len2":D
    invoke-static {v8, v9}, Ljava/lang/Double;->isNaN(D)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 82
    new-instance v18, Ljava/lang/IllegalArgumentException;

    const-string v19, "Ordinates must not be NaN"

    invoke-direct/range {v18 .. v19}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v18

    .line 83
    :cond_1
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    sub-double v18, v18, v20

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    mul-double v18, v18, v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v24, v0

    sub-double v22, v22, v24

    mul-double v20, v20, v22

    add-double v18, v18, v20

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v24, v0

    sub-double v22, v22, v24

    mul-double v20, v20, v22

    add-double v18, v18, v20

    div-double v16, v18, v8

    .line 86
    .local v16, "r":D
    const-wide/16 v18, 0x0

    cmpg-double v18, v16, v18

    if-gtz v18, :cond_2

    .line 87
    invoke-static/range {p0 .. p1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distance(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v18

    goto/16 :goto_0

    .line 88
    :cond_2
    const-wide/high16 v18, 0x3ff0000000000000L    # 1.0

    cmpl-double v18, v16, v18

    if-ltz v18, :cond_3

    .line 89
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distance(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v18

    goto/16 :goto_0

    .line 92
    :cond_3
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    mul-double v20, v20, v16

    add-double v10, v18, v20

    .line 93
    .local v10, "qx":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    mul-double v20, v20, v16

    add-double v12, v18, v20

    .line 94
    .local v12, "qy":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v22, v0

    sub-double v20, v20, v22

    mul-double v20, v20, v16

    add-double v14, v18, v20

    .line 96
    .local v14, "qz":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v18, v0

    sub-double v2, v18, v10

    .line 97
    .local v2, "dx":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v18, v0

    sub-double v4, v18, v12

    .line 98
    .local v4, "dy":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v18, v0

    sub-double v6, v18, v14

    .line 99
    .local v6, "dz":D
    mul-double v18, v2, v2

    mul-double v20, v4, v4

    add-double v18, v18, v20

    mul-double v20, v6, v6

    add-double v18, v18, v20

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v18

    goto/16 :goto_0
.end method

.method public static distanceSegmentSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D
    .locals 38
    .param p0, "A"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p1, "B"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "C"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "D"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 120
    invoke-virtual/range {p0 .. p1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals3D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 121
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distancePointSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    .line 178
    :goto_0
    return-wide v4

    .line 122
    :cond_0
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals3D(Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 123
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distancePointSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    goto :goto_0

    .line 128
    :cond_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/math/Vector3D;->dot(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v20

    .line 129
    .local v20, "a":D
    invoke-static/range {p0 .. p3}, Lcom/vividsolutions/jts/math/Vector3D;->dot(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v22

    .line 130
    .local v22, "b":D
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/math/Vector3D;->dot(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v24

    .line 131
    .local v24, "c":D
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p0

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/math/Vector3D;->dot(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v26

    .line 132
    .local v26, "d":D
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p2

    move-object/from16 v3, p0

    invoke-static {v0, v1, v2, v3}, Lcom/vividsolutions/jts/math/Vector3D;->dot(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v30

    .line 134
    .local v30, "e":D
    mul-double v4, v20, v24

    mul-double v12, v22, v22

    sub-double v28, v4, v12

    .line 135
    .local v28, "denom":D
    invoke-static/range {v28 .. v29}, Ljava/lang/Double;->isNaN(D)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 136
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Ordinates must not be NaN"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 140
    :cond_2
    const-wide/16 v4, 0x0

    cmpg-double v4, v28, v4

    if-gtz v4, :cond_4

    .line 145
    const-wide/16 v32, 0x0

    .line 147
    .local v32, "s":D
    cmpl-double v4, v22, v24

    if-lez v4, :cond_3

    .line 148
    div-double v34, v26, v22

    .line 156
    .local v34, "t":D
    :goto_1
    const-wide/16 v4, 0x0

    cmpg-double v4, v32, v4

    if-gez v4, :cond_5

    .line 157
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distancePointSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    goto :goto_0

    .line 150
    .end local v34    # "t":D
    :cond_3
    div-double v34, v30, v24

    .restart local v34    # "t":D
    goto :goto_1

    .line 153
    .end local v32    # "s":D
    .end local v34    # "t":D
    :cond_4
    mul-double v4, v22, v30

    mul-double v12, v24, v26

    sub-double/2addr v4, v12

    div-double v32, v4, v28

    .line 154
    .restart local v32    # "s":D
    mul-double v4, v20, v30

    mul-double v12, v22, v26

    sub-double/2addr v4, v12

    div-double v34, v4, v28

    .restart local v34    # "t":D
    goto :goto_1

    .line 158
    :cond_5
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v32, v4

    if-lez v4, :cond_6

    .line 159
    invoke-static/range {p1 .. p3}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distancePointSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    goto/16 :goto_0

    .line 160
    :cond_6
    const-wide/16 v4, 0x0

    cmpg-double v4, v34, v4

    if-gez v4, :cond_7

    .line 161
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distancePointSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    goto/16 :goto_0

    .line 162
    :cond_7
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v34, v4

    if-lez v4, :cond_8

    .line 163
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distancePointSegment(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    goto/16 :goto_0

    .line 169
    :cond_8
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v36, v0

    sub-double v12, v12, v36

    mul-double v12, v12, v32

    add-double v6, v4, v12

    .line 170
    .local v6, "x1":D
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v36, v0

    sub-double v12, v12, v36

    mul-double v12, v12, v32

    add-double v8, v4, v12

    .line 171
    .local v8, "y1":D
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v36, v0

    sub-double v12, v12, v36

    mul-double v12, v12, v32

    add-double v10, v4, v12

    .line 173
    .local v10, "z1":D
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p3

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    move-wide/from16 v36, v0

    sub-double v12, v12, v36

    mul-double v12, v12, v34

    add-double v14, v4, v12

    .line 174
    .local v14, "x2":D
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p3

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    move-wide/from16 v36, v0

    sub-double v12, v12, v36

    mul-double v12, v12, v34

    add-double v16, v4, v12

    .line 175
    .local v16, "y2":D
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-object/from16 v0, p3

    iget-wide v12, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    move-wide/from16 v36, v0

    sub-double v12, v12, v36

    mul-double v12, v12, v34

    add-double v18, v4, v12

    .line 178
    .local v18, "z2":D
    new-instance v5, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct/range {v5 .. v11}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    new-instance v13, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct/range {v13 .. v19}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    invoke-static {v5, v13}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms3D;->distance(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v4

    goto/16 :goto_0
.end method
