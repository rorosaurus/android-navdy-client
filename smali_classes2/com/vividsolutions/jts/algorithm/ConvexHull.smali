.class public Lcom/vividsolutions/jts/algorithm/ConvexHull;
.super Ljava/lang/Object;
.source "ConvexHull.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/algorithm/ConvexHull$RadialComparator;
    }
.end annotation


# instance fields
.field private geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private inputPts:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 2
    .param p1, "geometry"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 61
    invoke-static {p1}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->extractCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/vividsolutions/jts/algorithm/ConvexHull;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 62
    return-void
.end method

.method public constructor <init>([Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 0
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "geomFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->inputPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 69
    iput-object p2, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 70
    return-void
.end method

.method private cleanRing([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 8
    .param p1, "original"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 413
    const/4 v6, 0x0

    aget-object v6, p1, v6

    array-length v7, p1

    add-int/lit8 v7, v7, -0x1

    aget-object v7, p1, v7

    invoke-static {v6, v7}, Lcom/vividsolutions/jts/util/Assert;->equals(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 414
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 415
    .local v0, "cleanedRing":Ljava/util/ArrayList;
    const/4 v5, 0x0

    .line 416
    .local v5, "previousDistinctCoordinate":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    add-int/lit8 v6, v6, -0x2

    if-gt v3, v6, :cond_3

    .line 417
    aget-object v2, p1, v3

    .line 418
    .local v2, "currentCoordinate":Lcom/vividsolutions/jts/geom/Coordinate;
    add-int/lit8 v6, v3, 0x1

    aget-object v4, p1, v6

    .line 419
    .local v4, "nextCoordinate":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v2, v4}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 416
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 422
    :cond_1
    if-eqz v5, :cond_2

    invoke-direct {p0, v5, v2, v4}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->isBetween(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 426
    :cond_2
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    move-object v5, v2

    goto :goto_1

    .line 429
    .end local v2    # "currentCoordinate":Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v4    # "nextCoordinate":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_3
    array-length v6, p1

    add-int/lit8 v6, v6, -0x1

    aget-object v6, p1, v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 430
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v1, v6, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 431
    .local v1, "cleanedRingCoordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lcom/vividsolutions/jts/geom/Coordinate;

    check-cast v6, [Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v6
.end method

.method private computeOctPts([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 10
    .param p1, "inputPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 293
    const/16 v3, 0x8

    new-array v2, v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 294
    .local v2, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 295
    const/4 v3, 0x0

    aget-object v3, p1, v3

    aput-object v3, v2, v1

    .line 294
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 297
    :cond_0
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    array-length v3, p1

    if-ge v0, v3, :cond_9

    .line 298
    aget-object v3, p1, v0

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    const/4 v3, 0x0

    aget-object v3, v2, v3

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1

    .line 299
    const/4 v3, 0x0

    aget-object v4, p1, v0

    aput-object v4, v2, v3

    .line 301
    :cond_1
    aget-object v3, p1, v0

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    aget-object v3, p1, v0

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v4, v6

    const/4 v3, 0x1

    aget-object v3, v2, v3

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    const/4 v3, 0x1

    aget-object v3, v2, v3

    iget-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v6, v8

    cmpg-double v3, v4, v6

    if-gez v3, :cond_2

    .line 302
    const/4 v3, 0x1

    aget-object v4, p1, v0

    aput-object v4, v2, v3

    .line 304
    :cond_2
    aget-object v3, p1, v0

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    const/4 v3, 0x2

    aget-object v3, v2, v3

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpl-double v3, v4, v6

    if-lez v3, :cond_3

    .line 305
    const/4 v3, 0x2

    aget-object v4, p1, v0

    aput-object v4, v2, v3

    .line 307
    :cond_3
    aget-object v3, p1, v0

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    aget-object v3, p1, v0

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v4, v6

    const/4 v3, 0x3

    aget-object v3, v2, v3

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    const/4 v3, 0x3

    aget-object v3, v2, v3

    iget-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v6, v8

    cmpl-double v3, v4, v6

    if-lez v3, :cond_4

    .line 308
    const/4 v3, 0x3

    aget-object v4, p1, v0

    aput-object v4, v2, v3

    .line 310
    :cond_4
    aget-object v3, p1, v0

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    const/4 v3, 0x4

    aget-object v3, v2, v3

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpl-double v3, v4, v6

    if-lez v3, :cond_5

    .line 311
    const/4 v3, 0x4

    aget-object v4, p1, v0

    aput-object v4, v2, v3

    .line 313
    :cond_5
    aget-object v3, p1, v0

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    aget-object v3, p1, v0

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v4, v6

    const/4 v3, 0x5

    aget-object v3, v2, v3

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    const/4 v3, 0x5

    aget-object v3, v2, v3

    iget-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v6, v8

    cmpl-double v3, v4, v6

    if-lez v3, :cond_6

    .line 314
    const/4 v3, 0x5

    aget-object v4, p1, v0

    aput-object v4, v2, v3

    .line 316
    :cond_6
    aget-object v3, p1, v0

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    const/4 v3, 0x6

    aget-object v3, v2, v3

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpg-double v3, v4, v6

    if-gez v3, :cond_7

    .line 317
    const/4 v3, 0x6

    aget-object v4, p1, v0

    aput-object v4, v2, v3

    .line 319
    :cond_7
    aget-object v3, p1, v0

    iget-wide v4, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    aget-object v3, p1, v0

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v4, v6

    const/4 v3, 0x7

    aget-object v3, v2, v3

    iget-wide v6, v3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    const/4 v3, 0x7

    aget-object v3, v2, v3

    iget-wide v8, v3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v6, v8

    cmpg-double v3, v4, v6

    if-gez v3, :cond_8

    .line 320
    const/4 v3, 0x7

    aget-object v4, p1, v0

    aput-object v4, v2, v3

    .line 297
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 323
    :cond_9
    return-object v2
.end method

.method private computeOctRing([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4
    .param p1, "inputPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 279
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->computeOctPts([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 280
    .local v1, "octPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v0, Lcom/vividsolutions/jts/geom/CoordinateList;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    .line 281
    .local v0, "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/vividsolutions/jts/geom/CoordinateList;->add([Lcom/vividsolutions/jts/geom/Coordinate;Z)Z

    .line 284
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;->size()I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_0

    .line 285
    const/4 v2, 0x0

    .line 288
    :goto_0
    return-object v2

    .line 287
    :cond_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;->closeRing()V

    .line 288
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    goto :goto_0
.end method

.method private static extractCoordinates(Lcom/vividsolutions/jts/geom/Geometry;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 74
    new-instance v0, Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;

    invoke-direct {v0}, Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;-><init>()V

    .line 75
    .local v0, "filter":Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/CoordinateFilter;)V

    .line 76
    invoke-virtual {v0}, Lcom/vividsolutions/jts/util/UniqueCoordinateArrayFilter;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    return-object v1
.end method

.method private grahamScan([Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/util/Stack;
    .locals 6
    .param p1, "c"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v5, 0x0

    .line 232
    new-instance v2, Ljava/util/Stack;

    invoke-direct {v2}, Ljava/util/Stack;-><init>()V

    .line 233
    .local v2, "ps":Ljava/util/Stack;
    aget-object v3, p1, v5

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 234
    .local v1, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x1

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    check-cast v1, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 235
    .restart local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v3, 0x2

    aget-object v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    check-cast v1, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 236
    .restart local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x3

    .local v0, "i":I
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_1

    .line 237
    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    check-cast v1, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 241
    .restart local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_1
    invoke-virtual {v2}, Ljava/util/Stack;->empty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v4, p1, v0

    invoke-static {v3, v1, v4}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->computeOrientation(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v3

    if-lez v3, :cond_0

    .line 242
    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    check-cast v1, Lcom/vividsolutions/jts/geom/Coordinate;

    .restart local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    goto :goto_1

    .line 244
    :cond_0
    invoke-virtual {v2, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    check-cast v1, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 245
    .restart local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    check-cast v1, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 236
    .restart local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    :cond_1
    aget-object v3, p1, v5

    invoke-virtual {v2, v3}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    check-cast v1, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 248
    .restart local v1    # "p":Lcom/vividsolutions/jts/geom/Coordinate;
    return-object v2
.end method

.method private isBetween(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 6
    .param p1, "c1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "c2"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "c3"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 256
    invoke-static {p1, p2, p3}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->computeOrientation(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 275
    :cond_0
    :goto_0
    return v0

    .line 259
    :cond_1
    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_3

    .line 260
    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_2

    iget-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_2

    move v0, v1

    .line 261
    goto :goto_0

    .line 263
    :cond_2
    iget-wide v2, p3, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_3

    iget-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_3

    move v0, v1

    .line 264
    goto :goto_0

    .line 267
    :cond_3
    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_0

    .line 268
    iget-wide v2, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_4

    iget-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_4

    move v0, v1

    .line 269
    goto :goto_0

    .line 271
    :cond_4
    iget-wide v2, p3, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_0

    iget-wide v2, p2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_0

    move v0, v1

    .line 272
    goto :goto_0
.end method

.method private lineOrPolygon([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 6
    .param p1, "coordinates"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 396
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->cleanRing([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object p1

    .line 397
    array-length v1, p1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 398
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v3, p1, v4

    aput-object v3, v2, v4

    aget-object v3, p1, v5

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    .line 403
    :goto_0
    return-object v1

    .line 402
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v0

    .line 403
    .local v0, "linearRing":Lcom/vividsolutions/jts/geom/LinearRing;
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v1

    goto :goto_0
.end method

.method private padArray3([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 3
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 192
    const/4 v2, 0x3

    new-array v1, v2, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 193
    .local v1, "pad":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 194
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 195
    aget-object v2, p1, v0

    aput-object v2, v1, v0

    .line 193
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    :cond_0
    const/4 v2, 0x0

    aget-object v2, p1, v2

    aput-object v2, v1, v0

    goto :goto_1

    .line 200
    :cond_1
    return-object v1
.end method

.method private preSort([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 7
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    const/4 v6, 0x0

    .line 209
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 210
    aget-object v2, p1, v0

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    aget-object v4, p1, v6

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpg-double v2, v2, v4

    if-ltz v2, :cond_0

    aget-object v2, p1, v0

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    aget-object v4, p1, v6

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_1

    aget-object v2, p1, v0

    iget-wide v2, v2, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    aget-object v4, p1, v6

    iget-wide v4, v4, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    cmpg-double v2, v2, v4

    if-gez v2, :cond_1

    .line 211
    :cond_0
    aget-object v1, p1, v6

    .line 212
    .local v1, "t":Lcom/vividsolutions/jts/geom/Coordinate;
    aget-object v2, p1, v0

    aput-object v2, p1, v6

    .line 213
    aput-object v1, p1, v0

    .line 209
    .end local v1    # "t":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218
    :cond_2
    const/4 v2, 0x1

    array-length v3, p1

    new-instance v4, Lcom/vividsolutions/jts/algorithm/ConvexHull$RadialComparator;

    aget-object v5, p1, v6

    invoke-direct {v4, v5}, Lcom/vividsolutions/jts/algorithm/ConvexHull$RadialComparator;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-static {p1, v2, v3, v4}, Ljava/util/Arrays;->sort([Ljava/lang/Object;IILjava/util/Comparator;)V

    .line 221
    return-object p1
.end method

.method private reduce([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6
    .param p1, "inputPts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 156
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->computeOctRing([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 160
    .local v1, "polyPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    if-nez v1, :cond_0

    .line 187
    .end local p1    # "inputPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :goto_0
    return-object p1

    .line 167
    .restart local p1    # "inputPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    new-instance v3, Ljava/util/TreeSet;

    invoke-direct {v3}, Ljava/util/TreeSet;-><init>()V

    .line 168
    .local v3, "reducedSet":Ljava/util/TreeSet;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v4, v1

    if-ge v0, v4, :cond_1

    .line 169
    aget-object v4, v1, v0

    invoke-virtual {v3, v4}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 168
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 177
    :cond_1
    const/4 v0, 0x0

    :goto_2
    array-length v4, p1

    if-ge v0, v4, :cond_3

    .line 178
    aget-object v4, p1, v0

    invoke-static {v4, v1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isPointInRing(Lcom/vividsolutions/jts/geom/Coordinate;[Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 179
    aget-object v4, p1, v0

    invoke-virtual {v3, v4}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 177
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 182
    :cond_3
    invoke-static {v3}, Lcom/vividsolutions/jts/geom/CoordinateArrays;->toCoordinateArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 185
    .local v2, "reducedPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    array-length v4, v2

    const/4 v5, 0x3

    if-ge v4, v5, :cond_4

    .line 186
    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->padArray3([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object p1

    goto :goto_0

    :cond_4
    move-object p1, v2

    .line 187
    goto :goto_0
.end method


# virtual methods
.method public getConvexHull()Lcom/vividsolutions/jts/geom/Geometry;
    .locals 7

    .prologue
    .line 93
    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->inputPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v4, v4

    if-nez v4, :cond_0

    .line 94
    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v4

    .line 118
    :goto_0
    return-object v4

    .line 96
    :cond_0
    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->inputPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v4, v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 97
    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v5, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->inputPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPoint(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Point;

    move-result-object v4

    goto :goto_0

    .line 99
    :cond_1
    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->inputPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v4, v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 100
    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    iget-object v5, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->inputPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v4, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v4

    goto :goto_0

    .line 103
    :cond_2
    iget-object v2, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->inputPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 105
    .local v2, "reducedPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->inputPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v4, v4

    const/16 v5, 0x32

    if-le v4, v5, :cond_3

    .line 106
    iget-object v4, p0, Lcom/vividsolutions/jts/algorithm/ConvexHull;->inputPts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {p0, v4}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->reduce([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    .line 109
    :cond_3
    invoke-direct {p0, v2}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->preSort([Lcom/vividsolutions/jts/geom/Coordinate;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    .line 112
    .local v3, "sortedPts":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->grahamScan([Lcom/vividsolutions/jts/geom/Coordinate;)Ljava/util/Stack;

    move-result-object v1

    .line 115
    .local v1, "cHS":Ljava/util/Stack;
    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->toCoordinateArray(Ljava/util/Stack;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 118
    .local v0, "cH":[Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/algorithm/ConvexHull;->lineOrPolygon([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    goto :goto_0
.end method

.method protected toCoordinateArray(Ljava/util/Stack;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 4
    .param p1, "stack"    # Ljava/util/Stack;

    .prologue
    .line 126
    invoke-virtual {p1}, Ljava/util/Stack;->size()I

    move-result v3

    new-array v1, v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 127
    .local v1, "coordinates":[Lcom/vividsolutions/jts/geom/Coordinate;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Ljava/util/Stack;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 128
    invoke-virtual {p1, v2}, Ljava/util/Stack;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 129
    .local v0, "coordinate":Lcom/vividsolutions/jts/geom/Coordinate;
    aput-object v0, v1, v2

    .line 127
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 131
    .end local v0    # "coordinate":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    return-object v1
.end method
