.class public Lcom/vividsolutions/jts/algorithm/CentroidPoint;
.super Ljava/lang/Object;
.source "CentroidPoint.java"


# instance fields
.field private centSum:Lcom/vividsolutions/jts/geom/Coordinate;

.field private ptCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->ptCount:I

    .line 48
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 52
    return-void
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 6
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 78
    iget v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->ptCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->ptCount:I

    .line 79
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 80
    iget-object v0, p0, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget-wide v4, p1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 81
    return-void
.end method

.method public add(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 61
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/Point;

    if-eqz v2, :cond_1

    .line 62
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->add(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 70
    :cond_0
    return-void

    .line 64
    :cond_1
    instance-of v2, p1, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    .line 66
    .local v0, "gc":Lcom/vividsolutions/jts/geom/GeometryCollection;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 67
    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 66
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getCentroid()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 6

    .prologue
    .line 85
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>()V

    .line 86
    .local v0, "cent":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    iget v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->ptCount:I

    int-to-double v4, v1

    div-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    .line 87
    iget-object v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->centSum:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v2, v1, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    iget v1, p0, Lcom/vividsolutions/jts/algorithm/CentroidPoint;->ptCount:I

    int-to-double v4, v1

    div-double/2addr v2, v4

    iput-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    .line 88
    return-object v0
.end method
