.class public Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;
.super Ljava/lang/Object;
.source "BufferCurveMaximumDistanceFinder.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MaxPointDistanceFilter"
.end annotation


# instance fields
.field private geom:Lcom/vividsolutions/jts/geom/Geometry;

.field private maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

.field private minPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    new-instance v0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    .line 89
    new-instance v0, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    .line 93
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 94
    return-void
.end method


# virtual methods
.method public filter(Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 2
    .param p1, "pt"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->initialize()V

    .line 98
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;->geom:Lcom/vividsolutions/jts/geom/Geometry;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-static {v0, p1, v1}, Lcom/vividsolutions/jts/operation/buffer/validate/DistanceToPointFinder;->computeDistance(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;)V

    .line 99
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    iget-object v1, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;->minPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;->setMaximum(Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;)V

    .line 100
    return-void
.end method

.method public getMaxPointDistance()Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/buffer/validate/BufferCurveMaximumDistanceFinder$MaxPointDistanceFilter;->maxPtDist:Lcom/vividsolutions/jts/operation/buffer/validate/PointPairDistance;

    return-object v0
.end method
