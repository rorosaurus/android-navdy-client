.class public Lcom/vividsolutions/jts/operation/linemerge/LineMerger;
.super Ljava/lang/Object;
.source "LineMerger.java"


# instance fields
.field private edgeStrings:Ljava/util/Collection;

.field private factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private graph:Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

.field private mergedLineStrings:Ljava/util/Collection;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

    invoke-direct {v0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->graph:Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

    .line 73
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->mergedLineStrings:Ljava/util/Collection;

    .line 74
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 123
    iput-object v1, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->edgeStrings:Ljava/util/Collection;

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/vividsolutions/jts/operation/linemerge/LineMerger;Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 0
    .param p0, "x0"    # Lcom/vividsolutions/jts/operation/linemerge/LineMerger;
    .param p1, "x1"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->add(Lcom/vividsolutions/jts/geom/LineString;)V

    return-void
.end method

.method private add(Lcom/vividsolutions/jts/geom/LineString;)V
    .locals 1
    .param p1, "lineString"    # Lcom/vividsolutions/jts/geom/LineString;

    .prologue
    .line 117
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    if-nez v0, :cond_0

    .line 118
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/LineString;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->graph:Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;->addEdge(Lcom/vividsolutions/jts/geom/LineString;)V

    .line 121
    return-void
.end method

.method private buildEdgeStringStartingWith(Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;)Lcom/vividsolutions/jts/operation/linemerge/EdgeString;
    .locals 4
    .param p1, "start"    # Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;

    .prologue
    .line 179
    new-instance v1, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;

    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->factory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-direct {v1, v2}, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 180
    .local v1, "edgeString":Lcom/vividsolutions/jts/operation/linemerge/EdgeString;
    move-object v0, p1

    .line 182
    .local v0, "current":Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;
    :cond_0
    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->add(Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;)V

    .line 183
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/planargraph/Edge;->setMarked(Z)V

    .line 184
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getNext()Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_1

    if-ne v0, p1, :cond_0

    .line 186
    :cond_1
    return-object v1
.end method

.method private buildEdgeStringsForIsolatedLoops()V
    .locals 0

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->buildEdgeStringsForUnprocessedNodes()V

    .line 149
    return-void
.end method

.method private buildEdgeStringsForNonDegree2Nodes()V
    .locals 4

    .prologue
    .line 162
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->graph:Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;->getNodes()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 163
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/planargraph/Node;

    .line 164
    .local v1, "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 165
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->buildEdgeStringsStartingAt(Lcom/vividsolutions/jts/planargraph/Node;)V

    .line 166
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/planargraph/Node;->setMarked(Z)V

    goto :goto_0

    .line 169
    .end local v1    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    :cond_1
    return-void
.end method

.method private buildEdgeStringsForObviousStartNodes()V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->buildEdgeStringsForNonDegree2Nodes()V

    .line 145
    return-void
.end method

.method private buildEdgeStringsForUnprocessedNodes()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 152
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->graph:Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;->getNodes()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 153
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/planargraph/Node;

    .line 154
    .local v1, "node":Lcom/vividsolutions/jts/planargraph/Node;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/Node;->isMarked()Z

    move-result v2

    if-nez v2, :cond_0

    .line 155
    invoke-virtual {v1}, Lcom/vividsolutions/jts/planargraph/Node;->getDegree()I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_1

    move v2, v3

    :goto_1
    invoke-static {v2}, Lcom/vividsolutions/jts/util/Assert;->isTrue(Z)V

    .line 156
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->buildEdgeStringsStartingAt(Lcom/vividsolutions/jts/planargraph/Node;)V

    .line 157
    invoke-virtual {v1, v3}, Lcom/vividsolutions/jts/planargraph/Node;->setMarked(Z)V

    goto :goto_0

    .line 155
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 160
    .end local v1    # "node":Lcom/vividsolutions/jts/planargraph/Node;
    :cond_2
    return-void
.end method

.method private buildEdgeStringsStartingAt(Lcom/vividsolutions/jts/planargraph/Node;)V
    .locals 4
    .param p1, "node"    # Lcom/vividsolutions/jts/planargraph/Node;

    .prologue
    .line 171
    invoke-virtual {p1}, Lcom/vividsolutions/jts/planargraph/Node;->getOutEdges()Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/planargraph/DirectedEdgeStar;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 172
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;

    .line 173
    .local v0, "directedEdge":Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;->getEdge()Lcom/vividsolutions/jts/planargraph/Edge;

    move-result-object v2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/planargraph/Edge;->isMarked()Z

    move-result v2

    if-nez v2, :cond_0

    .line 174
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->edgeStrings:Ljava/util/Collection;

    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->buildEdgeStringStartingWith(Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;)Lcom/vividsolutions/jts/operation/linemerge/EdgeString;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 176
    .end local v0    # "directedEdge":Lcom/vividsolutions/jts/operation/linemerge/LineMergeDirectedEdge;
    :cond_1
    return-void
.end method

.method private merge()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 127
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->mergedLineStrings:Ljava/util/Collection;

    if-eqz v2, :cond_1

    .line 141
    :cond_0
    return-void

    .line 130
    :cond_1
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->graph:Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;->nodeIterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/planargraph/GraphComponent;->setMarked(Ljava/util/Iterator;Z)V

    .line 131
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->graph:Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/operation/linemerge/LineMergeGraph;->edgeIterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, v3}, Lcom/vividsolutions/jts/planargraph/GraphComponent;->setMarked(Ljava/util/Iterator;Z)V

    .line 133
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->edgeStrings:Ljava/util/Collection;

    .line 134
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->buildEdgeStringsForObviousStartNodes()V

    .line 135
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->buildEdgeStringsForIsolatedLoops()V

    .line 136
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->mergedLineStrings:Ljava/util/Collection;

    .line 137
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->edgeStrings:Ljava/util/Collection;

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;

    .line 139
    .local v0, "edgeString":Lcom/vividsolutions/jts/operation/linemerge/EdgeString;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->mergedLineStrings:Ljava/util/Collection;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/operation/linemerge/EdgeString;->toLineString()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 1
    .param p1, "geometry"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 93
    new-instance v0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger$1;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMerger$1;-><init>(Lcom/vividsolutions/jts/operation/linemerge/LineMerger;)V

    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    .line 100
    return-void
.end method

.method public add(Ljava/util/Collection;)V
    .locals 3
    .param p1, "geometries"    # Ljava/util/Collection;

    .prologue
    .line 110
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->mergedLineStrings:Ljava/util/Collection;

    .line 111
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 113
    .local v0, "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->add(Lcom/vividsolutions/jts/geom/Geometry;)V

    goto :goto_0

    .line 115
    .end local v0    # "geometry":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-void
.end method

.method public getMergedLineStrings()Ljava/util/Collection;
    .locals 1

    .prologue
    .line 195
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->merge()V

    .line 196
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/linemerge/LineMerger;->mergedLineStrings:Ljava/util/Collection;

    return-object v0
.end method
