.class public Lcom/vividsolutions/jts/operation/relate/RelateComputer;
.super Ljava/lang/Object;
.source "RelateComputer.java"


# instance fields
.field private arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

.field private im:Lcom/vividsolutions/jts/geom/IntersectionMatrix;

.field private invalidPoint:Lcom/vividsolutions/jts/geom/Coordinate;

.field private isolatedEdges:Ljava/util/ArrayList;

.field private li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

.field private nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

.field private ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;


# direct methods
.method public constructor <init>([Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V
    .locals 2
    .param p1, "arg"    # [Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 66
    new-instance v0, Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/PointLocator;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    .line 68
    new-instance v0, Lcom/vividsolutions/jts/geomgraph/NodeMap;

    new-instance v1, Lcom/vividsolutions/jts/operation/relate/RelateNodeFactory;

    invoke-direct {v1}, Lcom/vividsolutions/jts/operation/relate/RelateNodeFactory;-><init>()V

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geomgraph/NodeMap;-><init>(Lcom/vividsolutions/jts/geomgraph/NodeFactory;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->im:Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->isolatedEdges:Ljava/util/ArrayList;

    .line 77
    iput-object p1, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    .line 78
    return-void
.end method

.method private computeDisjointIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V
    .locals 6
    .param p1, "im"    # Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 281
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v2, v2, v4

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 282
    .local v0, "ga":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 283
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v2

    invoke-virtual {p1, v4, v3, v2}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->set(III)V

    .line 284
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getBoundaryDimension()I

    move-result v2

    invoke-virtual {p1, v5, v3, v2}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->set(III)V

    .line 286
    :cond_0
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v2, v2, v5

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 287
    .local v1, "gb":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 288
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v2

    invoke-virtual {p1, v3, v4, v2}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->set(III)V

    .line 289
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/Geometry;->getBoundaryDimension()I

    move-result v2

    invoke-virtual {p1, v3, v5, v2}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->set(III)V

    .line 291
    :cond_1
    return-void
.end method

.method private computeIntersectionNodes(I)V
    .locals 8
    .param p1, "argIndex"    # I

    .prologue
    .line 234
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v6, v6, p1

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getEdgeIterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 235
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 236
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v1

    .line 237
    .local v1, "eLoc":I
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getEdgeIntersectionList()Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "eiIt":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 238
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 239
    .local v2, "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    iget-object v7, v2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->addNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/operation/relate/RelateNode;

    .line 240
    .local v5, "n":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    const/4 v6, 0x1

    if-ne v1, v6, :cond_2

    .line 241
    invoke-virtual {v5, p1}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->setLabelBoundary(I)V

    goto :goto_0

    .line 243
    :cond_2
    invoke-virtual {v5}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/vividsolutions/jts/geomgraph/Label;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 244
    const/4 v6, 0x0

    invoke-virtual {v5, p1, v6}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->setLabel(II)V

    goto :goto_0

    .line 249
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v1    # "eLoc":I
    .end local v2    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    .end local v3    # "eiIt":Ljava/util/Iterator;
    .end local v5    # "n":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    :cond_3
    return-void
.end method

.method private computeProperIntersectionIM(Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V
    .locals 8
    .param p1, "intersector"    # Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
    .param p2, "im"    # Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 164
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v0

    .line 165
    .local v0, "dimA":I
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v4, v4, v6

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v1

    .line 166
    .local v1, "dimB":I
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasProperIntersection()Z

    move-result v2

    .line 167
    .local v2, "hasProper":Z
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;->hasProperInteriorIntersection()Z

    move-result v3

    .line 174
    .local v3, "hasProperInterior":Z
    if-ne v0, v7, :cond_1

    if-ne v1, v7, :cond_1

    .line 175
    if-eqz v2, :cond_0

    const-string v4, "212101212"

    invoke-virtual {p2, v4}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->setAtLeast(Ljava/lang/String;)V

    .line 205
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    if-ne v0, v7, :cond_3

    if-ne v1, v6, :cond_3

    .line 186
    if-eqz v2, :cond_2

    const-string v4, "FFF0FFFF2"

    invoke-virtual {p2, v4}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->setAtLeast(Ljava/lang/String;)V

    .line 187
    :cond_2
    if-eqz v3, :cond_0

    const-string v4, "1FFFFF1FF"

    invoke-virtual {p2, v4}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->setAtLeast(Ljava/lang/String;)V

    goto :goto_0

    .line 189
    :cond_3
    if-ne v0, v6, :cond_5

    if-ne v1, v7, :cond_5

    .line 190
    if-eqz v2, :cond_4

    const-string v4, "F0FFFFFF2"

    invoke-virtual {p2, v4}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->setAtLeast(Ljava/lang/String;)V

    .line 191
    :cond_4
    if-eqz v3, :cond_0

    const-string v4, "1F1FFFFFF"

    invoke-virtual {p2, v4}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->setAtLeast(Ljava/lang/String;)V

    goto :goto_0

    .line 202
    :cond_5
    if-ne v0, v6, :cond_0

    if-ne v1, v6, :cond_0

    .line 203
    if-eqz v3, :cond_0

    const-string v4, "0FFFFFFFF"

    invoke-virtual {p2, v4}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->setAtLeast(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private copyNodesAndLabels(I)V
    .locals 5
    .param p1, "argIndex"    # I

    .prologue
    .line 218
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v3, v3, p1

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getNodeIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 219
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 220
    .local v0, "graphNode":Lcom/vividsolutions/jts/geomgraph/Node;
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->addNode(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v2

    .line 221
    .local v2, "newNode":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v3

    invoke-virtual {v2, p1, v3}, Lcom/vividsolutions/jts/geomgraph/Node;->setLabel(II)V

    goto :goto_0

    .line 224
    .end local v0    # "graphNode":Lcom/vividsolutions/jts/geomgraph/Node;
    .end local v2    # "newNode":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_0
    return-void
.end method

.method private insertEdgeEnds(Ljava/util/List;)V
    .locals 3
    .param p1, "ee"    # Ljava/util/List;

    .prologue
    .line 155
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 156
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .line 157
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v2, v0}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->add(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V

    goto :goto_0

    .line 159
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    :cond_0
    return-void
.end method

.method private labelIntersectionNodes(I)V
    .locals 8
    .param p1, "argIndex"    # I

    .prologue
    .line 259
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v6, v6, p1

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getEdgeIterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 260
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 261
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v1

    .line 262
    .local v1, "eLoc":I
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->getEdgeIntersectionList()Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geomgraph/EdgeIntersectionList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "eiIt":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 263
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;

    .line 264
    .local v2, "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    iget-object v6, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    iget-object v7, v2, Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;->coord:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v6, v7}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->find(Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geomgraph/Node;

    move-result-object v5

    check-cast v5, Lcom/vividsolutions/jts/operation/relate/RelateNode;

    .line 265
    .local v5, "n":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    invoke-virtual {v5}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v6

    invoke-virtual {v6, p1}, Lcom/vividsolutions/jts/geomgraph/Label;->isNull(I)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 266
    const/4 v6, 0x1

    if-ne v1, v6, :cond_2

    .line 267
    invoke-virtual {v5, p1}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->setLabelBoundary(I)V

    goto :goto_0

    .line 269
    :cond_2
    const/4 v6, 0x0

    invoke-virtual {v5, p1, v6}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->setLabel(II)V

    goto :goto_0

    .line 274
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    .end local v1    # "eLoc":I
    .end local v2    # "ei":Lcom/vividsolutions/jts/geomgraph/EdgeIntersection;
    .end local v3    # "eiIt":Ljava/util/Iterator;
    .end local v5    # "n":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    :cond_3
    return-void
.end method

.method private labelIsolatedEdge(Lcom/vividsolutions/jts/geomgraph/Edge;ILcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/Edge;
    .param p2, "targetIndex"    # I
    .param p3, "target"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 349
    invoke-virtual {p3}, Lcom/vividsolutions/jts/geom/Geometry;->getDimension()I

    move-result v1

    if-lez v1, :cond_0

    .line 353
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v0

    .line 354
    .local v0, "loc":I
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Lcom/vividsolutions/jts/geomgraph/Label;->setAllLocations(II)V

    .line 360
    .end local v0    # "loc":I
    :goto_0
    return-void

    .line 357
    :cond_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, p2, v2}, Lcom/vividsolutions/jts/geomgraph/Label;->setAllLocations(II)V

    goto :goto_0
.end method

.method private labelIsolatedEdges(II)V
    .locals 3
    .param p1, "thisIndex"    # I
    .param p2, "targetIndex"    # I

    .prologue
    .line 333
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getEdgeIterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "ei":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 334
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 335
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Edge;->isIsolated()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 336
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v2, v2, p2

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v2

    invoke-direct {p0, v0, p2, v2}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->labelIsolatedEdge(Lcom/vividsolutions/jts/geomgraph/Edge;ILcom/vividsolutions/jts/geom/Geometry;)V

    .line 337
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->isolatedEdges:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 340
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_1
    return-void
.end method

.method private labelIsolatedNode(Lcom/vividsolutions/jts/geomgraph/Node;I)V
    .locals 4
    .param p1, "n"    # Lcom/vividsolutions/jts/geomgraph/Node;
    .param p2, "targetIndex"    # I

    .prologue
    .line 392
    iget-object v1, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->ptLocator:Lcom/vividsolutions/jts/algorithm/PointLocator;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Node;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v3, v3, p2

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/vividsolutions/jts/algorithm/PointLocator;->locate(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Geometry;)I

    move-result v0

    .line 393
    .local v0, "loc":I
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v1

    invoke-virtual {v1, p2, v0}, Lcom/vividsolutions/jts/geomgraph/Label;->setAllLocations(II)V

    .line 395
    return-void
.end method

.method private labelIsolatedNodes()V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 373
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "ni":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 374
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/Node;

    .line 375
    .local v1, "n":Lcom/vividsolutions/jts/geomgraph/Node;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v0

    .line 377
    .local v0, "label":Lcom/vividsolutions/jts/geomgraph/Label;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/Label;->getGeometryCount()I

    move-result v3

    if-lez v3, :cond_1

    move v3, v4

    :goto_1
    const-string v6, "node with empty label found"

    invoke-static {v3, v6}, Lcom/vividsolutions/jts/util/Assert;->isTrue(ZLjava/lang/String;)V

    .line 378
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/Node;->isIsolated()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 379
    invoke-virtual {v0, v5}, Lcom/vividsolutions/jts/geomgraph/Label;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 380
    invoke-direct {p0, v1, v5}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->labelIsolatedNode(Lcom/vividsolutions/jts/geomgraph/Node;I)V

    goto :goto_0

    :cond_1
    move v3, v5

    .line 377
    goto :goto_1

    .line 382
    :cond_2
    invoke-direct {p0, v1, v4}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->labelIsolatedNode(Lcom/vividsolutions/jts/geomgraph/Node;I)V

    goto :goto_0

    .line 385
    .end local v0    # "label":Lcom/vividsolutions/jts/geomgraph/Label;
    .end local v1    # "n":Lcom/vividsolutions/jts/geomgraph/Node;
    :cond_3
    return-void
.end method

.method private labelNodeEdges()V
    .locals 4

    .prologue
    .line 295
    iget-object v2, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "ni":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 296
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/operation/relate/RelateNode;

    .line 297
    .local v1, "node":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->getEdges()Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    invoke-virtual {v2, v3}, Lcom/vividsolutions/jts/geomgraph/EdgeEndStar;->computeLabelling([Lcom/vividsolutions/jts/geomgraph/GeometryGraph;)V

    goto :goto_0

    .line 301
    .end local v1    # "node":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    :cond_0
    return-void
.end method

.method private updateIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V
    .locals 5
    .param p1, "im"    # Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    .prologue
    .line 309
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->isolatedEdges:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "ei":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 310
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/Edge;

    .line 311
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->updateIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V

    goto :goto_0

    .line 314
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/Edge;
    :cond_0
    iget-object v4, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->nodes:Lcom/vividsolutions/jts/geomgraph/NodeMap;

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geomgraph/NodeMap;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "ni":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 315
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/vividsolutions/jts/operation/relate/RelateNode;

    .line 316
    .local v3, "node":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    invoke-virtual {v3, p1}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->updateIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V

    .line 318
    invoke-virtual {v3, p1}, Lcom/vividsolutions/jts/operation/relate/RelateNode;->updateIMFromEdges(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V

    goto :goto_1

    .line 322
    .end local v3    # "node":Lcom/vividsolutions/jts/operation/relate/RelateNode;
    :cond_1
    return-void
.end method


# virtual methods
.method public computeIM()Lcom/vividsolutions/jts/geom/IntersectionMatrix;
    .locals 10

    .prologue
    const/4 v5, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 82
    new-instance v3, Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    invoke-direct {v3}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;-><init>()V

    .line 84
    .local v3, "im":Lcom/vividsolutions/jts/geom/IntersectionMatrix;
    invoke-virtual {v3, v5, v5, v5}, Lcom/vividsolutions/jts/geom/IntersectionMatrix;->set(III)V

    .line 87
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v5

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v5

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v6, v6, v9

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getGeometry()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v6

    invoke-virtual {v6}, Lcom/vividsolutions/jts/geom/Geometry;->getEnvelopeInternal()Lcom/vividsolutions/jts/geom/Envelope;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/vividsolutions/jts/geom/Envelope;->intersects(Lcom/vividsolutions/jts/geom/Envelope;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 89
    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->computeDisjointIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V

    .line 150
    :goto_0
    return-object v3

    .line 92
    :cond_0
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v5, v5, v8

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v5, v6, v8}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeSelfNodes(Lcom/vividsolutions/jts/algorithm/LineIntersector;Z)Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .line 93
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v5, v5, v9

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v5, v6, v8}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeSelfNodes(Lcom/vividsolutions/jts/algorithm/LineIntersector;Z)Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    .line 96
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v5, v5, v8

    iget-object v6, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v6, v6, v9

    iget-object v7, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v5, v6, v7, v8}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->computeEdgeIntersections(Lcom/vividsolutions/jts/geomgraph/GeometryGraph;Lcom/vividsolutions/jts/algorithm/LineIntersector;Z)Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;

    move-result-object v4

    .line 98
    .local v4, "intersector":Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;
    invoke-direct {p0, v8}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->computeIntersectionNodes(I)V

    .line 99
    invoke-direct {p0, v9}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->computeIntersectionNodes(I)V

    .line 104
    invoke-direct {p0, v8}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->copyNodesAndLabels(I)V

    .line 105
    invoke-direct {p0, v9}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->copyNodesAndLabels(I)V

    .line 110
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->labelIsolatedNodes()V

    .line 114
    invoke-direct {p0, v4, v3}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->computeProperIntersectionIM(Lcom/vividsolutions/jts/geomgraph/index/SegmentIntersector;Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V

    .line 123
    new-instance v2, Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;

    invoke-direct {v2}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;-><init>()V

    .line 124
    .local v2, "eeBuilder":Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v5, v5, v8

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getEdgeIterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;->computeEdgeEnds(Ljava/util/Iterator;)Ljava/util/List;

    move-result-object v0

    .line 125
    .local v0, "ee0":Ljava/util/List;
    invoke-direct {p0, v0}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->insertEdgeEnds(Ljava/util/List;)V

    .line 126
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->arg:[Lcom/vividsolutions/jts/geomgraph/GeometryGraph;

    aget-object v5, v5, v9

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->getEdgeIterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBuilder;->computeEdgeEnds(Ljava/util/Iterator;)Ljava/util/List;

    move-result-object v1

    .line 127
    .local v1, "ee1":Ljava/util/List;
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->insertEdgeEnds(Ljava/util/List;)V

    .line 132
    invoke-direct {p0}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->labelNodeEdges()V

    .line 144
    invoke-direct {p0, v8, v9}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->labelIsolatedEdges(II)V

    .line 146
    invoke-direct {p0, v9, v8}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->labelIsolatedEdges(II)V

    .line 149
    invoke-direct {p0, v3}, Lcom/vividsolutions/jts/operation/relate/RelateComputer;->updateIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V

    goto :goto_0
.end method
