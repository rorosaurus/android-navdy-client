.class public Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;
.super Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
.source "EdgeEndBundle.java"


# instance fields
.field private edgeEnds:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V
    .locals 5
    .param p1, "boundaryNodeRule"    # Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;
    .param p2, "e"    # Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .prologue
    .line 59
    invoke-virtual {p2}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;->getEdge()Lcom/vividsolutions/jts/geomgraph/Edge;

    move-result-object v0

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;->getCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;->getDirectedCoordinate()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    new-instance v3, Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-virtual {p2}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/vividsolutions/jts/geomgraph/Label;-><init>(Lcom/vividsolutions/jts/geomgraph/Label;)V

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;-><init>(Lcom/vividsolutions/jts/geomgraph/Edge;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geomgraph/Label;)V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->edgeEnds:Ljava/util/List;

    .line 60
    invoke-virtual {p0, p2}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->insert(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V

    .line 67
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V
    .locals 1
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;-><init>(Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V

    .line 72
    return-void
.end method

.method private computeLabelOn(ILcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V
    .locals 6
    .param p1, "geomIndex"    # I
    .param p2, "boundaryNodeRule"    # Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .prologue
    .line 134
    const/4 v0, 0x0

    .line 135
    .local v0, "boundaryCount":I
    const/4 v2, 0x0

    .line 137
    .local v2, "foundInterior":Z
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 138
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .line 139
    .local v1, "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    invoke-virtual {v1}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v5

    invoke-virtual {v5, p1}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(I)I

    move-result v4

    .line 140
    .local v4, "loc":I
    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    add-int/lit8 v0, v0, 0x1

    .line 141
    :cond_1
    if-nez v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 143
    .end local v1    # "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    .end local v4    # "loc":I
    :cond_2
    const/4 v4, -0x1

    .line 144
    .restart local v4    # "loc":I
    if-eqz v2, :cond_3

    const/4 v4, 0x0

    .line 145
    :cond_3
    if-lez v0, :cond_4

    .line 146
    invoke-static {p2, v0}, Lcom/vividsolutions/jts/geomgraph/GeometryGraph;->determineBoundary(Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;I)I

    move-result v4

    .line 148
    :cond_4
    iget-object v5, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-virtual {v5, p1, v4}, Lcom/vividsolutions/jts/geomgraph/Label;->setLocation(II)V

    .line 150
    return-void
.end method

.method private computeLabelSide(II)V
    .locals 5
    .param p1, "geomIndex"    # I
    .param p2, "side"    # I

    .prologue
    const/4 v4, 0x2

    .line 176
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 177
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .line 178
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v3

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geomgraph/Label;->isArea()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 179
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Lcom/vividsolutions/jts/geomgraph/Label;->getLocation(II)I

    move-result v2

    .line 180
    .local v2, "loc":I
    if-nez v2, :cond_2

    .line 181
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, p2, v4}, Lcom/vividsolutions/jts/geomgraph/Label;->setLocation(III)V

    .line 188
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    .end local v2    # "loc":I
    :cond_1
    return-void

    .line 184
    .restart local v0    # "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    .restart local v2    # "loc":I
    :cond_2
    if-ne v2, v4, :cond_0

    .line 185
    iget-object v3, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-virtual {v3, p1, p2, v4}, Lcom/vividsolutions/jts/geomgraph/Label;->setLocation(III)V

    goto :goto_0
.end method

.method private computeLabelSides(I)V
    .locals 1
    .param p1, "geomIndex"    # I

    .prologue
    .line 156
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->computeLabelSide(II)V

    .line 157
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->computeLabelSide(II)V

    .line 158
    return-void
.end method


# virtual methods
.method public computeLabel(Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V
    .locals 6
    .param p1, "boundaryNodeRule"    # Lcom/vividsolutions/jts/algorithm/BoundaryNodeRule;

    .prologue
    const/4 v5, -0x1

    .line 93
    const/4 v2, 0x0

    .line 94
    .local v2, "isArea":Z
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 95
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .line 96
    .local v0, "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;->getLabel()Lcom/vividsolutions/jts/geomgraph/Label;

    move-result-object v4

    invoke-virtual {v4}, Lcom/vividsolutions/jts/geomgraph/Label;->isArea()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    .line 98
    .end local v0    # "e":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    :cond_1
    if-eqz v2, :cond_3

    .line 99
    new-instance v4, Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-direct {v4, v5, v5, v5}, Lcom/vividsolutions/jts/geomgraph/Label;-><init>(III)V

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    .line 104
    :goto_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    const/4 v4, 0x2

    if-ge v1, v4, :cond_4

    .line 105
    invoke-direct {p0, v1, p1}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->computeLabelOn(ILcom/vividsolutions/jts/algorithm/BoundaryNodeRule;)V

    .line 106
    if-eqz v2, :cond_2

    .line 107
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->computeLabelSides(I)V

    .line 104
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 101
    .end local v1    # "i":I
    :cond_3
    new-instance v4, Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-direct {v4, v5}, Lcom/vividsolutions/jts/geomgraph/Label;-><init>(I)V

    iput-object v4, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    goto :goto_1

    .line 109
    .restart local v1    # "i":I
    :cond_4
    return-void
.end method

.method public getEdgeEnds()Ljava/util/List;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->edgeEnds:Ljava/util/List;

    return-object v0
.end method

.method public getLabel()Lcom/vividsolutions/jts/geomgraph/Label;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    return-object v0
.end method

.method public insert(Lcom/vividsolutions/jts/geomgraph/EdgeEnd;)V
    .locals 1
    .param p1, "e"    # Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->edgeEnds:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->edgeEnds:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public print(Ljava/io/PrintStream;)V
    .locals 4
    .param p1, "out"    # Ljava/io/PrintStream;

    .prologue
    .line 199
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "EdgeEndBundle--> Label: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 200
    invoke-virtual {p0}, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 201
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;

    .line 202
    .local v0, "ee":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geomgraph/EdgeEnd;->print(Ljava/io/PrintStream;)V

    .line 203
    invoke-virtual {p1}, Ljava/io/PrintStream;->println()V

    goto :goto_0

    .line 205
    .end local v0    # "ee":Lcom/vividsolutions/jts/geomgraph/EdgeEnd;
    :cond_0
    return-void
.end method

.method updateIM(Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V
    .locals 1
    .param p1, "im"    # Lcom/vividsolutions/jts/geom/IntersectionMatrix;

    .prologue
    .line 195
    iget-object v0, p0, Lcom/vividsolutions/jts/operation/relate/EdgeEndBundle;->label:Lcom/vividsolutions/jts/geomgraph/Label;

    invoke-static {v0, p1}, Lcom/vividsolutions/jts/geomgraph/Edge;->updateIM(Lcom/vividsolutions/jts/geomgraph/Label;Lcom/vividsolutions/jts/geom/IntersectionMatrix;)V

    .line 196
    return-void
.end method
