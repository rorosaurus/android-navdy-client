.class public interface abstract Lcom/vividsolutions/jts/awt/PointShapeFactory;
.super Ljava/lang/Object;
.source "PointShapeFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/awt/PointShapeFactory$X;,
        Lcom/vividsolutions/jts/awt/PointShapeFactory$Cross;,
        Lcom/vividsolutions/jts/awt/PointShapeFactory$Circle;,
        Lcom/vividsolutions/jts/awt/PointShapeFactory$Triangle;,
        Lcom/vividsolutions/jts/awt/PointShapeFactory$Star;,
        Lcom/vividsolutions/jts/awt/PointShapeFactory$Square;,
        Lcom/vividsolutions/jts/awt/PointShapeFactory$Point;,
        Lcom/vividsolutions/jts/awt/PointShapeFactory$BasePointShapeFactory;
    }
.end annotation


# virtual methods
.method public abstract createPoint(Ljava/awt/geom/Point2D;)Ljava/awt/Shape;
.end method
