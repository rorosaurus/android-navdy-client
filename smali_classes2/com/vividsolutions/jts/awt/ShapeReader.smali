.class public Lcom/vividsolutions/jts/awt/ShapeReader;
.super Ljava/lang/Object;
.source "ShapeReader.java"


# static fields
.field private static INVERT_Y:Ljava/awt/geom/AffineTransform;


# instance fields
.field private geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 63
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-static {v0, v1, v2, v3}, Ljava/awt/geom/AffineTransform;->getScaleInstance(DD)Ljava/awt/geom/AffineTransform;

    move-result-object v0

    sput-object v0, Lcom/vividsolutions/jts/awt/ShapeReader;->INVERT_Y:Ljava/awt/geom/AffineTransform;

    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 0
    .param p1, "geometryFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/vividsolutions/jts/awt/ShapeReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 96
    return-void
.end method

.method private isHole([Lcom/vividsolutions/jts/geom/Coordinate;)Z
    .locals 1
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 133
    invoke-static {p1}, Lcom/vividsolutions/jts/algorithm/CGAlgorithms;->isCCW([Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v0

    return v0
.end method

.method private static nextCoordinateArray(Ljava/awt/geom/PathIterator;)[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 11
    .param p0, "pathIt"    # Ljava/awt/geom/PathIterator;

    .prologue
    const/4 v10, 0x1

    const/4 v5, 0x0

    .line 158
    const/4 v4, 0x6

    new-array v2, v4, [D

    .line 159
    .local v2, "pathPt":[D
    const/4 v0, 0x0

    .line 160
    .local v0, "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    const/4 v1, 0x0

    .line 161
    .local v1, "isDone":Z
    :cond_0
    invoke-interface {p0}, Ljava/awt/geom/PathIterator;->isDone()Z

    move-result v4

    if-nez v4, :cond_1

    .line 162
    invoke-interface {p0, v2}, Ljava/awt/geom/PathIterator;->currentSegment([D)I

    move-result v3

    .line 163
    .local v3, "segType":I
    packed-switch v3, :pswitch_data_0

    .line 185
    :pswitch_0
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "unhandled (non-linear) segment type encountered"

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 165
    :pswitch_1
    if-eqz v0, :cond_2

    .line 167
    const/4 v1, 0x1

    .line 187
    :goto_0
    if-eqz v1, :cond_0

    .line 190
    .end local v3    # "segType":I
    :cond_1
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;->toCoordinateArray()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    return-object v4

    .line 170
    .restart local v3    # "segType":I
    :cond_2
    new-instance v0, Lcom/vividsolutions/jts/geom/CoordinateList;

    .end local v0    # "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;-><init>()V

    .line 171
    .restart local v0    # "coordList":Lcom/vividsolutions/jts/geom/CoordinateList;
    new-instance v4, Lcom/vividsolutions/jts/geom/Coordinate;

    aget-wide v6, v2, v5

    aget-wide v8, v2, v10

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Ljava/lang/Object;)Z

    .line 172
    invoke-interface {p0}, Ljava/awt/geom/PathIterator;->next()V

    goto :goto_0

    .line 176
    :pswitch_2
    new-instance v4, Lcom/vividsolutions/jts/geom/Coordinate;

    aget-wide v6, v2, v5

    aget-wide v8, v2, v10

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    invoke-virtual {v0, v4}, Lcom/vividsolutions/jts/geom/CoordinateList;->add(Ljava/lang/Object;)Z

    .line 177
    invoke-interface {p0}, Ljava/awt/geom/PathIterator;->next()V

    goto :goto_0

    .line 180
    :pswitch_3
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/CoordinateList;->closeRing()V

    .line 181
    invoke-interface {p0}, Ljava/awt/geom/PathIterator;->next()V

    .line 182
    const/4 v1, 0x1

    .line 183
    goto :goto_0

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static read(Ljava/awt/Shape;DLcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 3
    .param p0, "shp"    # Ljava/awt/Shape;
    .param p1, "flatness"    # D
    .param p3, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 88
    sget-object v1, Lcom/vividsolutions/jts/awt/ShapeReader;->INVERT_Y:Ljava/awt/geom/AffineTransform;

    invoke-interface {p0, v1, p1, p2}, Ljava/awt/Shape;->getPathIterator(Ljava/awt/geom/AffineTransform;D)Ljava/awt/geom/PathIterator;

    move-result-object v0

    .line 89
    .local v0, "pathIt":Ljava/awt/geom/PathIterator;
    invoke-static {v0, p3}, Lcom/vividsolutions/jts/awt/ShapeReader;->read(Ljava/awt/geom/PathIterator;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method public static read(Ljava/awt/geom/PathIterator;Lcom/vividsolutions/jts/geom/GeometryFactory;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "pathIt"    # Ljava/awt/geom/PathIterator;
    .param p1, "geomFact"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    .line 74
    new-instance v0, Lcom/vividsolutions/jts/awt/ShapeReader;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/awt/ShapeReader;-><init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V

    .line 75
    .local v0, "pc":Lcom/vividsolutions/jts/awt/ShapeReader;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/awt/ShapeReader;->read(Ljava/awt/geom/PathIterator;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    return-object v1
.end method

.method public static toCoordinates(Ljava/awt/geom/PathIterator;)Ljava/util/List;
    .locals 3
    .param p0, "pathIt"    # Ljava/awt/geom/PathIterator;

    .prologue
    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 147
    .local v0, "coordArrays":Ljava/util/List;
    :goto_0
    invoke-interface {p0}, Ljava/awt/geom/PathIterator;->isDone()Z

    move-result v2

    if-nez v2, :cond_0

    .line 148
    invoke-static {p0}, Lcom/vividsolutions/jts/awt/ShapeReader;->nextCoordinateArray(Ljava/awt/geom/PathIterator;)[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    .line 149
    .local v1, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    if-nez v1, :cond_1

    .line 153
    .end local v1    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    return-object v0

    .line 151
    .restart local v1    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public read(Ljava/awt/geom/PathIterator;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 10
    .param p1, "pathIt"    # Ljava/awt/geom/PathIterator;

    .prologue
    .line 106
    invoke-static {p1}, Lcom/vividsolutions/jts/awt/ShapeReader;->toCoordinates(Ljava/awt/geom/PathIterator;)Ljava/util/List;

    move-result-object v4

    .line 108
    .local v4, "pathPtSeq":Ljava/util/List;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 109
    .local v5, "polys":Ljava/util/List;
    const/4 v7, 0x0

    .line 110
    .local v7, "seqIndex":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    if-ge v7, v9, :cond_1

    .line 113
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/vividsolutions/jts/geom/Coordinate;

    move-object v6, v9

    check-cast v6, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 114
    .local v6, "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v9, p0, Lcom/vividsolutions/jts/awt/ShapeReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v9, v6}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v8

    .line 115
    .local v8, "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    add-int/lit8 v7, v7, 0x1

    .line 117
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 119
    .local v3, "holes":Ljava/util/List;
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    if-ge v7, v9, :cond_0

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/vividsolutions/jts/geom/Coordinate;

    check-cast v9, [Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {p0, v9}, Lcom/vividsolutions/jts/awt/ShapeReader;->isHole([Lcom/vividsolutions/jts/geom/Coordinate;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 120
    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Lcom/vividsolutions/jts/geom/Coordinate;

    move-object v2, v9

    check-cast v2, [Lcom/vividsolutions/jts/geom/Coordinate;

    .line 121
    .local v2, "holePts":[Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v9, p0, Lcom/vividsolutions/jts/awt/ShapeReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v9, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLinearRing([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v0

    .line 122
    .local v0, "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    add-int/lit8 v7, v7, 0x1

    .line 124
    goto :goto_1

    .line 125
    .end local v0    # "hole":Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v2    # "holePts":[Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_0
    invoke-static {v3}, Lcom/vividsolutions/jts/geom/GeometryFactory;->toLinearRingArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/LinearRing;

    move-result-object v1

    .line 126
    .local v1, "holeArray":[Lcom/vividsolutions/jts/geom/LinearRing;
    iget-object v9, p0, Lcom/vividsolutions/jts/awt/ShapeReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v9, v8, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createPolygon(Lcom/vividsolutions/jts/geom/LinearRing;[Lcom/vividsolutions/jts/geom/LinearRing;)Lcom/vividsolutions/jts/geom/Polygon;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 128
    .end local v1    # "holeArray":[Lcom/vividsolutions/jts/geom/LinearRing;
    .end local v3    # "holes":Ljava/util/List;
    .end local v6    # "pts":[Lcom/vividsolutions/jts/geom/Coordinate;
    .end local v8    # "shell":Lcom/vividsolutions/jts/geom/LinearRing;
    :cond_1
    iget-object v9, p0, Lcom/vividsolutions/jts/awt/ShapeReader;->geometryFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v9, v5}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v9

    return-object v9
.end method
