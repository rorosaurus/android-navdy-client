.class public Lcom/vividsolutions/jts/precision/MinimumClearance;
.super Ljava/lang/Object;
.source "MinimumClearance.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/precision/MinimumClearance$1;,
        Lcom/vividsolutions/jts/precision/MinimumClearance$MinClearanceDistance;
    }
.end annotation


# instance fields
.field private inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

.field private minClearance:D

.field private minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 0
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 165
    iput-object p1, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    .line 166
    return-void
.end method

.method private compute()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 207
    iget-object v3, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    if-eqz v3, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/vividsolutions/jts/geom/Coordinate;

    iput-object v3, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 211
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    iput-wide v4, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->minClearance:D

    .line 214
    iget-object v3, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v3}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 218
    iget-object v3, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-static {v3}, Lcom/vividsolutions/jts/operation/distance/FacetSequenceTreeBuilder;->build(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/index/strtree/STRtree;

    move-result-object v0

    .line 220
    .local v0, "geomTree":Lcom/vividsolutions/jts/index/strtree/STRtree;
    new-instance v3, Lcom/vividsolutions/jts/precision/MinimumClearance$MinClearanceDistance;

    invoke-direct {v3, v6}, Lcom/vividsolutions/jts/precision/MinimumClearance$MinClearanceDistance;-><init>(Lcom/vividsolutions/jts/precision/MinimumClearance$1;)V

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/index/strtree/STRtree;->nearestNeighbour(Lcom/vividsolutions/jts/index/strtree/ItemDistance;)[Ljava/lang/Object;

    move-result-object v2

    .line 221
    .local v2, "nearest":[Ljava/lang/Object;
    new-instance v1, Lcom/vividsolutions/jts/precision/MinimumClearance$MinClearanceDistance;

    invoke-direct {v1, v6}, Lcom/vividsolutions/jts/precision/MinimumClearance$MinClearanceDistance;-><init>(Lcom/vividsolutions/jts/precision/MinimumClearance$1;)V

    .line 222
    .local v1, "mcd":Lcom/vividsolutions/jts/precision/MinimumClearance$MinClearanceDistance;
    const/4 v3, 0x0

    aget-object v3, v2, v3

    check-cast v3, Lcom/vividsolutions/jts/operation/distance/FacetSequence;

    const/4 v4, 0x1

    aget-object v4, v2, v4

    check-cast v4, Lcom/vividsolutions/jts/operation/distance/FacetSequence;

    invoke-virtual {v1, v3, v4}, Lcom/vividsolutions/jts/precision/MinimumClearance$MinClearanceDistance;->distance(Lcom/vividsolutions/jts/operation/distance/FacetSequence;Lcom/vividsolutions/jts/operation/distance/FacetSequence;)D

    move-result-wide v4

    iput-wide v4, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->minClearance:D

    .line 225
    invoke-virtual {v1}, Lcom/vividsolutions/jts/precision/MinimumClearance$MinClearanceDistance;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v3

    iput-object v3, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    goto :goto_0
.end method

.method public static getDistance(Lcom/vividsolutions/jts/geom/Geometry;)D
    .locals 4
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 134
    new-instance v0, Lcom/vividsolutions/jts/precision/MinimumClearance;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/precision/MinimumClearance;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 135
    .local v0, "rp":Lcom/vividsolutions/jts/precision/MinimumClearance;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/precision/MinimumClearance;->getDistance()D

    move-result-wide v2

    return-wide v2
.end method

.method public static getLine(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 2
    .param p0, "g"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 149
    new-instance v0, Lcom/vividsolutions/jts/precision/MinimumClearance;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/precision/MinimumClearance;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 150
    .local v0, "rp":Lcom/vividsolutions/jts/precision/MinimumClearance;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/precision/MinimumClearance;->getLine()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getDistance()D
    .locals 2

    .prologue
    .line 180
    invoke-direct {p0}, Lcom/vividsolutions/jts/precision/MinimumClearance;->compute()V

    .line 181
    iget-wide v0, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->minClearance:D

    return-wide v0
.end method

.method public getLine()Lcom/vividsolutions/jts/geom/LineString;
    .locals 2

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/vividsolutions/jts/precision/MinimumClearance;->compute()V

    .line 199
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_1

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v1

    const/4 v0, 0x0

    check-cast v0, [Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    .line 201
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->inputGeom:Lcom/vividsolutions/jts/geom/Geometry;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v0

    iget-object v1, p0, Lcom/vividsolutions/jts/precision/MinimumClearance;->minClearancePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v0

    goto :goto_0
.end method
