.class public final Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequenceFactory;
.super Ljava/lang/Object;
.source "CoordinateArraySequenceFactory.java"

# interfaces
.implements Lcom/vividsolutions/jts/geom/CoordinateSequenceFactory;
.implements Ljava/io/Serializable;


# static fields
.field private static instanceObject:Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequenceFactory; = null

.field private static final serialVersionUID:J = -0x38e49fa6cf6f2ea9L


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequenceFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequenceFactory;-><init>()V

    sput-object v0, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequenceFactory;->instanceObject:Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequenceFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    return-void
.end method

.method public static instance()Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequenceFactory;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequenceFactory;->instanceObject:Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequenceFactory;

    return-object v0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 54
    invoke-static {}, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequenceFactory;->instance()Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequenceFactory;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public create(II)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 2
    .param p1, "size"    # I
    .param p2, "dimension"    # I

    .prologue
    .line 89
    const/4 v0, 0x3

    if-le p2, v0, :cond_0

    .line 90
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "dimension must be <= 3"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    new-instance v0, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequence;

    invoke-direct {v0, p1, p2}, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequence;-><init>(II)V

    return-object v0
.end method

.method public create(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 1
    .param p1, "coordSeq"    # Lcom/vividsolutions/jts/geom/CoordinateSequence;

    .prologue
    .line 80
    new-instance v0, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequence;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequence;-><init>(Lcom/vividsolutions/jts/geom/CoordinateSequence;)V

    return-object v0
.end method

.method public create([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/CoordinateSequence;
    .locals 1
    .param p1, "coordinates"    # [Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 73
    new-instance v0, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequence;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequence;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-object v0
.end method
