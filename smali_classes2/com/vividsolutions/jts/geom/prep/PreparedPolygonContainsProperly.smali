.class Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContainsProperly;
.super Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;
.source "PreparedPolygonContainsProperly.java"


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V
    .locals 0
    .param p1, "prepPoly"    # Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonPredicate;-><init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V

    .line 85
    return-void
.end method

.method public static containsProperly(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 2
    .param p0, "prep"    # Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 73
    new-instance v0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContainsProperly;

    invoke-direct {v0, p0}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContainsProperly;-><init>(Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;)V

    .line 74
    .local v0, "polyInt":Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContainsProperly;
    invoke-virtual {v0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContainsProperly;->containsProperly(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v1

    return v1
.end method


# virtual methods
.method public containsProperly(Lcom/vividsolutions/jts/geom/Geometry;)Z
    .locals 6
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    const/4 v4, 0x0

    .line 101
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContainsProperly;->isAllTestComponentsInTargetInterior(Lcom/vividsolutions/jts/geom/Geometry;)Z

    move-result v0

    .line 102
    .local v0, "isAllInPrepGeomAreaInterior":Z
    if-nez v0, :cond_1

    .line 123
    :cond_0
    :goto_0
    return v4

    .line 107
    :cond_1
    invoke-static {p1}, Lcom/vividsolutions/jts/noding/SegmentStringUtil;->extractSegmentStrings(Lcom/vividsolutions/jts/geom/Geometry;)Ljava/util/List;

    move-result-object v2

    .line 108
    .local v2, "lineSegStr":Ljava/util/List;
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContainsProperly;->prepPoly:Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getIntersectionFinder()Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/vividsolutions/jts/noding/FastSegmentSetIntersectionFinder;->intersects(Ljava/util/Collection;)Z

    move-result v3

    .line 109
    .local v3, "segsIntersect":Z
    if-nez v3, :cond_0

    .line 117
    instance-of v5, p1, Lcom/vividsolutions/jts/geom/Polygonal;

    if-eqz v5, :cond_2

    .line 119
    iget-object v5, p0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContainsProperly;->prepPoly:Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    invoke-virtual {v5}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;->getRepresentativePoints()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p0, p1, v5}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygonContainsProperly;->isAnyTargetComponentInAreaTest(Lcom/vividsolutions/jts/geom/Geometry;Ljava/util/List;)Z

    move-result v1

    .line 120
    .local v1, "isTargetGeomInTestArea":Z
    if-nez v1, :cond_0

    .line 123
    .end local v1    # "isTargetGeomInTestArea":Z
    :cond_2
    const/4 v4, 0x1

    goto :goto_0
.end method
