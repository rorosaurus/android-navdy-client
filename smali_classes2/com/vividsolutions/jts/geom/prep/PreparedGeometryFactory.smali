.class public Lcom/vividsolutions/jts/geom/prep/PreparedGeometryFactory;
.super Ljava/lang/Object;
.source "PreparedGeometryFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    return-void
.end method

.method public static prepare(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/prep/PreparedGeometry;
    .locals 1
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 59
    new-instance v0, Lcom/vividsolutions/jts/geom/prep/PreparedGeometryFactory;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/prep/PreparedGeometryFactory;-><init>()V

    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/geom/prep/PreparedGeometryFactory;->create(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/prep/PreparedGeometry;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public create(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/prep/PreparedGeometry;
    .locals 1
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 73
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Polygonal;

    if-eqz v0, :cond_0

    .line 74
    new-instance v0, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;

    check-cast p1, Lcom/vividsolutions/jts/geom/Polygonal;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPolygon;-><init>(Lcom/vividsolutions/jts/geom/Polygonal;)V

    .line 83
    :goto_0
    return-object v0

    .line 75
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Lineal;

    if-eqz v0, :cond_1

    .line 76
    new-instance v0, Lcom/vividsolutions/jts/geom/prep/PreparedLineString;

    check-cast p1, Lcom/vividsolutions/jts/geom/Lineal;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedLineString;-><init>(Lcom/vividsolutions/jts/geom/Lineal;)V

    goto :goto_0

    .line 77
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    instance-of v0, p1, Lcom/vividsolutions/jts/geom/Puntal;

    if-eqz v0, :cond_2

    .line 78
    new-instance v0, Lcom/vividsolutions/jts/geom/prep/PreparedPoint;

    check-cast p1, Lcom/vividsolutions/jts/geom/Puntal;

    .end local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/prep/PreparedPoint;-><init>(Lcom/vividsolutions/jts/geom/Puntal;)V

    goto :goto_0

    .line 83
    .restart local p1    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_2
    new-instance v0, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/prep/BasicPreparedGeometry;-><init>(Lcom/vividsolutions/jts/geom/Geometry;)V

    goto :goto_0
.end method
