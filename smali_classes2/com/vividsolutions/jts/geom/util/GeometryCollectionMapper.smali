.class public Lcom/vividsolutions/jts/geom/util/GeometryCollectionMapper;
.super Ljava/lang/Object;
.source "GeometryCollectionMapper.java"


# instance fields
.field private mapOp:Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;)V
    .locals 1
    .param p1, "mapOp"    # Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/geom/util/GeometryCollectionMapper;->mapOp:Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;

    .line 59
    iput-object p1, p0, Lcom/vividsolutions/jts/geom/util/GeometryCollectionMapper;->mapOp:Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;

    .line 60
    return-void
.end method

.method public static map(Lcom/vividsolutions/jts/geom/GeometryCollection;Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;)Lcom/vividsolutions/jts/geom/GeometryCollection;
    .locals 2
    .param p0, "gc"    # Lcom/vividsolutions/jts/geom/GeometryCollection;
    .param p1, "op"    # Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;

    .prologue
    .line 52
    new-instance v0, Lcom/vividsolutions/jts/geom/util/GeometryCollectionMapper;

    invoke-direct {v0, p1}, Lcom/vividsolutions/jts/geom/util/GeometryCollectionMapper;-><init>(Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;)V

    .line 53
    .local v0, "mapper":Lcom/vividsolutions/jts/geom/util/GeometryCollectionMapper;
    invoke-virtual {v0, p0}, Lcom/vividsolutions/jts/geom/util/GeometryCollectionMapper;->map(Lcom/vividsolutions/jts/geom/GeometryCollection;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public map(Lcom/vividsolutions/jts/geom/GeometryCollection;)Lcom/vividsolutions/jts/geom/GeometryCollection;
    .locals 5
    .param p1, "gc"    # Lcom/vividsolutions/jts/geom/GeometryCollection;

    .prologue
    .line 64
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .local v2, "mapped":Ljava/util/List;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getNumGeometries()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 66
    iget-object v3, p0, Lcom/vividsolutions/jts/geom/util/GeometryCollectionMapper;->mapOp:Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;

    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;->map(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 67
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 68
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 70
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/GeometryCollection;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v3

    invoke-static {v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->toGeometryArray(Ljava/util/Collection;)[Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createGeometryCollection([Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/GeometryCollection;

    move-result-object v3

    return-object v3
.end method
