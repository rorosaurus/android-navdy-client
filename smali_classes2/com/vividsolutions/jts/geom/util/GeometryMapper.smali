.class public Lcom/vividsolutions/jts/geom/util/GeometryMapper;
.super Ljava/lang/Object;
.source "GeometryMapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    return-void
.end method

.method public static map(Lcom/vividsolutions/jts/geom/Geometry;Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;)Lcom/vividsolutions/jts/geom/Geometry;
    .locals 4
    .param p0, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;
    .param p1, "op"    # Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;

    .prologue
    .line 62
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 63
    .local v2, "mapped":Ljava/util/List;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 64
    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    invoke-interface {p1, v3}, Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;->map(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 65
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    if-eqz v0, :cond_0

    .line 66
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 68
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/vividsolutions/jts/geom/GeometryFactory;->buildGeometry(Ljava/util/Collection;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    return-object v3
.end method

.method public static map(Ljava/util/Collection;Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;)Ljava/util/Collection;
    .locals 5
    .param p0, "geoms"    # Ljava/util/Collection;
    .param p1, "op"    # Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;

    .prologue
    .line 73
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v3, "mapped":Ljava/util/List;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 75
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 76
    .local v0, "g":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-interface {p1, v0}, Lcom/vividsolutions/jts/geom/util/GeometryMapper$MapOp;->map(Lcom/vividsolutions/jts/geom/Geometry;)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v1

    .line 77
    .local v1, "gr":Lcom/vividsolutions/jts/geom/Geometry;
    if-eqz v1, :cond_0

    .line 78
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 80
    .end local v0    # "g":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v1    # "gr":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    return-object v3
.end method
