.class public abstract Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;
.super Ljava/lang/Object;
.source "ShortCircuitedGeometryVisitor.java"


# instance fields
.field private isDone:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;->isDone:Z

    .line 49
    return-void
.end method


# virtual methods
.method public applyTo(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 3
    .param p1, "geom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 52
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-boolean v2, p0, Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;->isDone:Z

    if-nez v2, :cond_0

    .line 53
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 54
    .local v0, "element":Lcom/vividsolutions/jts/geom/Geometry;
    instance-of v2, v0, Lcom/vividsolutions/jts/geom/GeometryCollection;

    if-nez v2, :cond_1

    .line 55
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;->visit(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 56
    invoke-virtual {p0}, Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;->isDone()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 57
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;->isDone:Z

    .line 64
    .end local v0    # "element":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-void

    .line 62
    .restart local v0    # "element":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_1
    invoke-virtual {p0, v0}, Lcom/vividsolutions/jts/geom/util/ShortCircuitedGeometryVisitor;->applyTo(Lcom/vividsolutions/jts/geom/Geometry;)V

    .line 52
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected abstract isDone()Z
.end method

.method protected abstract visit(Lcom/vividsolutions/jts/geom/Geometry;)V
.end method
