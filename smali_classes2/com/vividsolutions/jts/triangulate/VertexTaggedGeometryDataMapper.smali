.class public Lcom/vividsolutions/jts/triangulate/VertexTaggedGeometryDataMapper;
.super Ljava/lang/Object;
.source "VertexTaggedGeometryDataMapper.java"


# instance fields
.field private coordDataMap:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/VertexTaggedGeometryDataMapper;->coordDataMap:Ljava/util/Map;

    .line 60
    return-void
.end method

.method private loadVertices([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V
    .locals 3
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 80
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/VertexTaggedGeometryDataMapper;->coordDataMap:Ljava/util/Map;

    aget-object v2, p1, v0

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_0
    return-void
.end method


# virtual methods
.method public getCoordinates()Ljava/util/List;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/VertexTaggedGeometryDataMapper;->coordDataMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public loadSourceGeometries(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 4
    .param p1, "geomColl"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 72
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 73
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 74
    .local v0, "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getUserData()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/vividsolutions/jts/triangulate/VertexTaggedGeometryDataMapper;->loadVertices([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 76
    .end local v0    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-void
.end method

.method public loadSourceGeometries(Ljava/util/Collection;)V
    .locals 4
    .param p1, "geoms"    # Ljava/util/Collection;

    .prologue
    .line 64
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 65
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 66
    .local v0, "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v2

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getUserData()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/vividsolutions/jts/triangulate/VertexTaggedGeometryDataMapper;->loadVertices([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V

    goto :goto_0

    .line 68
    .end local v0    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-void
.end method

.method public transferData(Lcom/vividsolutions/jts/geom/Geometry;)V
    .locals 4
    .param p1, "targetGeom"    # Lcom/vividsolutions/jts/geom/Geometry;

    .prologue
    .line 101
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/vividsolutions/jts/geom/Geometry;->getNumGeometries()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 102
    invoke-virtual {p1, v1}, Lcom/vividsolutions/jts/geom/Geometry;->getGeometryN(I)Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v0

    .line 103
    .local v0, "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getUserData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/geom/Coordinate;

    .line 104
    .local v2, "vertexKey":Lcom/vividsolutions/jts/geom/Coordinate;
    if-nez v2, :cond_0

    .line 101
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 105
    :cond_0
    iget-object v3, p0, Lcom/vividsolutions/jts/triangulate/VertexTaggedGeometryDataMapper;->coordDataMap:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/vividsolutions/jts/geom/Geometry;->setUserData(Ljava/lang/Object;)V

    goto :goto_1

    .line 107
    .end local v0    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    .end local v2    # "vertexKey":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    return-void
.end method
