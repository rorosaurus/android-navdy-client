.class public Lcom/vividsolutions/jts/triangulate/Segment;
.super Ljava/lang/Object;
.source "Segment.java"


# instance fields
.field private data:Ljava/lang/Object;

.field private ls:Lcom/vividsolutions/jts/geom/LineSegment;


# direct methods
.method public constructor <init>(DDDDDD)V
    .locals 11
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "z1"    # D
    .param p7, "x2"    # D
    .param p9, "y2"    # D
    .param p11, "z2"    # D

    .prologue
    .line 56
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    move-wide v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    new-instance v3, Lcom/vividsolutions/jts/geom/Coordinate;

    move-wide/from16 v4, p7

    move-wide/from16 v6, p9

    move-wide/from16 v8, p11

    invoke-direct/range {v3 .. v9}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    invoke-direct {p0, v1, v3}, Lcom/vividsolutions/jts/triangulate/Segment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 57
    return-void
.end method

.method public constructor <init>(DDDDDDLjava/lang/Object;)V
    .locals 13
    .param p1, "x1"    # D
    .param p3, "y1"    # D
    .param p5, "z1"    # D
    .param p7, "x2"    # D
    .param p9, "y2"    # D
    .param p11, "z2"    # D
    .param p13, "data"    # Ljava/lang/Object;

    .prologue
    .line 63
    new-instance v3, Lcom/vividsolutions/jts/geom/Coordinate;

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v3 .. v9}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    new-instance v5, Lcom/vividsolutions/jts/geom/Coordinate;

    move-wide/from16 v6, p7

    move-wide/from16 v8, p9

    move-wide/from16 v10, p11

    invoke-direct/range {v5 .. v11}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DDD)V

    move-object/from16 v0, p13

    invoke-direct {p0, v3, v5, v0}, Lcom/vividsolutions/jts/triangulate/Segment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V

    .line 64
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V
    .locals 1
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/Segment;->data:Ljava/lang/Object;

    .line 85
    new-instance v0, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v0, p1, p2}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V
    .locals 1
    .param p1, "p0"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "p1"    # Lcom/vividsolutions/jts/geom/Coordinate;
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/Segment;->data:Ljava/lang/Object;

    .line 74
    new-instance v0, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v0, p1, p2}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    iput-object v0, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    .line 75
    iput-object p3, p0, Lcom/vividsolutions/jts/triangulate/Segment;->data:Ljava/lang/Object;

    .line 76
    return-void
.end method


# virtual methods
.method public equalsTopo(Lcom/vividsolutions/jts/triangulate/Segment;)Z
    .locals 2
    .param p1, "s"    # Lcom/vividsolutions/jts/triangulate/Segment;

    .prologue
    .line 201
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/Segment;->getLineSegment()Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;->equalsTopo(Lcom/vividsolutions/jts/geom/LineSegment;)Z

    move-result v0

    return v0
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/Segment;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public getEnd()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getEndX()D
    .locals 4

    .prologue
    .line 142
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 143
    .local v0, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    return-wide v2
.end method

.method public getEndY()D
    .locals 4

    .prologue
    .line 152
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 153
    .local v0, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    return-wide v2
.end method

.method public getEndZ()D
    .locals 4

    .prologue
    .line 162
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 163
    .local v0, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    return-wide v2
.end method

.method public getLineSegment()Lcom/vividsolutions/jts/geom/LineSegment;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    return-object v0
.end method

.method public getStart()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getStartX()D
    .locals 4

    .prologue
    .line 112
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 113
    .local v0, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    return-wide v2
.end method

.method public getStartY()D
    .locals 4

    .prologue
    .line 122
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 123
    .local v0, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    return-wide v2
.end method

.method public getStartZ()D
    .locals 4

    .prologue
    .line 132
    iget-object v1, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/vividsolutions/jts/geom/LineSegment;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 133
    .local v0, "p":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->z:D

    return-wide v2
.end method

.method public intersection(Lcom/vividsolutions/jts/triangulate/Segment;)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 2
    .param p1, "s"    # Lcom/vividsolutions/jts/triangulate/Segment;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-virtual {p1}, Lcom/vividsolutions/jts/triangulate/Segment;->getLineSegment()Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/LineSegment;->intersection(Lcom/vividsolutions/jts/geom/LineSegment;)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/vividsolutions/jts/triangulate/Segment;->data:Ljava/lang/Object;

    .line 191
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/vividsolutions/jts/triangulate/Segment;->ls:Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/LineSegment;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
