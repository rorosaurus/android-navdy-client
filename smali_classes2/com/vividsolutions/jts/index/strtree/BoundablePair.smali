.class Lcom/vividsolutions/jts/index/strtree/BoundablePair;
.super Ljava/lang/Object;
.source "BoundablePair.java"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field private boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

.field private boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

.field private distance:D

.field private itemDistance:Lcom/vividsolutions/jts/index/strtree/ItemDistance;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/index/strtree/ItemDistance;)V
    .locals 2
    .param p1, "boundable1"    # Lcom/vividsolutions/jts/index/strtree/Boundable;
    .param p2, "boundable2"    # Lcom/vividsolutions/jts/index/strtree/Boundable;
    .param p3, "itemDistance"    # Lcom/vividsolutions/jts/index/strtree/ItemDistance;

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

    .line 66
    iput-object p2, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

    .line 67
    iput-object p3, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->itemDistance:Lcom/vividsolutions/jts/index/strtree/ItemDistance;

    .line 68
    invoke-direct {p0}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->distance()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->distance:D

    .line 69
    return-void
.end method

.method private static area(Lcom/vividsolutions/jts/index/strtree/Boundable;)D
    .locals 2
    .param p0, "b"    # Lcom/vividsolutions/jts/index/strtree/Boundable;

    .prologue
    .line 175
    invoke-interface {p0}, Lcom/vividsolutions/jts/index/strtree/Boundable;->getBounds()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getArea()D

    move-result-wide v0

    return-wide v0
.end method

.method private distance()D
    .locals 3

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->isLeaves()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v2, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->itemDistance:Lcom/vividsolutions/jts/index/strtree/ItemDistance;

    iget-object v0, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

    check-cast v0, Lcom/vividsolutions/jts/index/strtree/ItemBoundable;

    iget-object v1, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

    check-cast v1, Lcom/vividsolutions/jts/index/strtree/ItemBoundable;

    invoke-interface {v2, v0, v1}, Lcom/vividsolutions/jts/index/strtree/ItemDistance;->distance(Lcom/vividsolutions/jts/index/strtree/ItemBoundable;Lcom/vividsolutions/jts/index/strtree/ItemBoundable;)D

    move-result-wide v0

    .line 101
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

    invoke-interface {v0}, Lcom/vividsolutions/jts/index/strtree/Boundable;->getBounds()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Envelope;

    iget-object v1, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

    invoke-interface {v1}, Lcom/vividsolutions/jts/index/strtree/Boundable;->getBounds()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Envelope;->distance(Lcom/vividsolutions/jts/geom/Envelope;)D

    move-result-wide v0

    goto :goto_0
.end method

.method private expand(Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/util/PriorityQueue;D)V
    .locals 6
    .param p1, "bndComposite"    # Lcom/vividsolutions/jts/index/strtree/Boundable;
    .param p2, "bndOther"    # Lcom/vividsolutions/jts/index/strtree/Boundable;
    .param p3, "priQ"    # Lcom/vividsolutions/jts/util/PriorityQueue;
    .param p4, "minDistance"    # D

    .prologue
    .line 220
    check-cast p1, Lcom/vividsolutions/jts/index/strtree/AbstractNode;

    .end local p1    # "bndComposite":Lcom/vividsolutions/jts/index/strtree/Boundable;
    invoke-virtual {p1}, Lcom/vividsolutions/jts/index/strtree/AbstractNode;->getChildBoundables()Ljava/util/List;

    move-result-object v2

    .line 221
    .local v2, "children":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 222
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/index/strtree/Boundable;

    .line 223
    .local v1, "child":Lcom/vividsolutions/jts/index/strtree/Boundable;
    new-instance v0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;

    iget-object v4, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->itemDistance:Lcom/vividsolutions/jts/index/strtree/ItemDistance;

    invoke-direct {v0, v1, p2, v4}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;-><init>(Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/index/strtree/ItemDistance;)V

    .line 226
    .local v0, "bp":Lcom/vividsolutions/jts/index/strtree/BoundablePair;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->getDistance()D

    move-result-wide v4

    cmpg-double v4, v4, p4

    if-gez v4, :cond_0

    .line 227
    invoke-virtual {p3, v0}, Lcom/vividsolutions/jts/util/PriorityQueue;->add(Ljava/lang/Comparable;)V

    goto :goto_0

    .line 230
    .end local v0    # "bp":Lcom/vividsolutions/jts/index/strtree/BoundablePair;
    .end local v1    # "child":Lcom/vividsolutions/jts/index/strtree/Boundable;
    :cond_1
    return-void
.end method

.method public static isComposite(Ljava/lang/Object;)Z
    .locals 1
    .param p0, "item"    # Ljava/lang/Object;

    .prologue
    .line 170
    instance-of v0, p0, Lcom/vividsolutions/jts/index/strtree/AbstractNode;

    return v0
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 152
    move-object v0, p1

    check-cast v0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;

    .line 153
    .local v0, "nd":Lcom/vividsolutions/jts/index/strtree/BoundablePair;
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->distance:D

    iget-wide v4, v0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->distance:D

    cmpg-double v1, v2, v4

    if-gez v1, :cond_0

    const/4 v1, -0x1

    .line 155
    :goto_0
    return v1

    .line 154
    :cond_0
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->distance:D

    iget-wide v4, v0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->distance:D

    cmpl-double v1, v2, v4

    if-lez v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    .line 155
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public expandToQueue(Lcom/vividsolutions/jts/util/PriorityQueue;D)V
    .locals 8
    .param p1, "priQ"    # Lcom/vividsolutions/jts/util/PriorityQueue;
    .param p2, "minDistance"    # D

    .prologue
    .line 187
    iget-object v0, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

    invoke-static {v0}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->isComposite(Ljava/lang/Object;)Z

    move-result v6

    .line 188
    .local v6, "isComp1":Z
    iget-object v0, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

    invoke-static {v0}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->isComposite(Ljava/lang/Object;)Z

    move-result v7

    .line 195
    .local v7, "isComp2":Z
    if-eqz v6, :cond_1

    if-eqz v7, :cond_1

    .line 196
    iget-object v0, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

    invoke-static {v0}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->area(Lcom/vividsolutions/jts/index/strtree/Boundable;)D

    move-result-wide v0

    iget-object v2, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

    invoke-static {v2}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->area(Lcom/vividsolutions/jts/index/strtree/Boundable;)D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 197
    iget-object v1, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

    iget-object v2, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->expand(Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/util/PriorityQueue;D)V

    .line 211
    :goto_0
    return-void

    .line 201
    :cond_0
    iget-object v1, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

    iget-object v2, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->expand(Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/util/PriorityQueue;D)V

    goto :goto_0

    .line 205
    :cond_1
    if-eqz v6, :cond_2

    .line 206
    iget-object v1, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

    iget-object v2, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->expand(Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/util/PriorityQueue;D)V

    goto :goto_0

    .line 209
    :cond_2
    if-eqz v7, :cond_3

    .line 210
    iget-object v1, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

    iget-object v2, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->expand(Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/index/strtree/Boundable;Lcom/vividsolutions/jts/util/PriorityQueue;D)V

    goto :goto_0

    .line 214
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "neither boundable is composite"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getBoundable(I)Lcom/vividsolutions/jts/index/strtree/Boundable;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 80
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

    .line 81
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

    goto :goto_0
.end method

.method public getDistance()D
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->distance:D

    return-wide v0
.end method

.method public isLeaves()Z
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable1:Lcom/vividsolutions/jts/index/strtree/Boundable;

    invoke-static {v0}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->isComposite(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->boundable2:Lcom/vividsolutions/jts/index/strtree/Boundable;

    invoke-static {v0}, Lcom/vividsolutions/jts/index/strtree/BoundablePair;->isComposite(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
