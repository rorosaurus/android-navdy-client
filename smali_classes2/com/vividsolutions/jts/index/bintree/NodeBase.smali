.class public abstract Lcom/vividsolutions/jts/index/bintree/NodeBase;
.super Ljava/lang/Object;
.source "NodeBase.java"


# instance fields
.field protected items:Ljava/util/List;

.field protected subnode:[Lcom/vividsolutions/jts/index/bintree/Node;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->items:Ljava/util/List;

    .line 69
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/vividsolutions/jts/index/bintree/Node;

    iput-object v0, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    .line 72
    return-void
.end method

.method public static getSubnodeIndex(Lcom/vividsolutions/jts/index/bintree/Interval;D)I
    .locals 5
    .param p0, "interval"    # Lcom/vividsolutions/jts/index/bintree/Interval;
    .param p1, "centre"    # D

    .prologue
    .line 56
    const/4 v0, -0x1

    .line 57
    .local v0, "subnodeIndex":I
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/bintree/Interval;->min:D

    cmpl-double v1, v2, p1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    .line 58
    :cond_0
    iget-wide v2, p0, Lcom/vividsolutions/jts/index/bintree/Interval;->max:D

    cmpg-double v1, v2, p1

    if-gtz v1, :cond_1

    const/4 v0, 0x0

    .line 59
    :cond_1
    return v0
.end method


# virtual methods
.method public add(Ljava/lang/Object;)V
    .locals 1
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    return-void
.end method

.method public addAllItems(Ljava/util/List;)Ljava/util/List;
    .locals 2
    .param p1, "items"    # Ljava/util/List;

    .prologue
    .line 82
    iget-object v1, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->items:Ljava/util/List;

    invoke-interface {p1, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 83
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 84
    iget-object v1, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lcom/vividsolutions/jts/index/bintree/Node;->addAllItems(Ljava/util/List;)Ljava/util/List;

    .line 83
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :cond_1
    return-object p1
.end method

.method public addAllItemsFromOverlapping(Lcom/vividsolutions/jts/index/bintree/Interval;Ljava/util/Collection;)V
    .locals 3
    .param p1, "interval"    # Lcom/vividsolutions/jts/index/bintree/Interval;
    .param p2, "resultItems"    # Ljava/util/Collection;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 102
    if-eqz p1, :cond_1

    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/index/bintree/NodeBase;->isSearchMatch(Lcom/vividsolutions/jts/index/bintree/Interval;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->items:Ljava/util/List;

    invoke-interface {p2, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 108
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v0, v0, v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v0, v0, v1

    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/index/bintree/Node;->addAllItemsFromOverlapping(Lcom/vividsolutions/jts/index/bintree/Interval;Ljava/util/Collection;)V

    .line 109
    :cond_2
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v0, v0, v2

    invoke-virtual {v0, p1, p2}, Lcom/vividsolutions/jts/index/bintree/Node;->addAllItemsFromOverlapping(Lcom/vividsolutions/jts/index/bintree/Interval;Ljava/util/Collection;)V

    goto :goto_0
.end method

.method depth()I
    .locals 4

    .prologue
    .line 162
    const/4 v1, 0x0

    .line 163
    .local v1, "maxSubDepth":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v3, 0x2

    if-ge v0, v3, :cond_1

    .line 164
    iget-object v3, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v3, v3, v0

    if-eqz v3, :cond_0

    .line 165
    iget-object v3, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/vividsolutions/jts/index/bintree/Node;->depth()I

    move-result v2

    .line 166
    .local v2, "sqd":I
    if-le v2, v1, :cond_0

    .line 167
    move v1, v2

    .line 163
    .end local v2    # "sqd":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 170
    :cond_1
    add-int/lit8 v3, v1, 0x1

    return v3
.end method

.method public getItems()Ljava/util/List;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->items:Ljava/util/List;

    return-object v0
.end method

.method public hasChildren()Z
    .locals 2

    .prologue
    .line 151
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 152
    iget-object v1, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 153
    const/4 v1, 0x1

    .line 155
    :goto_1
    return v1

    .line 151
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public hasItems()Z
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPrunable()Z
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/vividsolutions/jts/index/bintree/NodeBase;->hasChildren()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/vividsolutions/jts/index/bintree/NodeBase;->hasItems()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract isSearchMatch(Lcom/vividsolutions/jts/index/bintree/Interval;)Z
.end method

.method nodeSize()I
    .locals 3

    .prologue
    .line 186
    const/4 v1, 0x0

    .line 187
    .local v1, "subSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_1

    .line 188
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 189
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/index/bintree/Node;->nodeSize()I

    move-result v2

    add-int/2addr v1, v2

    .line 187
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 192
    :cond_1
    add-int/lit8 v2, v1, 0x1

    return v2
.end method

.method public remove(Lcom/vividsolutions/jts/index/bintree/Interval;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "itemInterval"    # Lcom/vividsolutions/jts/index/bintree/Interval;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/index/bintree/NodeBase;->isSearchMatch(Lcom/vividsolutions/jts/index/bintree/Interval;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 123
    const/4 v0, 0x0

    .line 141
    :cond_0
    :goto_0
    return v0

    .line 125
    :cond_1
    const/4 v0, 0x0

    .line 126
    .local v0, "found":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v2, 0x2

    if-ge v1, v2, :cond_2

    .line 127
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v2, v2, v1

    if-eqz v2, :cond_3

    .line 128
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1, p2}, Lcom/vividsolutions/jts/index/bintree/Node;->remove(Lcom/vividsolutions/jts/index/bintree/Interval;Ljava/lang/Object;)Z

    move-result v0

    .line 129
    if-eqz v0, :cond_3

    .line 131
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/vividsolutions/jts/index/bintree/Node;->isPrunable()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 132
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    const/4 v3, 0x0

    aput-object v3, v2, v1

    .line 138
    :cond_2
    if-nez v0, :cond_0

    .line 140
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->items:Ljava/util/List;

    invoke-interface {v2, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 141
    goto :goto_0

    .line 126
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method size()I
    .locals 3

    .prologue
    .line 175
    const/4 v1, 0x0

    .line 176
    .local v1, "subSize":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_1

    .line 177
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 178
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->subnode:[Lcom/vividsolutions/jts/index/bintree/Node;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/vividsolutions/jts/index/bintree/Node;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 176
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_1
    iget-object v2, p0, Lcom/vividsolutions/jts/index/bintree/NodeBase;->items:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v2, v1

    return v2
.end method
