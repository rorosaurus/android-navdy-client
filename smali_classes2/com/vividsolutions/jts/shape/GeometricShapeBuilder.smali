.class public abstract Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;
.super Ljava/lang/Object;
.source "GeometricShapeBuilder.java"


# instance fields
.field protected extent:Lcom/vividsolutions/jts/geom/Envelope;

.field protected geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field protected numPts:I


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/GeometryFactory;)V
    .locals 10
    .param p1, "geomFactory"    # Lcom/vividsolutions/jts/geom/GeometryFactory;

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v1, Lcom/vividsolutions/jts/geom/Envelope;

    move-wide v6, v2

    move-wide v8, v4

    invoke-direct/range {v1 .. v9}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(DDDD)V

    iput-object v1, p0, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->extent:Lcom/vividsolutions/jts/geom/Envelope;

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->numPts:I

    .line 46
    iput-object p1, p0, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 47
    return-void
.end method


# virtual methods
.method protected createCoord(DD)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 3
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 105
    new-instance v0, Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 106
    .local v0, "pt":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v1, p0, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->geomFactory:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/GeometryFactory;->getPrecisionModel()Lcom/vividsolutions/jts/geom/PrecisionModel;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/vividsolutions/jts/geom/PrecisionModel;->makePrecise(Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 107
    return-object v0
.end method

.method public getCentre()Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->extent:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->centre()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    return-object v0
.end method

.method public getDiameter()D
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->extent:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Envelope;->getHeight()D

    move-result-wide v0

    iget-object v2, p0, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->extent:Lcom/vividsolutions/jts/geom/Envelope;

    invoke-virtual {v2}, Lcom/vividsolutions/jts/geom/Envelope;->getWidth()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public getExtent()Lcom/vividsolutions/jts/geom/Envelope;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->extent:Lcom/vividsolutions/jts/geom/Envelope;

    return-object v0
.end method

.method public abstract getGeometry()Lcom/vividsolutions/jts/geom/Geometry;
.end method

.method public getRadius()D
    .locals 4

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->getDiameter()D

    move-result-wide v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public getSquareBaseLine()Lcom/vividsolutions/jts/geom/LineSegment;
    .locals 10

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->getRadius()D

    move-result-wide v4

    .line 78
    .local v4, "radius":D
    invoke-virtual {p0}, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->getCentre()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 79
    .local v0, "centre":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v1, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v6, v4

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v8, v4

    invoke-direct {v1, v6, v7, v8, v9}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 80
    .local v1, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v2, Lcom/vividsolutions/jts/geom/Coordinate;

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v6, v4

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v8, v4

    invoke-direct {v2, v6, v7, v8, v9}, Lcom/vividsolutions/jts/geom/Coordinate;-><init>(DD)V

    .line 81
    .local v2, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v3, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v3, v1, v2}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    return-object v3
.end method

.method public getSquareExtent()Lcom/vividsolutions/jts/geom/Envelope;
    .locals 12

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->getRadius()D

    move-result-wide v10

    .line 88
    .local v10, "radius":D
    invoke-virtual {p0}, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->getCentre()Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    .line 89
    .local v0, "centre":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v1, Lcom/vividsolutions/jts/geom/Envelope;

    iget-wide v2, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    sub-double/2addr v2, v10

    iget-wide v4, v0, Lcom/vividsolutions/jts/geom/Coordinate;->x:D

    add-double/2addr v4, v10

    iget-wide v6, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    sub-double/2addr v6, v10

    iget-wide v8, v0, Lcom/vividsolutions/jts/geom/Coordinate;->y:D

    add-double/2addr v8, v10

    invoke-direct/range {v1 .. v9}, Lcom/vividsolutions/jts/geom/Envelope;-><init>(DDDD)V

    return-object v1
.end method

.method public setExtent(Lcom/vividsolutions/jts/geom/Envelope;)V
    .locals 0
    .param p1, "extent"    # Lcom/vividsolutions/jts/geom/Envelope;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->extent:Lcom/vividsolutions/jts/geom/Envelope;

    .line 52
    return-void
.end method

.method public setNumPoints(I)V
    .locals 0
    .param p1, "numPts"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/vividsolutions/jts/shape/GeometricShapeBuilder;->numPts:I

    return-void
.end method
