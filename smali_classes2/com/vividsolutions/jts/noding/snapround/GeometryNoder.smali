.class public Lcom/vividsolutions/jts/noding/snapround/GeometryNoder;
.super Ljava/lang/Object;
.source "GeometryNoder.java"


# instance fields
.field private geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

.field private isValidityChecked:Z

.field private pm:Lcom/vividsolutions/jts/geom/PrecisionModel;


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/geom/PrecisionModel;)V
    .locals 1
    .param p1, "pm"    # Lcom/vividsolutions/jts/geom/PrecisionModel;

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/vividsolutions/jts/noding/snapround/GeometryNoder;->isValidityChecked:Z

    .line 72
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/snapround/GeometryNoder;->pm:Lcom/vividsolutions/jts/geom/PrecisionModel;

    .line 73
    return-void
.end method

.method private extractLines(Ljava/util/Collection;)Ljava/util/List;
    .locals 5
    .param p1, "geoms"    # Ljava/util/Collection;

    .prologue
    .line 127
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v3, "lines":Ljava/util/List;
    new-instance v2, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;

    invoke-direct {v2, v3}, Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;-><init>(Ljava/util/Collection;)V

    .line 129
    .local v2, "lce":Lcom/vividsolutions/jts/geom/util/LinearComponentExtracter;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 130
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 131
    .local v0, "geom":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0, v2}, Lcom/vividsolutions/jts/geom/Geometry;->apply(Lcom/vividsolutions/jts/geom/GeometryComponentFilter;)V

    goto :goto_0

    .line 133
    .end local v0    # "geom":Lcom/vividsolutions/jts/geom/Geometry;
    :cond_0
    return-object v3
.end method

.method private toLineStrings(Ljava/util/Collection;)Ljava/util/List;
    .locals 5
    .param p1, "segStrings"    # Ljava/util/Collection;

    .prologue
    .line 114
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 115
    .local v1, "lines":Ljava/util/List;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 116
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/vividsolutions/jts/noding/SegmentString;

    .line 118
    .local v2, "ss":Lcom/vividsolutions/jts/noding/SegmentString;
    invoke-interface {v2}, Lcom/vividsolutions/jts/noding/SegmentString;->size()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    .line 120
    iget-object v3, p0, Lcom/vividsolutions/jts/noding/snapround/GeometryNoder;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    invoke-interface {v2}, Lcom/vividsolutions/jts/noding/SegmentString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/vividsolutions/jts/geom/GeometryFactory;->createLineString([Lcom/vividsolutions/jts/geom/Coordinate;)Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 122
    .end local v2    # "ss":Lcom/vividsolutions/jts/noding/SegmentString;
    :cond_1
    return-object v1
.end method

.method private toSegmentStrings(Ljava/util/Collection;)Ljava/util/List;
    .locals 6
    .param p1, "lines"    # Ljava/util/Collection;

    .prologue
    .line 138
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 139
    .local v2, "segStrings":Ljava/util/List;
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "it":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 140
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LineString;

    .line 141
    .local v1, "line":Lcom/vividsolutions/jts/geom/LineString;
    new-instance v3, Lcom/vividsolutions/jts/noding/NodedSegmentString;

    invoke-virtual {v1}, Lcom/vividsolutions/jts/geom/LineString;->getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/vividsolutions/jts/noding/NodedSegmentString;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 143
    .end local v1    # "line":Lcom/vividsolutions/jts/geom/LineString;
    :cond_0
    return-object v2
.end method


# virtual methods
.method public node(Ljava/util/Collection;)Ljava/util/List;
    .locals 6
    .param p1, "geoms"    # Ljava/util/Collection;

    .prologue
    .line 94
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/vividsolutions/jts/geom/Geometry;

    .line 95
    .local v0, "geom0":Lcom/vividsolutions/jts/geom/Geometry;
    invoke-virtual {v0}, Lcom/vividsolutions/jts/geom/Geometry;->getFactory()Lcom/vividsolutions/jts/geom/GeometryFactory;

    move-result-object v5

    iput-object v5, p0, Lcom/vividsolutions/jts/noding/snapround/GeometryNoder;->geomFact:Lcom/vividsolutions/jts/geom/GeometryFactory;

    .line 97
    invoke-direct {p0, p1}, Lcom/vividsolutions/jts/noding/snapround/GeometryNoder;->extractLines(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/vividsolutions/jts/noding/snapround/GeometryNoder;->toSegmentStrings(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v3

    .line 99
    .local v3, "segStrings":Ljava/util/List;
    new-instance v4, Lcom/vividsolutions/jts/noding/snapround/MCIndexSnapRounder;

    iget-object v5, p0, Lcom/vividsolutions/jts/noding/snapround/GeometryNoder;->pm:Lcom/vividsolutions/jts/geom/PrecisionModel;

    invoke-direct {v4, v5}, Lcom/vividsolutions/jts/noding/snapround/MCIndexSnapRounder;-><init>(Lcom/vividsolutions/jts/geom/PrecisionModel;)V

    .line 100
    .local v4, "sr":Lcom/vividsolutions/jts/noding/Noder;
    invoke-interface {v4, v3}, Lcom/vividsolutions/jts/noding/Noder;->computeNodes(Ljava/util/Collection;)V

    .line 101
    invoke-interface {v4}, Lcom/vividsolutions/jts/noding/Noder;->getNodedSubstrings()Ljava/util/Collection;

    move-result-object v1

    .line 104
    .local v1, "nodedLines":Ljava/util/Collection;
    iget-boolean v5, p0, Lcom/vividsolutions/jts/noding/snapround/GeometryNoder;->isValidityChecked:Z

    if-eqz v5, :cond_0

    .line 105
    new-instance v2, Lcom/vividsolutions/jts/noding/NodingValidator;

    invoke-direct {v2, v1}, Lcom/vividsolutions/jts/noding/NodingValidator;-><init>(Ljava/util/Collection;)V

    .line 106
    .local v2, "nv":Lcom/vividsolutions/jts/noding/NodingValidator;
    invoke-virtual {v2}, Lcom/vividsolutions/jts/noding/NodingValidator;->checkValid()V

    .line 109
    .end local v2    # "nv":Lcom/vividsolutions/jts/noding/NodingValidator;
    :cond_0
    invoke-direct {p0, v1}, Lcom/vividsolutions/jts/noding/snapround/GeometryNoder;->toLineStrings(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v5

    return-object v5
.end method

.method public setValidate(Z)V
    .locals 0
    .param p1, "isValidityChecked"    # Z

    .prologue
    .line 82
    iput-boolean p1, p0, Lcom/vividsolutions/jts/noding/snapround/GeometryNoder;->isValidityChecked:Z

    .line 83
    return-void
.end method
