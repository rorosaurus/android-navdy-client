.class public Lcom/vividsolutions/jts/noding/BasicSegmentString;
.super Ljava/lang/Object;
.source "BasicSegmentString.java"

# interfaces
.implements Lcom/vividsolutions/jts/noding/SegmentString;


# instance fields
.field private data:Ljava/lang/Object;

.field private pts:[Lcom/vividsolutions/jts/geom/Coordinate;


# direct methods
.method public constructor <init>([Lcom/vividsolutions/jts/geom/Coordinate;Ljava/lang/Object;)V
    .locals 0
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 69
    iput-object p2, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->data:Ljava/lang/Object;

    .line 70
    return-void
.end method


# virtual methods
.method public getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 87
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    return-object v0
.end method

.method public getData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->data:Ljava/lang/Object;

    return-object v0
.end method

.method public getSegmentOctant(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 104
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    .line 105
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/vividsolutions/jts/noding/BasicSegmentString;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {p0, v1}, Lcom/vividsolutions/jts/noding/BasicSegmentString;->getCoordinate(I)Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/vividsolutions/jts/noding/Octant;->octant(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)I

    move-result v0

    goto :goto_0
.end method

.method public isClosed()Z
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v2, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/vividsolutions/jts/geom/Coordinate;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public setData(Ljava/lang/Object;)V
    .locals 0
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->data:Ljava/lang/Object;

    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequence;

    iget-object v1, p0, Lcom/vividsolutions/jts/noding/BasicSegmentString;->pts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {v0, v1}, Lcom/vividsolutions/jts/geom/impl/CoordinateArraySequence;-><init>([Lcom/vividsolutions/jts/geom/Coordinate;)V

    invoke-static {v0}, Lcom/vividsolutions/jts/io/WKTWriter;->toLineString(Lcom/vividsolutions/jts/geom/CoordinateSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
