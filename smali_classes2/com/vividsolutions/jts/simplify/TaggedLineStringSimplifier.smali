.class public Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;
.super Ljava/lang/Object;
.source "TaggedLineStringSimplifier.java"


# instance fields
.field private distanceTolerance:D

.field private inputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

.field private li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

.field private line:Lcom/vividsolutions/jts/simplify/TaggedLineString;

.field private linePts:[Lcom/vividsolutions/jts/geom/Coordinate;

.field private outputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

.field private validSectionIndex:[I


# direct methods
.method public constructor <init>(Lcom/vividsolutions/jts/simplify/LineSegmentIndex;Lcom/vividsolutions/jts/simplify/LineSegmentIndex;)V
    .locals 2
    .param p1, "inputIndex"    # Lcom/vividsolutions/jts/simplify/LineSegmentIndex;
    .param p2, "outputIndex"    # Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;

    invoke-direct {v0}, Lcom/vividsolutions/jts/algorithm/RobustLineIntersector;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    .line 52
    new-instance v0, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    invoke-direct {v0}, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->inputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    .line 53
    new-instance v0, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    invoke-direct {v0}, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;-><init>()V

    iput-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->outputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    .line 56
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->distanceTolerance:D

    .line 169
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->validSectionIndex:[I

    .line 61
    iput-object p1, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->inputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    .line 62
    iput-object p2, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->outputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    .line 63
    return-void
.end method

.method private findFurthestPoint([Lcom/vividsolutions/jts/geom/Coordinate;II[D)I
    .locals 9
    .param p1, "pts"    # [Lcom/vividsolutions/jts/geom/Coordinate;
    .param p2, "i"    # I
    .param p3, "j"    # I
    .param p4, "maxDistance"    # [D

    .prologue
    .line 137
    new-instance v7, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v7}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>()V

    .line 138
    .local v7, "seg":Lcom/vividsolutions/jts/geom/LineSegment;
    aget-object v8, p1, p2

    iput-object v8, v7, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 139
    aget-object v8, p1, p3

    iput-object v8, v7, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 140
    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    .line 141
    .local v4, "maxDist":D
    move v3, p2

    .line 142
    .local v3, "maxIndex":I
    add-int/lit8 v2, p2, 0x1

    .local v2, "k":I
    :goto_0
    if-ge v2, p3, :cond_1

    .line 143
    aget-object v6, p1, v2

    .line 144
    .local v6, "midPt":Lcom/vividsolutions/jts/geom/Coordinate;
    invoke-virtual {v7, v6}, Lcom/vividsolutions/jts/geom/LineSegment;->distance(Lcom/vividsolutions/jts/geom/Coordinate;)D

    move-result-wide v0

    .line 145
    .local v0, "distance":D
    cmpl-double v8, v0, v4

    if-lez v8, :cond_0

    .line 146
    move-wide v4, v0

    .line 147
    move v3, v2

    .line 142
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 150
    .end local v0    # "distance":D
    .end local v6    # "midPt":Lcom/vividsolutions/jts/geom/Coordinate;
    :cond_1
    const/4 v8, 0x0

    aput-wide v4, p4, v8

    .line 151
    return v3
.end method

.method private flatten(II)Lcom/vividsolutions/jts/geom/LineSegment;
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 157
    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->linePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v1, v3, p1

    .line 158
    .local v1, "p0":Lcom/vividsolutions/jts/geom/Coordinate;
    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->linePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v2, v3, p2

    .line 159
    .local v2, "p1":Lcom/vividsolutions/jts/geom/Coordinate;
    new-instance v0, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v0, v1, v2}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 161
    .local v0, "newSeg":Lcom/vividsolutions/jts/geom/LineSegment;
    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->line:Lcom/vividsolutions/jts/simplify/TaggedLineString;

    invoke-direct {p0, v3, p1, p2}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->remove(Lcom/vividsolutions/jts/simplify/TaggedLineString;II)V

    .line 162
    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->outputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    invoke-virtual {v3, v0}, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;->add(Lcom/vividsolutions/jts/geom/LineSegment;)V

    .line 163
    return-object v0
.end method

.method private hasBadInputIntersection(Lcom/vividsolutions/jts/simplify/TaggedLineString;[ILcom/vividsolutions/jts/geom/LineSegment;)Z
    .locals 4
    .param p1, "parentLine"    # Lcom/vividsolutions/jts/simplify/TaggedLineString;
    .param p2, "sectionIndex"    # [I
    .param p3, "candidateSeg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 197
    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->inputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    invoke-virtual {v3, p3}, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;->query(Lcom/vividsolutions/jts/geom/LineSegment;)Ljava/util/List;

    move-result-object v2

    .line 198
    .local v2, "querySegs":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 199
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/simplify/TaggedLineSegment;

    .line 200
    .local v1, "querySeg":Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
    invoke-direct {p0, v1, p3}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->hasInteriorIntersection(Lcom/vividsolutions/jts/geom/LineSegment;Lcom/vividsolutions/jts/geom/LineSegment;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 201
    invoke-static {p1, p2, v1}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->isInLineSection(Lcom/vividsolutions/jts/simplify/TaggedLineString;[ILcom/vividsolutions/jts/simplify/TaggedLineSegment;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 203
    const/4 v3, 0x1

    .line 206
    .end local v1    # "querySeg":Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private hasBadIntersection(Lcom/vividsolutions/jts/simplify/TaggedLineString;[ILcom/vividsolutions/jts/geom/LineSegment;)Z
    .locals 2
    .param p1, "parentLine"    # Lcom/vividsolutions/jts/simplify/TaggedLineString;
    .param p2, "sectionIndex"    # [I
    .param p3, "candidateSeg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    const/4 v0, 0x1

    .line 176
    invoke-direct {p0, p3}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->hasBadOutputIntersection(Lcom/vividsolutions/jts/geom/LineSegment;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 178
    :cond_0
    :goto_0
    return v0

    .line 177
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->hasBadInputIntersection(Lcom/vividsolutions/jts/simplify/TaggedLineString;[ILcom/vividsolutions/jts/geom/LineSegment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 178
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasBadOutputIntersection(Lcom/vividsolutions/jts/geom/LineSegment;)Z
    .locals 4
    .param p1, "candidateSeg"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 183
    iget-object v3, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->outputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    invoke-virtual {v3, p1}, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;->query(Lcom/vividsolutions/jts/geom/LineSegment;)Ljava/util/List;

    move-result-object v2

    .line 184
    .local v2, "querySegs":Ljava/util/List;
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 185
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/vividsolutions/jts/geom/LineSegment;

    .line 186
    .local v1, "querySeg":Lcom/vividsolutions/jts/geom/LineSegment;
    invoke-direct {p0, v1, p1}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->hasInteriorIntersection(Lcom/vividsolutions/jts/geom/LineSegment;Lcom/vividsolutions/jts/geom/LineSegment;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 187
    const/4 v3, 0x1

    .line 190
    .end local v1    # "querySeg":Lcom/vividsolutions/jts/geom/LineSegment;
    :goto_0
    return v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private hasInteriorIntersection(Lcom/vividsolutions/jts/geom/LineSegment;Lcom/vividsolutions/jts/geom/LineSegment;)Z
    .locals 5
    .param p1, "seg0"    # Lcom/vividsolutions/jts/geom/LineSegment;
    .param p2, "seg1"    # Lcom/vividsolutions/jts/geom/LineSegment;

    .prologue
    .line 232
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    iget-object v1, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v2, p1, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v3, p2, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    iget-object v4, p2, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->computeIntersection(Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;Lcom/vividsolutions/jts/geom/Coordinate;)V

    .line 233
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->li:Lcom/vividsolutions/jts/algorithm/LineIntersector;

    invoke-virtual {v0}, Lcom/vividsolutions/jts/algorithm/LineIntersector;->isInteriorIntersection()Z

    move-result v0

    return v0
.end method

.method private static isInLineSection(Lcom/vividsolutions/jts/simplify/TaggedLineString;[ILcom/vividsolutions/jts/simplify/TaggedLineSegment;)Z
    .locals 5
    .param p0, "line"    # Lcom/vividsolutions/jts/simplify/TaggedLineString;
    .param p1, "sectionIndex"    # [I
    .param p2, "seg"    # Lcom/vividsolutions/jts/simplify/TaggedLineSegment;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 222
    invoke-virtual {p2}, Lcom/vividsolutions/jts/simplify/TaggedLineSegment;->getParent()Lcom/vividsolutions/jts/geom/Geometry;

    move-result-object v3

    invoke-virtual {p0}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->getParent()Lcom/vividsolutions/jts/geom/LineString;

    move-result-object v4

    if-eq v3, v4, :cond_1

    .line 227
    :cond_0
    :goto_0
    return v1

    .line 224
    :cond_1
    invoke-virtual {p2}, Lcom/vividsolutions/jts/simplify/TaggedLineSegment;->getIndex()I

    move-result v0

    .line 225
    .local v0, "segIndex":I
    aget v3, p1, v1

    if-lt v0, v3, :cond_0

    aget v3, p1, v2

    if-ge v0, v3, :cond_0

    move v1, v2

    .line 226
    goto :goto_0
.end method

.method private remove(Lcom/vividsolutions/jts/simplify/TaggedLineString;II)V
    .locals 3
    .param p1, "line"    # Lcom/vividsolutions/jts/simplify/TaggedLineString;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    .line 246
    move v0, p2

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 247
    invoke-virtual {p1, v0}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->getSegment(I)Lcom/vividsolutions/jts/simplify/TaggedLineSegment;

    move-result-object v1

    .line 248
    .local v1, "seg":Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
    iget-object v2, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->inputIndex:Lcom/vividsolutions/jts/simplify/LineSegmentIndex;

    invoke-virtual {v2, v1}, Lcom/vividsolutions/jts/simplify/LineSegmentIndex;->remove(Lcom/vividsolutions/jts/geom/LineSegment;)V

    .line 246
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    .end local v1    # "seg":Lcom/vividsolutions/jts/simplify/TaggedLineSegment;
    :cond_0
    return-void
.end method

.method private simplifySection(III)V
    .locals 12
    .param p1, "i"    # I
    .param p2, "j"    # I
    .param p3, "depth"    # I

    .prologue
    .line 91
    add-int/lit8 p3, p3, 0x1

    .line 92
    const/4 v7, 0x2

    new-array v5, v7, [I

    .line 93
    .local v5, "sectionIndex":[I
    add-int/lit8 v7, p1, 0x1

    if-ne v7, p2, :cond_0

    .line 94
    iget-object v7, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->line:Lcom/vividsolutions/jts/simplify/TaggedLineString;

    invoke-virtual {v7, p1}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->getSegment(I)Lcom/vividsolutions/jts/simplify/TaggedLineSegment;

    move-result-object v4

    .line 95
    .local v4, "newSeg":Lcom/vividsolutions/jts/geom/LineSegment;
    iget-object v7, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->line:Lcom/vividsolutions/jts/simplify/TaggedLineString;

    invoke-virtual {v7, v4}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->addToResult(Lcom/vividsolutions/jts/geom/LineSegment;)V

    .line 133
    .end local v4    # "newSeg":Lcom/vividsolutions/jts/geom/LineSegment;
    :goto_0
    return-void

    .line 100
    :cond_0
    const/4 v3, 0x1

    .line 108
    .local v3, "isValidToSimplify":Z
    iget-object v7, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->line:Lcom/vividsolutions/jts/simplify/TaggedLineString;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->getResultSize()I

    move-result v7

    iget-object v8, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->line:Lcom/vividsolutions/jts/simplify/TaggedLineString;

    invoke-virtual {v8}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->getMinimumSize()I

    move-result v8

    if-ge v7, v8, :cond_1

    .line 109
    add-int/lit8 v6, p3, 0x1

    .line 110
    .local v6, "worstCaseSize":I
    iget-object v7, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->line:Lcom/vividsolutions/jts/simplify/TaggedLineString;

    invoke-virtual {v7}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->getMinimumSize()I

    move-result v7

    if-ge v6, v7, :cond_1

    .line 111
    const/4 v3, 0x0

    .line 114
    .end local v6    # "worstCaseSize":I
    :cond_1
    const/4 v7, 0x1

    new-array v1, v7, [D

    .line 115
    .local v1, "distance":[D
    iget-object v7, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->linePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    invoke-direct {p0, v7, p1, p2, v1}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->findFurthestPoint([Lcom/vividsolutions/jts/geom/Coordinate;II[D)I

    move-result v2

    .line 117
    .local v2, "furthestPtIndex":I
    const/4 v7, 0x0

    aget-wide v8, v1, v7

    iget-wide v10, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->distanceTolerance:D

    cmpl-double v7, v8, v10

    if-lez v7, :cond_2

    const/4 v3, 0x0

    .line 119
    :cond_2
    new-instance v0, Lcom/vividsolutions/jts/geom/LineSegment;

    invoke-direct {v0}, Lcom/vividsolutions/jts/geom/LineSegment;-><init>()V

    .line 120
    .local v0, "candidateSeg":Lcom/vividsolutions/jts/geom/LineSegment;
    iget-object v7, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->linePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v7, v7, p1

    iput-object v7, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p0:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 121
    iget-object v7, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->linePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    aget-object v7, v7, p2

    iput-object v7, v0, Lcom/vividsolutions/jts/geom/LineSegment;->p1:Lcom/vividsolutions/jts/geom/Coordinate;

    .line 122
    const/4 v7, 0x0

    aput p1, v5, v7

    .line 123
    const/4 v7, 0x1

    aput p2, v5, v7

    .line 124
    iget-object v7, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->line:Lcom/vividsolutions/jts/simplify/TaggedLineString;

    invoke-direct {p0, v7, v5, v0}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->hasBadIntersection(Lcom/vividsolutions/jts/simplify/TaggedLineString;[ILcom/vividsolutions/jts/geom/LineSegment;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v3, 0x0

    .line 126
    :cond_3
    if-eqz v3, :cond_4

    .line 127
    invoke-direct {p0, p1, p2}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->flatten(II)Lcom/vividsolutions/jts/geom/LineSegment;

    move-result-object v4

    .line 128
    .restart local v4    # "newSeg":Lcom/vividsolutions/jts/geom/LineSegment;
    iget-object v7, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->line:Lcom/vividsolutions/jts/simplify/TaggedLineString;

    invoke-virtual {v7, v4}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->addToResult(Lcom/vividsolutions/jts/geom/LineSegment;)V

    goto :goto_0

    .line 131
    .end local v4    # "newSeg":Lcom/vividsolutions/jts/geom/LineSegment;
    :cond_4
    invoke-direct {p0, p1, v2, p3}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->simplifySection(III)V

    .line 132
    invoke-direct {p0, v2, p2, p3}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->simplifySection(III)V

    goto :goto_0
.end method


# virtual methods
.method public setDistanceTolerance(D)V
    .locals 1
    .param p1, "distanceTolerance"    # D

    .prologue
    .line 73
    iput-wide p1, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->distanceTolerance:D

    .line 74
    return-void
.end method

.method simplify(Lcom/vividsolutions/jts/simplify/TaggedLineString;)V
    .locals 2
    .param p1, "line"    # Lcom/vividsolutions/jts/simplify/TaggedLineString;

    .prologue
    const/4 v1, 0x0

    .line 84
    iput-object p1, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->line:Lcom/vividsolutions/jts/simplify/TaggedLineString;

    .line 85
    invoke-virtual {p1}, Lcom/vividsolutions/jts/simplify/TaggedLineString;->getParentCoordinates()[Lcom/vividsolutions/jts/geom/Coordinate;

    move-result-object v0

    iput-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->linePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    .line 86
    iget-object v0, p0, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->linePts:[Lcom/vividsolutions/jts/geom/Coordinate;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v1, v0, v1}, Lcom/vividsolutions/jts/simplify/TaggedLineStringSimplifier;->simplifySection(III)V

    .line 87
    return-void
.end method
